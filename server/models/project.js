const mongoose = require("mongoose");
const bcrypt = require("bcrypt-nodejs");
var Project = require("./project");
const config = require("../config");

const Schema = mongoose.Schema;
var NotEmptyString = {type: String, minLength: 1,required: true},
    EmtyStr = {type: String};

const projectSchema = new Schema({
    title: NotEmptyString,
    link: NotEmptyString,
    image: EmtyStr,
    owner: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    created: {
        type: Number,
        default:Date.now()
    },
    published: {
        type: Boolean,
        default:false
    },
    model:new Schema({
        linkAction:EmtyStr,
        link:EmtyStr,
        linkGlobal:EmtyStr,
        name:EmtyStr,
        created: {
            type: Number,
            default:Date.now()
        },
        useGlobals: new Schema({
            controls: [{
                type: Boolean,
                default: false
            }],
            tooltip: [{
                type: Boolean,
                default: false
            }],
            preloader: [{
                type: Boolean,
                default: false
            }]
        })
    })
});

projectSchema.statics._remove =function(args,next){
    if(!args.id){
        next({message: 'can`t drop the project'});
    }else{
        var _PROJECT =  this.model('Project');
        _PROJECT.findOne({_id: args.id}, function (err, project) {
            if (err) {
                return  next(err);
            } else if (!project) {
                return  next({message:'no project found'});
            } else {
                _PROJECT.remove({_id: project._id}, function (er) {
                    if (er) {

                    }  else {
                        args.req.session.lastEditProject = null;
                        if (project.image)config.saveImage(false, project.image);
                        if (project.model && project.model.link)config.help.deleteFolderRecursive(config.DIR.UPLOADS + config.DIR.PROJECTS + project.model.link, true);
                    }
                    next(er);
                })
            }
        });

    }

}
module.exports = mongoose.model("Project", projectSchema);
