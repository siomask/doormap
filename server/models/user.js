const mongoose = require("mongoose");
const bcrypt = require("bcrypt-nodejs");
const Project = require("./project");

const Schema = mongoose.Schema;
const Types = mongoose.Schema.Types;
var config = require('../config'),
    EmtyStr = {type: String, default: null};

const userSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    secondName: {
        type: String
    },
    settings: new Schema({//dynamically created on user update settings for views
        link: EmtyStr,
        linkAction: EmtyStr,

       /* opacityFill: {type:Number},
        colorActive:{type:String},
        colorNoActive:{type:String},
        thicknessOutline:{type:Number},
        colorOutline:{type:String},
        opacityOutLine: {type:Number},*/

        files: [EmtyStr],
        created: {
            type: Number,
            default: Date.now()
        }
    }),
    role: {
        type: Number,
        default: config.USER_ROLE.USER
    },
    parent: {
        type: String,
        default: null
    },
    created: {
        type: Number,
        default: Date.now()
    },
    active: {
        type: Boolean,
        default: true
    },
    avatar: {
        type: String,
        default: ''
    },
    users: [{
        type: Types.ObjectId,
        ref: 'User',
        default: []
    }],
    projects: [{
        type: Types.ObjectId,
        ref: 'Project',
        default: []
    }],
    token: {
        type: String
    }
});

userSchema.statics._remove = function (args, next) {
    if (!args.id) {
        next({message: 'can`t drop the user'});
    } else {
        var _USER = this.model('User');
        _USER.findOne({_id: args.id}, function (err, user) {
            if (err) {
                return next(err);
            } else if (!user) {
                return next({message: "No user found"});
            } else {

                (function removeProjects() {
                    if (user.projects && user.projects.length) {
                        Project._remove({req: args.req, id: user.projects.shift()}, function (er) {
                            if (er) {
                                return next(er || {message: "can`t remove the user project"});
                            } else {
                                removeProjects();
                            }
                        });
                    } else {
                        (function removeUsers() {
                            if (user.users && user.users.length) {
                                _USER._remove({req: args.req, id: user.users.shift()}, function (er) {
                                    if (er) {
                                        return next(er || {message: "can`t remove the user child"});
                                    } else {
                                        removeUsers();
                                    }
                                });
                            } else {
                                //To Do  need to drop the session if some user has it
                                _USER.remove({_id: user._id}, function (er) {
                                    if (er) {
                                        return next(er || {message: "No user found"});
                                    } else {
                                        var req = args.req;
                                        if (user.settings && user.settings.link) config.help.deleteFolderRecursive(config.DIR.SET_TEMPL + user.settings.link, true);
                                        if (user.avatar)config.saveImage(false, user.avatar);

                                        _USER.update({_id: req.user._id},
                                            {$pull: {users: {$in: [args.id]}}}
                                            , function (err) {
                                                if (!err && req.user.users.indexOf(args.id) > -1)req.user.users.splice(req.user.users.indexOf(args.id), 1);
                                                next(err);
                                            }
                                        );
                                    }
                                });
                            }
                        })();
                    }
                })()

            }

        });

    }

}
userSchema.methods.comparePassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};


userSchema.pre("save", function (next) {
    if (this.isModified("password") || this.isNew) {
        this.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(10), null);
    }

    next();
});

module.exports = mongoose.model("User", userSchema);
