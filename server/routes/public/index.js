const router = require("express").Router();
const config = require("../../config");
const User = require("../../models/user");
const Project = require("../../models/project");
const request = require('request');
const async = require("async");

function getSettings(opt, res) {
    var userId = opt.userId,
        __userId = opt._userId,
        projId = opt.projId,
        linkAction =opt.responce.project &&opt.responce.project.model?opt.responce.project.model.linkAction:false;


  function getUserSet(_userId) {
        if (_userId) {
            User.findOne({_id: _userId}, {settings: 1, parent: 1}, function (err, user) {
              if (user && user._id == __userId && opt.responce.project)opt.responce.project.published = true;
                if (err || !user) {
                    opt.responce.settings = {
                        status: false,
                        message: err || "Error, no parent found "
                    };
                    return res.json(opt.responce);
                } else if (user && [config.USER_ROLE.ADMIN, config.USER_ROLE.SUPER].indexOf(user.role) < 0 && user.settings && user.settings.link) {
                    opt.responce.settings = {
                        linkAction:linkAction,
                        status: true,
                        data: user.settings
                    };
                    if(linkAction)opt.responce.settings.data.linkAction = linkAction;
                    return res.json(opt.responce);
                } else {
                    return getUserSet(user.parent);
                }
            });
        } else {
            return res.json(opt.responce);
        }

    }

    if (userId) {
        return getUserSet(userId);
    } else {
        /* Project.findOne({_id: projId})
         .populate({path: 'owner', select: ['parent', 'role']})
         .exec(function (err, project) {
         if (err || !project) {
         opt.responce.settings = {
         status: false,
         message: err || "no Project found"
         };
         return res.json(opt.responce);
         } else {
         return getUserSet(project.owner.parent);
         }
         });*/
        Project.findOne({_id: projId}, {owner: 1}, function (err, project) {
            if (err || !project) {
                opt.responce.settings = {
                    status: false,
                    message: err || "no Project found"
                };
                return res.json(opt.responce);
            } else {
                return getUserSet(project.owner);
            }
        });
    }

}
router.all("/", function (req, res) {
    if (req.body.projectDir) {

        var projDir = config.DIR.UPLOADS + config.DIR.PROJECTS + req.body.projectDir;
        if (!fs.existsSync(projDir + config.DIR.PROJECT_TEMPLATE.HTML)) {

        }
    } else {
        res.json({
            status: false,
            message: "missing project dir or template type"
        });
    }

});

router.post("/project/isactive", function (req, res) {
    if (!req.body.id) {
        return res.json({
            status: false,
            message: "forgot something"
        });
    } else {
        Project.findOne({_id: req.body.id}, {_id: 1, "model.link": 1,"model.linkAction": 1, published: 1, owner: 1, "model.useGlobals" : 1}).populate({
            path: 'owner',
            select: ['parent', 'active']
        }).exec(function (err, project) {
            if (err || !project) {
                return res.json({
                    status: false,
                    message: err || "Undefined error, no project found."
                });
            } else {
                var userId, owner = project.owner,
                    _userId = req.body.userId;
                project.owner = null;
                if (owner) {
                    userId = owner._id;

                    if (userId == _userId) {
                        project.published = true;
                    }
                    if (!owner.active) {
                        project.published = false;
                        return res.json({
                            status: true,
                            message: 'user is inactive',
                            project: project
                        });
                    }
                }
                if (!project.model) {
                    return res.json({
                        status: true,
                        message: 'no scene found',
                        project: project
                    });
                }

                if (userId) {
                    return getSettings({
                        userId: userId,
                        _userId: _userId,
                        responce: {
                            test:3234,
                            status: true,
                            project: project
                        }
                    }, res);
                } else {
                    return res.json({
                        status: true,
                        project: project
                    });
                }


            }
        });
    }
});
router.post("/project/settings", function (req, res) {
    if (!req.body.projId && !req.body.userId) {
        return res.json({
            status: false,
            message: "forgot something"
        });
    } else {
        return getSettings({projId: req.body.projId, userId: req.body.userId, responce: {}}, res)
    }
});

router.post("/model/remote", function (req, res) {
    if (req.body.dataUrl) {
        request.get(req.body.dataUrl, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                res.json({
                    status: true,
                    message: "forgot something",
                    data: body
                });
            } else {
                res.json({
                    status: false,
                    message: error
                });
            }
        });

    } else if (req.body.id) {
        Project.findOne({_id: req.body.id}, {link: 1}, function (err, project) {
            if (err || !project) {
                opt.responce.settings = {
                    status: false,
                    message: err || "no Project found"
                };
                return res.json(opt.responce);
            } else {
                    request.get(project.link, function (error, response, body) {
                    //console.log(_q);
                    if (!error && response.statusCode == 200) {
                        res.json({
                            status: true,
                            message: "forgot something",
                            data: body
                        });
                    } else {
                        res.json({
                            status: false,
                            message: error
                        });
                    }
                });
            }
        });
    } else {
        res.json({
            status: false,
            message: "No data"
        });
    }
});

router.get("/model/remote", function (req, res) {

    var _q = req.query;


    if(!_q.dataUrl){
        _q.dataUrl= "https://staging.blockwatne.no/odata/oxivisuals/S779ZOvP/ProjectPages?\$format=json&\$select=Name,Id,ResidenceList/Name"+
        "ResidenceList/Id,ResidenceList/Price,ResidenceList/LivingSpace,ResidenceList/PrimarySpace,ResidenceList/PlotArea,ResidenceList/Floor,ResidenceList/Overheads"+
        "ResidenceList/PublicDebtShare,ResidenceList/Planes,ResidenceList/EnergyMarking,ResidenceList/EnergyClass,ResidenceList/ResidenceNumber,ResidenceList/ConstructionStartDate"+
        "ResidenceList/ConstructionCompleteDate,ResidenceList/ExternalId,ResidenceList/SaleStartDate,ResidenceList/Rooms,ResidenceList/Bedrooms,ResidenceList/ResidenceType,ResidenceList/Sold"+
        "ResidenceList/Reserved&\$expand=ResidenceList";

        var _id = _q.id||_q.ID;
        if (_id) {
            _id = _id.split(",");
            _q.dataUrl += '&$filter=Id%20eq%20';
            _q.dataUrl += _id.join('%20or%20Id%20eq%20');

        }
    }
    //console.log(_q);
    request.get(_q.dataUrl, function (error, response, body) {
        //console.log(_q);
        if (!error && response.statusCode == 200) {
            res.json({
                status: true,
                message: "forgot something",
                data: body
            });
        } else {
            res.json({
                status: false,
                message: error
            });
        }
    });

});



module.exports = router;
