const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const settingsSchema = new Schema({
    settings: {},

});

module.exports = mongoose.model("settings", settingsSchema);
