const path = require("path");
const router = require("express").Router();

const authenticate = require("../middleware/authenticate");

router.use("/api", authenticate, require("./api"));

router.use("/auth", require("./auth"));
router.use("/public", require("./public"));
router.use("/test", require("./globe_project_api/routes/settings"));

module.exports = router;
