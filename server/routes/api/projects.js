const router = require("express").Router();
const async = require("async");
const fs = require("fs");
const config = CONFIG = require("../../config");
const path = require("path");
const webp = require('webp-converter');
var CWebp = require('cwebp').CWebp;


const Jimp = require("jimp");
const Project = require("../../models/project");
const User = require("../../models/user");

function checkPermissionOnProject(req, res, next) {
  Project.find({owner: [req.user._id].concat(req.user.users)}, {"_id": 1}).exec(function (err, projects) {
    if (req.body._id && projects.filter((proj) => {
      return proj._id == req.body._id
    }).length >= 0) {
      if (req.session.lastEditProject && req.session.lastEditProject.model && req.session.lastEditProject.model.link && req.session.lastEditProject._id == req.body._id) {
        next(req, res);
      } else {
        Project.findOne({_id: req.body._id}, {_id: 1, model: 1, image: 1, link: 1}, function (err, project) {
          if (err || !project) {
            return res.json({
              status: false,
              message: "permission denied!!!"
            });
          } else {
            req.session.lastEditProject = project;
            //if (project.model && project.model.link) {
            //    project.data = fs.readSync(path.normalize(config.DIR.UPLOADS + config.DIR.PROJECTS + project.model.link + "/" + config.DIR.SITE_STRUCTURE), 'utf8');
            //    project.data = project.data ? JSON.parse(project.data) : [];
            //}
            next(req, res);
          }
        });
      }
    } else {
      return res.json({
        status: false,
        message: "permission denied"
      });
    }
  });

}

function saveProjectFiles(options, req, res, next) {

  if (req.files) {
    var keyses = config.FILE_UPLOAD_ATTR,
      modelDir = options.modelDir,
      area = options.area;

    if (!fs.existsSync(path.normalize(modelDir))) {
      fs.mkdirSync(path.normalize(modelDir), config.FILE_UPLOAD_ACCEC);
    }
    checkFileKeys({
      keyses: keyses,
      modelDir: modelDir,
      options: options,
      req: req,
      area: area,
      next: function () {
        next(req, res);
      }
    })

  } else {
    next(req, res);
  }
}

function getGlobalLink(req, res, next) {
  if (!req.body._id) {
    next("");
    return;
  }
  async.waterfall([
    function (done) {
      Project.findOne({_id: req.body._id}, {
        'owner': 1
      }, function (err, project) {
        done(err, project.owner)
      });
    },
    function (ownerID, done) {
      User.findOne({_id: ownerID}, {
        '_id': 1,
        'role': 1,
        'parent': 1
      }, function (err, user) {
        if (user.role <= config.USER_ROLE.ADMIN) {
          done(err, user._id)
        } else {
          done(err, user.parent)
        }

      });
    },
    function (globalUserID, done) {
      User.findOne({_id: globalUserID}, {
        'settings': 1,
      }, function (err, user) {
        done(err, user.settings.link);
      });
    }
  ], function (err, link) {

    if (err) {
      next(req, res, "");
    } else {
      next(req, res, link);
    }

  });
}

function checkFileKeys(opt) {
  var keyses = opt.keyses,
    next = opt.next,
    key = 0,
    req = opt.req,
    modelDir = opt.modelDir,
    options = opt.options,
    area = opt.area;

  (function checkKey(_keyses) {
    var urlSaveFile,
      severalTypes,
      modelSaved = null,
      keys = keyses[key++],

      filesName;

    if (keys) {

      if (!req.files[keys] || !req.files[keys].length) return checkKey(_keyses);
      switch (keys) {
        case config.FILE_UPLOAD_ATTR[7]:
        case config.FILE_UPLOAD_ATTR[0]: {
          var isSvg = keys == config.FILE_UPLOAD_ATTR[7],
            prevState = area && area.camera && area.camera.isSVG;
          modelSaved = isSvg ? 'svgDestination' : 'destination';
          if (isSvg && area && area.camera && !area.camera.isSVG) area.camera.isSVG = true;
          urlSaveFile = modelDir;
          var pathF = path.normalize(urlSaveFile);
          if (fs.existsSync(pathF)) {
            if ((area && (isSvg && !prevState || prevState && !isSvg))) {
              for (var u = 0, files = fs.readdirSync(urlSaveFile); u < files.length; u++) {
                var file = files[u],
                  curPath = pathF + "/" + file;
                if (fs.lstatSync(curPath).isDirectory()) {
                } else {
                  if (curPath.indexOf((isSvg ? config.FILE_UPLOAD_EXT[2] : config.FILE_UPLOAD_EXT[0]))) {
                    fs.unlinkSync(curPath);
                  }
                }
              }
            }
          } else {
            fs.mkdirSync(path.normalize(urlSaveFile), config.FILE_UPLOAD_ACCEC);
          }
          break;
        }
        case config.FILE_UPLOAD_ATTR[1]: {
          urlSaveFile = modelDir + config.DIR.IMAGES;
          if (fs.existsSync(urlSaveFile)) {
            if (req.files[keys]) config.help.deleteFolderRecursive(urlSaveFile);
          } else {
            fs.mkdirSync(urlSaveFile, config.FILE_UPLOAD_ACCEC);
          }

          severalTypes = config.DIR.IMG_TYPES;
          for (var i = 0; i < severalTypes.length; i++) {
            if (!fs.existsSync(urlSaveFile + severalTypes[i])) fs.mkdirSync(urlSaveFile + severalTypes[i], config.FILE_UPLOAD_ACCEC);
          }
          break;
        }
        case config.FILE_UPLOAD_ATTR[2]: {

          urlSaveFile = modelDir + config.DIR.ALIGN_IMAGES;
          if (fs.existsSync(urlSaveFile)) {
            if (req.files[keys]) config.help.deleteFolderRecursive(urlSaveFile);
          } else {
            fs.mkdirSync(urlSaveFile, config.FILE_UPLOAD_ACCEC);
          }
          break;
        }
        case config.FILE_UPLOAD_ATTR[3]: {
          fs.writeFileSync(path.normalize(modelDir + config.DIR.SITE_STRUCTURE), fs.readFileSync(req.files[keys][0].path));
          break;
        }

        case config.FILE_UPLOAD_ATTR[4]:
        case config.FILE_UPLOAD_ATTR[5]:
        case config.FILE_UPLOAD_ATTR[6]: {
          if (!options.hasNewUrl) {
            urlSaveFile = modelDir + config.DIR.PROJECT_TEMPLATE.NAME;
            if (!fs.existsSync(urlSaveFile)) {
              fs.mkdirSync(urlSaveFile, config.FILE_UPLOAD_ACCEC);
            }
          } else {
            urlSaveFile = modelDir;
          }

          urlSaveFile += keys.split("[]")[0] + config.DIR.DELIMETER;
          if (!fs.existsSync(urlSaveFile)) {
            fs.mkdirSync(urlSaveFile, config.FILE_UPLOAD_ACCEC);
          }
          filesName = [config.DIR.PROJECT_TEMPLATE.CSS, config.DIR.PROJECT_TEMPLATE.HTML, config.DIR.PROJECT_TEMPLATE.JS, config.DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE];
          if (options.callback) options.callback(keys);
          break;
        }
      }
      if (!urlSaveFile && !fs.existsSync(urlSaveFile)) return checkKey(_keyses);


      saveFiles({
        req: req,
        keys: keys,
        modelSaved: modelSaved,
        area: area,
        severalTypes: severalTypes,
        urlSaveFile: urlSaveFile,
        filesName: filesName,
        next: function () {
          checkKey(_keyses);
        }
      })
    } else {
      if (key < _keyses.length) {
        checkKey(_keyses);
      } else {
        next();
      }
    }
  })(keyses)
}

function saveFiles(opt) {
  var req = opt.req,
    keys = opt.keys,
    _files = req.files[keys],
    itemFile = 0,
    next = opt.next,
    modelSaved = opt.modelSaved,
    area = opt.area,
    severalTypes = opt.severalTypes,
    filesName = opt.filesName,
    urlSaveFile = opt.urlSaveFile
  ;

  (function saveFile(_fList) {
    var _file = _fList[itemFile++];

    if (_file) {

      var _fileName = _file.originalname ? _file.originalname : filesName ? filesName.shift() : '.bak';

      if (area && modelSaved) area[modelSaved] = _fileName;
      fs.writeFileSync(urlSaveFile + _fileName, fs.readFileSync(_file.path));
      if (severalTypes) {
        if (area && area.images) {
          area.images.push(_fileName);
        }
        convertimages({
          _fileName: _fileName,
          _file: _file,
          severalTypes: severalTypes,
          urlSaveFile: urlSaveFile,
          next: function () {
            saveFile(_fList);
          }
        });

      } else {
        saveFile(_fList);
      }
    } else {
      if (itemFile < _fList.length) {
        saveFile(_fList);
      } else {
        next();
      }

    }

  })(_files)
}

function convertimages(opt) {
  var next = opt.next,
    _fileName = opt._fileName,
    separator = ".",
    _file = opt._file,
    severalTypes = opt.severalTypes,
    urlSaveFile = opt.urlSaveFile,
    fileName = _fileName.split(separator);
  fileName.pop();

  console.log("Jimp Start-----", _file);
  if (_file.size > 1024 * 2000) {//MORE THAn 2 mb
    console.log("No image changes ----");
    next();
  } else {
    Jimp.read(_file.path).then((function (url) {
        return function (lenna) {
          lenna.resize(720, Jimp.AUTO).quality(85).write(url); // save
          console.log("Jimp Resize-----");


          webp.cwebp(_file.path, urlSaveFile + severalTypes[0] + fileName.join(separator) + '.webp', "-q 50", function (status) {
            console.log("Webp ----", status);
            next();
          });

          /* var encoder = new CWebp(_file.path);
           encoder.quality(50);
           encoder.write(urlSaveFile + severalTypes[0] + fileName.join(separator) + '.webp', function (err) {
           if (err) {
           console.log("Error webp-----", err);
           } else {
           console.log('encoded successfully');
           }
           next();
           });*/
        }
      })(urlSaveFile + severalTypes[1] + _fileName)
    ).catch(function (err) {
      console.error(err);
      next();
    });

  }

}

function Structure(req, res) {
  var hasFile = req.session.lastEditProject && req.session.lastEditProject.model && req.session.lastEditProject.model.link;
  var modelDir = config.DIR.UPLOADS + config.DIR.PROJECTS + (hasFile ? req.session.lastEditProject.model.link : '') + config.DIR.DELIMETER;
  this.getData = function () {
    if (!hasFile) return {};
    var structure = fs.readFileSync(modelDir + config.DIR.SITE_STRUCTURE, 'utf-8');
    if (!structure) return res.json({
      status: false,
      message: "can`t save template"
    });
    this.structure = JSON.parse(structure)[0];
    return this.structure;
  }
  this.saveData = function () {
    if (!hasFile) return null;
    fs.writeFileSync(path.normalize(modelDir + config.DIR.SITE_STRUCTURE), JSON.stringify([this.structure]), 'utf8');
  }
}

function createProjectDes(req, res) {
  var modelName = req.body.name,
    id_project = req.body._id;

  if (!id_project || !(req.files[config.FILE_UPLOAD_ATTR[0]] || req.files[config.FILE_UPLOAD_ATTR[7]]) || !req.files[config.FILE_UPLOAD_ATTR[1]]) {
    return res.json({
      status: false,
      message: "frame(s) and obj/svg are required"
    });
  } else {
    checkPermissionOnProject(req, res, function () {
      var area = {
          _id: id_project,
          name: modelName,
          projFilesDirname: /*modelName + "_" +*/ config.randomString(),
          created: Date.now(),
          camera: {_category: 3},
          _category: 2,
          images: []
        },
        modelDir = config.DIR.UPLOADS + config.DIR.PROJECTS + area.projFilesDirname + config.DIR.DELIMETER;

      area.preview = req.session.lastEditProject.image;
      area.dataSource = req.session.lastEditProject.link;
      if (req.session.lastEditProject.data) {
        return ress.json({
          status: false,
          message: "Project destination already exist"
        });
      }
      saveProjectFiles({modelDir: modelDir, area: area}, req, res, function (reqq, ress) {
        fs.writeFileSync(modelDir + config.DIR.SITE_STRUCTURE, JSON.stringify([area]));
        Project.update({_id: id_project}, {
          $set: {
            "model.link": area.projFilesDirname,
            "model.name": modelName
          }
        }, function (err) {
          return ress.json({
            status: !err,
            message: err ? err : "area was created",
            model: {
              link: area.projFilesDirname,
              name: modelName,
              data: area
            }
          });
        });
      });

    });


  }
}

//update project
router.put("/project", function (request, responce) {
  checkPermissionOnProject(request, responce, function (req, res) {
    Project.findOne({_id: req.body._id}, function (err, project) {
      if (err) {
        return res.json({
          status: false,
          message: "Undefined error, no project found."
        });
      }

      if (!project) {
        return res.json({
          status: false,
          message: "No project found."
        });
      }


      var needToYpdateImg = req.body.image && req.body.image !== project.image,
        updateProj = function (fields) {
          var _sets = {},
            exept = ['_id'];

          for (var _field  in req.body) {
            if (req.body.hasOwnProperty(_field) && exept.indexOf(_field) < 0) {
              _sets[_field] = req.body[_field];
            }
          }
          if (fields) for (var _field  in fields) {
            if (fields.hasOwnProperty(_field) && exept.indexOf(_field) < 0) {
              _sets[_field] = fields[_field];
            }
          }

          Project.update({_id: req.body._id}, {$set: _sets}, function (err) {
            getGlobalLink(req, res, (req, ress, linkGlobal) => {


              if (!err && _sets.link) {

                var _str = new Structure(req, res),
                  structure = _str.getData();
                structure.dataSource = "";
                structure.linkGlobal = linkGlobal;
                _str.saveData();

              }
              if (req.session.lastEditProject) delete req.session.lastEditProject;
              return res.json({
                status: !err,
                message: err ? (err.message || err) : "Project successfully was changed."
              });
            })
          });
        };


      if (needToYpdateImg) {
        CONFIG.saveImage(req.body.image, project.image, function (err, img) {
          if (err) {
            return res.json({
              status: false,
              res: project,
              message: err
            })
          }
          updateProj({image: img});
        })
      } else {
        updateProj();
      }


    });
  });
});
//create project
  router.post("/project", function (req, res) {
    if (!req.body.title || !req.body.link) {
      return res.json({
        status: false,
        message: "Empty fields."
      });
    }


    async.waterfall([
      function (done) {

        var newProject = new Project({
          title: req.body.title,
          link: req.body.link,
          owner: req.user._id,
          image: req.body.image
        });

        //newProject.owner = (req.user.role === 'super') ? null : req.user._id;

        if (req.body.image) {
          CONFIG.saveImage(req.body.image, null, function (err, img) {
            newProject.image = img;
            done(err, newProject);
          })
        } else {
          done(null, newProject);
        }
      }
    ], function (err, project) {

      if (err) {
        throw err;
      }

      project.save(function (err, tempProject) {
        if (err) {
          return res.json({
            status: false,
            message: err && err.message ? err.message : "model was not saved"
          });
        } else {
          User.update({_id: req.user._id}, {$push: {projects: tempProject._id}}, function (err) {
            if (!err) req.user.projects.push(tempProject._id);
            return res.json({
              status: !err,
              message: err ? err : "Project was successfully created.",
              res: tempProject
            });
          });

        }
      });

    });

  });
router.post("/project/model/create", function (req, res) {
  createProjectDes(req, res);
})
;
router.post("/project/model/update", function (request, responce) {
  var body = request.body;
  if (!body.dir || body.dir === 'undefined') {
    body.dir = '';
    return createProjectDes(request, responce);
  } else {
    checkPermissionOnProject(request, responce, function (req, res) {

      if (!body.dir || !req.session.lastEditProject || !req.session.lastEditProject.model || !req.session.lastEditProject.model.link) {
        return res.json({
          status: false,
          message: "something got incorect!!!"
        });
      } else {

        var modelDir = config.DIR.UPLOADS + config.DIR.PROJECTS + req.session.lastEditProject.model.link + body.dir.replace(req.session.lastEditProject.model.link, '') + config.DIR.DELIMETER;
        saveProjectFiles({modelDir: modelDir}, req, res, function (reqq, ress) {
          return ress.json({
            status: true,
            message: "project area was updated"
          });
        });

      }
    });
  }
});
router.post("/project/template/update", function (request, responce) {
  var body = request.body;
  if (!body.dir && !body._id) {
    if (request.user.role <= config.USER_ROLE.ADMIN) {
      var modelDir = config.DIR.SET_TEMPL,
        result = {status: true, message: "Settings views was saved"};

      if (request.user.settings && request.user.settings.link) {
        modelDir += request.user.settings.link;
      } else {
        var link = config.randomString() + config.DIR.DELIMETER;
        modelDir += link;
        while (fs.existsSync(path.normalize(modelDir))) {
          link = config.randomString() + config.DIR.DELIMETER;
          modelDir = config.DIR.SET_TEMPL + link;
        }
        fs.mkdirSync(path.normalize(modelDir), config.FILE_UPLOAD_ACCEC);
        result.templatesUrl = link;
      }

      saveProjectFiles({
        hasNewUrl: true,
        modelDir: modelDir
      }, request, responce, function (reqq, ress) {
        var settings = request.user.settings,
          _passportUser = request.session.passport.user;

        function updateUser() {

          settings.linkAction = _passportUser.settings.linkAction = body.linkAction;
          User.updateOne({_id: reqq.user._id}, {settings: settings}, function (err, res) {
            if (err) {
              settings.link = _passportUser.settings.link = result.templatesUrl = null;
              result.status = false;
              result.message = "Error: " + e;
            } else {
            }
            return ress.json(result);
          })
        }

        if (result.templatesUrl) {
          if (!settings) settings = request.user.settings = _passportUser.settings = {};
          settings.link = _passportUser.settings.link = link;

          updateUser();
        } else {
          updateUser();
        }


      });
    } else {
      return responce.json({
        status: false,
        message: "permission denied"
      });
    }
  } else {
    checkPermissionOnProject(request, responce, function (req, res) {

      if (!body.dir || !req.session.lastEditProject || !req.session.lastEditProject.model || !req.session.lastEditProject.model.link) {
        return res.json({
          status: false,
          message: "something got incorect!!!"
        });
      } else {


        var modelDir = config.DIR.UPLOADS + config.DIR.PROJECTS + req.session.lastEditProject.model.link + config.DIR.DELIMETER;
        var
          _str = new Structure(req, res),
          structure = _str.getData();


        saveProjectFiles({

          modelDir: modelDir, callback: function (key) {
            if (!structure.templates) structure.templates = [];
            if (structure.templates.length > config.DIR.PROJECT_TEMPLATE.TYPES) return;
            var item = 0;
            switch (key) {
              case config.FILE_UPLOAD_ATTR[4]: {
                item = 2;
                break;
              }
              case config.FILE_UPLOAD_ATTR[5]: {
                item = 1;
                break;
              }
              case config.FILE_UPLOAD_ATTR[6]: {
                item = 0;
                break;
              }
            }
            if (structure.templates.indexOf(item) > -1) return;
            structure.templates.push(item)
          }
        }, req, res, function (reqq, ress) {
          if (body.linkAction || body["useGlobals.controls"] || body["useGlobals.tooltip"] || body["useGlobals.preloader"]) {
            getGlobalLink(req, ress, (req, resp, link) => {
              req.session.lastEditProject.model.linkAction = body.linkAction;
              req.session.lastEditProject.model.useGlobals = body.useGlobals;

              structure.linkGlobal = link;

              structure.useGlobals = {
                "controls": [body["useGlobals.controls"][0], body["useGlobals.controls"][1]],
                "tooltip": [body["useGlobals.tooltip"][0], body["useGlobals.tooltip"][1], body["useGlobals.tooltip"][2], body["useGlobals.tooltip"][3]],
                "preloader": [body["useGlobals.preloader"][0], body["useGlobals.preloader"][1]]
              };
              _str.saveData();
              Project.update({_id: body._id},
                {
                  $set: {
                    "model.linkAction": body.linkAction,
                    "model.linkGlobal": link,
                    "model.useGlobals.controls": body["useGlobals.controls"],
                    "model.useGlobals.tooltip": body["useGlobals.tooltip"],
                    "model.useGlobals.preloader": body["useGlobals.preloader"]
                  }
                }, function (err, res) {
                  return ress.json({
                    error: err,
                    status: true,
                    message: "project templates was updated"
                  });

                });
            });
          } else {
            return ress.json({
              status: true,
              message: "project templates was updated"
            });
          }

        });

      }
    });
  }

});
router.post("/project/template/globals/update", function (request, responce) {
  var body = request.body;
  if (!body._id) {

    return responce.json({
      status: false,
      message: "Unable to set global set"
    });

  } else {
    checkPermissionOnProject(request, responce, function (req, ress) {

      if (!req.session.lastEditProject || !req.session.lastEditProject.model || !req.session.lastEditProject.model.link) {
        return ress.json({
          status: false,
          message: "something got incorect!!!"
        });
      } else {
        if (body["useGlobals.controls"] || body["useGlobals.tooltip"] || body["useGlobals.preloader"]) {
          getGlobalLink(req, ress, (req, resp, link) => {


            req.session.lastEditProject.model.useGlobals = body.useGlobals;
            var _str = new Structure(req, ress),
              structure = _str.getData();
            structure.linkGlobal = link;
            structure.useGlobals = {
              "controls": [body["useGlobals.controls"][0], body["useGlobals.controls"][1]],
              "tooltip": [body["useGlobals.tooltip"][0], body["useGlobals.tooltip"][1], body["useGlobals.tooltip"][2], body["useGlobals.tooltip"][3]],
              "preloader": [body["useGlobals.preloader"][0], body["useGlobals.preloader"][1]]
            };
            _str.saveData();
            Project.update({_id: body._id},
              {
                $set: {
                  "model.linkGlobal": link,
                  "model.useGlobals.controls": body["useGlobals.controls"],
                  "model.useGlobals.tooltip": body["useGlobals.tooltip"],
                  "model.useGlobals.preloader": body["useGlobals.preloader"]
                }
              }, function (err, res) {
                return ress.json({
                  error: err,
                  status: true,
                  message: "project templates was updated"
                });

              });
          });
        } else {
          return ress.json({
            status: true,
            message: "project templates was updated"
          });
        }

      }
    });
  }

});


//devare user
router.delete("/project", function (request, responce) {
  checkPermissionOnProject(request, responce, function (req, res) {
    Project._remove({req: req, id: req.body._id}, function (er) {
      if (er) {
        res.json({
          status: false,
          message: er.message
        });
      } else {
        User.update({_id: req.user._id},
          {$pull: {projects: {$in: [req.body._id]}}}
          , function (err) {
            if (!err && req.user.projects.indexOf(req.body._id) > -1) req.user.projects.splice(req.user.projects.indexOf(req.body._id), 1);
            res.json({
              status: !err,
              message: err ? err.message : "Project was deleted"
            });
          }
        );
      }
    });
  });
});

module.exports = router;
