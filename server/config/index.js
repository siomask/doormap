// process.env.NODE_ENV = 'production';selectedChild
const fs = require("fs");

var CONFIG = {
    env: process.env.NODE_ENV,
    DIR: {
        PUBLIC: '/resources',
        UPLOADS: 'resources/uploads/',
        PROJECTS: 'projects/',
        IMAGES: 'images/',
        IMG_TYPES: ['webp/', 'low/'],
        ALIGN_IMAGES: 'align_images/',
        SITE_STRUCTURE: '/site_structure.json',
        DELIMETER: '/',
        PROJECT_TEMPLATE: {
            NAME: 'templates/',
            CSS: 'style.css',
            JS: 'index.js',
            JS_DATA_STRUCTURE: 'index.ds',
            HTML: 'index.html',
            TYPES: ['controls/', 'tooltip/','preloader/'],
            _TYPE:{
                PRELOADER:2,
                TOOLTIP:1,
                CONTROLS:0
            }

        }
    },
    FILE_UPLOAD_EXT: ['.obj', 'image/','.svg'],
    FILE_UPLOAD_ATTR: ['model[]', 'frames[]', 'alignFrames[]', 'structure','preloader[]','tooltip[]','controls[]','svgs[]'],
    FILE_UPLOAD_ACCEC: parseInt("0777", 8),
    port: process.env.PORT || 3006,
    mongoose: {
        uri: "mongodb://localhost/oxivisual"
    },
    security: {
        secret: "t45g3w45r34tw5ye454uhdgdf",
        expiresIn: "24h"
    },
    superadmin: {
        email: "superuser@gmail.com",
        password: "superpass"
    },
    help: {
        deleteFolderRecursive: function (path, flag) {
            var _self = this;
            if (fs.existsSync(path)) {
                for (var u = 0, files = fs.readdirSync(path); u < files.length; u++) {
                    var file = files[u],
                        curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        _self.deleteFolderRecursive(curPath, true);
                    } else {
                        fs.unlinkSync(curPath);
                    }
                }
                if (flag)fs.rmdirSync(path);
            }
        }
    },
    USER_ROLE: {
        SUPER: 1,
        ADMIN: 2,
        USER: 3
    }
};
CONFIG.DIR.SET_TEMPL=CONFIG.DIR.UPLOADS+"settings/"+CONFIG.DIR.PROJECT_TEMPLATE.NAME;
CONFIG.randomString =function (l) {

    var length_ = l || 25,
        chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
    if (typeof length_ !== "number") {
        length_ = Math.floor(Math.random() * chars.length_);
    }
    var str = '';
    for (var i = 0; i < length_; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str + Date.now().toString(32);
}
CONFIG.saveImage = function (image, avatar, done) {


    if (!image) {
        if (avatar) {
            var filePath = './resources' + avatar;
            fs.unlinkSync(filePath);
        }
        if (done)done(null, '');
        return;
    }

    if (avatar) {
        var filePath = './resources' + avatar;
        fs.unlinkSync(filePath);
    }

    var imageTypeRegularExpression = /\/(.*?)$/;

    var crypto = require('crypto');
    var seed = crypto.randomBytes(20);
    var uniqueSHA1String = crypto.createHash('sha1').update(seed).digest('hex');

    var imageBuffer = CONFIG.decodeBase64Image(image);
    var userUploadedFeedMessagesLocation = 'resources/uploads/img/projects/';

    var uniqueRandomImageName = 'image-' + uniqueSHA1String; //uniqueSHA1String;
    var imageTypeDetected = CONFIG.decodeBase64Image(image).type.match(imageTypeRegularExpression);
    var userUploadedImagePath = userUploadedFeedMessagesLocation + uniqueRandomImageName + '.' + imageTypeDetected[1];
    var clientPath = '/uploads/img/projects/' + uniqueRandomImageName + '.' + imageTypeDetected[1];

    fs.writeFile(userUploadedImagePath, imageBuffer.data, 'base64', function (err) {
        console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
        if (done)done(err, clientPath);
    });
};
CONFIG.decodeBase64Image = function(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3)
        return new Error('Invalid input string');

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}
module.exports = CONFIG;
