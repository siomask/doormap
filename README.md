# Oxivisual
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.2.

## Installing && Development server

Make sure on your instance you have node 4+, mongodb;

1) run `git clone https://bitbucket.org/unilimes/oxivisual_admin`
2) run `npm install`
3) run `npm run watch` - to build frontend in folder `dist` 
   Need to be run in folder src/preview 

4) run `npm run server` - Navigate to `http://localhost:3006/` to view app


## Prodution
1) download pem file http://18.184.90.152/build/oxivisual.pem
2) ssh -i "oxivisual.pem" ec2-user@ec2-18-184-90-152.eu-central-1.compute.amazonaws.com

3) cd /home/ec2-user/oxivisuals
/dist  -frontend
 /server - backend

 sudo pm2 restart 0 - to restart servere

 also for preview app see in oxivisual_admin/src/preview, the builded file should store on
 oxivisual_admin/resources/build

