<?php
/**
 * Created by PhpStorm.
 * User: edinorog
 * Date: 14.05.18
 * Time: 19:34
 */
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");


$spreadsheet_url = "https://staging.blockwatne.no/odata/oxivisuals/S779ZOvP/ProjectPages?\$format=json&\$select=Name,Id,ResidenceList/Name,
ResidenceList/Id,ResidenceList/Price,ResidenceList/LivingSpace,ResidenceList/PrimarySpace,ResidenceList/PlotArea,ResidenceList/Floor,ResidenceList/Overheads,
ResidenceList/PublicDebtShare,ResidenceList/Planes,ResidenceList/EnergyMarking,ResidenceList/EnergyClass,ResidenceList/ResidenceNumber,ResidenceList/ConstructionStartDate,
ResidenceList/ConstructionCompleteDate,ResidenceList/ExternalId,ResidenceList/SaleStartDate,ResidenceList/Rooms,ResidenceList/Bedrooms,ResidenceList/ResidenceType,ResidenceList/Sold,
ResidenceList/Reserved&\$expand=ResidenceList";


$reqsId = isset($_REQUEST['Id']) ? $_REQUEST['Id'] : (isset($_REQUEST['id']) ? $_REQUEST['id'] : false);
if ($reqsId) {
    $reqsId = explode(",", $reqsId);
    $spreadsheet_url .= '&$filter=Id%20eq%20';
    $spreadsheet_url .= join('%20or%20Id%20eq%20', $reqsId);

}

//if(isset($_REQUEST['test']))$spreadsheet_url ='http://webgl.unilimes.com/project/oxivisuals/test/testDataSource.json';

require_once 't/Requests/library/Requests.php';
Requests::register_autoloader();
die( Requests::get($spreadsheet_url)->body);
?>