import {Input,ViewChild,Component,OnInit,ViewContainerRef} from '@angular/core';
//import * as ENTITY from '../../../entities/entities';


@Component({
    selector: 'app-block-screen',
    templateUrl: './index.html',
    styleUrls: ['./index.sass']
})
export class BlockScreen implements OnInit {

    private enabled:boolean = false;
    statusShow:boolean = false;
    statusUpload:string='';
    blockStatus:string='';
    title:string='';
    remain:string='';

    constructor() {

    }

    ngOnInit() {

    }

    toggle(enable:boolean = false,title:string='') {
        this.enabled = enable;
        this.title = title;
        if(!enable){
            this.blockStatus = '';
            this.remain = '';
            this.statusShow = false;
        }
    }
}