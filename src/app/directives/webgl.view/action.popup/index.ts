import {ViewChild,Component,OnInit} from '@angular/core';

declare var alertify:any;
declare var CodeMirror:any;

@Component({
    selector: 'app-action-pop-up',
    templateUrl: './index.html',
    styleUrls: ['./index.sass']
})
export class PopUpAction   {
    @ViewChild("formPopUp")
        formPopUp:any;

    inputData:any = {
        name: null,
        jsCode: 'alert(\'Test\');',
        URL: 'https://www.google.com/'
    };
    action:any = {
        jsCode: false,
        URL: false
    }

    resetInputs(opt=null) {
        this.action = {
            jsCode: false,
            URL: false
        }
        if(opt)this.action[opt.field] = true;
    }
}
