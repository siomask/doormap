import {Input,ViewChild,Component,OnInit,OnChanges,EventEmitter,AfterViewInit,ElementRef} from '@angular/core';


@Component({
    selector: 'app-project-svg-drag-point',
    templateUrl: './index.html',
    styleUrls: ['./index.sass']
})
export class SVGDragPoint implements OnInit {
    @Input() parent:any;
    private left:string='-100000px';
    private top:string='-100000px';
    private transform:string='';

    ngOnInit(){

        this.parent._template = this;
    }
    update(zoom:number=1,zoomScale:number=1){
        let dimen = 'px';
        this.left =  zoom*this.parent.left+dimen;
        this.top =  zoom*this.parent.top+dimen;
    }
}