import {Input,ViewChild,Component,OnInit,OnChanges,EventEmitter,AfterViewInit,ElementRef} from '@angular/core';
import * as ENTITY from '../../../entities/entities'
import {WebglView,OxiToolTip}  from '../webgl.view';
//import 'fabric';

declare var fabric:any;
declare var alertify:any;
declare var TWEEN:any;
declare var Array:any;

@Component({
    selector: 'app-project-svg-view',
    templateUrl: './svg.view.html',
    styleUrls: ['./svg.view.sass']
})
export class SVGView implements OnInit,AfterViewInit {

    private keys:Array<number> = [];
      lastHovered:any;
    private dataSrc:string;
    private canEdit:boolean = false;
    private isFinish:boolean = false;
    private zoomValue:number = 1;
    private zoomDelta:number = 10;
    private scaleDelta:number = 1;//1.00152;
    private options:any = {};
    currentShape:any;
    lastSelectedShape:any;
    private curKeyCode:number;
    private showHelpZoomer:boolean = false;
    private finishParseZoomer:boolean = false;
    private curFill:any;
    private curGroup:any;
    private curImgZoom:any;
    private curImgBufferZoom:any;
    private lastPixelRatio:any;
      mode:any;
    private COLORS = ['#00ff00', "#ff0000"];
      MODES:any = {};
    private MOUSE:any;
    private upperC:any;
    private defOpacity:number = 0.7;
    private eventsData:any;
    private idCb:number = Date.now();
    private cntx:Array<any> = [];
    private firstDraw:boolean = true;
    private lastSize:any = {x: 1, y: 1};
    private allPoints:any = [];
    shapes:any;
    settings:any;
    fabricJS:any;
    @ViewChild("parentEl")
        parentEl:HTMLElement;
    @ViewChild("pointsContainer")
        pointsContainer:HTMLElement;
    @ViewChild("zoomer")
        zoomer:HTMLElement;
    @ViewChild("bufferC")
        bufferC:HTMLElement;
    @ViewChild("dataEl")
        dataEl:ElementRef;
    @Input() selected:any;
    @Input() glapp:WebglView;

    constructor() {
        this.MODES = ENTITY.Config.SVG_MODE;
        this.MOUSE = {DOWN: 1, UP: 2, CUR: 0, GROUP: 3};
        this.mode = this.MODES.ADD;
        this.curImgZoom = new Image();
        this.curImgBufferZoom = new Image();
        let _self = this;
        Array.prototype.traverse = function (callback) {
            for (let i = 0; i < this.length; i++) {
                if ((this[i]._objects) instanceof Array) {
                    this[i]._objects.traverse(callback);
                }
                callback(this[i]);
            }
        }
        Array.prototype.traverseInline = function (callback, onlyS) {
            let result = [];
            this.traverse((e)=> {
                if (onlyS) {
                    if (e.type == _self.shapes.POLYGON || e.type == _self.shapes.GROUP)result.push(e);
                } else {
                    result.push(e);
                }

            });
            return result;
        }
    }

    ngOnInit() {
        this.glapp.app._animation.add(()=> {
            TWEEN.update()
        }, this.idCb);
    }

    ngOnDestroy() {
        //delete this.selected.svgDestination;

        this.eventsData.forEach((el)=> {
            fabric.util.removeListener(el.cntx, el.name, el.callback);
        });
        this.glapp.app._animation.remove(this.idCb);
        //this.selected.cash.svg = this.fabricJS.getObjects().concat([]);
        this.saveLastState(-1);
        this.glapp.app._slider.container.style.transformOrigin = '';
        this.glapp.app._slider.container.style.transform = this.glapp.app._slider.container.style.transform.replace(/scale\(\d.*\)/, 'scale(1)');
        this.fabricJS = null;
    }


    ngAfterViewInit() {
        //let svg = this.selected.svgDestination;
        //if (svg instanceof Array)svg = svg[0].name;
        //this.dataSrc = svg && svg.match('.svg') ? ENTITY.Config.PROJ_LOC + this.selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER + svg : null;
        let _self:SVGView = this,
            domElem = this.dataEl['nativeElement'],
            handler = (domElem.addEventListener || domElem.attachEvent).bind(domElem);
        this.canEdit = this.selected.canEdit;
        fabric.Object.prototype.set({
            //hoverCursor:'pointer',
            //selectable: false,
            //transparentCorners: false,
            //cornerColor: 'rgba(102,153,255,0.95)',
            //cornerSize: 12,
            //padding: 7,
            _add: function (e) {
                this.remove(e);

                e._parent = this;
                if (e.type === _self.shapes.CIRCLE && _self.mode !== _self.MODES.ADD && e.selectable)return this;
                return this.addWithUpdate(e);
            },
            dropSelf: function () {
                this.currentShape = null;
                if (this._objects) {
                    while (this._objects.length) this._objects[0].dropSelf();

                } else if (this.type == _self.shapes.CIRCLE) {
                    if (this.parent)return this.parent.dropSelf();
                    if (this._parent) {
                        this._parent.remove(this);
                    } else {
                        _self.fabricJS.remove(this);
                    }
                } else {
                    let _p = this.get('_points');
                    if (_p)_p.forEach((e)=> {
                        _self.removeHtmlPoint(e);

                        if (e._parent) {
                            e._parent.remove(e);
                        } else {
                            _self.fabricJS.remove(e);
                        }
                    });
                }
                if (this._dataSource)this._dataSource.active = false;
                if (this._parent) {
                    this._parent.remove(this);
                } else {
                    _self.fabricJS.remove(this);
                }
                _self.fabricJS.renderAll();
                _self.toSVG();

            },
            _clone: function (isHard, selectable, withTransform, byPnts, withoutParent) {

                let selfT:any = this;
                selfT.setCoords();
                let clone = [
                        'fill', 'hoverFill', 'isInGroup', 'defFill', 'opacity', '_hasUpdate', 'scaleX0', 'scaleY0',
                        'points0', 'id', '_tooltip', '_data', '_dataSource', 'material', 'click', 'scaleX', 'scaleY',
                        'selectable', '_objects', '_border', '_defLeft', '_defTop'],
                    _pn = this.get('_points');
                let _points = isHard || selfT._hasUpdate ? selfT.get('points') : selfT.get('points').map((el, d)=> {
                        return new fabric.Point((el.x) * selfT.scaleX0, (el.y) * selfT.scaleY0);
                        //this.left = this.top = 0;
                        //this._hasUpdate /*= this.scaleX0 = this.scaleY0 = this.scaleY = this.scaleX */ = 1;
                    }),
                    newObj = new selfT.constructor(_points);

                if (byPnts) {
                    if (selfT.get('_points')) {
                        _points = selfT.get('_points').map((point)=> {
                            return new fabric.Point((point.left), (point.top));
                        })
                        newObj = new selfT.constructor(_points);
                    }
                }
                //newObj.setAngle(selfT.getAngle());
                //if (_self.isFinish && selfT.type === _self.shapes.POLYGON) {
                //_points.forEach((point:any)=> {
                //    point.x += selfT.left - newObj.left;
                //    point.y += selfT.top - newObj.top;
                //});
                //newObj = new selfT.constructor(_points)
                //newObj.scaleX =selfT.scaleX;
                //newObj.scaleY =selfT.scaleY;
                //newObj.left =selfT.left;
                //newObj.top =selfT.top;

                //}

                newObj.tempOrigin = this;
                for (var i = 0; i < clone.length; i++) {
                    newObj[clone[i]] = selfT[clone[i]];
                }
                newObj.perPixelTargetFind = !_self.selected.canEdit;
                newObj.selectable = !!selectable;
                newObj.objectCaching = false;
                if (selectable)newObj.lockScalingX = newObj.lockScalingY = newObj.lockRotation = true;
                if (!newObj.id)newObj.set('id', ENTITY.Config.randomstr());

                if (!_self.canEdit) {
                    let lineWidth = 6;
                    let lineBorder = new fabric.Polyline(_points.map((el)=> {
                        el.x += lineWidth / 2.5;
                        el.y += lineWidth / 2.5;
                        return el;
                    }).concat(_points[0]), {
                        left: newObj.left,
                        top: newObj.top,
                        stroke: 'yellow',
                        opacity: 0,
                        strokeWidth: lineWidth,
                        strokeLineCap: 'round',
                        strokeLineJoin: 'round',
                        fill: null
                    });
                    if (newObj._border) _self.fabricJS.remove(lineBorder);
                    _self.fabricJS._add(lineBorder);
                    newObj._border = lineBorder;
                }

                if (!withoutParent) {
                    if (selfT._parent) {
                        selfT._parent._add(newObj);
                    } else {
                        _self.fabricJS._add(newObj);
                    }
                }

                if (_pn) {
                    newObj.set('_points', _pn);
                    for (var i = 0; i < _pn.length; i++) {
                        if (!withoutParent) {
                            if (_pn[i]._parent) {
                                _pn[i]._parent.remove(_pn[i].parent).remove(_pn[i])._add(_pn[i]);
                            } else {
                                _self.fabricJS.remove(_pn[i].parent).remove(_pn[i])._add(_pn[i]);
                            }
                        }

                        _pn[i].parent = newObj;
                    }
                }
                if (selfT._parent) {
                    selfT._parent.remove(selfT);
                } else {
                    _self.fabricJS.remove(selfT);
                }
                newObj.setCoords();

                if (newObj._data)newObj._data._modelFromeScene = newObj;
                return newObj;
            },
            getScreenPst: function () {
                this.onscreenParams = this.getCenterPoint();
                this.onscreenOffset = _self.glapp.app._offset();

            },
            getBoundingBox: function () {
                if (this.type == _self.shapes.POLYGON) {
                    let points = this.get('points'),
                        xMin, xMax, yMin, yMax;

                    xMin = xMax = yMin = yMax = 0;
                    for (let i = 0; i < points.length; i++) {
                        let x = points[i].x,
                            y = points[i].y;

                        if (x < xMin) {
                            xMin = x;
                        }
                        if (x > xMax) {
                            xMax = x;
                        }

                        if (y < yMin) {
                            yMin = y;
                        }
                        if (y > yMax) {
                            yMax = y;
                        }
                    }

                    this.width = xMax - xMin;
                    this.height = yMax - yMin;
                    if (this._parent) {

                        this.minX = this._parent.left - xMin;
                        this.minY = this._parent.top - yMin;
                    } else {
                        this.minX = xMin;
                        this.minY = yMin;

                    }
                    this.left += this.minX;
                    this.top += this.minY;

                }
            }
        });
        fabric.Canvas.prototype.set({
            _add: function (e) {
                this.remove(e);
                e._parent = this;
                if (e.type === _self.shapes.CIRCLE && _self.mode !== _self.MODES.ADD && e.selectable)return this;
                return this.add(e);
            }
        });
        this.fabricJS = new fabric.Canvas(this.dataEl.nativeElement, {selection: false});
        this.fabricJS._id = Date.now();

        //this.fabricJS.getElement().parentNode.appendChild(this.pointsContainer['nativeElement']);
        this.fabricJS._pointContainer = document.createElement('div');
        this.fabricJS._pointContainer.className = 'points-container';
        this.fabricJS.getElement().parentNode.appendChild(this.fabricJS._pointContainer);

        //var filter = new fabric.Image.filters.Resize();
        //this.fabricJS.filters.push(filter);
        //this.fabricJS.applyFilters(canvas.renderAll.bind(canvas));

        this.curFill = this.getRandomColor();
        this.settings = {
            CIRCLE: {
                defRadius: 6,
                defStroke: 2,
                radius: 6,
                opacity: 0.0000001,
                strokeWidth: 2,
                fill: '#1976D2',
                stroke: '#ffffff',
                selectable: false,
                excludeFromExport: true,
                hasControls: false,
                hasBorders: false,
                originX: 'center',
                originY: 'center',
                hoverCursor: "url('/assets/img/Cursor_2x_1.png')8.5 0, auto",
                //shadow: 'rgba(0,0,0,0.6) 0px 1px 3px'
            },
            POLYGON: {
                fill: this.curFill,
                opacity: this.selected.camera.opacity,
                selectable: false,
                perPixelTargetFind: !this.selected.canEdit,
                objectCaching: false

            }, GROUP: {
                subTargetCheck: true,
                selectable: false,
                objectCaching: false,
                perPixelTargetFind: !this.selected.canEdit
            }
        };
        this.shapes = {
            POLYLINE: 'polyline',
            GROUP: 'group',
            POLYGON: 'polygon',
            CIRCLE: 'circle'
        };
        this.resize();
        setTimeout(()=> {
            this.updateView(-1);
        }, 200)


    }

    reload(data) {
        this.clear();
        fabric.loadSVGFromString(data, (o, _options)=> {
            this.options = _options;
            _options.width *= this.scaleDelta;
            _options.height *= this.scaleDelta;
            this.parseSVG(data, ((_objects)=> {
                if (_options)_options.objects = [];
                this.onFinishParse(o, _options, _objects);
                this.toSVG();
            }));
        });
    }

    private saveLastState(_prevItem:number = -1) {
        this.updateMode(this.MODES.ADD);

        let _selected = this.selected,
            prevItem = (_prevItem < 0 ? _selected.currentItem : _prevItem),
            _cash = _selected.cash,
            _objs = this.fabricJS.getObjects().concat([]);

        if (!_cash.svg)_cash.svg = [];
        _cash.svg[prevItem] = _objs;
        _cash.svg[prevItem].set = {
            _w: this.fabricJS.width,
            _h: this.fabricJS.height
        }
        this.toSVG(prevItem);
        this.clear(_objs);
    }

    private clear(_els:any = this.fabricJS.getObjects().concat([])) {
        //this.fabricJS.clear();
        for (let i = 0, els = _els; i < els.length; i++) {
            this.fabricJS.remove(els[i]);
        }
        this.removeHtmlPoint();
    }

    updateView(_prevItem:number = -1) {
        let _self = this,
            _selected = this.selected,
            _cash = _selected.cash,
            prevItem = (typeof _prevItem === 'number' && _prevItem > -1 ? _prevItem : _selected.currentItem);
        this.zoomWrapper(0);
        //this.resize();
        if (_selected.currentItem !== prevItem)this.saveLastState(prevItem);
        this.isFinish = false;
        //this.fabricJS._points = new fabric.Group([], this.settings.GROUP);
        //this.fabricJS._add(this.fabricJS._points);

        this.fabricJS._set = {
            _w: 0,
            _h: 0,
            _viewBox: [0, 0, 0, 0]
        };
        /* if (_cash.svg[_selected.currentItem] && 0) {
         //_self.resize();
         this.isFinish = true;

         let els = _cash.svg[_selected.currentItem];
         for (let i = 0; i < els.length; i++) {
         let obj = els[i];
         //if (obj.type == this.shapes.GROUP) {
         //    console.log(obj);
         //}
         //if (els[i]._parent instanceof fabric.Canvas) {

         //_self.resize(false, false, els[i]._parent);
         //break;
         //} else {
         this.fabricJS._add(obj);

         //}
         }
         this.fabricJS._set = els.set;
         //els.forEach((obj)=> {
         //    obj._parent.remove(obj);
         //    this.fabricJS._add(obj);
         //});
         //setTimeout(()=> {
         //    _self.resize();
         //}, 200);
         _self.resize();
         this.fabricJS.calcOffset().renderAll();
         this.onLoadSVG();

         } else */
        if (_selected.svgDestination && _selected.svgDestination[_selected.currentItem]) {
            let svgUrl = ENTITY.Config.PROJ_LOC + _selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER;

            if (typeof _selected.svgDestination == 'string') {
                svgUrl += (  _selected.svgDestination  );
            } else if (_selected.svgDestination[_selected.currentItem].svgString) {
                svgUrl = null;
            } else {
                svgUrl += ( _selected.svgDestination[_selected.currentItem]);
            }

            if (!svgUrl) {
                this.loadSVGFromString(_selected.svgDestination[_selected.currentItem].svgString, _self, _selected.svgDestination[_selected.currentItem].zoomValue);
            } else {
                this.glapp.authServ.get(svgUrl, {hasAuthHeader: false}).subscribe((res:any)=> {
                    this.loadSVGFromString(res._body, _self);
                });
            }


        } else {
            this.onLoadSVG();
        }
    }


    private loadSVGFromString(svgString, _self, zoomValue:any = 1) {
        //console.log(svgString);
        fabric.loadSVGFromString(svgString, (o, options)=> {
            this.options = options;


            //options.width *= this.scaleDelta;
            //options.height *= this.scaleDelta;
            this.parseSVG(svgString, ((_objects, optionss)=> {
                this.fabricJS._set = {
                    _w: (options.width),
                    _h: (options.height ),
                    _viewBox: optionss.viewBox
                };
                if (options)options.objects = o;
                _self.onFinishParse(o, options, _objects);
                this.onLoadSVG();
            }));
        });
    }

    private onFinishParse(o, options, _objects) {
        let _self = this;
        this.finishParseZoomer = false;
        if (o && options) {
            _self.resize(false, false, options);
            let scaleMultiplierX = 1,//_self.fabricJS.width / options.width,
                scaleMultiplierY = 1,// _self.fabricJS.height / options.height,
                deltaXVieport = options.width / this.fabricJS._set._viewBox[2],
                deltaYVieport = options.height / this.fabricJS._set._viewBox[3],
                outs = [], groups = [];

            this.fabricJS._set.scale = deltaXVieport;
            this.zoomWrapper(0);


            for (let itm = 0, maxL = o.length; itm < maxL; itm++) {
                let cur = o[itm];
                if (cur.type == _self.shapes.POLYGON) {

                    //this.fabricJS.add(cur);
                    //continue;
                    let center = cur.getCenterPoint(),
                        _points = [],
                        points0 = cur.get('points'),
                        _p = points0.concat([]).map((el, d)=> {

                            cur.left = cur.top = 0;
                            cur.scaleX = cur.scaleY = 1;
                            let _pC = new fabric.Point(
                                center.x + el.x * scaleMultiplierX * deltaXVieport,
                                center.y + el.y * scaleMultiplierY * deltaYVieport
                            );
                            if (_self.canEdit) {
                                let circle = new fabric.Circle(_self.settings.CIRCLE);
                                circle.left = _pC.x;
                                circle.top = _pC.y;
                                circle._iter = d;
                                circle.__parent = circle.parent = cur;
                                _points.push(circle);
                                _self.createHtmlPoint(circle);
                            }
                            return _pC;
                        });
                    if (_points.length) cur.set('_points', _points);
                    cur.set('points', _p);
                    cur.set('points0', points0);
                    cur.set('_center', center);
                    outs.push(cur);
                } else if (cur.type == _self.shapes.GROUP) {
                    //_self.fabricJS._add(cur);
                    //parseAndInsert(cur._objects, options);
                }
                let orCloner, cloneCur = cur._clone(true);
                orCloner = cloneCur;
                for (let u = 0; u < _objects.length; u++) {
                    if (cloneCur.id == _objects[u].id) {
                        let _c = _objects.splice(u, 1)[0];
                        if (_c.group) {
                            let _group;
                            for (let g = 0; g < groups.length; g++) {
                                if (_c.group.id == groups[g].id) {
                                    _group = groups[g];
                                    break;
                                }
                            }
                            if (!_group) {
                                _group = new SVGGroup(_self, _c.group.id);
                                groups.push(_group);
                            }
                            _group.add(cloneCur);
                            cloneCur = _group;
                        }
                        break;
                    }
                }
                for (let i = 0, areas = _self.selected.areas; areas && i < areas.length; i++) {
                    if (areas[i]._id.match(cloneCur.id)) {
                        cloneCur._data = areas[i];
                        cloneCur._data._modelFromeScene = cloneCur;
                        break;
                    }
                }
                for (let i = 0, sources = _self.glapp.preToolTip.dataElem; sources && i < sources.length; i++) {
                    if (sources[i]._id == cloneCur.id || (cloneCur._data && (sources[i]._id == cloneCur._data.dataSourceId || ( sources[i]._id == cloneCur._data.dataSourceGeneratedIdId && !cloneCur._data.dataSourceId ) ))) {
                        //console.log(cloneCur);
                        cloneCur._dataSource = sources[i];
                        break;
                    }
                }
                orCloner.opacity = _self.canEdit ? _self.selected.camera.opacity : _self.defOpacity;

                if (!orCloner.defFill) orCloner.defFill = orCloner.fill;
                if (!orCloner.hoverFill)orCloner.hoverFill = cloneCur._data && cloneCur._data.areas && cloneCur._data.areas.length || cloneCur._dataSource ? _self.COLORS[0] : _self.COLORS[1];
                if (!cloneCur._tooltip && cloneCur.type == _self.shapes.POLYGON)cloneCur._tooltip = new OxiToolTip(cloneCur, _self.glapp.app);
            }
            for (let g = 0; g < groups.length; g++) {
                let _n = groups[g].make();
                for (let f = 0, args = ['_data', '_dataSource', 'id']; f < args.length; f++) {
                    _n[args[f]] = groups[g][args[f]];
                }
                _n._tooltip = new OxiToolTip(_n, _self.glapp.app);
            }
            this.fabricJS.calcOffset().renderAll();
        }
        this.finishParseZoomer = true;

        this.updateMode(this.MODES.EDIT);
        this.updateMode(this.MODES.ADD);
        this.glapp.app.checkIgnore();
        //this.mode = 2;
        //this.updateMode(this.MODES.NO);
    }


    private updateImgZoomer(callback) {
        let _self = this;
        this.curImgBufferZoom.src = this.fabricJS.toDataURL();
        let _bCnvc = _self.bufferC['nativeElement'];
        if (!this.cntx[1])this.cntx[1] = _bCnvc.getContext('2d');
        let _cnvs = this.cntx[1];
        _cnvs.drawImage(_self.glapp.app._slider.isDebug ? _self.glapp.app._slider.currentAlignFrame : _self.glapp.app._slider.currentFrame, 0, 0, _bCnvc.width, _bCnvc.height);
        _cnvs.drawImage(_self.curImgBufferZoom, 0, 0);
        _self.curImgZoom.src = _bCnvc.toDataURL();
        if (callback)callback();
    }

    private drawZoom(event) {
        this.showHelpZoomer = this.selected.camera.showZoomHelper && [this.MODES.EDIT, this.MODES.ADD].indexOf(this.mode) > -1;
        if (!this.showHelpZoomer)return this.cntx = [];
        let
            _deltaX = this.zoomDelta,
            _deltaY = _deltaX * this.curImgZoom.height / this.curImgZoom.width,
            zCnvs = this.zoomer['nativeElement'],
            _offset = event.e.target.getBoundingClientRect();
        this.updateImgZoomer(()=> {
            if (!this.cntx[0])this.cntx[0] = zCnvs.getContext('2d');
            this.cntx[0].drawImage(this.curImgZoom, event.e.clientX - _offset.left - _deltaX, event.e.clientY - _offset.top - _deltaY, 2 * _deltaX, 2 * _deltaY, 0, 0, zCnvs.width, zCnvs.height);
        });

        console.log("draw zoom")

    }

    private onLoadSVG() {
        let _self = this,
            fabricJS = this.fabricJS;

        if (fabricJS.hasEvents)return this.isFinish = true;
        fabricJS.hasEvents = true;
        if (this.canEdit) {
            let mouse:any = {}, step = 1,
                domElement = this.parentEl['nativeElement'];
            this.upperC = document.getElementsByClassName('upper-canvas')[0];
            this.eventsData = [
                {cntx: domElement, name: ENTITY.Config.EVENTS_NAME.MOUSE_DOWN, callback: (e)=>this._onMouseDown(e)},
                {cntx: domElement, name: ENTITY.Config.EVENTS_NAME.MOUSE_UP, callback: (e)=>this._onMouseUp(e)},
                {cntx: domElement, name: ENTITY.Config.EVENTS_NAME.MOUSE_MOVE, callback: (e)=>this._onMouseMove(e)},
                {
                    cntx: window, name: ENTITY.Config.EVENTS_NAME.MOUSE_UP, callback: (e)=> {
                    if (fabricJS._shouldResave)this.toSVG();
                    fabricJS._shouldResave = false;
                }
                },
                {cntx: window, name: ENTITY.Config.EVENTS_NAME.KEY.UP, callback: (e)=>this.onKeyUp(e)},
                {cntx: window, name: ENTITY.Config.EVENTS_NAME.KEY.DOWN, callback: (e)=>this.onKeyDown(e)},
                {
                    cntx: this.upperC,
                    name: ENTITY.Config.EVENTS_NAME.CNTXMENU,
                    callback: (e)=>this.onMouseDown(e)
                },
                {
                    cntx: this.upperC,
                    name: ENTITY.Config.EVENTS_NAME.MOUSE_UP,
                    callback: (e)=>this._onMouseUp(e)
                },
            ];
            this.eventsData.forEach((el)=> {
                fabric.util.addListener(el.cntx, el.name, el.callback);
            });

            /* let _p = this.parentEl['nativeElement'],
             _handlerP = (_p.addEventListener || _p.attachEvent).bind(this);
             _handlerP('mousedown', (e)=> {
             if (this.zoomValue !== 1) {
             fabricJS.startTranSlatePst = {x:e.clientX,y:e.clientY};

             }
             });0482428325
             _handlerP('mouseup', (e)=> {
             if (fabricJS.startTranSlatePst) {
             fabricJS.hoverCursor = fabricJS.defaultCursor = 'default';
             fabricJS.startTranSlatePst = fabricJS.startTranslatePersent = fabricJS.moveTranSlatePst = false;
             fabricJS.renderAll();
             e.preventDefault();
             return false;
             }
             });
             _handlerP('mousemove', (e)=> {
             if (fabricJS.startTranSlatePst) {
             //fabricJS.moveTranSlatePst = {x:fabricJS.startTranSlatePst.x- pos.x,y:fabricJS.startTranSlatePst.y- pos.y};
             fabricJS.moveTranSlatePst = {x:e.clientX,y:e.clientY};
             this.translateWorkArea();
             e.preventDefault();
             return false;
             }
             });*/

            /*fabricJS.on('object:selected', (e)=> {
             //if (this.mode != this.MODES.EDIT)this.mode = this.MODES.NO;
             })*/
            ;
            /*fabricJS.on('before:selection:cleared', (options)=> {
             //if (this.mode != this.MODES.EDIT)this.mode = this.MODES.ADD;
             });*/
            fabricJS.on('mouse:wheel', (event)=> {
                let detail = (event.e.deltaY || event.e.detail || event.e.wheelDelta) / 10;
                this.zoomDelta += detail;
                this.drawZoom(event);
                if (this.showHelpZoomer)return event.e.preventDefault();
                if (this.keys.indexOf(16) > -1) {
                    this.zoomWrapper(detail < 0 ? 1 : -1, 0.1);
                    event.e.preventDefault();
                }

            });
            fabricJS.on("mouse:move", (event:any)=> {
                let pos = fabricJS.getPointer(event.e);
                this.glapp.app._projControls.updateTooltipInfo(event.target, event.e, {x: pos.x* fabricJS.getZoom(), y: pos.y* fabricJS.getZoom()});


                if (fabricJS.mouseDown && fabricJS.mouseDown.x - pos.x > 1 && fabricJS.mouseDown.y - pos.y > 1) {
                    fabricJS.mouseMove = true;
                }
                if (this.mode === this.MODES.EDIT && this.currentShape) {
                    //if (event.target.type == this.shapes.POINT)this.selectBar(event.target);
                    var points = this.currentShape.get("points");
                    if (points && points.length) {
                        points[points.length - 1].x = pos.x;
                        points[points.length - 1].y = pos.y;
                        this.currentShape.set({
                            points: points
                        });
                        this.currentShape._clone(true);
                        fabricJS.renderAll();
                    }
                }
                if (fabricJS.startTranSlatePst) {
                    return;
                }
                this.glapp.app._projControls.showControls(event.e, false);
                if (this.mode == this.MODES.NO) {
                } else if (this.mode == this.MODES.DRAG) {
                    //this.fabricJS.wrapperEl.cursor = '-webkit-grabbing';
                    //let tr  = this.fabricJS.wrapperEl.style.transformOrigin.split(" ")
                    //    ;

                    //this.fabricJS.wrapperEl.style.transformOrigin= (100-(mouse.down.clientX -event.e.clientX/ event.e.target.clientWidth)*100)+'% '+(100-(mouse.down.clientY -event.e.clientY/ event.e.target.clientHeight)*100)+'%';
                    //this.fabricJS.wrapperEl.style.transformOrigin= (parseFloat(tr[0])+(mouse.down.clientX >event.e.clientX?-1:1)*step)+"% "+ (parseFloat(tr[1])+(mouse.down.clientY >event.e.clientY?-1:1)*step)+"%";
                }
                this.drawZoom(event);
                //this.fabricJS.wrapperEl.cursor = '';
            });
            fabricJS.on("mouse:up", (event:any)=> {

                fabricJS.previousEvent = null;
                this.MOUSE.CUR = this.MOUSE.UP;
                //if (this.mode == this.MODES.DRAG)this.mode = this.MODES.NO;
                //this.currentShape
                //if (this.lastHovered && this.mode === this.MODES.NO && this.lastHovered && [this.shapes.POLYGON, this.shapes.GROUP].indexOf(this.lastHovered.type) > -1) {
                //    if (!this.glapp.app._projControls.showAttachPopUp(this.lastHovered)) {
                //        this.mode = this.MODES.ADD;
                //        this.currentShape = false;
                //    }
                //}
                let isMove = fabricJS.mouseMove,
                    _target = event.target;
                fabricJS.mouseMove = fabricJS.mouseDown = false;
                if (fabricJS.startTranSlatePst) {
                    fabricJS.hoverCursor = fabricJS.defaultCursor = 'default';
                    fabricJS.startTranSlatePst = fabricJS.startTranslatePersent = fabricJS.moveTranSlatePst = false;

                }

                let pos = fabricJS.getPointer(event.e),
                    polySet = this.settings.POLYGON,
                    pointSet = {left: pos.x, top: pos.y};

                Object.assign(pointSet, this.settings.CIRCLE);


                if (this.mode === this.MODES.ADD) {
                    this.makeObjeInFront();
                } else if (this.mode === this.MODES.EDIT && this.currentShape && this.currentShape._iseCreating) {
                    this.drawPoly([], pointSet, isMove);
                }

                if (isMove)return;


                if (this.mode == this.MODES.GROUP) {
                    if (event.target && event.target.type == this.shapes.POLYGON) {
                        this.curGroup.add(event.target, 1);
                    }
                } else if (this.mode == this.MODES.NO) {
                    if (this.lastHovered) {
                        if (!this.glapp.app._projControls.showAttachPopUp(this.lastHovered)) {
                            //this.mode = this.MODES.ADD;
                            this.currentShape = false;

                        }
                    }
                    /*else if(this.zoomValue>1){
                     this.mode = this.MODES.DRAG;
                     mouse.down = event.e;
                     if(!this.fabricJS.wrapperEl.style.transformOrigin)this.fabricJS.wrapperEl.style.transformOrigin="50% 50%";
                     } */
                    else {
                        //this.mode = this.MODES.ADD;
                    }
                } else if (this.mode === this.MODES.ADD) {
                    /*this.drawPoly([{
                     x: pos.x,
                     y: pos.y
                     }, {
                     x: pos.x + 1,
                     y: pos.y + 1
                     }], pointSet);*/

                } /*else if (this.mode === this.MODES.EDIT && this.currentShape && this.currentShape._iseCreating) {
                 this.drawPoly([], pointSet);
                 }*/ else if (this.mode === this.MODES.CONTINUE_CREATE) {
                    if (_target) {
                        this.updateMode(this.MODES.EDIT);
                        this.currentShape = _target;
                        _target._iseCreating = _target._isEditing = true;
                        this.drawPoly([], pointSet);
                        this.fabricJS.remove(_target.get("_points").pop());
                    }

                }
            });
            fabricJS.on("mouse:down", (event:any)=> {
                let _target = event.target;
                this.MOUSE.CUR = this.MOUSE.DOWN;
                fabricJS.mouseDown = fabricJS.getPointer(event.e);
                fabricJS.previousEvent = event;

                if (_target &&
                    (
                        (this.MODES.ADD === this.mode && _target.type === this.shapes.CIRCLE) ||
                        (this.MODES.DRAG === this.mode && _target.type === this.shapes.POLYGON)
                    )
                ) {

                } else {
                    fabricJS.startTranSlatePst = true;
                    fabricJS.hoverCursor = fabricJS.defaultCursor = 'move';
                    fabricJS.renderAll();
                }
                /* if (this.mode === this.MODES.CONTINUE_CREATE) {
                 if (_target) {
                 this.updateMode(this.MODES.EDIT);
                 this.currentShape = _target;
                 _target._iseCreating = _target._isEditing = true;
                 }

                 }*/

            });
            fabricJS.on('object:moving', (e:any)=> {
                let x = 0,//e.e.movementX,
                    y = e.e.movementY,
                    curActiv:any = e.target;

                //if(!x) {
                //    x = e.e.screenX - fabricJS.previousEvent.e.screenX;
                //    y = e.e.screenY - fabricJS.previousEvent.e.screenY;
                //}
                fabricJS.previousEvent = e;
                if (curActiv) {
                    switch (curActiv.type) {
                        case 'polygon':
                        {
                            //curActiv.get("_points").forEach((obj)=> {
                            //    obj.set('left', obj.left + x);
                            //    obj.set('top', obj.top + y);
                            //obj.setCoords();
                            //});
                            //console.log(curActiv.getAngle());

                            //console.log(curActiv.center);
                            break;
                        }
                        case 'circle':
                        {
                            //let
                            //    _p = curActiv.parent.get('points');
                            //_p[curActiv._iter].x += x * this.lastSize.x;
                            //_p[curActiv._iter].y += y * this.lastSize.y;

                            curActiv.parent.points[curActiv._iter] = {
                                x: curActiv.getCenterPoint().x,
                                y: curActiv.getCenterPoint().y
                            };
                            this.updatePoints(curActiv._htmlPoint);
                            //curActiv.parent._parent.remove(curActiv.parent);
                            //curActiv.parent.set('points',_p);
                            //curActiv.parent.setCoords();
                            //console.log(curActiv.parent.getBoundingRect())
                            ;
                            //curActiv.parent._parent._add(curActiv.parent)

                            //curActiv.parent._parent._add(curActiv.parent);

                            //curActiv.parent._clone(true);

                            //var bound = curActiv.parent.getBoundingRect();
                            //curActiv.setTop(curActiv.originalState.top);
                            //curActiv.setLeft(curActiv.originalState.left);
                            //curActiv.setScaleX(curActiv.originalState.scaleX);
                            //curActiv.setScaleY(curActiv.originalState.scaleY);
                            //curActiv.parent.setCoords();
                            break;
                        }
                    }

                    //fabricJS.renderAll();
                    fabricJS._shouldResave = true;
                }

            });
            /*fabricJS.on('object:modified', (e)=> {
             var obj = e.target;
             var matrix = obj.calcTransformMatrix();
             if (obj.type == 'polygon') {
             obj.get("_points").forEach(function (p) {
             return fabric.util.transformPoint(p, matrix);
             });
             fabricJS.renderAll();
             }

             });*/
            fabricJS.on('mouse:over', (e)=> {
                if ((this.mode != this.MODES.NO && this.mode != this.MODES.GROUP) || !e.target)return;
                if (e.target.type == this.shapes.POLYGON || e.target.type == this.shapes.GROUP)this.lastHovered = e.target;
                this.fadeSelected(e.target, true);
            });
            fabricJS.on('mouse:out', (e)=> {

                if (!this.lastHovered || !e.target)return;
                this.lastHovered = null;
                this.fadeSelected(e.target);

            });
        }


        /* fabricJS.on("after:render", function () {
         fabricJS.calcOffset();
         });*/
        setTimeout(()=> {
            if (this.glapp.app && this.glapp.app._events) this.glapp.app._events.onWindowResize();
            this.isFinish = true;
        }, 100)
    }

    private animate() {
        //fabric.util.requestAnimFrame(()=>{this.animate()}, this.fabricJS.getElement());
        //this.fabricJS.renderAll();
    }

    getRandomColor() {
        let letters = '0123456789ABCDEF';
        var color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    updateOpacity() {
        this.fabricJS._objects.forEach((el:any)=> {
            if (el.type == this.shapes.GROUP || el.type == this.shapes.POLYGON)el.opacity = this.selected.camera.opacity;
        });
        this.fabricJS.renderAll();
    }

    private fadeSelected(target:any, show:boolean = false) {
        if (target.type == this.shapes.POLYLINE || target.type == this.shapes.CIRCLE)return;
        if (target.tween) {
            if (target.tween.show == show) {
                return;
            } else {
                target.tween.stop();
            }
        }
        let defOp = (typeof this.selected.camera.opacity == 'number' ? this.selected.camera.opacity : this.defOpacity),
            endO = (show ) ? 1 : 1 - defOp,
            lineDeltaOpacity = 0.2;
        let isGreen = target._data && target._data.areas && target._data.areas.length || target._dataSource;
        target.tween = new TWEEN.Tween({delta: 0}).to({delta: 1}, 400)
            //.easing(TWEEN.Easing.Exponential.In)
            .onStart(()=> {
                if (!this.canEdit && target._tooltip) target._tooltip.display(show);
            })
            .onUpdate((delta)=> {
                let _opct = show ? defOp + delta * endO : (1 - endO * delta),
                    lineOpacity = show ? delta : (1 - delta);
                if (this.canEdit) {
                    if (target.type == this.shapes.GROUP) {
                        for (let i = 0; i < target._objects.length; i++) {
                            target._objects[i].set('opacity', _opct);
                        }
                    } else {
                        target.set('opacity', _opct);
                    }
                } else {
                    if (target.type == this.shapes.GROUP) {
                        for (let i = 0; i < target._objects.length; i++) {
                            let el = target._objects[i];
                            if (!el.defFill)el.defFill = el.fill;
                            if (!el.hoverFill)el.hoverFill = isGreen ? this.COLORS[0] : this.COLORS[1];
                            el.set('fill', el.hoverFill);
                            el.set('opacity', _opct);
                            if (el._border)el._border.set('opacity', lineOpacity);
                        }

                    } else {
                        target.set('fill', target.hoverFill).set('opacity', _opct);
                        if (target._border)target._border.set('opacity', lineOpacity);
                    }
                }
                this.fabricJS.renderAll();

            })
            .onComplete(() => {
                if (!show) {
                    if (target.type == this.shapes.GROUP) {
                        for (let i = 0; i < target._objects.length; i++) {
                            if (target._objects[i].defFill)target._objects[i].set('fill', target._objects[i].defFill);
                        }
                    } else {
                        if (target.defFill)target.set('fill', target.defFill);
                    }
                }
                this.fabricJS.renderAll();
                //this.currentShape = show ? null : target;
            })
            .start();
        target.tween.show = show;

    }

    private toSVG(item:number = null) {
        let _item = (typeof item == 'number' ? item : this.selected.currentItem),
            svgName = _item + '.svg',
            svgString;
        if (!this.selected.svgDestination || typeof this.selected.svgDestination == "string")this.selected.svgDestination = {};
        for (let i = 0, _els = this.fabricJS._objects; i < _els.length; i++) {
            let el = _els[i];
            if (el.type == this.shapes.POLYGON && (!el.get('points') || !el.get('points').length)) {
                this.fabricJS.remove(el);
                i--;
            }
        }
        svgString = this.fabricJS.toSVG(['name']);
        this.selected.svgDestination[_item] = {
            name: svgName,
            file: new File([new Blob([svgString], {type: 'text/*'})], svgName),
            zoomValue: this.fabricJS.getZoom(),
            svgString: svgString
        };
        this.selected.hasChanges = this.selected._project.hasChanges = true;
    }

    private   getBase64(file, next) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            next(reader.result);
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    private toggleGroupingElements(show:boolean = false) {
        /* let elms = show ? this.fabricJS.lastObjects : this.fabricJS.getObjects().concat([]);
         if (!show) this.fabricJS.lastObjects = elms;
         for (let i = 0; i < elms.length; i++) {
         let els = elms[i];
         if (els.type == this.shapes.POLYGON) {

         } else {
         if (show) {
         //els.left = els.originLeft;
         els._parent.add(els);
         } else {
         //els.originLeft = els.left;
         //els.left = 2 * this.fabricJS.width;
         els._parent.remove(els);
         }
         }
         }
         this.fabricJS.calcOffset().renderAll();*/
        this.toggleElements(show, this.shapes.GROUP);
    }

    private makeObjeInFront() {
        for (let i = 0, arrgs = this.fabricJS.getObjects().traverseInline(); i < arrgs.length; i++) {
            let els = arrgs[i];
            if (els.type === this.shapes.POLYGON) {
                for (let u = 0, _points = els.get('_points') || []; u < _points.length; u++) {
                    let els = _points[u];
                    this.fabricJS.moveTo(els, arrgs.length);
                }
            }
        }
    }

    private toggleElements(show:boolean = false, type:string = this.shapes.POLYGON) {
        let elms = /*this.fabricJS.lastObjects ? this.fabricJS.lastObjects : */this.fabricJS.getObjects().concat([]);
        //if (!this.fabricJS.lastObjects) this.fabricJS.lastObjects = elms;

        switch (type) {
            case this.shapes.POLYGON:
            {
                for (let i = 0, arrgs = elms; i < arrgs.length; i++) {
                    let els = arrgs[i];
                    if (els.type == this.shapes.POLYGON && !els.isInGroup) {
                        if (show) {
                            els._defLeft = els.left;
                            els._defTop = els.top;
                        } else {

                            let _swiftX = els.left - els._defLeft,
                                _swiftY = els.top - els._defTop;
                            els._defTop = els._defLeft = null;
                            els.get("_points").forEach((circle:any)=> {
                                circle.left += _swiftX;
                                circle.top += _swiftY;
                                //if(els.angle)this.applyAgleToCircleHelper(circle,els.getCenterPoint(),els.angle);
                            });

                            els._clone(true, show, !show, true);
                            continue;
                        }
                        els._clone(true, show, !show);

                    } else if (els.type == this.shapes.GROUP) {
                        if (show)els.copyObj = [];
                        for (let u = 0, _polies = show ? els._objects : els.copyObj; u < _polies.length; u++) {
                            let poly = _polies[u];
                            if (show) {
                                els.remove(poly);
                                poly._parent = this.fabricJS;
                                poly = poly._clone(true, true);
                                poly._defLeft = poly.left;
                                poly._defTop = poly.top;
                                els.copyObj.push(poly);
                                u--;
                            } else {
                                this.fabricJS.remove(poly);
                                poly._parent = els;
                                if (typeof poly._defLeft == 'number') {
                                    let _swiftX = poly.left - poly._defLeft,
                                        _swiftY = poly.top - poly._defTop;
                                    poly._defTop = poly._defLeft = null;
                                    poly.get("_points").forEach((circle:any)=> {
                                        circle.left += _swiftX;
                                        circle.top += _swiftY;
                                    })
                                    poly = poly._clone(false, false, false, true);
                                    continue;
                                }
                                poly = poly._clone(false, false);
                            }
                            poly.isInGroup = true;

                        }

                    }
                }
                break;
            }
            case this.shapes.CIRCLE:
            {
                for (let i = 0, arrgs = elms.traverseInline(); i < arrgs.length; i++) {
                    let els = arrgs[i];
                    if (els.type === this.shapes.POLYGON) {
                        for (let u = 0, _points = els.get('_points') || []; u < _points.length; u++) {
                            let els = _points[u];
                            if (show) {
                                //els.left = els.originLeft;
                                els.selectable = true;
                                els._parent._add(els);
                            } else {
                                //els.originLeft = els.left;
                                //els.left = 2 * this.fabricJS.width;
                                els._parent.remove(els);
                            }
                        }

                    }
                }
                this.hideHtmlPoint(!show);
                break;
            }
            case this.shapes.GROUP:
            {
                let elms = show ? this.fabricJS.lastObjects : this.fabricJS.getObjects().concat([]);
                if (!show) this.fabricJS.lastObjects = elms;
                for (let i = 0, arrgs = elms; i < arrgs.length; i++) {
                    let els = arrgs[i];
                    if (els.type == this.shapes.POLYGON || !els._parent) continue;
                    if (show) {
                        //els.left = els.originLeft;
                        els._parent.add(els);
                    } else {
                        //els.originLeft = els.left;
                        //els.left = 2 * this.fabricJS.width;
                        els._parent.remove(els);
                    }
                }
                break;
            }
            default:
            {
                //if (show) {
                //    //els.left = els.originLeft;
                //    els._parent.add(els);
                //} else {
                //    //els.originLeft = els.left;
                //    //els.left = 2 * this.fabricJS.width;
                //    els._parent.remove(els);
                //}
            }
        }


        this.fabricJS.calcOffset().renderAll();
    }

    private applyAgleToCircleHelper(crcle, center, angle) {
        let _x = crcle.left - center.y,
            _y = crcle.left - center.y,
            angleRadians = Math.atan2(_x, _y),
            _angle = angleRadians + angle * 180 / Math.PI;
        let distance = Math.sqrt(Math.pow(_x, 2) + Math.pow(_y, 2));
        crcle.left = center.x + distance * Math.cos(_angle);
        crcle.top = center.y + distance * Math.sin(_angle);

    }

    private translateWorkArea() {
        if (this.zoomValue > 1) {
            if (!this.fabricJS.wrapperEl.style.transformOrigin)this.fabricJS.wrapperEl.style.transformOrigin = "50% 50%";
            let tr = this.fabricJS.wrapperEl.style.transformOrigin.split(" "),
                x = parseFloat(tr[0]),
                y = parseFloat(tr[1]),
                fabricJS = this.fabricJS;

            //this.fabricJS.wrapperEl.style.transformOrigin= (parseFloat(tr[0])+(mouse.down.clientX >event.e.clientX?-1:1)*step)+"% "+ (parseFloat(tr[1])+(mouse.down.clientY >event.e.clientY?-1:1)*step)+"%";
            if (fabricJS.startTranSlatePst) {

                if (!fabricJS.startTranslatePersent)fabricJS.startTranslatePersent = {x: x, y: y};

                let _full = 100;
                let _x = fabricJS.startTranslatePersent.x + (fabricJS.startTranSlatePst.x / fabricJS.width * _full) - (fabricJS.moveTranSlatePst.x / fabricJS.width * _full);
                let _y = fabricJS.startTranslatePersent.y + (fabricJS.startTranSlatePst.y / fabricJS.height * _full) - (fabricJS.moveTranSlatePst.y / fabricJS.height * _full);
                if (_x >= 0 && _x <= _full)x = _x;
                if (_y >= 0 && _y <= _full)y = _y;
            } else {
                for (let i = 0; i < this.keys.length; i++) {
                    switch (this.keys[i]) {
                        case 83:
                        case 40:
                        {//down
                            if (y < 100)y += this.zoomValue;
                            break;
                        }
                        case 68:
                        case 39:
                        {//right
                            if (x < 100)x += this.zoomValue;
                            break;
                        }
                        case 87:
                        case 38:
                        {//up
                            if (y > 0)y -= this.zoomValue;
                            break;
                        }
                        case 65:
                        case 37:
                        {//left
                            if (x > 0)x -= this.zoomValue;
                            break;
                        }
                    }


                }
            }

            this.glapp.app._slider.container.style.transformOrigin = this.fabricJS.wrapperEl.style.transformOrigin = x + "% " + y + "%";
        }
    }

    private onKeyUp(e) {
        if (e.keyCode === 27 || e.keyCode === 13) {//Esc or enter
            this.onFinishDraw();
            if (this.mode == this.MODES.EDIT) {
                this.drawPoly();
            }

        }
        if (this.keys.indexOf(e.keyCode) > -1) this.keys.splice(this.keys.indexOf(e.keyCode), 1);
        switch (e.keyCode) {
            case 90://z
            {
                this.updateMode(this.MODES.ADD);
                break;
            }
            case 88://x
            {
                this.updateMode(this.MODES.DRAG);
                break;
            }
            case 67://c
            {
                this.updateMode(this.MODES.EDIT);
                break;
            }
            case 86://v
            {
                this.updateMode(this.MODES.NO);
                break;
            }
            case 66://b
            {
                this.updateMode(this.MODES.GROUP);
                break;
            }
            case 78://n
            {
                this.updateMode(this.MODES.CONTINUE_CREATE);
                break;
            }
        }
    }

    private onKeyDown(e) {

        if (this.keys.indexOf(e.keyCode) < 0)this.keys.push(e.keyCode);
        this.translateWorkArea();

        switch (e.keyCode) {
            case 107:
            {//+
                this.zoomWrapper(1);
                break;
            }
            case 109:
            {//-
                this.zoomWrapper(-1);
                break;
            }
            case 18:
            {//Alt
                break;
            }
            case 16:
            {//down
                //this.onFinishDraw();
                //this.mode = this.MODES.GROUP;
                //if (!this.curGroup) {
                //    this.curGroup = new SVGGroup(this);
                //}
                //this.toggleGroupingElements();
                break;
            }

        }
    }

    private onMouseDown(e) {
        e.preventDefault();
        this.lastSelectedShape = null;
        if (e.button == 2) {
            switch (this.mode) {
                case this.MODES.GROUP:
                case this.MODES.NO:
                {
                    if (this.lastHovered) {
                        this.glapp.app.cntxMenu = this.glapp.app.needsUpdateCntxMenu = true;
                        this.lastSelectedShape = this.lastHovered;
                        this.glapp.app._projControls.showControls(e);
                    }
                    break;
                }
                case this.MODES.EDIT:
                {
                    //this.onFinishDraw();
                    //this.drawPoly();
                    break;
                }
                /*case this.MODES.GROUP:
                 {
                 //this.toggleCreateGroup();
                 //this.toggleCreateGroup(true);
                 //alertify.warning('new group has been create, please select polygon for it');
                 break;
                 }*/
            }

        } else {
            //if (this.lastHovered && this.mode === this.MODES.NO && this.lastHovered && [this.shapes.POLYGON, this.shapes.GROUP].indexOf(this.lastHovered.type) > -1) {
            //    if (!this.glapp.app._projControls.showAttachPopUp(this.lastHovered)) {
            //        this.mode = this.MODES.ADD;
            //        this.currentShape = false;
            //    }
            //}
        }
    }

    private _onMouseDown(e) {
        let fabricJS = this.fabricJS;
        if (this.zoomValue !== 1) {
            if (fabricJS.startTranSlatePst)fabricJS.startTranSlatePst = {x: e.clientX, y: e.clientY};

        }
    }

    private _onMouseUp(e) {
        e.preventDefault();
        //this.lastSelectedShape = null;
        if (e.button == 2) {
            switch (this.mode) {
                case this.MODES.EDIT:
                {
                    this.onFinishDraw();
                    this.drawPoly();
                    break;
                }
            }

        } else {
        }
    }

    private _onMouseMove(e) {
        let fabricJS = this.fabricJS;
        if (fabricJS.startTranSlatePst) {
            //fabricJS.moveTranSlatePst = {x:fabricJS.startTranSlatePst.x- pos.x,y:fabricJS.startTranSlatePst.y- pos.y};
            fabricJS.moveTranSlatePst = {x: e.clientX, y: e.clientY};
            this.translateWorkArea();
            e.preventDefault();
            //console.log("on mouse move");
            return false;
        }
    }

    private drawPoly(points:Array<any> = [], pointSet:any = null, isMove:boolean = false) {
        let _points = points,
            _pointsSel = [],
            _curShape = this.currentShape;
        if (this.currentShape) this.currentShape._parent.remove(this.currentShape);
        if (pointSet && _curShape && _curShape._iseCreating) {
            _points = this.currentShape.get("points");
            _pointsSel = this.currentShape.get("_points");
            _points.push({
                x: pointSet.left,
                y: pointSet.top
            });
            if (_points.length == 1)_points.push({
                x: pointSet.left + 1,
                y: pointSet.top + 1
            });

            _pointsSel.forEach((e)=> {
                e._parent.remove(e);
            });
        }
        this.currentShape = new fabric.Polygon(_points, this.settings.POLYGON);
        if (_curShape)this.currentShape._isEditing = _curShape._isEditing;
        this.fabricJS._add(this.currentShape);
        if (pointSet) {
            let circle = new fabric.Circle(pointSet);
            circle.__parent = circle.parent = this.currentShape;
            circle._iter = _pointsSel.length;
            //if (_curShape._isEditing)circle._iter = _pointsSel.length - 1;
            _pointsSel.push(circle);
            this.createHtmlPoint(circle);
        }
        _pointsSel.forEach((e)=> {
            this.fabricJS._add(e, true);
        });
        this.currentShape.set("_points", _pointsSel);
        this.currentShape._iseCreating = true;
        this.fabricJS.renderAll();

    }

    private onFinishDraw() {
        let _curAShape = this.currentShape;
        if (_curAShape && _curAShape._iseCreating) {
            let points = _curAShape.get('points');
            points.pop();
            if (_curAShape.get("_points").length < 3) {
                _curAShape.get("_points").forEach((point)=> {
                    point._parent.remove(point);
                    this.removeHtmlPoint(point);
                });
                //if(_curAShape.parent)_curAShape.parent.remove(_curAShape);
                //_curAShape._parent.remove(_curAShape);
                _curAShape._parent._objects.pop();

            } else {

                _curAShape.set({
                    points: points
                });
                _curAShape._clone(true);
                if (this.mode !== this.MODES.ADD)_curAShape.get('_points').forEach((point:any)=> {
                    point.selectable = true;
                    point._parent.remove(point);
                });
                this.hideHtmlPoint(true);
                this.toSVG();
            }


        }
        if (_curAShape)_curAShape._isEditing = false;

        this.curFill = this.settings.POLYGON.fill = this.getRandomColor();
        //this.mode = this.MODES.NO;
        this.currentShape = null;
        this.fabricJS./*deactivateAll().*/renderAll();
    }

    resize(_w = null, _h = null, options:any = null) {
        if (!this.fabricJS)return;
        if (this.mode != this.MODES.ADD)this.updateMode(this.MODES.ADD);

        if (!_w && this.glapp.app._container)_w = this.glapp.app._container.clientWidth;
        if (!_h && this.glapp.app._container)_h = this.glapp.app._container.clientHeight;
        if (!this.fabricJS._set._w)this.fabricJS._set._w = _w;
        if (!this.fabricJS._set._h)this.fabricJS._set._h = _h;
        let _fabric = this.fabricJS,
            prevW = this.fabricJS._set._w,//this.fabricJS.width,
            prevH = this.fabricJS._set._h,//this.fabricJS.height,
            scaleMultiplierX = _w / prevW,
            scaleMultiplierY = _h / prevH,
            scaleX0 = scaleMultiplierX,
            scaleY0 = scaleMultiplierY,
            objects = this.fabricJS._objects,
            zoom = 1, pixelRatio = devicePixelRatio

        if (prevW == _w && _h == prevH)return;
        if (_w)_fabric.setWidth(_w);
        if (_h)_fabric.setHeight(_h);

        var c = _fabric.getElement(); // canvas = fabric.Canvas
        var w = c.width, h = c.height;

        //if (prevW < _w) {
        //    zoom = $(window).height() / background.height;
        //
        //    if ((zoom * background.width) > $(window).width()) {
        //        zoom = $(window).width() / background.width;
        //    }
        //
        //} else {
        //    zoom = $(window).width() / background.width;
        //
        //    if ((zoom * background.height) > $(window).height()) {
        //        zoom = $(window).height() / background.height;
        //    }
        //}
        //canvas.setZoom(zoom );

        if (prevW - _w > prevH - _h) {
            zoom = scaleMultiplierY;

            if ((zoom * prevW) > _w) {
                zoom = _w / prevW;
            }

        } else {
            zoom = scaleMultiplierX;

            if ((zoom * prevH) > _h) {
                zoom = _h / prevH;
            }
        }
        if (!isNaN(zoom)) {
            _fabric.setZoom(zoom);
        }
        this.updatePoints();
        //console.log(zoom, prevH, prevW);
        //if(this.fabricJS._objects[0])console.log(this.fabricJS._objects[0].points[0]);

        this.lastPixelRatio = pixelRatio;
    }

    private createHtmlPoint(point) {
        let _p:any = new SvgDragPointH(point, this.fabricJS._pointContainer);
        _p._index = this.allPoints.length;
        this.allPoints.push(_p);
        this.updatePoints(_p);
    }

    private removeHtmlPoint(circle:any = null) {
        if (circle) {
            let point = circle._htmlPoint;
            this.allPoints.splice(point._index, 1);
            this.allPoints.forEach((point:SvgDragPointH, key:number)=> {
                point._index = key;
            })
        } else {
            while (this.allPoints.length)this.allPoints.shift().remove();
        }

    }

    private hideHtmlPoint(hide) {
        this.allPoints.forEach((point:SvgDragPointH, key)=> {
            point.hide(hide);
        })
    }

    private updatePoints(_point:any = null) {
        let _z = this.fabricJS.getZoom(),
            _sSc = this.zoomValue;
        let _sc = this.fabricJS._set.scale,
            _delta = 1 / this.zoomValue;
        if (_point) {
            _point.update(_z, _delta);
        } else {
            this.allPoints.forEach((point:SvgDragPointH)=> {
                point.update(_z, _delta);
            })
        }

    }

    private   selectBar(bar) {
        if (this.fabricJS.selectedBar) {
            //remove the old topmost bar and put it back in the right zindex
            //PROBLEM: It doesn't go back; it stays at the same zindex
            //selectedBar.remove();
            this.fabricJS.moveTo(this.fabricJS.selectedBar, this.fabricJS._objects.length);
            this.fabricJS.selectedBar = null;
        }
        if (bar) {
            //put a placeholder object ("XXX" for now) in the position
            //where the bar was, and put the bar in the top position
            //so it shows topmost
            this.fabricJS.selectedBar = bar;
            this.fabricJS.bringToFront(this.fabricJS.selectedBar);
            //canvasS.moveTo(selectedBar, canvas.size()-1);
            //canvasS.add(bar);
            this.fabricJS.renderAll();
        }
    }

    private resizeElements(objects, scaleX0, scaleY0, scaleMultiplierX, scaleMultiplierY, withoutParent:boolean = false) {

        let _list = objects.sort((a, b)=> {
            return a.type > b.type
        });
        for (let i = 0, list = _list.concat([]); i < list.length; i++) {
            let cur = list[i];


            cur.scaleX0 = scaleX0;
            cur.scaleY0 = scaleY0;
            cur._hasUpdate = 0;
            cur.left = ((cur.left * scaleMultiplierX));
            cur.top = (cur.top * scaleMultiplierY);
            if (cur.type != this.shapes.CIRCLE && !this.isFinish) {
                cur.scaleX *= scaleMultiplierX;
                cur.scaleY *= scaleMultiplierY;
            }

            if (cur._tooltip)cur._tooltip.show(false);

            if (this.isFinish) {
                if (cur.type == this.shapes.POLYGON) {
                    cur._clone(false, false, false, true, withoutParent);
                    if (withoutParent)_list[i] = cur;
                }
                else if (cur.type == this.shapes.GROUP) {
                    this.resizeElements(cur._objects, scaleX0, scaleY0, scaleMultiplierX, scaleMultiplierY, withoutParent);
                }

            }

        }
    }

    private componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    private rgbToHex(arr) {
        return "#" + this.componentToHex(arr[0]) + this.componentToHex(arr[1]) + this.componentToHex(arr[2]);
    }

    private getBoundingBox(points) {

    }

    private parseSVG(svg, callback) {
        let
            _self = this, parser = new DOMParser(),
            doc = parser.parseFromString(svg, "image/svg+xml"),
            allElem = [],
            options = {},
            _svg:any = doc.childNodes[1],
            parseDom = (function parseDom(_el, parent = null) {
                for (let i = 0; i < _el.childNodes.length; i++) {
                    let e:any = _el.childNodes[i],
                        elem,
                        setAttr = function setAttr(elem) {
                            if (elem) {

                                if (elem === 2) {
                                    elem = {};
                                } else {
                                    if (parent) {
                                        parent._add(elem);
                                    } //else {
                                    //_self.fabricJS._add(elem);
                                    allElem.push(elem);
                                    //}
                                }

                                for (let attr in e.attributes) {
                                    let cntn = e.attributes[attr].textContent,
                                        _field = e.attributes[attr].localName;
                                    switch (_field) {
                                        case 'id':
                                        {
                                            elem[_field] = cntn;
                                            break;
                                        }
                                        case 'style':
                                        {
                                            cntn.split(";").forEach((e)=> {
                                                if (!e || !e.trim().length)return;
                                                let _el = e.split(":"),
                                                    _f = _el[0].trim();
                                                switch (_f) {
                                                    case 'opacity':
                                                    {
                                                        elem[_f] = parseFloat(_el[1]);
                                                        break;
                                                    }
                                                    case 'fill':
                                                    {
                                                        elem[_f] = _el[1].trim();
                                                        /*_self.rgbToHex(_el[1].split(",").map((e)=> {
                                                         return +e.replace(/[^0-9.]/g, "")
                                                         }));*/
                                                        break;
                                                    }
                                                }
                                            });
                                            break;
                                        }
                                        case 'transform':
                                        {
                                            let _f = ['left', "top", "scaleX", "scaleY"];
                                            cntn.split(" ").forEach((e:any, key)=> {
                                                if (!_f[key] || !e)return;
                                                elem[_f[key]] = /*(parent && key < 2 ? parent[_f[key]] : 0) +*/ parseFloat(e.replace(/[^-0-9.]/g, ""));
                                            });
                                            break;
                                        }
                                    }
                                }


                                if (!elem.id)elem.id = ENTITY.Config.randomstr();
                                elem.getBoundingBox();
                                return elem;


                            }
                        }

                    if (e.nodeName == _self.shapes.POLYGON) {
                        //let _elem = setAttr(2),
                        //    _pX = parent ? parent._left : 0,
                        //    _pY = parent ? parent._top : 0,
                        //    _cX = parent ? _elem._left : 0,
                        //    _cY = parent ? _elem._top : 0;

                        elem = new fabric.Polygon(e.attributes.points.textContent.split(" ").map((el)=> {
                            let _d = el.split(",");
                            if (_d.length < 2)return null;
                            return new fabric.Point(parseFloat(_d[0]), parseFloat(_d[1]));
                        }).filter((e)=> {
                            if (e)return e;
                        }), _self.settings.POLYGON);

                        setAttr(elem);

                    } else if (e.nodeName == 'g') {
                        elem = new fabric.Group();
                        if (e.childNodes.length) {

                            setAttr(elem);
                            parseDom(e, elem);
                        }
                    }

                }
            })(_svg);


        for (let attr in _svg.attributes) {
            let _fN = _svg.attributes[attr].localName;
            switch (_fN) {
                case 'height':
                case 'width':
                {
                    options[_fN] = parseFloat(_svg.attributes[attr].textContent);
                    break;
                }
                case 'viewBox':
                {
                    options[_fN] = (_svg.attributes[attr].textContent.split(" ").map((el)=> {
                        return parseFloat(el)
                    }));
                    break;
                }
            }
        }
        callback(allElem, options);

    }

    private intersectingCheck(activeObject, onSuccess) {
        activeObject.setCoords();
        if (typeof activeObject.refreshLast != 'boolean') {
            activeObject.refreshLast = true
        }

        //loop canvas objects
        activeObject.canvas.forEachObject(function (targ) {
            if (targ === activeObject) return; //bypass self

            //check intersections with every object in canvas
            if (activeObject.intersectsWithObject(targ)
                || activeObject.isContainedWithinObject(targ)
                || targ.isContainedWithinObject(activeObject)) {
                //objects are intersecting - deny saving last non-intersection position and break loop

                if (typeof activeObject.lastLeft == 'number') {
                    activeObject.left = activeObject.lastLeft;
                    activeObject.top = activeObject.lastTop;
                    activeObject.refreshLast = false;
                    return;
                }
            }
            else {
                activeObject.refreshLast = true;
            }
        });

        if (activeObject.refreshLast) {
            //save last non-intersecting position if possible
            activeObject.lastLeft = activeObject.left;
            activeObject.lastTop = activeObject.top;
        }
        if (onSuccess)onSuccess();

    }

    zoomWrapper(zoomIn:number = 0, speed:number = 0.5) {
        switch (zoomIn) {
            case 1:
            {
                this.zoomValue += speed;
                break;
            }
            case -1:
            {
                if (this.zoomValue > 1)this.zoomValue -= speed;
                break;
            }
            default:
            {
                this.zoomValue = 1;
            }
        }
        this.fabricJS.wrapperEl.style.transform = 'scale(' + this.zoomValue + ')';
        if (this.glapp.app._slider.container.style.transform.match('scale')) {
            this.glapp.app._slider.container.style.transform = this.glapp.app._slider.container.style.transform.replace(/scale\(\d.*\)/, 'scale(' + this.zoomValue + ')');
        } else {
            this.glapp.app._slider.container.style.transform = 'translate(-50%, -50%) scale(' + this.zoomValue + ')';
        }
        if (this.zoomValue <= 1) {
            this.fabricJS.wrapperEl.style.transformOrigin = this.glapp.app._slider.container.style.transformOrigin = '';
        }

        let _delta = (this.fabricJS._set.scale || 1) * 1 / this.zoomValue;
        this.lastSize = {x: _delta, y: _delta};

        this.resize()

        //this.settings.CIRCLE.radius = this.settings.CIRCLE.defRadius * _delta ;
        //this.settings.CIRCLE.strokeWidth = this.settings.CIRCLE.defStroke * _delta ;
        /*
         for (let i = 0, arrgs = this.fabricJS.getObjects().traverseInline(); i < arrgs.length; i++) {
         let els = arrgs[i];
         if (els.type === this.shapes.POLYGON) {
         let _poins = [];
         for (let u = 0, _points = (els.get('_points') || []).concat([]); u < _points.length; u++) {
         let els = _points[u],
         pointSet = {left: els.left, top: els.top};
         Object.assign(pointSet, this.settings.CIRCLE);

         els._parent.remove(els);
         let circle = new fabric.Circle(pointSet);
         circle._iter = els._iter;
         circle.selectable = els.selectable;
         circle.parent = circle.__parent = els.parent;
         //circle.scaleX =circle.scaleY = 0.5;
         els._parent._add(circle);
         _poins.push(circle);
         }
         els.set('_points', _poins);
         }
         }*/
        this.updateCircleScale();
    }

    private updateCircleScale() {
        let _sc = this.fabricJS._set.scale,
            _delta = _sc + 1 / this.zoomValue;

        if (isNaN(_delta) || !_delta)return;
        //this.settings.CIRCLE.radius = this.settings.CIRCLE.defRadius * _delta;
        this.settings.CIRCLE.scaleX = this.settings.CIRCLE.scaleY = _delta;
        for (let i = 0, arrgs = this.fabricJS.getObjects().traverseInline(); i < arrgs.length; i++) {
            let els = arrgs[i];
            if (els.type === this.shapes.POLYGON) {
                //let _poins = [];
                for (let u = 0, _points = (els.get('_points') || []); u < _points.length; u++) {
                    let els = _points[u];
                    els.scaleX = els.scaleY = _delta;
                }
                //els.set('_points', _poins);
            }
        }
        //console.log(_delta);
        this.fabricJS.renderAll();
    }

    exportJSON(e) {
        e.preventDefault();
        let
            _self = this,
            fileName = 'export',
            itr = 0,
            pathesStore = [],
            scale = 100,
            xR = scale / _self.fabricJS.width,
            yR = scale / _self.fabricJS.height,
            round = 2;


        (function parse(elmnts) {
            elmnts.forEach((e)=> {
                if (e.type == _self.shapes.POLYGON) {
                    pathesStore.push({id: "a" + itr++, path: e.points});
                } else if (e._objects) {
                    parse(e._objects);
                }
            });
        })(this.fabricJS._objects);


        pathesStore.forEach(function (o:any, i) {
            if (!o.path.length)return;
            let __s = '';
            o.path.reduce((p, c, n)=> {
                let s = p;
                /*if (p instanceof fabric.Point)*/
                s = "M " + (c.x * xR).toFixed(round) + "," + (c.y * yR).toFixed(round);
                __s += s + " L " + (c.x * xR).toFixed(round) + "," + (c.y * yR).toFixed(round);
                return p
            });
            o.path = __s + " Z"
        });
        this.saveFile(JSON.stringify(pathesStore), 'text/json', fileName, '.json')
    }

    exportSVG(e) {
        e.preventDefault();
        this.saveFile(new Blob([this.fabricJS.toSVG(['name'])], {type: 'text/*'}), 'text/*', 'oxi', '.svg')
    }

    private saveFile(data, type, fileName, fileType) {
        let file = new File([data], fileName, {type: type}),
            a = document.createElement('a') as any;
        a.setAttribute('href', URL.createObjectURL(file));
        a.download = fileName + fileType;
        a.innerHTML = fileName;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    private toggleCreateGroup(show:boolean = false,next:Function=null) {
        if (show) {
            if (!this.curGroup) {
                this.curGroup = new SVGGroup(this);
            }
        } else {
            if (this.curGroup) {
                if(this.curGroup.tempGroup &&!this.curGroup.tempGroup._data && this.curGroup.tempGroup._objects.length ){
                    return alertify.confirm("There are any data action, if OK group wil be destroyed", (e)=> {
                        this.curGroup.destroy();
                        this.curGroup = null;
                        this.toggleGroupingElements(!show);

                    }, (e)=> {
                        this.updateMode(this.MODES.GROUP);
                    }).set({padding: true, maximizable: true, resizable: true});
                }else{
                    this.toSVG();
                    this.curGroup = null;
                }
                //this.curGroup.clone();


            }
        }
        this.toggleGroupingElements(!show);

    }

    updateMode(mode:number) {
        let prevMode = this.mode;
        if (prevMode === mode)return;
        this.mode = mode;
        switch (prevMode) {
            case this.MODES.ADD:
            {
                this.toggleElements(false, this.shapes.CIRCLE);
                break;
            }
            case this.MODES.EDIT:
            {
                this.onFinishDraw();
                break;
            }
            case this.MODES.DRAG:
            {
                this.toggleElements(false);
                break;
            }
            case this.MODES.GROUP:
            {
                this.toggleCreateGroup();
                break;
            }
            case this.MODES.CONTINUE_CREATE:
            {
                this.toggleCreateGroup(false);
                //this.toggleCreateGroup()
                break;
            }
        }
        this.mode = mode;

        switch (mode) {
            case this.MODES.ADD:
            {
                this.toggleElements(true, this.shapes.CIRCLE);
                break;
            }
            case this.MODES.EDIT:
            {
                this.drawPoly();
                break;
            }
            case this.MODES.DRAG:
            {
                this.toggleElements(true);
                break;
            }
            case this.MODES.GROUP:
            {
                this.toggleCreateGroup(true);
                break;
            }
            case this.MODES.CONTINUE_CREATE:
            {
                this.toggleCreateGroup(true);
                alertify.warning('Select poly for continue to add points');
                break;
            }
        }

        this.updatePoints();

    }


}

class SVGGroup {
    group:Array<any>;
    oldGroup:Array<any>;
    editPoints:Array<any>;
    private id:any;
    private canvas:SVGView;
    private color:any;
      tempGroup:any;

    constructor(canvas:SVGView, id = ENTITY.Config.randomstr()) {
        this.group = [];//new fabric.Group({selectable: false});
        this.editPoints = [];
        this.canvas = canvas;
        this.id = id;
        // this.color = this.canvas.getRandomColor()
    }

    getScreenPst() {
        return {x: 0, y: 0};
    }

    add(element, type = null) {
        console.log();
        if (element && element.type == this.canvas.shapes.POLYGON) {
            if (element.isGroup) {

                element._parent.remove(element);
                for (let i = 0; i < this.group.length; i++) {
                    if (this.group[i].id == element.id) {
                        this.group.splice(i, 1);
                        break;
                    }

                }
                this.canvas.fabricJS._add(element.tempOrigin);
                return;
            }
            if (!this.color)this.color = element.fill;
            if(!element._startColor) element._startColor = element.fill;
            element.fill = element.defFill = this.color;
            let shouldAdd = false;
            [element].concat(element.get('_points')).forEach((elem:any)=> {
                if (!elem)return;

                elem._parent.remove(elem);
                let _c = elem.type == this.canvas.shapes.POLYGON ? elem._clone(true, true, false, true, true) : elem;
                /*if (elem._dataSource)elem._dataSource.active = false;
                 if (elem._tooltip)elem._tooltip.remove();
                 if (elem._data) {
                 for (let i = 0, _el = this.canvas.selected.areas; i < _el.length; i++) {
                 if (_el[i]._id == elem._data._id) {
                 _el.splice(i, 1);
                 break;
                 }
                 }
                 }
                 elem._data = elem._dataSource = elem._tooltip = null;*/


                for (let i = 0; i < this.group.length; i++) {
                    if (this.group[i].id == _c.id) {
                        shouldAdd = true;
                        this.group.splice(i, 1, _c);
                        break;
                    }

                }
                if (!shouldAdd) {
                    if (elem.type == element.type) {
                        this.group.push(_c);
                        //_c._tempGroup = this.group;
                    } else {
                        this.editPoints.push(_c);
                    }
                }
            });


            if (type == 1) {
                if (this.tempGroup) {
                    this.clone(this.tempGroup, true);
                } else {
                    this.make();
                }
            } else {
                //this.canvas.fabricJS.renderAll();
            }


        }

    }

    childs() {
        return this.group;
    }

    clone(from = this.tempGroup, hard = false) {
        //this.group=[];
        //this.editPoints=[];
        if (from._parent) {
            from._parent.remove(from);
        } else {
            this.canvas.fabricJS.remove(from);
        }
        from._objects.forEach((el:any)=> {
            this.add(hard ? el.tempOrigin : el);
        });
        return this.make();
    }

    destroy(){
        if(this.tempGroup){
            if (this.tempGroup._parent) {
                this.tempGroup._parent.remove(this.tempGroup);
            } else {
                this.canvas.fabricJS.remove(this.tempGroup);
            }
            this.tempGroup._objects.forEach((el:any)=> {
                let _parPoly = el.tempOrigin||el;
                this.canvas.fabricJS._add(_parPoly);
                if(_parPoly){
                    let _pn = _parPoly.get('_points');
                    for (var i = 0; i < _pn.length; i++) {
                        _pn[i].parent =  _parPoly;
                    }
                }
            });
        }
    }
    make() {
        if (!this.group.length)return;


        let group = new fabric.Group(this.childs(), this.canvas.settings.GROUP);
        group.set('id', this.id);
        this.group.forEach((e:any, index:number)=> {
            //
            e._parent = group;
            e.isGroup = true;

            e.test = index + '----';
            e.on('mousedown', (ev)=> {
                //let e = ev.target;

                if(this.canvas.MODES.GROUP==this.canvas.mode &&e._parent.type=='group'){
                    e._parent.remove(e);
                    if(this.tempGroup.id !== e.id) {
                        if(e.tempOrigin.defFill === e.tempOrigin._startColor)
                            e.tempOrigin._startColor = this.canvas.getRandomColor();
                        e.tempOrigin.fill = e.tempOrigin.defFill = e.tempOrigin._startColor;
                    }

                    console.log(e.test);
                    //for (let i = 0; i < this.group.length; i++) {
                    //    if(this.group[i].id==e.id){
                    //        this.group.splice(parseInt(e.test),1);
                    //        break;
                    //    }
                    //
                    //}
                    //for (let i = 0; i < this.editPoints.length; i++) {
                    //    if (this.editPoints[i].__parent.id == e.id) {
                    //        this.editPoints.splice(i--, 1);
                    //    }
                    //
                    //}
                    this.editPoints=[];
                    this.group = [];
                    this.clone(this.tempGroup,true);
                    setTimeout(()=>{
                        let _parPoly = e.tempOrigin||e;
                        this.canvas.fabricJS._add(_parPoly);
                        if(_parPoly){
                            let _pn = _parPoly.get('_points');
                            for (var i = 0; i < _pn.length; i++) {
                                _pn[i].parent =  _parPoly;
                            }
                        }
                    },500)

                }

            })
        });
        //this.canvas.fabricJS._add(group);
        this.canvas.fabricJS.sendToBack(group);
        this.editPoints.forEach((el)=> {
            //el._parent.remove(el);
            el._parent._add(el);
        });
        this.canvas.fabricJS.renderAll();

        //if(this.tempGroup){
        //    this.tempGroup._parent.remove(this.tempGroup);
        //    this.tempGroup = null;
        //}
        if(this.tempGroup){
            group._data = this.tempGroup._data;
            group.id = this.tempGroup.id;
        }
        if(!group._data){
            for (let i = 0; i < this.group.length; i++) {
                if(this.group[i]._data){
                    group._data = this.group[i]._data;
                    group.id = this.group[i].id;
                    break;
                }

            }
        }
        this.tempGroup = group;
        //console.log(group);
        return group;
    }
}


class SvgDragPointH {
    private html:any;
    _index:number;

    constructor(private parent, private container) {
        this.html = document.createElement('div');
        this.html.className = 'drag-point';
        container.appendChild(this.html);
        parent._htmlPoint = this;

    }

    update(zoom:number = 1, zoomScale:number = 1) {
        let dimen = 'px',
            _center = this.parent.getCenterPoint();
        this.html.style.left = Math.round(zoom * _center.x) + dimen;
        this.html.style.top = Math.round(zoom * _center.y) + dimen;
        this.html.style.transform = 'scale(' + zoomScale * 0.74 + ') translate(-50%,-50%)'
    }

    remove() {
        this.html.parentNode.removeChild(this.html);
    }

    hide(hide:boolean = false) {
        this.html.style.display = hide ? 'none' : '';
    }
}
export {SVGDragPoint} from './points';
