import {Input, ViewChild, Component, OnInit, ViewContainerRef} from '@angular/core';
import * as ENTITY from '../../../entities/entities';
import {AbstractChangesView} from '../abstract.changes.view';
import {AuthService, ProjectService} from '../../../services/services';
import 'rxjs/add/operator/map';

declare var alertify: any;

@Component({
  selector: 'app-project-webgl-tooltip',
  templateUrl: './tooltip.html',
  styleUrls: ['./tooltip.sass']
})
export class WTooltip extends AbstractChangesView implements OnInit {
  @ViewChild('scripts')
  scripts;
  HTMLElement;
  @Input() isEdit: any;
  dataSource: any = [];
  private dataSourceLoaded: boolean = false;
  private parser: any;
  private dsParser: any;
  private html: any;
  dataElem: any;
  indexEl: number = 0;

  constructor(private authService: AuthService, public vc: ViewContainerRef, private projectService: ProjectService) {
    super();
    this.dataSource = {
      properties: [{
        info_box_header: 'Default Header',
        info_box_title: 'Only visible for debug mode'
      }]
    };
    this.dataElem = [];
  }

  ngOnInit() {
    let onfinish = () => {
      setTimeout(() => {
        super.ngOnInit();
        if (this.htmlTemplate) {
          this.initParser();
          this.updateHTMLInput();
        } else {
          this.callbacks.push(() => {
            this.initParser();
            this.updateHTMLInput();
          });
        }
        this.dataSourceLoaded = true;
      });
    }
    if (!this.modelData || !this.modelData._id) {
      onfinish();
      return console.warn('Data source is not defined');
    }
    this.authService.post('public/model/remote', {id: this.modelData._id}, {
      hasAuthHeader: false,
      isCross: true
    }).subscribe((res: any) => {
      res = res.json();
      if (res.status) {
        try {
          this.dataSource = JSON.parse(res.data);
        } catch (e) {
          console.warn(e);
        } finally {
          onfinish();
        }
      } else {
        onfinish();
      }

    }, (res) => {
      onfinish();
      console.warn('Incorrect dataSource: ' + res && res.message ? res.message : res);
    });
  }

  private onEventPrevent(event) {
    event.preventDefault();
    return false;
  }

  private initParser(JSvalue: any = null, JSDSvalue: any = null) {
    let val = JSvalue;
    let valDS = JSDSvalue;
    try {
      if (!JSvalue) {
        val = this.jsTemplate;
      }
      if (!JSDSvalue) {
        valDS = this.dsTemplate;
      }

      if (!val && this.tempLoad) {
        val = this.tempLoad.jsTemplate;
      }
      if (!valDS && this.tempLoad) {
        valDS = this.tempLoad.dsTemplate;
      }

      this.parser = this.authService.safeJS(val);
      this.dsParser = this.authService.safeJS(valDS);

      let _datastructure = this.dsParser instanceof Function ? this.dsParser() : [];
      let _data = this.parser instanceof Function ? this.parser({dataSource: this.dataSource, dataStructure: _datastructure}) : [];
      _data.forEach((el: any, i) => {
        if (!el.tooltip) {
          el.tooltip = {};
        }
        if (this.isEdit && i == 0) {
          el.tooltip.active = el.active = true;
          el._left = 160 + 'px';
          el._top = 220 + 'px';
        }
        el.onclick = () => {
        };
      });

      this.dataElem = _data;

    } catch (e) {
      console.warn('Incorrect dataSource: ' + e)
    }
  }


  updateHTMLInput(value: any = null) {
    super.updateHTMLInput(value, this.dataElem && this.dataElem.length ? this.dataElem[this.indexEl] : {});
  }

  updateJSInput(JSvalue: any = null, JSDSvalue: any = null) {
    if (JSvalue && JSDSvalue) {
      this.initParser(JSvalue, JSDSvalue);
    }
  }
}
