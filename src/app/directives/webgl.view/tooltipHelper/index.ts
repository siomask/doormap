import {ViewChild,Component,OnInit} from '@angular/core';

declare var alertify:any;
declare var CodeMirror:any;

@Component({
    selector: 'app-tooltip-help',
    templateUrl: './index.html',
    styleUrls: ['./index.sass']
})
export class TooltipHelper {
    left:any = -Infinity;
    top:any = -Infinity;
    text:any = '';
    show:boolean = false;
    lasHovered:any;
    reset(){
        this.show = false;
        if (this.lasHovered)this.lasHovered._isHovered = false;
    }
}
