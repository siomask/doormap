import {Input, ViewChild, Component, OnInit} from '@angular/core';
import * as ENTITY from '../../entities/entities';

declare var Mustache: any;

@Component({
  selector: 'app-project-webgl-changes-view',
})
export class AbstractChangesView implements OnInit {

  @ViewChild('tempLoad')
  tempLoad: any;
  @Input() data: any;
  @Input() htmlTemplate: any;
  @Input() jsTemplate: any;
  @Input() dsTemplate: any;
  @Input() modelData: any;
  @Input() htmlUrl: any;
  @Input() cssUrl: any;
  @Input() parent: any;

  private DIR: any;
  private ProjClasses: any;
  protected callbacks: Array<Function>;
  protected htmlOutPut: any;
  protected SELF: any;

  constructor() {
    this.DIR = ENTITY.Config.FILE.DIR;
    this.ProjClasses = ENTITY.ProjClasses;
    this.callbacks = [];
  }


  ngOnInit() {
    this.SELF = this;
    if (this.data && this.data[1] && this.data[1].value) {
      this.htmlTemplate = this.data[1].value;
      if (this.data[2]) {
        this.jsTemplate = this.data[2].value;
      }
      if (this.data[3]) {
        this.dsTemplate = this.data[3].value;
      }
    } else {
      this.callbacks.push(() => this.updateHTMLInput());
      if (this.tempLoad) {
        this.tempLoad.project = this['projectService'].getProject();
        this.tempLoad.callbacks.push(() => {
          //this.cssUrl =  this.tempLoad.cssUrl;
          this.htmlTemplate = this.tempLoad.htmlTemplate;
          this.jsTemplate = this.tempLoad.jsTemplate;
          this.dsTemplate = this.tempLoad.dsTemplate;
          this.update();
        });
      } else {
        this.update();
      }
    }
    if (this.parent && this.parent.onLoadTemplate) {
      setTimeout(() => this.parent.onLoadTemplate(this.constructor.name.toLowerCase()), 100);
    }
  }

  update() {
    for (let i = 0; i < this.callbacks.length; i++) {
      this.callbacks[i]();
    }
  }

  updateHTMLInput(value: any = null, obj: any = {}) {
    if (!value) {
      value = this.htmlTemplate;
    }
    if (value && typeof value == 'string') {
      try {
        this.htmlOutPut = Mustache.render(value, obj);
      } catch (e) {
        console.warn('Some bad input:', e);
      }
    }
  }

  updateJSInput() {

  }

}

