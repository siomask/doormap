import {Input, ViewChild, Component, OnInit, OnChanges, EventEmitter, Injectable, HostListener} from '@angular/core';
import * as ENTITY from '../../entities/entities';
import {AuthService} from '../../services/services';
import {Location} from '@angular/common';
import {Confirm, Prompt, Dialog} from '../dialogs/dialog';
import {WTooltip} from './tooltip/tooltip';
import {WControls} from './controls/controls';
import {Preloader} from './preloader/preloader';
import {SVGView} from './svg.draw/svg.draw';
import {PopUpAction} from './action.popup/index';
import {TooltipHelper} from './tooltipHelper/index';

declare var alertify: any;
declare var THREE: any;
declare var Pace: any;
declare var TWEEN: any;

@Injectable()
export class WebGLService {
  navchange: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  emit(data) {
    this.navchange.emit(data);
  }

  subscribe(component, callback) {
    // set 'this' to component when callback is called
    return this.navchange.subscribe(data => {
      callback.call(component, data);
    });
  }
}

@Component({
  selector: 'app-project-webgl-view',
  templateUrl: './webgl.view.html',
  styleUrls: ['./webgl.view.sass']
})
export class WebglView implements OnInit, OnChanges {

  preview: string;
  _self: WebglView;
  @ViewChild('renderParent')
  renderParent: HTMLElement;
  @ViewChild('preloader')
  preloader: Preloader;
  @ViewChild('preToolTip')
  preToolTip: WTooltip;
  @ViewChild('preControls')
  preControls: WControls;
  @ViewChild('projCnt')
  projCnt: any;
  @ViewChild('svgEl')
  svgEl: SVGView;
  @ViewChild('actionPopUp')
  actionPopUp: PopUpAction;
  @ViewChild('tooltipHelp')
  tooltipHelp: TooltipHelper;
  @Input() selected: any;

  app: OxiAPP;
  location: Location;
  authServ: AuthService;
  calcViews: boolean = false;
  private _id: number = Date.now();
  private inited: boolean = false;

  constructor(location: Location, authServ: AuthService) {
    this.location = location;
    this.authServ = authServ;
    this._self = this;
  }

  ngOnChanges(changes) {
    if (changes.selected.currentValue.created != changes.selected.previousValue.created) {
      this.initWebgl();
    }
    if (this.selected) {
      this.selected.glApp = this.app;
    }

  }

  ngOnInit() {
    this.initWebgl();
    if (this.selected) {
      this.selected.glApp = this.app;
    }
  }

  initWebgl() {

    if (this.selected.images.length) {
      this.preview = ENTITY.Config.PROJ_LOC + this.selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER + ENTITY.Config.FILE.DIR.PROJECT_PREVIEW + this.selected.images[this.selected.currentItem || 0];
    }/* else if (this.selected.preview) {
         this.preview = this.selected.preview;
         }*/ else {
      this.preview = null;
    }
    if (!this.inited) {
      return this.inited = true;
    }
    this.app = new OxiAPP(this);
  }

  ngOnDestroy() {
    //console.log('webgl context ' + this._id + " was clear");
    this.selected._selected = false;
    if (this.app._animation) {
      this.app._animation.stop();
    }
    if (this.app._events) {
      this.app._events.onDestroy();
    }


  }

  @HostListener('mousemove') onMouseMove() {
    this.tooltipHelp.reset();
  }

}

class OxiAPP {
  cntxMenu: any = false;
  needsUpdateCntxMenu: boolean = false;
  isMobile: boolean = false;
  imgType: string = '';
  lights: any;
  scene: any;
  model: any;
  camera: any;
  gl: any;
  _angle: number = 10;
  screen: any = {};
  main: WebglView;
  loader: any;
  _projControls: OxiControls;
  _slider: OxiSlider;
  _events: OxiEvents;
  _animation: OxiAnimation;
  controls: any;
  _files: any = {};
  _fileReader: FileReader;
  _container: any;
  _preloaderStatus: any;
  allLoad: boolean = false;
  private curLoadedTemplates: number = 0;
  private templates: Array<any>;
  private shader: any;
  private helper: any;
  private colorsS: any = ['0_0_0'];
  private timeRender: any;
  private isFinishTweenAnimations: boolean = true;
  infoHTML: Array<OxiToolTip> = [];
  TEMPLATES: any = {
    TOOLTIP: 'app-project-webgl-tooltip',
    CONTROLS: 'app-project-webgl-controls',
    PRELOADER: 'app-project-preloader'
  };
  HOVER_COLOR: any = {
    ACTIVE: new THREE.Color(0.1, 1.0, 0.1),
    SOLGHT: new THREE.Color(1.0, 0.0, 0.1),
  };
  effectFXAA: any;
  selectedIframe: any;
  meshes: Array<any> = [];
  composer: any;
  outlinePass: any;

  constructor(main: WebglView) {
    this.main = main;
    this.isMobile = this.deviceCheck();
    //this.colorsS = new THREE.Color(ENTITY.Config.randomArbitary(0, 0.4), ENTITY.Config.randomArbitary(0.1, 0.5), ENTITY.Config.randomArbitary(0.1, 0.8));
    this.shader = {
      'outline': {
        vertex_shader: [
          'uniform float opacity;',
          'varying float _opacity;',
          'void main() {',
          'vec4 pos = modelViewMatrix * vec4( position + normal * 25.0, 1.0 );',
          '_opacity = opacity;',
          'gl_Position = projectionMatrix * pos;',
          '}'
        ].join('\n'),

        fragment_shader: [
          'varying float _opacity;',
          'void main(){',
          'gl_FragColor = vec4( 1.0, 1.0, 0.0, _opacity );',
          '}'
        ].join('\n')
      }
    };
    this.scene = new THREE.Scene();
    this.model = new THREE.Object3D();
    this.model.inters = [];
    this.scene.add(this.model);
    let renderer = this.gl = new THREE.WebGLRenderer({
        preserveDrawingBuffer: true,
        antialias: true,
        alpha: true,

        //clearAlpha: true,
        //sortObjects: false
      }),
      SCREEN_WIDTH = this.screen.width = 720,
      SCREEN_HEIGHT = this.screen.height = 405,
      _self = this;
    renderer.domElement.className = 'gl-view';

    if (main.selected.canEdit) {
      main.projCnt.nativeElement.style.height = main.projCnt.nativeElement.clientWidth * (SCREEN_HEIGHT / SCREEN_WIDTH) + 'px';
    }
    this._preloaderStatus = document.querySelector('.preloader-data.preloader-status') || {style: {}};
    renderer.setClearColor(0x000000, 0);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.camera = new THREE.PerspectiveCamera(70, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 200000);
    this.controls = main.selected.canEdit ? new THREE.OrbitControls(this.camera, renderer.domElement) : {
      update: () => {
      }, target: this.scene.position.clone()
    };
    this.controls.keys = {};
    this.controls.enabled = !!this.main.selected.canEdit;
    if (this.controls.enabled) {
      this.controls.addEventListener('change', () => {
        main.selected.hasRecalcChanges = main.selected.hasChanges = main.selected._project.hasChanges = true;
        if (main.selected.camera.frameState[main.selected.currentItem]) {
          main.selected.camera.frameState[main.selected.currentItem].hasChanges = true;
        }
        this.camera.updateProjectionMatrix();
        this.dataSave();
        this.controls.targetObj.position.copy(this.controls.target);
        this._animation.play();
      });
    }
    /*-----------set config data----------*/
    this.camera.positionDef = new THREE.Vector3(10, 10, 10);
    if (main.selected.camera) {
      if (main.selected.camera.rotation) {
        Object.assign(this.camera.rotation, main.selected.camera.rotation);
      }
      if (main.selected.camera.position) {
        this.camera.positionDef.copy(main.selected.camera.position);
      }
      if (main.selected.camera.fov) {
        this.camera.fov = main.selected.camera.fov;
      }
      if (main.selected.camera.scale) {
        this.model.scale.multiplyScalar(main.selected.camera.scale);
      }
      if (main.selected.camera.zoom) {
        this.camera.zoom = main.selected.camera.zoom;
      }
      if (main.selected.camera.target) {
        this.controls.target.copy(main.selected.camera.target);
      }
    }
    if (main.selected.controls) {
      Object.assign(this.controls, main.selected.controls);
    }
    this.camera.position.copy(this.camera.positionDef);
    this.camera.updateProjectionMatrix();

    let curDist = this.camera.positionDef.distanceTo(this.controls.target),
      curAngle = Math.acos((this.camera.positionDef.x - this.controls.target.x) / curDist);
    this.camera.updateView = (angle) => {
      let _cm = main.selected.camera,
        _p = _cm.frameState[main.selected.currentItem];
      if (!angle) {
        angle = main.selected.currentItem;
      }
      if (_p) {
        this.camera.position.set(_p.x, _p.y, _p.z);
        if (_p.target) {
          this.controls.target.set(_p.target.x, _p.target.y, _p.target.z);
        }
      } else {
        let quaternion = new THREE.Quaternion();
        quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), (angle * this._angle) * Math.PI / 180);
        this.camera.position.copy(this.camera.positionDef.clone().applyQuaternion(quaternion));
        if (_cm.target) {
          this.controls.target.set(_cm.target.x, _cm.target.y, _cm.target.z);
        }
      }
      this.updateRenderOrder();
      this.camera.lookAt(this.controls.target);
      this.camera.updateProjectionMatrix();
      this._animation.play();

    };


    this.helper = new THREE.Object3D();

    /*--lights----*/
    this.lights = new THREE.Object3D();
    //this.scene.add(this.lights);
    let a = new THREE.AmbientLight('#e1e1e1');
    this.lights.add(a);

    let d = new THREE.DirectionalLight('#e1e1e1', 1.0);
    d.position.set(0, 20, 30).normalize();
    this.lights.add(d);


    if (this.main.selected.canEdit) {
      this.helper.visible = false;
      //this.helper.add(new THREE.AxesHelper(600));
      this.controls.targetObj = new THREE.AxisHelper(200);
      this.helper.add(this.controls.targetObj);
      this.scene.add(this.helper);
    }


    //let light = new THREE.DirectionalLight(0xffffff);
    //light.position.set(1, 1, 1);
    //this.scene.add(light);

    // postprocessing
    //this.gl.setClearAlpha( 0.0 );
    //this.composer = new THREE.EffectComposer( renderer );
    //let renderPass =   new THREE.RenderPass( this.scene, this.camera );
    //this.composer.addPass( renderPass );
    //let outlinePass = this.outlinePass = new THREE.OutlinePass( new THREE.Vector2(window.innerWidth, window.innerHeight), this.scene, this.camera);
    //this.composer.addPass( outlinePass );
    //outlinePass.hiddenEdgeColor = outlinePass.visibleEdgeColor = new THREE.Color(1.0,1.0,0.0);
    ////outlinePass.hiddenEdgeColor = new THREE.Color(0.117,0.117,0.117);
    //outlinePass.edgeStrength =outlinePass.edgeGlow = 0;
    //this.effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
    //this.effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight );
    //this.effectFXAA.renderToScreen = true;
    //this.composer.addPass(  this.effectFXAA );
    THREE.Mesh.prototype.getScreenPst = function () {
      let mesh: any = this,
        m = _self.gl.domElement,
        offset = _self._offset(),
        width = m.clientWidth, height = m.clientHeight,
        widthHalf = width / 2, heightHalf = height / 2,
        position: any = new THREE.Vector3();

      mesh.updateMatrixWorld();
      mesh.updateMatrix();
      mesh.geometry.computeBoundingBox();
      mesh.geometry.computeBoundingSphere();

      let boundingBox = mesh.geometry.boundingBox;
      position.subVectors(boundingBox.max, boundingBox.min);
      position.multiplyScalar(0.5);
      position.add(boundingBox.min);

      position.project(_self.camera);
      position.x = (position.x * widthHalf) + widthHalf + offset.left;
      position.y = -(position.y * heightHalf) + heightHalf + offset.top;
      mesh.onscreenParams = position;
      mesh.onscreenOffset = offset;
    };
    this.templates = [main.preToolTip, main.preControls, main.preloader];
    this.templates.forEach((el) => {
      el.callbacks.push(() => {
        this.onFinishLoadTemplates()
      });
    });
  }

  updateRenderOrder() {
    this.model.inters.forEach((child: any) => {
      child._distToCamera = child._helper.geometry.boundingSphere.center.distanceTo(this.camera.position);
    })
    this.model.inters.sort((a, b) => {
      return a._distToCamera < b._distToCamera;
    })
    this.model.inters.forEach((child: any, key: number) => {
      child.renderOrder = child.defRenderOrder = this.model.inters.length - key;
    });
  }

  private onFinishLoadTemplates() {
    let main = this.main;
    if (++this.curLoadedTemplates == this.templates.length) {
      setTimeout(() => {
        this.loadModel(() => {
          this.checkLoadedImg(() => {
            let parentCanvas = this._container = main.projCnt['nativeElement'],

              onFinish = () => {
                let onEnd = () => {
                  this._events.onWindowResize();
                  this._animation.play();
                  this.allLoad = true;
                  this.camera.updateView();
                  let _preloader = document.querySelector(this.TEMPLATES.PRELOADER);
                  if (_preloader) {
                    _preloader.parentNode.removeChild(_preloader);
                  }
                  //console.log(this.main.selected);
                };
                if (this._slider.isLoaded) {
                  onEnd();
                } else {
                  this._slider.onFinish = () => {
                    onEnd();
                  }
                }

              };
            if (main.preloader.prevImg) {
              main.preloader.prevImg['nativeElement'].className += ' active';
            } else {
              main.preloader.preloader['nativeElement'].className += ' active';
            }
            parentCanvas.appendChild(this.gl.domElement);
            this._projControls = new OxiControls(this);
            this._slider = new OxiSlider(this);
            this._events = new OxiEvents(this);
            this._animation = new OxiAnimation(this);

            let _inter = setTimeout(() => {
              Pace.stop();
              onFinish();
            }, 2000);
            Pace.once('done', (e) => {
              clearTimeout(_inter);
              onFinish();
            });
          });
        });
      }, 100);
    }
  }

  private loadTemplates() {

  }

  private checkLoadedImg(callback) {
    let _self = this,
      allows = ['1', '2'],
      checkLow = () => {
        if (this.isMobile) {
          this.imgType = ENTITY.Config.FILE.DIR.PREVIEW.LOW;
        }
        callback()
      };

    if (this.main.selected.images.length) {
      let isAllowed: any = window.localStorage.getItem(ENTITY.Config.FILE.DIR.PREVIEW.WEBP);
      if (isAllowed) {
        if (isAllowed == allows[0]) {
          _self.imgType = ENTITY.Config.FILE.DIR.PREVIEW.WEBP;
          callback();
        } else {
          checkLow();
        }
      } else {
        let img = new Image();
        img.onload = function () {
          let isAllow = !!(img.height > 0 && img.width > 0);
          window.localStorage.setItem(ENTITY.Config.FILE.DIR.PREVIEW.WEBP, isAllow ? allows[0] : allows[1]);
          if (isAllow) {
            _self.imgType = ENTITY.Config.FILE.DIR.PREVIEW.WEBP;
            callback();
          } else {
            checkLow();
          }

        };
        img.onerror = function () {
          window.localStorage.setItem(ENTITY.Config.FILE.DIR.PREVIEW.WEBP, allows[1]);
          checkLow();
        };
        img.src = './assets/img/img_large_0_4.webp';
      }
    } else {
      checkLow();
    }
  }

  private toggleSVG() {
    if (this.main.svgEl) {
      this.main.svgEl.fabricJS._objects.forEach((el: any) => {
        if (el._dataSource) {
          el._dataSource.active = false;
        }
      });
    } else {
      this.model.traverse((el: any) => {
        if (el._dataSource) {
          el._dataSource.active = false;
        }
      });
    }

  }

  updateData(data) {
    let _selected = this.main.selected,
      settings = _selected.camera;
    _selected.hasChanges = _selected._project.hasChanges = _selected.hasRecalcChanges = true;
    if (_selected.camera.frameState[_selected.currentItem]) {
      _selected.camera.frameState[_selected.currentItem].hasChanges = true;
    }
    switch (data) {
      case'scale': {
        this.model.scale.z = this.model.scale.y = this.model.scale.x;
        break;
      }
      case'isSVG': {
        this.toggleSVG();
        break;
      }

      case'opacity': {
        if (this.model) {
          this.model.traverse((child) => {
            if (child.type == ENTITY.Config.TYPES.MESH) {
              child.material.opacity = child.material.opacity0 = settings.opacity;
            }
          });
        }
        if (this.main.svgEl) {
          this.main.svgEl.updateOpacity()
        }

        break;
      }
      case'update': {
        break;
      }
      case'kompass':
        this._projControls.kompas.onUpdate();
      {
        break;
      }
      case 'zoom': {
        this.camera.zoom = settings.zoom;
        this.camera.updateProjectionMatrix();
        break;
      }
      case 'zoomSpeed': {
        this.controls.zoomSpeed = _selected.controls.zoomSpeed;
        break;
      }
      case 'rotateSpeed': {
        this.controls.rotateSpeed = _selected.controls.rotateSpeed;
        break;
      }
      case 'cameraPst': {
        break;
      }
      case'y':
      case'x': {
        let
          val = settings.resolution[data],
          prop = this._slider._W() / this._slider._H(),
          isHeight = data == 'y',
          _px = 'px',
          elem: any = [this._slider.container.childNodes];
        if (!elem[0] || !elem[0].length) {
          break;
        }
        if (this._slider.alignImgContainer instanceof Node) {
          let el = this._slider.alignImgContainer.childNodes;
          if (el && el.length) {
            elem.push(el);
          }
        }
        elem.forEach(function (lstChilds) {
          [].forEach.call(lstChilds, function (el) {
            el.style.height = (isHeight ? val : val / prop) + _px;
            el.style.width = (isHeight ? val * prop : val) + _px;
          });
        });

        this._events.onWindowResize();
        break;
      }
      case'size':
      case'lens': {

        //settings.size =this._slider._W() * 1.1 > window.innerWidth ? (window.innerWidth / this._slider._W()) * 0.9 : 1;
        let
          sizeX = this.gl.domElement.clientWidth,
          sizeY = this.gl.domElement.clientHeight
        ;
        settings.aspect = sizeX / sizeY;
        this.camera.setLens(settings.lens * settings.aspect, 36);
        this.camera.updateProjectionMatrix();
        break;
      }
      default: {
        this.camera.updateProjectionMatrix();
      }
    }
    this.dataSave();
    this._animation.play();
  }

  private deviceCheck() {
    var check = false;
    (function (a) {
      if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
        check = true;
      }
    })(navigator.userAgent || navigator.vendor || window['opera']);
    return check;
  }

  recalc() {
    let
      _selected = this.main.selected,
      _c = this.camera.position.clone(),
      _t = this.controls.target.clone();
    _selected.camera.frameState = [];
    _selected.camera.frameState[_selected.currentItem] = {
      x: _c.x,
      y: _c.y,
      z: _c.z,
      target: {x: _t.x, y: _t.y, z: _t.z}
    };

    for (let i = 0; i < _selected.images.length; i++) {
      if (_selected.currentItem == i) {
        continue;
      }

      let quaternion = new THREE.Quaternion(),
        _cmC = _c.clone();
      quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), ((i - _selected.currentItem) * this._angle) * Math.PI / 180);
      _cmC.applyQuaternion(quaternion);
      _selected.camera.frameState[i] = {
        x: _cmC.x,
        y: _cmC.y,
        z: _cmC.z,
        target: {x: _t.x, y: _t.y, z: _t.z}
      };
    }
    _selected.hasRecalcChanges = false;
    _selected.hasChanges = _selected._project.hasChanges = true;
  }

  dataSave() {
    let _selected = this.main.selected,
      old = _selected.camera;
    if (!(old instanceof ENTITY.OxiCamera)) {
      this.main.selected.camera = new ENTITY.OxiCamera({
        position: this.camera.position,
        rotation: this.camera.rotation,
        target: this.controls.target,
        frameState: old.frameState,
        //resolution: new ENTITY.Vector3({x: this._slider._W(), y: this._slider._H()}),
        fov: this.camera.fov,
        zoom: this.camera.zoom,
        scale: this.model.scale.x,
        lens: old.lens,
        opacity: old.opacity,
        size: old.size,
        views: old.views
      });
    } else {
      old.position = this.camera.position.clone();
      old.rotation = this.camera.rotation.clone();
      old.target = this.controls.target.clone();
      old.fov = this.camera.fov;
      old.zoom = this.camera.zoom;
      old.scale = this.model.scale.x;
    }
    //_selected.controls = new ENTITY.OxiControls({
    //    rotateSpeed: _selected.controls,
    //    zoomSpeed: _selected.controls
    //})

  }

  loadModel(callback: Function = () => {
    console.log('load was finished succesed');
  }) {

    let _self = this;
    if (this.main.selected.cash.model) {
      this._onLoadModel(this.main.selected.cash.model, callback);
    } else if (this.main.selected.projFilesDirname && this.main.selected.destination && this.main.selected.destination.match('.obj')) {
      let loader = this.loader = this.loader || new THREE.OBJLoader();
      try {
        loader.load(ENTITY.Config.PROJ_LOC + this.main.selected.projFilesDirname + '/' + this.main.selected.destination, (object) => {
          this._onLoadModel(object, callback);
        }, (e) => this.onProgress(e), (e) => this.onError(e));
      } catch (e) {
        console.log(e);
        alertify.error(e);
      }

    } else {
      callback();
    }
  }

  private onError(xhr) {
    alertify.error(xhr);
  }

  private onProgress(xhr) {
    if (xhr.lengthComputable) {
      this.main.preloader.onUpdatePreloaderStatus(xhr.loaded / xhr.total);
      //console.log((xhr.loaded / xhr.total).toFixed(2) + '% downloaded');
    }
  }

  _onLoadModel(object, callback: Function = () => {
  }) {
    if (this.model.children) {
      for (let i = 0; i < this.model.children.length; i++) {
        this.model.remove(this.model.children[i]);
      }
    }
    if (this.main.selected.camera.isSVG) {

    } else {
      this.meshes = [];
      this.model.add(object);
      let _mat = new THREE.MeshBasicMaterial({
          transparent: true,
          opacity: this.main.selected.camera.opacity
        }),
        _mat1 = new THREE.MeshBasicMaterial({
          color: new THREE.Color(1.0, 1.0, 0.0),
          transparent: true,
          opacity: 0
        }),
        meshType = ENTITY.Config.TYPES.MESH,
        _elms = [],
        setpC = 1 / 1000;
      this.model.inters = [];
      object.traverse((child) => {
        if (child.type == meshType) {
          this.meshes.push(child);
          child._helper = new THREE.BoxHelper(child);
          child.material = _mat.clone();
          child.material.opacity0 = child.material.opacity;
          child.material.color = this.getColor();
          child.material.side = THREE.DoubleSide;
          //child.renderOrder = 0;
          child.category = -1111;
          this.model.inters.push(child);
          // if (child.name.toLowerCase().match(ENTITY.Config.IGNORE)) {
          //   child.material.color = new THREE.Color(0, 0, 0);
          // }

          for (let i = 0, areas = this.main.selected.areas; areas && i < areas.length; i++) {
            if (areas[i]._id.match(child.name)) {
              child._data = areas[i];
              child._data._modelFromeScene = child;
              break;
            }
          }
          let matches = [child._data ? child._data.dataSourceId || child._data.dataSourceGeneratedId : child.name];
          this.dataSourceMatch(null, matches, (source) => {
            child._dataSource = source;
          });
        }
      });
      this.updateRenderOrder();
    }
    //console.log(this.main.selected);
    this.main.selected.cash.model = object;
    this.helper.visible = false;
    this.checkIgnore();
    //if (this._animation) {
    //    this.focusCamera(null, callback);
    //} else {
    callback();
    //}
  }

  checkIgnore() {
    if(this.main.selected.camera.isSVG ){
      if (this.main.svgEl.fabricJS._objects) {
        this.main.svgEl.fabricJS._objects.forEach((child) => {
          if (this.main.selected.camera.forceIgnores.indexOf(child.id) < 0) {

            if(child.settedHoverFill){
              child.hoverFill = child.settedHoverFill;
              child.defFill = child.fill= child.settedFill;
            }

          } else {
            if (!child.settedHoverFill) {
              child.settedHoverFill = child.hoverFill;
              child.settedFill = child.fill;
            }
            child.hoverFill = child.defFill = child.fill='#000000';
          }
          child.set('fill', child.defFill || child.fill|| child.hoverFill);
        });
        this.main.svgEl.fabricJS.renderAll();
      }
    }else{
      this.model.traverse((child) => {
        if (!child.material) {
          return;
        }
        if (this.main.selected.camera.forceIgnores.indexOf(child.name) < 0) {
          if (child.material.settedColor) {
            child.material.color = child.material.settedColor;
          }
        } else {
          if (!child.material.settedColor) {
            child.material.settedColor = child.material.color.clone();
          }
          child.material.color = new THREE.Color(0, 0, 0);
        }
      })
    }

  }

  private focusCamera(model: any = null, next: Function = () => {
  }) {
    let
      _model = model || this.main.selected.cash.model,
      boxHelper = new THREE.BoxHelper(_model);
    boxHelper.geometry.computeBoundingBox();
    //boxHelper.geometry.computeBoundingSphere();
    let radius = _model.radius = boxHelper.geometry.boundingSphere.radius;
    _model.radius = radius;
    let sc = 2.2,
      newTarg = boxHelper.geometry.boundingSphere.center.clone(),
      newPst = newTarg.clone().addScaledVector(this.camera.getWorldDirection().negate(), radius * sc);
    this.camera.far = _model.radius * sc * 100;
    //this.controls.maxDistance = _model.radius * sc * 2;
    this.move({
      onComplete: () => {
        next();
      }, list: [{
        onUpdate: (delta) => {
          this.camera.position.lerp(newPst, delta);
          this.controls.target.lerp(newTarg, delta);
          this.camera.lookAt(this.controls.target);
          this.controls.targetObj.position.copy(this.controls.target);
          this.camera.updateProjectionMatrix();
        }
      }]
    });

  }

  private move(arg) {
    let _self = this,
      controls = this.controls;
    if (!_self.isFinishTweenAnimations) {
      return;
    }
    this._animation.play(true);
    let duration = arg.duration || 900,
      tween = new TWEEN.Tween({delta: 0}).to({delta: 1}, duration)
        .easing(TWEEN.Easing.Exponential.In)
        .onStart(() => {
          controls.enabled = _self.isFinishTweenAnimations = /*this.personControls.enabled = */false;
          if (arg.onStart) {
            arg.onStart();
          }
        })
        .onUpdate(function (delta) {
          for (let i = 0, list = arg.list || arg; i < list.length; i++) {
            list[i].onUpdate(delta);
          }
        })
        .onComplete(() => {
          _self.isFinishTweenAnimations = controls.enabled = true;
          if (arg.onComplete) {
            arg.onComplete();
          }
        })
        .start()

  }

  private getColor() {
    let color;
    while (this.colorsS.indexOf((color = Math.random() + '_' + Math.random() + '_' + Math.random())) > -1) {

    }
    this.colorsS.push(color);
    let _c = color.split('_');
    return new THREE.Color(_c[0], _c[1], _c[2]);
  }

  dataSourceMatch(_data, matches: Array<string> = null, onSucces: Function = null) {
    for (let i = 0, sources = _data || this.main.preToolTip.dataElem; sources && i < sources.length; i++) {
      if (!matches) {
        onSucces(sources[i]);
        if (sources[i].children) {
          this.dataSourceMatch(sources[i].children, matches, onSucces);
        }
      } else if (matches.indexOf(sources[i]._id) > -1 || matches.indexOf(sources[i]._id + '') > -1) {
        return onSucces(sources[i]);
      } else if (sources[i].children) {
        this.dataSourceMatch(sources[i].children, matches, onSucces);
      }
    }
  }

  updateInfoHTML() {
    for (let i = 0; i < this.infoHTML.length; i++) {
      this.infoHTML[i].update();
    }
  }

  generateNode(settings) {

    let _selected = this.main.selected;

    if (_selected.camera.isSVG) {

      if (this.main.svgEl.fabricJS._objects.length) {
        this.generateNodeChildren(this.main.svgEl.fabricJS._objects.filter((el) => {
          return (el.type == this.main.svgEl.shapes.POLYGON && el.points.length) || el.type == this.main.svgEl.shapes.GROUP
        }), settings);
      } else {
        alertify.error('Please create or upload some svg');
      }

    } else {
      if (this.model.children.length) {

        let meshType = 'Mesh', childs = [];
        this.model.traverse((child) => {
          if (child.type == meshType /*&& !child.name.toLowerCase().match(ENTITY.Config.IGNORE)*/) {
            childs.push(child);
          }
        });
        this.generateNodeChildren(childs, settings);
      } else {
        alertify.error('Please upload some model');
      }
    }
  }

  private generateNodeChildren(children, settings) {
    let _selected = this.main.selected,
      cleanBySettings = () => {
        let _areas = _selected.areas,
          _meshes = [],
          matches = [];

        if (!_areas || !_areas.length) {
          return matches;
        }
        for (let i = 0; i < children.length; i++) {
          let _cur = children[i],
            id = _cur.name || _cur.id;

          if (!id) {
            continue;
          }
          id = (id + '').toLowerCase();

          for (let u = 0; u < _areas.length; u++) {
            if (_areas[u]._id.toLowerCase().match(id)) {
              if (settings.replace) {
                //(_areas.splice(u, 1));
                _cur._data = null;
              } else {

              }
              matches.push(_areas[u]);

              break;
            }
          }
        }

        if (settings.removeExtra) {
          for (let u = 0; u < _areas.length; u++) {
            let _id = _areas[u]._id.toLowerCase(),
              shouldRemove = true;
            for (let i = 0; i < children.length; i++) {
              let _cur = children[i],
                id = _cur.name || _cur.id;
              if (!id) {
                continue;
              }
              id = (id + '').toLowerCase();

              if (_id.match(id)) {
                shouldRemove = false;
                break;
              }
            }
            if (shouldRemove) {
              _areas.splice(u--, 1);
            }
          }
        }

        if (settings.replace) {

          //if (settings.replace) {
          while (_areas.length) {
            _areas.shift();
          }
          //} else {
          //    for (let u = 0, _area = _areas.concat([]); u < _area.length; u++) {
          //        if (matches.indexOf(u) < 0)_areas.splice(u, 1);
          //    }
          //}

        }
        return matches
      },
      applyNode = (matches) => {

        if (!_selected.areas) {
          _selected.areas = [];
        }
        let constructorType,
          args = {destination: settings.destination, name: settings.name},
          replaces = ['@name', '@id'];
        switch (settings.type) {
          case 'Url link': {
            constructorType = ENTITY.LinkGeneralStructure;
            break;
          }
          case 'JS action': {
            constructorType = ENTITY.GeneralStructure;
            break;
          }
          default : {
            constructorType = ENTITY.ModelStructure;
          }
        }
        if (!_selected.area) {
          _selected.area = [];
        }
        for (let i = 0; i < children.length; i++) {
          let _cur = children[i],
            args = {
              destination: settings.destination,
              name: settings.name
            };


          ['name', 'destination'].forEach((field) => {
            if (args[field]) {
              replaces.forEach((atr) => {
                args[field] = args[field].replaceAll(atr, _cur[atr.substr(1)] || '');
              })
            }
          });

          if (!args.name) {
            args.name = _cur.name || _cur.id;
          }
          let child = new constructorType(args);

          child._id = (_cur.name || _cur.id || _cur.uuid || '') + '';
          if (!child.name) {
            child.name = child._id.toUpperCase(args);
          }

          //check if name already taken
          for (let j = 0; j < _selected.areas.length; j++) {
            if (_selected.areas[j].name == child.name) {
              child.name += Math.random().toString(18).split('.')[1];
              break;
            }

          }

          if (!settings.replace) {
            let id = child._id.toLowerCase(), noNeeToCreate = false;
            for (let j = 0; j < matches.length; j++) {

              if (matches[j]._id.toLowerCase().match(id)) {
                noNeeToCreate = true;
                break;
              }
            }
            if (noNeeToCreate) {
              continue;
            }
          }

          let _matches = [child.name];
          _cur._data = child;
          this.dataSourceMatch(null, _matches, (source) => {
            _cur._dataSource = source;
            _cur._data.dataSourceGeneratedId = _cur._dataSource._id;
          });
          child._id += Date.now();
          child._modelFromeScene = _cur;
          _selected.areas.push(child);
        }
        this.updateData('test');
      };
    ;
    applyNode(cleanBySettings());

  }

  //TODO
  _deleteArea(item) {


    if (item._data && item._data._modelFromeScene) {
      item._data._modelFromeScene._data = null;
      item._data._modelFromeScene = null;
    } else if (item._modelFromeScene) {
      item._modelFromeScene._data = null;
      item._modelFromeScene = null;
    }
    delete item._data;
    /*for (let i = 0, areas = this.main.selected.areas; areas && i < areas.length; i++) {
     if (areas[i].created == item.created) {
     delete item._data;
     break;
     }
     }*/
    for (let i = 0; i < this.model.inters.length; i++) {
      let obj = this.model.inters[i];
      if (obj._data && obj._data._id == item._id) {
        return obj._data = null
      }
    }
    if (this.main.svgEl) {
      for (let i = 0; i < this.main.svgEl.fabricJS._objects.length; i++) {
        let obj = this.main.svgEl.fabricJS._objects[i];
        if (obj._data && obj._data._id == item._id) {
          return obj._data = null
        }
      }
    }


  }

  _parent(): HTMLElement {
    return this.main.renderParent['nativeElement'];
  }

  onFilesSelected(files) {
    let _self = this,
      filereader = this._fileReader = this._fileReader || new FileReader(),
      _flStrg,
      onFinish;

    if (!files || !files.length) {
      return console.error('files had not been selected');
    }

    switch (files[0].category) {
      case ENTITY.Config.FILE.TYPE.MODEL_OBJ: {
        _flStrg = ENTITY.Config.FILE.STORAGE.MODEL_OBJ;
        onFinish = () => _self._animation.play();
        break;
      }
      case ENTITY.Config.FILE.TYPE.PREVIEW_IMG: {
        _flStrg = ENTITY.Config.FILE.STORAGE.PREVIEW_IMG;

        onFinish = () => {
          _self._slider.addFrames();
          if (!this.main.selected.images[this.main.selected.currentItem]) {
            _self._slider.move(0);
          }
        }
        this.main.selected.images = [];
        break;
      }
      case ENTITY.Config.FILE.TYPE.ALIGN_IMG: {
        _flStrg = ENTITY.Config.FILE.STORAGE.ALIGN_IMG;
        this.main.selected.alignImages = [];
        onFinish = () => _self._slider.addAlignImg();
        break;
      }
      case ENTITY.Config.FILE.TYPE.MODEL_SVG: {
        _flStrg = ENTITY.Config.FILE.STORAGE.SVG_FILE;
        onFinish = () => {
        };
        break;
      }
    }
    if (!_flStrg || !onFinish) {
      return alertify.error('file category is not recognized');
    }

    this._files[_flStrg] = files;
    let startFrom = 0;

    function parseFiles(cur) {
      if (!cur) {
        return onFinish();
      }
      filereader.onloadend = (e: any) => {

        switch (cur.category) {
          case ENTITY.Config.FILE.TYPE.MODEL_OBJ: {
            let loader = _self.loader = _self.loader || new THREE.OBJLoader();
            loader.parse(e.currentTarget.result, (m) => {
              _self.main.selected.destination = [new ENTITY.ProjFile({file: cur, name: cur.name})];
              _self._onLoadModel(m, () => {
                _self.focusCamera();
              });
            });
            break;
          }
          case ENTITY.Config.FILE.TYPE.MODEL_SVG: {
            _self.main.svgEl.reload(e.currentTarget.result);
            break;
          }
          case ENTITY.Config.FILE.TYPE.PREVIEW_IMG: {
            _self.main.selected.images.push(new ENTITY.ProjFile({
              file: cur,
              name: cur.name,
              data: e.currentTarget.result
            }));
            break;
          }
          case ENTITY.Config.FILE.TYPE.ALIGN_IMG: {
            _self.main.selected.alignImages.push(new ENTITY.ProjFile({
              file: cur,
              name: cur.name,
              data: e.currentTarget.result
            }));
            break;
          }
        }
        parseFiles(files[startFrom++]);
      };
      switch (cur.category) {
        case ENTITY.Config.FILE.TYPE.MODEL_SVG:
        case ENTITY.Config.FILE.TYPE.MODEL_OBJ: {
          filereader.readAsText(cur);
          break;
        }
        case ENTITY.Config.FILE.TYPE.PREVIEW_IMG:
        case ENTITY.Config.FILE.TYPE.ALIGN_IMG: {
          filereader.readAsDataURL(cur);
          break;
        }
        default : {
          parseFiles(files[startFrom++]);
        }
      }
    };
    parseFiles(files[startFrom++]);
  }

  _offset() {
    return this.gl.domElement.getBoundingClientRect()
  }

  onEventPrevent(event) {
    event.preventDefault();
    event.stopPropagation();
    return false;
  }

  updateAngle(frames) {
    this._angle = 360 / frames;
  }

  render() {
    if (Pace.running) {
      return;
    }
    this.updateInfoHTML();
    TWEEN.update();
    this.gl.render(this.scene, this.camera);

    //this.gl.autoClear = true;
    //this.gl.setClearColor( 0xffffff ,0);
    //this.gl.setClearAlpha( 0);
    //
    //
    //this.composer.render();
  }

  getViews() {
    let _self = this,
      _selected = this.main.selected,
      iframeItem = _selected.currentItem,
      count = _selected.images.length,
      startFrom = 0,
      meshType = 'Mesh', colorsSel = {},
      colorMax = 255,
      _cnvW = this.gl.domElement.clientWidth,
      _cnvH = this.gl.domElement.clientHeight;

    if (!_selected.canEdit) {
      return;
    }
    if (!this.meshes.length) {
      return alertify.error('you should load some model');
    }
    if (!_cnvW || !_cnvH) {
      return alertify.error('only for webgl view');
    }
    this.main.calcViews = true;

    _selected.sourcesApp.blockScreen.toggle(true, 'Please wait till app make some calcs');
    let canvas = document.createElement('canvas');
    canvas.width = _cnvW;
    canvas.height = _cnvH;
    let cntx = canvas.getContext('2d');

    this.helper.visible = false;
    this.model.traverse((child) => {
      if (child.type == meshType) {
        child.material.transparent = false;
        //child.material.opacity = 10;
        child.material.needsUpdate = true;
      }
    });
    let colorss = ['r', 'g', 'b'],
      render = this.timeRender = this.timeRender || new THREE.WebGLRenderer({
        antialias: false,
        alpha: false
      });
    render.setSize(_cnvW, _cnvH);
    this._animation.pause();

    function parse(img) {
      cntx.clearRect(0, 0, _cnvW, _cnvH);
      cntx.drawImage(img, 0, 0, _cnvW, _cnvH);
      let imgData = cntx.getImageData(0, 0, _cnvW, _cnvH),
        _colors = {};

      for (var i = 0; i < imgData.data.length; i += 4) {
        let _color = imgData.data[i] + '_' + imgData.data[i + 1] + '_' + imgData.data[i + 2],//+ "_" + imgData.data[i + 3],
          x = i / 4 % _cnvW,
          y = (i / 4 - x) / _cnvW;


        if (_colors[_color]) {
          let _l = _colors[_color];
          _l.count += 1;

          if (_l.minX > x) {
            _l.minX = x;
          }
          if (_l.maxX < x) {
            _l.maxX = x;
          }

          if (_l.minY > y) {
            _l.minY = y;
          }
          if (_l.maxY < y) {
            _l.maxY = y;
          }
        } else {
          _colors[_color] = {
            count: 1,
            iframeItem: _selected.currentItem,
            minX: x,
            minY: y,
            maxX: x,
            maxY: y,
            tooltips: {}
          };
        }
      }


      for (let field in _colors) {
        if (_colors.hasOwnProperty(field)) {
          let _e = _colors[field];
          if (!colorsSel[field]) {
            colorsSel[field] = _e;
          } else {
            if (colorsSel[field].count < _e.count) {
              colorsSel[field] = _e;
            }
          }

          if (_e.maxX == -Infinity || _e.maxY == -Infinity || field == '0_0_0') {

          } else {
            colorsSel[field].tooltips[_e.iframeItem] = getMiddleBtw4Points(
              new THREE.Vector2(_e.minX, _e.minY),
              new THREE.Vector2(_e.maxX, _e.maxY),

              new THREE.Vector2(_e.maxX, _e.minY),
              new THREE.Vector2(_e.minX, _e.maxY)
            );
          }

        }
      }
      //for (let field in colorsSel) {
      //if (colorsSel.hasOwnProperty(field)) {
      //        let quad = (colorsSel[field].maxX - colorsSel[field].minX) * (colorsSel[field].maxY - colorsSel[field].minY);
      //        if (!colorsSel[field].quad)colorsSel[field].quad = quad;
      //        if (quad > colorsSel[field].quad) {
      //            colorsSel[field].quad = quad;
      //            colorsSel[field].iframeItem = _selected.currentItem;
      //        }
      //    }
      //}
    }

    function onFinishParse() {
      //_self.helper.visible = true;
      _self._animation.play();
      _self.model.traverse((child) => {
        if (child.type == meshType /*&& !child.name.toLowerCase().match(ENTITY.Config.IGNORE)*/) {
          child.material.transparent = true;
          if (!child._color) {
            child._color = child.material.color.getStyle().replace('rgb(', '').replace(')', '').split(',').join('_');
          }
          if (!_selected.camera.views) {
            _selected.camera.views = {};
          }
          if (!colorsSel[child._color]) {
            let elC = child._color.split('_'),
              delta = 5,
              _r = +elC[0],
              _g = +elC[1],
              _b = +elC[2],
              matchsR = [],
              matchsB = [],
              matchsG = [];
            for (let field in colorsSel) {
              if (colorsSel.hasOwnProperty(field)) {
                let r = +field.split('_')[0];
                if (_r - delta <= r && _r + delta >= r) {
                  matchsR.push(field);
                }
              }
            }
            for (let i = 0; i < matchsR.length; i++) {
              let g = +matchsR[i].split('_')[1];
              if (_g - delta <= g && _g + delta >= g) {
                matchsG.push(matchsR[i]);
              }
            }
            for (let i = 0; i < matchsG.length; i++) {
              let b = +matchsG[i].split('_')[2];
              if (_b - delta <= b && _b + delta >= b) {
                matchsB.push(matchsG[i]);
              }
            }

            if (matchsB.length) {
              child._color = matchsB[0];
            }
            if (matchsB.length > 1) {
              alertify.warning('Warning, there are some views with almost-same colors');
            }
          }
          if (colorsSel[child._color]) {
            _selected.camera.views[child.name] = {
              iframeItem: colorsSel[child._color].iframeItem,
              tooltips: colorsSel[child._color].tooltips
            };
            colorsSel[child._color] = null;
            delete colorsSel[child._color]
          } else {
            console.warn(child.name + ' has some problem with color ' + child._color);
          }
        }
      });
      _self.main.calcViews = false;
      _selected.sourcesApp.blockScreen.toggle();
      _self.updateData('');
    }

    function getMiddleBtw4Points(p1, p2, p3, p4) {
      let A1 = (p1.y - p2.y) / (p1.x - p2.x);
      let A2 = (p3.y - p4.y) / (p3.x - p4.x);
      let b1 = p1.y - A1 * p1.x;
      let b2 = p3.y - A2 * p3.x;
      let Xa = (b2 - b1) / (A1 - A2);
      return {
        x: (Xa / _cnvW) * 100,
        y: ((A2 * Xa + b2) / _cnvH) * 100
      }
    }

    var iterator = () => {
      _selected.sourcesApp.blockScreen.blockStatus = (++startFrom) + '/' + count;
      this._slider.move(1);
      let needNextUpdate = _selected.currentItem == iframeItem;
      //console.log(_selected.currentItem,iframeItem);
      //console.info("Parsing step: ",  _selected.camera.current + "/" + count);
      render.render(this.scene, this.camera);
      let img = new Image();
      img.onload = function () {
        parse(img);
        if (needNextUpdate) {
          onFinishParse();
          //console.log(colorsSel);
        } else {
          iterator();
        }
      }
      img.src = render.domElement.toDataURL();
    };
    iterator();
  }

  moveToView() {
    let _selected = this.main.selected,
      meshName = this.selectedIframe,
      moVeTo = +_selected.currentItem,
      _mesh;
    if (!meshName || !_selected.camera.views || !_selected.camera.views[meshName] || !(typeof _selected.camera.views[meshName].iframeItem == 'number')) {
      return alertify.error('no active view');
    }
    let activeIframe = +_selected.camera.views[meshName].iframeItem;
    if (activeIframe == moVeTo) {
      alertify.warning('has same view');
    }
    if (activeIframe >= _selected.images.length || activeIframe < 0) {
      return alertify.error('no such view');
    }
    this.model.traverse((mesh) => {
      if (mesh.name == meshName) {
        mesh.material.opacity = 1;
        _mesh = mesh;
      }
    });
    var half = _selected.images.length / 2,
      moveRight = ((moVeTo < activeIframe && Math.abs(moVeTo - activeIframe) < half) || ((moVeTo - activeIframe) > 0 && moVeTo - activeIframe > half)),
      interval = setInterval(() => {
        this._slider.move(moveRight ? 1 : -1);
        if (_selected.currentItem == activeIframe) {
          clearInterval(interval);
        }
      }, 100);

    //console.log(moveRight, moVeTo, activeIframe, half);
  }
}

class OxiEvents {
  private EVENTS_NAME: any;
  mouse: OxiMouse;
  private raycaster: any;
  private main: OxiAPP;
  private canEdit: boolean = false;
  private pathOnMove: number = 50;
  private lastEv: any;
  private eventsData: any;
  lastInter: any;


  constructor(private app: OxiAPP) {
    let
      _self = this,
      elem = app.gl.domElement,
      handler = (elem.addEventListener || elem.attachEvent).bind(elem);
    this.canEdit = app.main.selected.canEdit;
    this.main = app;
    this.EVENTS_NAME = ENTITY.Config.EVENTS_NAME;
    this.mouse = new OxiMouse(app);
    this.raycaster = new THREE.Raycaster();


    if (!this.canEdit) {
      handler(this.EVENTS_NAME.MOUSE_OUT, (e) => this.onMouseOut(e));
    }
    this.eventsData = [
      {cntx: window, name: this.EVENTS_NAME.RESIZE, callback: (e) => this.onWindowResize()},
      {cntx: window, name: this.EVENTS_NAME.KEY.UP, callback: (e) => this.onKeyUp(e)},
      {cntx: window, name: this.EVENTS_NAME.KEY.DOWN, callback: (e) => this.onKeyDown(e)},
      {cntx: elem, name: this.EVENTS_NAME.MOUSE_DOWN, callback: (e) => this.onMouseDown(e)},
      {cntx: elem, name: this.EVENTS_NAME.MOUSE_UP, callback: (e) => this.onMouseUp(e)},
      {cntx: elem, name: this.EVENTS_NAME.MOUSE_MOVE, callback: (e) => this.onMouseMove(e)},
      {cntx: elem, name: this.EVENTS_NAME.DB_CLICK, callback: (e) => this.onDbClick(e)},
      {
        cntx: window, name: this.EVENTS_NAME.MOUSE_DOWN, callback: (e) => {

          if (e.target.getAttribute(ENTITY.Config.DOM_ATTR.CNTX_CNTRLS)) {
            return;
          }
          let _cntx = this.app.cntxMenu;
          this.app.cntxMenu = this.app.needsUpdateCntxMenu = false;
          if (_cntx) {
            this.main._projControls.show(e);
          }

        }
      },

      {
        cntx: elem, name: this.EVENTS_NAME.SELECT_START, callback: (e) => {
          app.onEventPrevent(e)
        }
      },
    ];
    this.eventsData.forEach((el) => {
      (el.cntx.addEventListener || el.cntx.attachEvent).bind(el.cntx)(el.name, el.callback);
    });
    this.onWindowResize();
  }

  onDestroy() {
    this.eventsData.forEach((el) => {
      (el.cntx.removeEventListener || el.cntx.detachEvent).bind(el.cntx)(el.name, el.callback);
    });
  }

  onWindowResize() {
    let app = this.main,
      _w = app._parent().clientWidth,//app._slider._W(),
      _h = app._parent().clientHeight,// app._slider._H(),
      _nIW = app._slider.currentFrame.naturalWidth,
      _nIH = app._slider.currentFrame.naturalHeight,
      _nat = _nIW / _nIH,
      //_resH = _nat ? _w / _nat : app._slider._H(),
      sizeX = _w,
      sizeY = _w * _nIH / _nIW,
      _px = 'px',
      svgEl = this.main.main.svgEl;

    if (_w / _h > _nIW / _nIH) {
      if (_nIH < _h) {
        _h = _nIH;
      }
      sizeX = _h * _nIW / _nIH;
      sizeY = _h;
    } else {
      if (_nIW < _w) {
        _w = _nIW;
      }
      sizeX = _w;
      sizeY = _w * _nIH / _nIW;
    }

    if (isNaN(sizeY)) {
      sizeY = _h;
    }

    app.camera.aspect = sizeX / sizeY;
    app.camera.updateProjectionMatrix();
    app.gl.setSize(sizeX, sizeY);

    console.log(`Setted size width: ${sizeX}px and height: ${sizeY}px`);

    app._container.style.height = sizeY + _px;
    app._container.style.width = sizeX + _px;


    //app.updateInfoHTML();
    if (svgEl) {
      svgEl.resize(sizeX, sizeY);
    }
    //app.effectFXAA.uniforms['resolution'].value.set(1 / _w, 1 / _h );
    //app.outlinePass.resolution.x = _w;
    //app.outlinePass.resolution.y = _h;
    //app.composer.setSize( _w, _h );
    if (app._animation) {
      app._animation.play();
    }
  }

  private onKeyUp(ev: any) {
  }

  private onKeyDown(ev: any) {
    if (this.app.main.selected.isSVG) {
      return;
    }
    switch (ev.keyCode) {
      case 187://+
      case 107: {//+
        this.app.controls.handleMouseWheel({deltaY: -1});
        break;
      }
      case 189://-
      case 109: {//-
        this.app.controls.handleMouseWheel({deltaY: 1});
        break;
      }
    }
  }

  private onMouseUp(ev: any) {
    if (!this.canEdit) {
      this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.DEFAULT;
    }
    let _self = this,
      _lastInter = this.lastInter,
      btn: number = ev.button;
    this.main._projControls.show(ev, false);

    if (!this.mouse.moveWithDown) {
      switch (btn) {
        case 0:
        case 1: {

          if (this.canEdit) {
            if (this.lastInter) {
              this.main._projControls.showAttachPopUp(_lastInter.object);
            }
          }
          if (this.lastInter && this.lastInter.object.click) {
            this.lastInter.object.click();
          }
          break;
        }
        case 2: {
          if (this.canEdit) {
            this.onSelected(ev, (inter) => {
              this.app.cntxMenu = this.app.needsUpdateCntxMenu = inter;
              this.main._projControls.show(ev);
            });
          }
          break;
        }

      }
    }


    this.mouse.moveWithDown = this.mouse.down = this.lastEv = null;
    ev.preventDefault();

  }

  private onMouseMove(ev: any) {


    const delta = 2;
    this.mouse.moveWithDown = this.mouse.down && ev && Math.abs(this.mouse.down.clientX-ev.clientX) >delta &&  Math.abs(this.mouse.down.clientY-ev.clientY) >delta?this.mouse.down:false;;
    if (this.canEdit) {
      if (this.mouse.down) {
        this.app.updateRenderOrder();
      } else {
        this.onSelected(ev, (inter) => {
          ev.preventDefault();
          ev.stopPropagation();
          inter.object.material.opacity = inter.object.material.opacity0;
          this.main._projControls.showTooltip(true, false);
        });
      }

    } else {
      this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.DEFAULT;
      if (this.mouse.down) {
        this.app.updateRenderOrder();
        this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.HOLDING;
        if (this.lastInter) {
          this.main._projControls.show(ev, false);
          //this.lastInter = null;
        }
        if (ev.touches && ev.touches.length) {
          ev.clientX = ev.touches[0].clientX;
          ev.clientY = ev.touches[0].clientY;
        }
        if (!this.lastEv) {
          return this.lastEv = ev;
        }
        if (
          Math.abs(ev.clientX - this.lastEv.clientX) > this.pathOnMove

        ) {
          this.main._slider.move((ev.clientX > this.lastEv.clientX ? -1 : 1));
          this.lastEv = ev;
        } else if (Math.abs(ev.clientY - this.lastEv.clientY) > this.pathOnMove) {
          this.main._slider.move((ev.clientY > this.lastEv.clientY ? -1 : 1));
          this.lastEv = ev;
        }
      } else {
        this.onSelected(ev, (inter) => {
          this.main._projControls.show(ev);
        });
      }
    }

  }

  private onSelected(ev, callback, onNoteSame = (last) => {
    if (this.lastInter) {
      if (last && last.object.uuid == this.lastInter.object.uuid) {

      } else {
        this.main._projControls.show(ev, false);
      }
    }

    this.lastInter = last;
  }) {
    let intersectList = this.inter(ev);
    if (intersectList && intersectList[0]) {
      // if (intersectList[0].object.name.toLowerCase().match(ENTITY.Config.IGNORE)) {
      //   return;
      // }
      onNoteSame(intersectList[0]);
      callback(intersectList[0]);
    } else {
      onNoteSame(null);
    }
  }

  private onMouseDown(ev: Event) {
    this.mouse.down = ev;
    this.lastEv = false;
    //this.lastInter = null;
    if (!this.canEdit) {
      this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.HOLDING;
    }
  }

  private onMouseOut(ev: any) {
    if (this.mouse.down) {
      this.onMouseUp(ev);
    }
  }

  private onDbClick(e: any) {
    if (this.canEdit) {
      this.main.controls.target.copy(this.main.scene.position);
      this.main.controls.update();
    }
    this.main.onEventPrevent(e);
  }

  onCntxMenu(event) {
    event.preventDefault();
    return false;
  }

  inter(ev: any, arg: any = null) {
    var _self = this,
      elements = arg && arg.childs ? arg.childs : _self.main.model.inters;

    if (!elements) {
      return false;
    }
    if (arg && arg.position) {
      var direction = new THREE.Vector3().subVectors(arg.target, arg.position);
      _self.raycaster.set(arg.position, direction.clone().normalize());
    } else {
      let
        mouseVector = _self.mouse.interPoint(ev);
      _self.raycaster.setFromCamera(mouseVector, _self.main.camera);
    }

    return _self.raycaster.intersectObjects(elements, true);
  }
}

class OxiMouse {
  main: OxiAPP;
  isDown: boolean;
  moveWithDown: Event;
  down: any;
  lastMouse: any = new THREE.Vector2();


  constructor(main) {
    this.isDown = false;
    this.main = main;
  }

  interPoint(ev) {
    let _slider = this.main.gl.domElement,
      rect = _slider.getBoundingClientRect(),
      canvasW = _slider.clientWidth,
      canvasH = _slider.clientHeight,

      _x = (ev ? ev.clientX : canvasW / 2) - rect.left,
      _y = (ev ? ev.clientY : canvasH / 2) - rect.top
    ;

    if (ev && ev.touches) {
      let firstFing = ev.touches.length ? ev.touches[0] : ev.changedTouches[0];
      _x = firstFing.clientX;
      _y = firstFing.clientY;
    }
    this.lastMouse.x = _x;
    this.lastMouse.y = _y;
    return new THREE.Vector2(((_x) / canvasW) * 2 - 1, -((_y) / canvasH) * 2 + 1);
  }

  cumulativeOffset(element) {
    var top = 0, left = 0;
    do {
      top += element.offsetTop || 0;
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);

    return {
      offsetLeft: top,
      offsetTop: left
    };
  }
}

class OxiAnimation {
  private canAnimate: boolean = false;
  private isStop: boolean = false;
  private lastUpdate: number = Date.now();
  private maxTimeUpdate: number = 1500;
  private id: number = Date.now();
  private animations: Array<any> = [];
  private lastIter: number = 0;
  private app: OxiAPP;

  constructor(main) {
    this.app = main;
    this.play();
    setTimeout(() => {
      this.animate();
    }, 100);


  }

  add(callback: Function, id: number = Date.now()) {
    this.animations.push({cb: callback, id: id});
  }

  remove(id) {
    for (let i = 0; i < this.animations.length; i++) {
      if (this.animations[i].id == id) {
        return this.animations.splice(i, 1);
      }
    }
  }

  animate() {
    // console.log('animation');
    requestAnimationFrame(() => {
      this.animate();
    });
    if (!this.app.gl.domElement.width || this.isStop) {
      return;
    }
    for (let i = 0; i < this.animations.length; i++) {
      this.animations[i].cb();
    }

    if (this.canAnimate) {
      this.canAnimate = this.lastUpdate > Date.now();
      if (!this.canAnimate || this.lastIter > 2) {
        this.lastIter = 0;
      }
      this.app.render();
    }
    let _p: any = this.app._parent();
    if (!_p.canResize && _p.clientWidth != _p.lastClientWidth) {
      _p.canResize = true;
      setTimeout(() => {
        _p.canResize = false;
        _p.lastClientWidth = _p.clientWidth;
        this.app._events.onWindowResize();
        this.app._projControls.show({}, false);
      }, 100)

    }


  }

  play(flag: boolean = true) {
    this.lastUpdate = Date.now() + (this.maxTimeUpdate);
    if (this.canAnimate) {
      return;
    }
    this.canAnimate = flag || !Pace.running;
    //this.animate();
  }

  pause() {
    this.canAnimate = false;
  }

  stop() {
    this.isStop = true;
    this.canAnimate = false;
    this.lastIter = 0;
  }
}

class OxiSlider {
  currentFrame: any = {};
  currentAlignFrame: any = {};
  private currentPagination: any = {};
  container: HTMLElement;
  alignImgContainer: HTMLElement;
  imgPagination: HTMLElement;
  app: OxiAPP;
  private canEdit: boolean = false;
  isDebug: boolean = false;
  isLoaded: boolean = true;
  onFinish: any;

  constructor(app: OxiAPP) {

    this.canEdit = app.main.selected.canEdit;
    this.app = app;
    this.addFrames();
    if (this.canEdit) {
      this.addAlignImg();
    }

  }

  addFrames() {
    let _self = this,
      app: OxiAPP = this.app;

    [this.container, this.imgPagination].forEach((domEl) => {
      this.removeNode(domEl);
    });


    let div = this.container = document.createElement('div'),
      imgPagination = this.imgPagination = document.createElement('ul'),
      _selected = app.main.selected,
      _resol = _selected.camera.resolution,
      __images = _selected.images,
      _px = 'px',
      canEdit = this.canEdit;

    if (!__images || !__images.length) {
      return;
    }
    imgPagination.className = 'img-pagination ' + (__images.length > 1 ? '' : 'hidden');

    this.app.updateAngle(__images.length);
    for (let i = 0; i < __images.length; i++) {
      let img = document.createElement('img'),
        curImg = __images[i], countOfErrors = 0;

      img.onerror = function () {
        if (countOfErrors++ < 2) {
          img.src = ENTITY.Config.PROJ_LOC + _selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER + ENTITY.Config.FILE.DIR.PROJECT_PREVIEW + curImg;
        } else {
          img.src = ENTITY.Config.FILE.DEFULT.NO_IMAGE;
        }
      }
      if (typeof curImg == 'string') {
        img.src = ENTITY.Config.PROJ_LOC + _selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER + ENTITY.Config.FILE.DIR.PROJECT_PREVIEW + app.imgType;
        if (app.imgType == ENTITY.Config.FILE.DIR.PREVIEW.WEBP) {
          let imgD = curImg.split('.');
          imgD.pop();
          img.src += imgD.join('.') + '.webp';
        } else {
          img.src += curImg;
        }
      } else {
        img.src = curImg.data;
      }

      if ((i) == _selected.currentItem) {
        img.className = ENTITY.ProjClasses.ACTIVE;
        this.currentFrame = img;
        _self.isLoaded = false;
        img.onload = function () {

          if (!_resol.x) {
            _resol.x = _self._W();
            _resol.y = _self._H();
          }
          _self.app._events.onWindowResize();
          //_self.onResize();
          _self.isLoaded = true;
          if (_self.onFinish) {
            _self.onFinish();
          }
        }
      }
      //if (_resol && _resol.x) {
      //    img.style.width = _resol.x + _px;
      //    img.style.height = _resol.y + _px;
      //}
      div.appendChild(img);

      if (canEdit) {
        let item = document.createElement('li');
        item.innerHTML = (+i + 1) + '';
        if (+i == _selected.currentItem) {
          item.className = ENTITY.ProjClasses.ACTIVE;
          this.currentPagination = item;
        }
        item.addEventListener(ENTITY.Config.EVENTS_NAME.CLICK, () => {
          _self.saveView(i);
        });
        imgPagination.appendChild(item);
      }
    }

    div.style.display = this.isDebug ? 'none' : '';
    div.className = [ENTITY.ProjClasses.CENTER_CONTAINER, ENTITY.ProjClasses.IMG_SLIDER].join(' ');
    app._container.appendChild(div);
    if (canEdit) {
      app._container.parentNode.appendChild(imgPagination);
      //imgPagination.style.bottom = -imgPagination.clientHeight + 'px';
    }
  }

  saveView(i: number, next: any = null) {
    let _self = this,
      app: OxiAPP = this.app,
      _selected = app.main.selected;
    if (typeof i == 'undefined' || i < 0) {
      i = _selected.currentItem;
    }

    let saveD = () => {
        let _c = app.camera.position,
          _t = app.controls.target;
        _selected.camera.frameState[_selected.currentItem] = {
          x: _c.x,
          y: _c.y,
          z: _c.z,
          target: {x: _t.x, y: _t.y, z: _t.z}
        };
      },
      anyway = () => {
        this.updateView(i);
        this.app.dataSave();
        if (next) {
          setTimeout(next, 500);
        }
      };

    if (!_selected.camera.frameState[_selected.currentItem]) {
      saveD();
      anyway();
    } else if (_selected.camera.frameState[_selected.currentItem].hasChanges) {
      delete _selected.camera.frameState[_selected.currentItem].hasChanges;
      if (_selected.camera.isSVG) {
        anyway();
      } else {
        alertify.confirm('The data view for current (' + (+_selected.currentItem + 1) + ') has been changed, accept to save, if cancel will lose',
          () => {
            saveD();
            anyway();
          }, () => {
            //delete _selected.camera.frameState[_selected.currentItem];
            anyway();
          });
      }

    } else {
      anyway();
    }

  }


  onResize() {
    let
      val = this.app.main.selected.camera.resolution,
      _px = 'px',
      elem: any = [this.container.childNodes];

    if (!elem[0] || !elem[0].length) {
      return;
    }
    if (this.alignImgContainer instanceof Node) {
      let el = this.alignImgContainer.childNodes;
      if (el && el.length) {
        elem.push(el);
      }
    }
    elem.forEach(function (lstChilds) {
      [].forEach.call(lstChilds, function (el) {
        el.style.height = val.y + _px;
        el.style.width = val.x + _px;
      });
    });
  }

  addAlignImg() {

    this.removeNode(this.alignImgContainer);
    if (!this.canEdit) {
      return;
    }

    let div = this.alignImgContainer = document.createElement('div'),
      _selected = this.app.main.selected,
      _resol = _selected.camera.resolution,
      _px = 'px';

    for (let i = 0, arr = _selected.alignImages, _length = arr.length; i < _length; i++) {
      let img = document.createElement('img'),
        curImg = arr[i];
      img.src = typeof curImg == 'string' ? ENTITY.Config.PROJ_LOC + _selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER + ENTITY.Config.FILE.DIR.PROJECT_ALIGN_IMG + curImg : curImg.data;
      if (i == _selected.currentItem) {
        img.className = ENTITY.ProjClasses.ACTIVE;
        this.currentAlignFrame = img;
      }
      //if (_resol && _resol.x) {
      //    img.style.width = _resol.x + _px;
      //    img.style.height = _resol.y + _px;
      //}
      div.appendChild(img);
    }
    div.style.display = this.isDebug ? '' : 'none';
    div.className = [ENTITY.ProjClasses.CENTER_CONTAINER, ENTITY.ProjClasses.IMG_SLIDER].join(' ');
    this.app._container.appendChild(div);

  }

  toggleDebug(state) {
    this.isDebug = !state;
    this.alignImgContainer.style.display = this.isDebug ? '' : 'none';
    this.container.style.display = !this.isDebug ? '' : 'none';
  }

  private removeNode(domEl: any) {
    if (!domEl) {
      return;
    }
    while (domEl.firstChild) {
      domEl.removeChild(domEl.firstChild);
    }
    if (domEl.parentNode) {
      domEl.parentNode.removeChild(domEl);
    }
  }

  updateView(selectedItem) {
    let prevItem = this.app.main.selected.currentItem;
    if (prevItem === selectedItem) {
      return;
    }
    this.currentFrame['className'] = this.currentAlignFrame['className'] = this.currentPagination['className'] = '';
    this.app.main.selected.currentItem = selectedItem;
    this.app.camera.updateView(selectedItem - this.app.main.selected.currentItem0);
    if (this.app.main.svgEl) {
      this.app.main.svgEl.updateView(prevItem);
    }

    this.currentFrame = this.container.childNodes[selectedItem];
    this.currentPagination = this.imgPagination && this.imgPagination.childNodes[selectedItem] ? this.imgPagination.childNodes[selectedItem] : {};
    this.currentAlignFrame = this.alignImgContainer && this.alignImgContainer.childNodes[selectedItem] ? this.alignImgContainer.childNodes[selectedItem] : {};
    this.currentFrame['className'] = this.currentAlignFrame['className'] = this.currentPagination['className'] = ENTITY.ProjClasses.ACTIVE;
    //this.app._events.onMouseOut({});
    this.app._projControls.show({}, false);
  }

  move(next: number) {
    if (next < 0) {
      if (this.app.main.selected.currentItem < 1) {
        return this.updateView(this.app.main.selected.images.length - 1);
      }
    } else {
      if (this.app.main.selected.currentItem >= this.app.main.selected.images.length - 1) {
        return this.updateView(0);
      }
    }
    this.updateView(+this.app.main.selected.currentItem + next);
  }

  _W() {
    return this.currentFrame.clientWidth || this.currentFrame.width || this.container.clientWidth /*|| this.app.main.selected.camera.resolution.x*/ || this.app.main.projCnt['nativeElement'].clientWidth || this.app.screen.width;
  }

  _H() {
    return this.currentFrame.clientHeight || this.currentFrame.height || this.container.clientHeight /*|| this.app.main.selected.camera.resolution.y*/ || this.app.main.projCnt['nativeElement'].clientHeight || this.app.screen.height;
  }

  _offsetLeft() {
    return this.container.offsetLeft || this.app._container.offsetLeft;
  }

  _offsetTop() {
    return this.container.offsetTop || this.app._container.offsetTop;
  }
}

class OxiControls {
  //private app:OxiAPP;
  private controls: HTMLElement;
  private _tooltips: HTMLElement;
  private objEdit: any
  kompas: any;

  constructor(private app: OxiAPP) {
    let
      _self = this,
      div: any = this.controls = document.createElement('div'),
      _div: any = document.querySelector(app.TEMPLATES.CONTROLS + ' .' + ENTITY.ProjClasses.PROJ_CONTROLS_MOVE),
      _backArea: any = document.querySelector(app.TEMPLATES.CONTROLS + ' .' + ENTITY.ProjClasses.PROJ_BACK_AREA),
      kompass: any = document.querySelector(app.TEMPLATES.CONTROLS + ' .' + ENTITY.ProjClasses.PROJ_COMPASS),
      attrCntControls = ENTITY.Config.DOM_ATTR.CNTX_CNTRLS
    ;
    if (_div) {
      _div.style.display = 'none';
    }
    if (_backArea) {
      _backArea.style.display = 'none';
    }

    if (app.main.selected.canEdit) {
      div.className = ENTITY.ProjClasses.PROJ_CONTROLS;

      app._parent().appendChild(div);
      let
        actinP = this.app.main.actionPopUp,
        getSelected = () => {
          return app.main.selected.camera.isSVG ? app.main.svgEl.lastSelectedShape : (this.app._events.lastInter ? this.app._events.lastInter.object : false);
        },
        childSelected = (child: any) => {
          let _elem = getSelected(),
            hasDataSource = true,
            _name = (child.name || '').trim();
          _elem._data = child;
          _elem._data._modelFromeScene = _elem;
          child._id = _elem.name || _elem.id || '';
          if (!child.name) {
            child.name = child._id.toUpperCase();
          }
          child._id += Date.now();

          if (!this.app.main.selected.areas) {
            this.app.main.selected.areas = [child];
          } else {
            for (let i = 0, areas = app.main.selected.areas; i < areas.length; i++) {
              if (areas[i].name == _name) {
                hasDataSource = false;
                break;
              }
            }
            this.app.main.selected.areas.push(child);
          }


          console.log('');
          if ((hasDataSource || (_elem._data.dataSourceGeneratedId && !_elem._data.dataSourceId)) && _name && !app.main.selected.camera.isSVG) {
            let _matches = [_name];
            app.dataSourceMatch(null, _matches, (source) => {
              _elem._dataSource = source;
              _elem._data.dataSourceGeneratedId = _elem._dataSource._id;
            });
          }
          return _elem
        },
        removeChild = () => {
          let _elem = getSelected();
          for (let i = 0, areas = app.main.selected.areas; i < areas.length; i++) {
            if (areas[i]._id == _elem._data._id) {
              areas.splice(i, 1);
              break;
            }
          }
          if (_elem._data && _elem._data._modelFromeScene && _elem._data._modelFromeScene._dataSource && _elem._data._modelFromeScene._dataSource.active) {
            _elem._data._modelFromeScene._dataSource.active = false;
          }
          if (_elem._data && _elem._data._modelFromeScene) {
            _elem._data._modelFromeScene = null;
          }
          _elem._data = null;
        };
      [

        {
          title: 'New level',
          className: 'attach-new none-ignore', click: (onFinish) => {
            let onChange = () => {
              alertify.confirm(actinP.formPopUp['nativeElement'], (e) => {
                childSelected(new ENTITY.ModelStructure({
                  name: (actinP.inputData.name || '').trim()
                }));
                onFinish();
              }, (e) => {
                alertify.warning('action was not attached');
              }).set({padding: true, maximizable: true, resizable: true});

            };
            let _elem = getSelected();
            actinP.resetInputs();
            actinP.inputData.name = _elem.name || _elem.id || _elem._id;

            if (_elem._data) {

              alertify.confirm('This area had already a structure (' + _elem._data.name + '), if ok will resave!!!',
                () => {
                  removeChild();
                  setTimeout(onChange, 100);
                }, () => {
                  onFinish();
                });
            } else {
              onChange();
            }

          }, icon: '../assets/img/ic_library_add_white_24px.svg'
        },
        {
          title: 'Link',
          className: 'attach-link none-ignore', click: (onFinish) => {
            let onChange = () => {
              alertify.confirm(actinP.formPopUp['nativeElement'], (e) => {
                childSelected(new ENTITY.LinkGeneralStructure({
                  destination: actinP.inputData.URL || '',
                  name: actinP.inputData.name || ''
                }));
                onFinish();
              }, (e) => {
                alertify.warning('link was not attached');
              }).set({padding: true, maximizable: true, resizable: true});

            };
            let _elem = getSelected();

            actinP.resetInputs({field: 'URL'});
            actinP.inputData.name = _elem.name || _elem.id || _elem._id;

            if (_elem._data) {

              alertify.confirm('This area had already a structure (' + _elem._data.name + '), if ok will resave!!!',
                () => {
                  removeChild();
                  setTimeout(onChange, 100);
                }, () => {
                  onFinish();
                });
            } else {
              onChange();
            }


          }, icon: '../assets/img/ic_link_white_24px.svg'
        },
        {
          title: 'JS snippet',
          className: 'attach-js none-ignore', click: (onFinish) => {

            let onChange = () => {
              alertify.confirm(actinP.formPopUp['nativeElement'], (e) => {
                childSelected(new ENTITY.GeneralStructure({
                  destination: actinP.inputData.jsCode || '',
                  name: actinP.inputData.name || ''
                }));
                onFinish();
              }, (e) => {
                alertify.warning('js code was not attached');
              }).set({padding: true, maximizable: true, resizable: true});
            };
            let _elem = getSelected();

            actinP.resetInputs({field: 'jsCode'});
            actinP.inputData.name = _elem.name || _elem.id || _elem._id;

            if (_elem._data) {

              alertify.confirm('This area had already a structure (' + _elem._data.name + '), if ok will resave!!!',
                () => {
                  removeChild();
                  setTimeout(onChange, 100);
                }, () => {
                  onFinish();
                });
            } else {
              onChange();
            }

          }, icon: '../assets/img/JS.svg'
        },
        {
          title: 'Close',
          className: 'cntrls-close', click: (onFinish) => {
            let _elem = getSelected();

            if (_elem._data) {

              alertify.confirm('This area had already a structure with name \'' + _elem._data.name + '\', if ok will remove!!!',
                () => {
                  removeChild();
                  if (app.main.selected.camera.isSVG) {
                    _elem.dropSelf();
                  }
                }, () => {
                  onFinish();
                });
            } else {
              if (app.main.selected.camera.isSVG) {
                _elem.dropSelf();
              }
              onFinish();
            }
          }, icon: '../assets/img/ic_close_white_24px.svg'
        },
        {
          title: 'Ignore / Enable',
          className: 'toggle-ignore', click: (onFinish) => {
            let _elem = getSelected();
            const key = this.app.main.selected.camera.isSVG ?'id':'name';
            const index = this.app.main.selected.camera.forceIgnores.indexOf(_elem[key]);
            if (index < 0) {
              this.app.main.selected.camera.forceIgnores.push(_elem[key]);
            } else {
              this.app.main.selected.camera.forceIgnores.splice(index, 1)
            }
            this.app.checkIgnore();
            onFinish();
          }, icon: ['../assets/img/round-add_circle_outline-24px.svg', '../assets/img/round-block-24px.svg']
        },
      ].forEach((el: any, item) => {
        let domEl = document.createElement('div');
        domEl.className = el.className;
        domEl.addEventListener(ENTITY.Config.EVENTS_NAME.CLICK, (e) => {
          this.app.cntxMenu = this.app.needsUpdateCntxMenu = false;
          let _el = getSelected();
          if (_el) {
            el.click(() => {
              this.show(e, false, el);
            });
          } else {
            this.show(e, false)
          }

          app.main.selected.hasChanges = app.main.selected._project.hasChanges = true;
        });
        div.appendChild(domEl);
        domEl.innerHTML = `<span class="temp-tooltip">${el.title}</span>`;
        if (Array.isArray(el.icon)) {
          el.icon.forEach((src, index) => {
            let icon = document.createElement('img');
            icon.src = src;
            icon.setAttribute('fillColor', '#ffffff');
            icon.setAttribute('fill', '#ffffff');
            icon.style.color = '#ffffff';
            if (index > 0) {
              icon.style.display = 'none';
            }

            domEl.appendChild(icon);
            if (!el.icons) {
              el.icons = [];
            }
            el.icons.push(icon);
          })
        } else {
          let icon = document.createElement('img');
          icon.src = el.icon;
          icon.setAttribute('fillColor', '#ffffff');
          icon.setAttribute('fill', '#ffffff');
          icon.style.color = '#ffffff';
          domEl.appendChild(icon);
        }

      });

      (function applyAttr(dom) {
        dom.setAttribute(attrCntControls, 1);
        if (dom.children && dom.children.length) {
          for (let i = 0; i < dom.children.length; i++) {
            applyAttr(dom.children[i]);
          }
        }
      })(div)

      app.model.traverse((child) => {
        if (child.type == 'Mesh' && child._dataSource) {
          child._toolTip = new OxiToolTip(child, app);
        }
      });
    } else {
      if (this.app.main.selected.images.length > 1) {


        let arrows = [{_c: 'left', _i: -1}, {_c: 'right', _i: 1}];
        if (_div) {
          div = _div;
          _div.style.display = '';
          for (let i = 0; i < _div.childNodes.length; i++) {
            let childDiv = _div.childNodes[i];
            for (let u = 0; u < arrows.length; u++) {
              if (childDiv.localName == 'div') {
                let dir = arrows.splice(u, 1)[0]._i;
                childDiv.addEventListener((this.app.isMobile ? ENTITY.Config.EVENTS_NAME.TOUCH_END : ENTITY.Config.EVENTS_NAME.CLICK), (e: Event) => {
                  this.app._slider.move(dir);
                });
                childDiv.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, this.app.onEventPrevent);
                break;
              }
            }

          }
        } else {
          div.className = ENTITY.ProjClasses.PROJ_CONTROLS_MOVE;
          app._container.appendChild(div);
          arrows.forEach((child) => {
            let childDiv = document.createElement('div');
            childDiv.className = child._c;
            childDiv.style.backgroundImage = 'url("assets/img/left_arrow.png")';
            div.appendChild(childDiv);
            childDiv.addEventListener((this.app.isMobile ? ENTITY.Config.EVENTS_NAME.TOUCH_END : ENTITY.Config.EVENTS_NAME.CLICK), (e: Event) => {
              this.app._slider.move(child._i);
            });
            childDiv.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, this.app.onEventPrevent);
          });
        }
      }
      div.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, this.app.onEventPrevent);

      app.model.traverse((child) => {
        if (child.type == 'Mesh') {
          //child.material.visible = false;
          child.material.opacity = 0;
          child._toolTip = new OxiToolTip(child, app);

        }
      });

      let path = this.app.main.location.path(),
        areas = path.split(ENTITY.Config.PROJ_DMNS[0]);
      if (areas.length > 1) {
        let back: any = _backArea;
        if (!back) {
          back = document.createElement('div');
          back.className = ENTITY.ProjClasses.PROJ_BACK_AREA;
          back.style.backgroundImage = 'url(\'assets/img/android-system-back.png\')';
          app._container.appendChild(back);
        } else {
          back.style.display = '';
        }
        areas.pop();
        back.addEventListener(ENTITY.Config.EVENTS_NAME.CLICK, (e) => {
          e.preventDefault();
          window.location.href = window.location.origin + (areas.length > 1 ? areas.join(ENTITY.Config.PROJ_DMNS[0]) : areas.join(''));
        });
        back.addEventListener(ENTITY.Config.EVENTS_NAME.CNTXMENU, (e) => app._events.onCntxMenu(e), false);

      }
    }
    div.addEventListener(ENTITY.Config.EVENTS_NAME.CNTXMENU, (e) => app._events.onCntxMenu(e), false);

    if (!kompass) {
      kompass = document.createElement('div');
      kompass.className = 'kompass';
      kompass.style.backgroundImage = 'url("assets/img/kompas.png")';
      app._container.appendChild(kompass);
    }
    this.kompas = kompass;
    kompass.onUpdate = () => {
      kompass.style.display = !_self.app.main.selected.camera.kompass.enabled ? 'none' : '';
      kompass.style.transform = 'rotate(' + (_self.app.main.selected.currentItem * app._angle + app.main.selected.camera.kompass.angle) + 'deg)';
    };
    kompass.onUpdate();
  }

  show(pos, flag: boolean = true, fl: boolean = true) {
    let canEdit = this.app.main.selected.canEdit;
    if (this.kompas) {
      this.kompas.onUpdate();
    }

    this.showTooltip(flag, fl);
    this.showControls(pos, flag);

    //this.app.updateInfoHTML();
  }

  showTooltip(flag: boolean = true, fl: boolean = true) {
    let _inter = this.app._events.lastInter;
    if (_inter) {
      _inter = _inter.object;
      this.updateTooltipInfo(_inter);
      //if (_inter._toolTip  )_inter._toolTip.show(flag);
      if (!_inter.material.defColor) {
        _inter.material.defColor = _inter.material.color.clone();
      }
      if (!_inter.material.onSelectColor) {
        _inter.material.onSelectColor = this.app.HOVER_COLOR.ACTIVE;
      }

      _inter.material.transparent = fl;
      //_inter.renderOrder = flag ? this.app.model.inters.length * 2 : _inter.defRenderOrder;
    }

    this.app._animation.play();
  }

  updateTooltipInfo(_inter, e = null, pos = null, z = 1) {

    let _t: TooltipHelper = this.app.main.tooltipHelp;
    _t.show = false;
    if (_t.lasHovered) {
      _t.lasHovered._isHovered = false;
    }
    if (!_inter || !_inter._data) {
      return;
    }
    let center = pos || this.app._events.mouse.lastMouse;
    _t.lasHovered = _inter._data;
    _t.show = _inter._data._isHovered = true;
    _t.left = center.x + 10 + 'px';
    _t.top = center.y + 10 + 'px';
    _t.text = _inter._data.name;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }

  }

  showControls(pos, flag: boolean = true) {
    let canEdit = this.app.main.selected.canEdit,
      _show = this.app.cntxMenu && pos;



    if (_show) {
      if (this.controls.className.indexOf(ENTITY.ProjClasses.ACTIVE) < 0) {
        this.controls.className += ' ' + ENTITY.ProjClasses.ACTIVE;
      }
    } else {
      this.objEdit = null;
      this.controls.className = this.controls.className.replace(' ' + ENTITY.ProjClasses.ACTIVE, '');
    }


    this.checkButtons();
    if (canEdit && _show && this.app.needsUpdateCntxMenu) {
      this.app.needsUpdateCntxMenu = false;
      //this.objEdit = this.app._events.lastInter.object;
      let _d: any = document.querySelector('app-aside');
      if (_d) {
        _d = _d.getBoundingClientRect();
      }
      this.controls.style.left = ((pos.clientX || pos.x) - 15 - (_d.right ? _d.right : 0)) + 'px';
      this.controls.style.top = ((pos.clientY || pos.y) - this.controls.clientHeight / 2 - 15) + 'px';

    } else {

    }

  }
  private checkButtons(){
    const list: any = this.controls.querySelectorAll('.none-ignore');
    let visibility = this.app.cntxMenu && this.app.cntxMenu.object && this.app.main.selected.camera.forceIgnores.indexOf(this.app.cntxMenu.object.name) < 0 ? '' : 'hidden';
    if(this.app.main.selected.camera.isSVG){
      visibility =  !this.app.main.svgEl|| !this.app.main.svgEl.lastSelectedShape || this.app.main.selected.camera.forceIgnores.indexOf(this.app.main.svgEl.lastSelectedShape.id) < 0 ? '' : 'hidden'
    }
    for (let i = 0; i < list.length; i++) {
      list[i].style.visibility = visibility;
    }
    const toggleIgnore: any = this.controls.querySelector('.toggle-ignore');
    toggleIgnore.children[1].style.display = visibility ? '' : 'none';
    toggleIgnore.children[2].style.display = visibility ? 'none' : '';
  }

  showAttachPopUp(elem) {
    if (!elem || !elem._data) {
      alertify.error('please create area for this element, on right click');
      return false;
    }
    let _mesh = elem,
      _listArr = this.app.main.preToolTip.dataElem,
      _dialog: any = new Dialog({title: 'Attach data source'}),
      _search = document.createElement('input'),
      _table = document.createElement('table'),
      _tbody: any = document.createElement('tbody'),
      _tthead = document.createElement('thead'),
      _tr = document.createElement('tr'),
      _tdId = document.createElement('th'),
      _tdAvailabel = document.createElement('th'),
      _tdAct = document.createElement('th');
    _dialog.body.style.display = '';
    _search.setAttribute('placeholder', 'serach by id');
    _search.addEventListener('input', () => {
      for (let i = 0; i < _tbody.childNodes.length; i++) {
        _tbody.childNodes[i].style.display = !_search.value || !_listArr[i]._id || _listArr[i]._id.toLowerCase().match(_search.value.toLowerCase()) ? '' : 'none';
      }
    });
    _tdId.innerText = 'Id';
    _tdAvailabel.innerText = 'Available/Sold/Total';
    _tdAct.innerText = 'Action';
    _tr.appendChild(_tdId);
    _tr.appendChild(_tdAvailabel);
    _tr.appendChild(_tdAct);
    _tthead.appendChild(_tr);
    _table.appendChild(_tthead);
    _table.appendChild(_tbody);
    _dialog.body.appendChild(_search);
    _dialog.body.appendChild(_table);
    this.app.dataSourceMatch(_listArr, null, (source) => {

      if (source.active) {
        return;
      }
      let d = document.createElement('tr'),
        _tdId: any = document.createElement('td'),
        _tdAvailabel = document.createElement('td'),
        _tdAct = document.createElement('td'),
        btn = document.createElement('button');
      btn.addEventListener('click', () => {
        if (_mesh._dataSource) {
          _mesh._dataSource.active = false;
        }
        _mesh._data.dataSourceId = source._id;
        _mesh._dataSource = source;
        _mesh._tooltip = new OxiToolTip(_mesh, this.app);
        _dialog.anyWay();
        this.app._animation.play();
      });
      btn.innerText = 'Attach';
      _tdId.innerText = source._id;

      ['available', 'sold', 'total'].forEach((un) => {
        if (source.label[un]) {
          _tdAvailabel.innerText += source.label[un] + '/';
          _tdId.style.fontWeight = 900;
        }
      });

      d.className = 'my-dialog';
      _tdAct.appendChild(btn);
      d.appendChild(_tdId);
      d.appendChild(_tdAvailabel);
      d.appendChild(_tdAct);
      _tbody.appendChild(d);
    });

    return true;
  }
}

export class OxiToolTip {
  tooltip: any;
  tooltipCnt: any;
  private mesh: any;
  private main: OxiAPP;
  private canEdit: boolean = false;
  private _id: number;

  constructor(mesh, main: OxiAPP) {
    let tooltip;
    let tooltipParent: any = main._preloaderStatus._tooltips;
    if (!tooltipParent) {
      tooltipParent = document.querySelector('.' + ENTITY.ProjClasses.PROJ_TOOLTIPS.CONTAINER);
      if (!tooltipParent) {
        tooltipParent = document.createElement('div');
        tooltipParent.className = ENTITY.ProjClasses.PROJ_TOOLTIPS.CONTAINER;
        main._parent().appendChild(tooltipParent);
      }
      main._preloaderStatus._tooltips = tooltipParent;
      tooltipParent.addEventListener(ENTITY.Config.EVENTS_NAME.CNTXMENU, (e) => main._events.onCntxMenu(e), false);
    }
    this.main = main;
    this.canEdit = main.main.selected.canEdit;
    if (mesh._tooltip) {
      for (let i = 0; i < main.infoHTML.length; i++) {
        if (main.infoHTML[i]._id == mesh._tooltip._id) {
          main.infoHTML.splice(i, 1);
          break;
        }
      }
    }
    this._id = Date.now() * Math.random();
    main.infoHTML.push(this);
    if (mesh._dataSource && !mesh._data.dataSourceId && mesh._data.dataSourceGeneratedId === mesh._dataSource._id) {

    } else if (mesh._dataSource) {
      mesh._dataSource.active = true;
      if (mesh._data) {
        mesh._data.dataSourceId = mesh._dataSource._id;
      }
    } else {

      tooltip = document.createElement('div');
      let
        tooltCnt: any = document.createElement('div'),
        head: any = document.createElement('div'),
        spanHead: any = document.createElement('span'),
        spanBody: any = document.createElement('div'),
        body: any = document.createElement('div');
      body.appendChild(spanBody);
      head.appendChild(spanHead);
      tooltCnt.appendChild(head);
      tooltCnt.appendChild(document.createElement('hr'));
      tooltCnt.appendChild(body);
      tooltip.appendChild(tooltCnt);
      tooltip.className = 'cos-info active';
      tooltCnt.className = 'cos-tooltip';
      body.className = 'cos-tooltip-body';
      spanBody.className = 'cos-tooltip-body-title';
      head.className = 'cos-tooltip-header';
      spanBody.innerHTML = mesh._data ? mesh._data.name : mesh.name;
      spanHead.innerHTML = mesh.name;


      //tooltip.innerHTML += '<div class="cos-label"><span>0</span></div>';
      let sp, ps;
      /*if (mesh._data && mesh._data.areas) {
       sp = document.createElement('div');
       sp.className = 'cos-label';
       ps = document.createElement('span');
       sp.appendChild(ps);
       ps.innerText = mesh._data.areas.length;
       tooltip.appendChild(sp);
       }*/
      this.tooltip = tooltip;
      this.tooltipCnt = tooltCnt;

      [tooltip, tooltCnt, sp, ps].forEach((e) => {
        if (e) {
          e.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, main.onEventPrevent);
        }
      });
    }
    this.mesh = mesh;
    if (!mesh.material) {
      mesh.material = {};
    }
    mesh.material.onSelectColor = main.HOVER_COLOR.SOLGHT;
    if (!main.main.selected.canEdit) {
      if (mesh._data || mesh._dataSource) {
        if (mesh._data && mesh._data._category == ENTITY.Config.PROJ_DESTINATION.ModelStructure) {
          mesh.material.onSelectColor = main.HOVER_COLOR.ACTIVE;
        }
        mesh.click = () => {
          if (mesh._data) {
            switch (mesh._data._category) {
              case ENTITY.Config.PROJ_DESTINATION.ModelStructure: {
                let _url = mesh._data.projFilesDirname.split('/');
                window.location.href += '&area=' + _url[_url.length - 1];
                break;
              }
              case ENTITY.Config.PROJ_DESTINATION.LinkGeneralStructure: {
                window.open(mesh._data.destination);
                break;
              }
              case ENTITY.Config.PROJ_DESTINATION.GeneralStructure: {
                try {
                  main.main.authServ.safeJS(mesh._data.destination)();
                } catch (e) {
                }

                break;
              }
            }
          } else {
            console.log('has to attach data');
          }

        };
        if (mesh._dataSource) {

          if (mesh._dataSource.URL && mesh._dataSource.URL.match(ENTITY.Config.PATTERNS.URL)) {
            mesh.click = () => {
              window.open(mesh._dataSource.URL);
            }
            mesh._dataSource.onclick = mesh.click;
            if (tooltip) {
              tooltip.addEventListener(ENTITY.Config.EVENTS_NAME.CLICK, (e) => mesh.click());
            }
            mesh.material.onSelectColor = main.HOVER_COLOR.ACTIVE;
          } else if (!mesh._dataSource.sold) {
            mesh.material.onSelectColor = main.HOVER_COLOR.ACTIVE;
          }
          mesh._dataSource.onclick = mesh.click;
        } else {
          tooltip.addEventListener(ENTITY.Config.EVENTS_NAME.CLICK, (e) => mesh.click());
        }
      }
    }

    if (!mesh._dataSource) {
      tooltipParent.appendChild(tooltip);
    }
    this.update();

  }

  show(show: boolean = true) {
    //this.mesh.material.visible = show || this.canEdit;
    this.main.gl.domElement.style.cursor = show ? this.mesh.click ? ENTITY.Config.CURSOR.POINTER : ENTITY.Config.CURSOR.UN_AVAILABLE : ENTITY.Config.CURSOR.DEFAULT;
    if (this.mesh.tween) {
      if (this.mesh.tween.show == show) {
        return;
      } else {
        this.mesh.tween.stop();
      }
    }
    let maxOp = this.mesh.material.opacity,
      _maxOpc = this.main.main.selected.camera.opacity,
      endO = (show || this.canEdit) ? _maxOpc : 0;
    this.mesh.tween = new TWEEN.Tween({delta: 0}).to({delta: 1}, 400)
    //.easing(TWEEN.Easing.Exponential.In)
      .onStart(() => {

        if (show) {
          this.mesh.material.color = this.mesh.material.onSelectColor;
          //this.main.outlinePass.selectedObjects = [this.mesh];
        }
      })
      .onUpdate((delta) => {
        this.mesh.material.opacity = endO == 0 ? (maxOp - delta) : delta * endO;
        //this.main.outlinePass.edgeStrength =  this.mesh.material.opacity*5 ;
      })
      .onComplete(() => {
        this.mesh.material.color = show ? this.mesh.material.onSelectColor : this.mesh.material.defColor;

        //if(!show) this.main.outlinePass.selectedObjects=[];
        //this.mesh.tween.stop();
        //this.mesh.tween = null;
        //this.mesh.material.depthTest =!show;
        //this.main.gl.setDepthTest(false);

      })
      .start();
    this.mesh.tween.show = show;

    this.display(show);
  }

  display(show) {
    if (this.tooltip) {

      this.tooltip.className = show ? 'cos-info active act' : 'cos-info active';
      this.tooltipCnt.className = show ? 'cos-tooltip active' : 'cos-tooltip';
    } else {
      this.mesh._dataSource.tooltip.active = !!show;
    }
    this.update();
  }

  update() {
    this.mesh.getScreenPst();

    if (this.tooltip) {
      this.tooltip.style.left = this.mesh.onscreenParams.x + 'px';
      this.tooltip.style.top = this.mesh.onscreenParams.y + 'px';
      this.tooltip.className += this.checkPst(this.mesh.onscreenParams.x, this.mesh.onscreenParams.y);
    } else {

      this.mesh._dataSource._left = (this.mesh.onscreenParams.x - this.mesh.onscreenOffset.left) + 'px';
      this.mesh._dataSource._top = (this.mesh.onscreenParams.y - this.mesh.onscreenOffset.top) + 'px';
      this.checkPst(this.mesh.onscreenParams.x, this.mesh.onscreenParams.y, this.mesh._dataSource);
    }
  }

  remove() {

  }

  private checkPst(x, y, dataSource = null) {
    if (!this.main._slider) {
      return '';
    }
    let minX = 0, minY = 0, maxX = this.main._slider._W(), maxY = this.main._slider._H(), tootlipWidth = 280, tooltipHeight = 160,
      classes = [' tooltip-align-left', ' tooltip-align-right', ' tooltip-align-bottom'];
    if (dataSource) {
      dataSource.alignLeft = dataSource.alignRight = dataSource.alignBottom = false;
    }
    if (x + tootlipWidth > maxX) {
      if (dataSource) {
        dataSource.alignLeft = true;
      }
      return classes[0];
    } else if (x - tootlipWidth < minX) {
      if (dataSource) {
        dataSource.alignRight = true;
      }
      return classes[1];
    } else if (y - tooltipHeight < minY) {
      if (dataSource) {
        dataSource.alignBottom = true;
      }
      return classes[2];
    }
    return '';
  }

  private htmlToElement(html) {
    let template = document.createElement('template') as any;
    template.innerHTML = html;
    return template.content.firstChild;
  }


}
