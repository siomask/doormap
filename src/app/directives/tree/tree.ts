import {
    Component,
    Input,
    //CORE_DIRECTIVES,
    EventEmitter,
    Output,
    ViewChild,
    OnInit
} from  '@angular/core';

@Component({
    selector: 'node',
    template: `
<li [ngClass]="{'last-item':lastE}">

	<div class ="iconButton" style="display: flex;" (click)="select(item )" [ngClass]="(item._selected?classes+' active':classes)+(item._isHovered?' hovered':'')"  #iconBtn>
	    <a   >{{item.name}}</a>
	    <div class="pop-up-icon" [class.pop-up-icon-active]="showPopUp"(click)="togglePoUp($event)">
          <i class="material-icons set-icon" >more_vert</i>
        </div>

         <div class="pop-up-icon" *ngIf="item.dataSourceId" (mouseover)="showTPopUp = true"  (mouseout)="showTPopUp = false">
            <i class="material-icons set-icon" >help</i>
        </div>
        <div class="pop-up-icon" *ngIf="(item.dataSourceGeneratedId && !item.dataSourceId)" (mouseover)="showGPopUp = true"  (mouseout)="showGPopUp = false">
            <i class="material-icons -icon" >label</i>
        </div>
	</div>
	<div *ngIf="!arrow" class="left-arrow" [ngClass]="lastE?'end':''"></div>




	    <div class="pop-up bla-t" [hidden]="!showPopUp" *ngIf="showPopUp" (click)="showPopUp = !showPopUp" (window:mouseup)="showPopUp = !showPopUp">
            <div class="pop-up-item"  *ngIf="item.areas && item.areas.length" (mousedown)="onWindowMouseDown($event,1)">
              <i class="material-icons">visibility</i>
              <div class="pop-up-row-name">
                <span>{{IsExpanded?"Hide":"Expand"}}</span>
              </div>
            </div>
            <div class="pop-up-item" (mousedown)="onWindowMouseDown($event,2)" *ngIf="item._id != parent._id">
              <i class="material-icons">delete</i>
              <div class="pop-up-row-name">
                <span>Delete</span>
              </div>
            </div>
        </div>

         <div class="pop-up bla-t"  *ngIf="showTPopUp"   (mouseout)="showTPopUp = false">
            <div class="pop-up-item"  >
             Has data source with id {{item.dataSourceId}}
            </div>
        </div>
        <div class="pop-up bla-t"  *ngIf="showGPopUp"   (mouseout)="showTPopUp = false">
            <div class="pop-up-item"  >
                Has generated data source with id {{item.dataSourceGeneratedId}}
            </div>
        </div>
    
        <div *ngIf="!arrow && lastE" class="left-arrow end-list"></div>
	<div *ngIf="IsExpanded">
        <ul *ngIf="item.areas" class="tree-webgl-view">
              <node  *ngFor="let subitem of item.areas; let itT = index"  [_iter]="itT" [classes]="subitem._category===0?'js-code':subitem._category==1?'link':'' " [mainParent]="mainParent"  [parent]="item" [item]="subitem" [lastE]="itT == item.areas.length-1"></node>
        </ul>
	</div>
	<div *ngIf="!arrow"  class="clear"></div>
</li>
`
})
export class MNode {
    @Input() classes:any;
    @Input() lastE:boolean;
    @Input() arrow:any;
    @Input() item:any;
    @Input() parent:any;
    @Input() mainParent:any;
    @Input() _iter:number;
    @ViewChild("iconBtn")
        iconBtn:HTMLElement;
    IsExpanded:boolean = true;
    showPopUp:boolean = false;
    showTPopUp:boolean = false;
    showGPopUp:boolean = false;

    constructor(){
        console.log();
    }
    onWindowMouseDown(ev, type) {
        ev.preventDefault();
        if (type == 1) {
            this.IsExpanded = !this.IsExpanded;
        } else {
            this.deleteItem();
        }
        return false;
    }

    private togglePoUp(e) {
        e.stopPropagation();
        e.preventDefault();
        this.showPopUp = !this.showPopUp;
        return false;
    }

    select(item) {
        this.mainParent.select(item);
        this.showPopUp = false;
    }

    deleteItem() {
        let itemDroped = this.parent.areas.splice(this._iter, 1)[0];
        if (this.mainParent.selectedChild.glApp){
            this.mainParent.selectedChild.glApp._deleteArea(itemDroped);
              this.mainParent.selectedChild.glApp.updateData('test');
        }
        /*if (this.mainParent.selectedChild._id === itemDroped._id)*/this.mainParent.select(this.parent);
    }

}

@Component({
    selector: 'tree',
    template: `
<ul class="tree-webgl-view first"    >
		<node *ngFor="let item of data" [arrow]="1" [classes]="'main'" [parent]="item" [mainParent]="mainParent" [item]="item"     ></node>
</ul>
`,
    styleUrls: ['./tree.sass']
})
export class MTree {
    @Input() data:any[];
    @Input() mainParent:any;

}