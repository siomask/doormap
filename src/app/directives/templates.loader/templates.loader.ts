import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import * as ENTITY from '../../entities/entities';
import {AuthService, MyHTTP} from '../../services/auth.service';
import {DomSanitizer, SafeResourceUrl,} from '@angular/platform-browser';


declare var alertify: any;

@Component({
  selector: 'app-template-loader',
  template: '<p></p>'
})
export class TemplatesLoader implements OnInit, OnDestroy {
  @Input() parent: any;
  @Input() model: any;
  @Input() templateType: any;
  @Input() htmlTemplates: any;
  project: any;
  cssUrl: any;
  htmlTemplate: any;
  jsTemplate: any;
  dsTemplate: any;
  callbacks: Array<Function> = [];
  private cssElement: any;

  constructor() {
    let
      cssId = ('cssInject' + ENTITY.Config.randomInteger()),
      cssEl = document.getElementById(cssId);
    if (!cssEl) {
      cssEl = document.createElement('style');
      cssEl.id = cssId;
      cssEl.setAttribute('type', 'text/css');
      document.head.appendChild(cssEl);
    }
    this.cssElement = cssEl;
  }

  ngOnInit() {
    let model = this.model,
      _DIR = ENTITY.Config.FILE.DIR;
    if (!model || isNaN(this.templateType) || this.htmlTemplates) {
      return;
    }

    let isTooltip = this.templateType == 1;
    //console.log(this.parent);
    TemplatesLoader.loadTemplated({
      templateType: _DIR.PROJECT_TEMPLATE.TYPES[this.templateType],
      model: model,
      project: this.project,
      callback: (res: any, tab: any) => {
        this[tab._f] = res._body;
        if (tab._f == 'cssUrl') {
          this.updateCss(res._body);
        }
        if (this['cssUrl'] && this['htmlTemplate']) {
          if (isTooltip && !this['jsTemplate']) {
            return;
          }
          for (let i = 0; i < this.callbacks.length; i++) {
            this.callbacks.shift()();
          }
          if (this.parent) {
            this.parent.update();
          }
        }
      }
    });
  }

  updateCss(value) {
    // console.log('updateCss', value);
    if (value) {
      this.cssElement.innerText = value;
    }
  }

  ngOnDestroy() {
    this.cssElement.parentNode.removeChild(this.cssElement);
  }

  static loadTemplated(opt) {

    // console.log('loadTemplated', opt)
    let
      _DIR = ENTITY.Config.FILE.DIR,
      model = opt.model,
      project = opt.project,
      templatesUrls: any = [
        {link: null, _f: 'cssUrl', fileName: _DIR.PROJECT_TEMPLATE.CSS},
        {link: null, _f: 'htmlTemplate', fileName: _DIR.PROJECT_TEMPLATE.HTML},
        {link: null, _f: 'jsTemplate', fileName: _DIR.PROJECT_TEMPLATE.JS},
        {link: null, _f: 'dsTemplate', fileName: _DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE}
      ],
      settings,
      updateLinks = (list, _template, isTooltip, arrayOfIndexes: Array<Number> = null) => {
        for (let i = 0; i < list.length; i++) {
          if (arrayOfIndexes && !arrayOfIndexes[i]) {
            continue;
          }
          list[i].link = _template + list[i].fileName;
          if (i >= 2 && !isTooltip) {
            list[i].link = null
          }
        }
        return list;
      },
      onLoadTemp = function (templateType, template, next) {
        let list = templatesUrls.concat([]),
          isTooltip = templateType == _DIR.PROJECT_TEMPLATE.TYPES[1],
          checkSett = (arrayOfIndexes = null) => {
            let onFinish = () => {
              if (settings && settings.settings && settings.settings.status) {
                list = updateLinks(list, ENTITY.Config.SETTING_TEMPLET_LOC + settings.settings.data.link + templateType, isTooltip, arrayOfIndexes);
              }
              next(list);
            }
            if (settings) {
              onFinish();
            } else {
              if (project || opt.user || model._id) {
                let req: any = {};
                if (opt.user) {
                  req.userId = opt.user._id;
                } else if (project) {
                  req.userId = project.owner && project.owner._id ? project.owner._id : project.owner;
                  req.projId = project._id;
                } else {
                  req.projId = model._id;
                }
                MyHTTP.posts('public/project/settings', req).subscribe((res) => {
                  settings = res.json();
                  onFinish();
                });
              } else {

                console.warn('can`t get any parent');
                onFinish();

              }

            }
          };
        if (project && project.model && project.model.useGlobals &&
          project.model.useGlobals[templateType.split('/')[0]].filter((a) => {
            return a;
          }).length > 0) {


          let parent_link = project.model.linkGlobal;
          let arr1 = project.model.useGlobals[templateType.split('/')[0]].slice();
          let arr2 = [];
          arr1.forEach((a) => {
            arr2.push(!a);
          });
          list = updateLinks(list, ENTITY.Config.SETTING_TEMPLET_LOC + parent_link.split('/')[0] + _DIR.DELIMETER + template.split('/')[2] + _DIR.DELIMETER, isTooltip, arr1);
          if (model && model.templates && model.templates.length) {
            next(updateLinks(list, ENTITY.Config.PROJ_LOC + model.projFilesDirname.split('/')[0] + _DIR.DELIMETER + template.replace('assets/', ''), isTooltip, arr2));
          } else {
            checkSett(arr2);
          }


        } else if (model && model.templates && model.templates.length) {
          next(updateLinks(list, ENTITY.Config.PROJ_LOC + model.projFilesDirname.split('/')[0] + _DIR.DELIMETER + template.replace('assets/', ''), isTooltip));
        } else if (opt.user) {
          if (!opt.user.settings || !opt.user.settings.link) {
            return checkSett();
          } else {
            next(updateLinks(list, ENTITY.Config.SETTING_TEMPLET_LOC + opt.user.settings.link + templateType, isTooltip));
          }
        } else {
          checkSett();
        }
      };
    // opt.templateType = false;
    if (opt.templateType && opt.configID) {
      let _template = _DIR.PROJECT_TEMPLATE.NAME + opt.templateType;
      templatesUrls = updateLinks(templatesUrls, _template, opt.templateType == _DIR.PROJECT_TEMPLATE.TYPES[1]);
      onLoadTemp(opt.templateType, _template, (list: any) => {
        let i = 0;
        (function loadTemp() {
          if (i >= list.length) {
            return;
          }
          if (!list[i].link) {
            i += 1;
            return loadTemp();
          }
          if (i != opt.configID) {
            i += 1;
            return loadTemp();
          }
          i += 1;
          MyHTTP.getS(list[i - 1].link).subscribe((res) => {
            if (opt._i !== undefined) {
              opt.callback(res, list[i - 1], i - 1, opt._i);
            } else {
              opt.callback(res, list[i - 1]);
            }
            if (i < list.length) {
              loadTemp();
            }
          });
        })()
      });
    } else if (opt.templateType) {
      let _template = _DIR.PROJECT_TEMPLATE.NAME + opt.templateType;
      templatesUrls = updateLinks(templatesUrls, _template, opt.templateType == _DIR.PROJECT_TEMPLATE.TYPES[1]);
      onLoadTemp(opt.templateType, _template, (list) => {
        let i = 0;
        (function loadTemp() {
          if (i >= list.length) {
            return;
          }
          if (!list[i++].link) {
            return loadTemp();
          }
          MyHTTP.getS(list[i - 1].link).subscribe((res) => {
            if (opt._i !== undefined) {
              opt.callback(res, list[i - 1], i - 1, opt._i);
            } else {
              opt.callback(res, list[i - 1]);
            }
            if (i < list.length) {
              loadTemp();
            }
          });
        })()
      });
    } else {
      let u = 0;
      (function checkUrls() {
        if (u >= _DIR.PROJECT_TEMPLATE.TYPES.length) {
          if (opt.onFinish) {
            opt.onFinish(settings);
          }
          return;
        }
        let _template = _DIR.PROJECT_TEMPLATE.NAME + _DIR.PROJECT_TEMPLATE.TYPES[u++];
        templatesUrls = updateLinks(templatesUrls, _template, (u - 1) == 1);
        onLoadTemp(_DIR.PROJECT_TEMPLATE.TYPES[u - 1], _template, (list) => {
          let i = 0;
          (function loadTemp() {
            if (i >= list.length) {
              return checkUrls();
            }
            if (!list[i++].link) {
              return loadTemp();
            }
            // console.log(list[i - 1].link);
            MyHTTP.getS(list[i - 1].link).subscribe((res) => {
              opt.callback(res, list[i - 1], i - 1, u - 1);
              if (i < list.length) {
                loadTemp();
              } else {
                checkUrls();
              }
            });
          })()
        });
      })();
    }

  }
}
