import {ViewChild, Component, OnInit, OnChanges, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import {DomSanitizer, SafeResourceUrl,} from '@angular/platform-browser';
import {UserService, AuthService} from '../../services/services';
import {Config} from '../../entities/constant.data';
import * as ENTITY from '../../entities/entities';
import {WControls, Preloader, WTooltip, TemplatesLoader, Confirm} from '../directives';

declare var alertify: any;
declare var CodeMirror: any;

@Component({
  selector: 'app-costumization',
  templateUrl: './index.html',
  styleUrls: ['./index.sass']
})
export class Custumization implements OnInit {
  private tooltipSettings: any = {};
  private inputed: any = {};
  private menuList: any;
  private modelData: any;
  public tabList: any;
  private cssUrl: any;
  private curItem: any;
  private curNameSpace: Custumization;
  private curTemplate: string;
  private _CONFIG: any;
  private indexDatasource: any = 0;
  private currentIndex: any = 0;
  private currentTabIndex: any = 0;
  private useGlobals: boolean = false;
  private parserTimeOut: any;
  @Input() project: any;
  @ViewChild('cssCode')
  cssCode: HTMLElement;
  @ViewChild('htmlCode')
  htmlCode: HTMLElement;
  @ViewChild('preloader')
  preloader: Preloader;
  @ViewChild('wcontrols')
  wcontrols: WControls;
  @ViewChild('wtooltip')
  wtooltip: WTooltip;

  constructor(private userS: UserService, private authService: AuthService) {

    let _user = this.userS.getUser();
    this.inputed.linkAction = _user.settings ? _user.settings.linkAction : null;
    if (this.project && this.project.model && this.project.model.linkAction) {
      this.inputed.linkAction = this.project.model.linkAction;
    }
    this._CONFIG = ENTITY.Config;
    this.curNameSpace = this;
    this.menuList = [
      {title: 'Corporate style', active: true, subtitle: 'controls'},
      {title: 'Tooltip', subtitle: 'tooltip'},
      {title: 'Pre-loader', subtitle: 'preloader'}
    ];

    this.tabList = [];
    for (let i = 0; i < this.menuList.length; i++) {
      var _useGlobals = (this.project && this.project.model && this.project.model.useGlobals) ? this.project.model.useGlobals[this.menuList[i].subtitle] : [false, false, false, false];
      this.tabList.push(new CodeConfig({cstm: this, _jsMode: i == 1, _useGlobals: _useGlobals}));
    }
  }

  ngOnInit() {
    this.loadTemplates();

    // console.log(this);
  }

  private loadTemplates(selected: any = null, selectedTab: any = null) {
    let tempObj: any = {
      callback: (res: any, _tab: any, i: number, u: number) => {
        if (!res) {
          for (let i = 0; i < this.tabList.length; i++) {
            if (i === this.currentIndex) {
              this.curItem = this.tabList[i];
              this.curItem.config.active = true
              this.useGlobals = this.curItem.useGlobals[this.currentTabIndex];
            } else {
              this.tabList[i].config.active = false;
            }
            for (let subi = 0; subi < this.tabList[i].config.length; subi++) {
              var subtab = this.tabList[i].config[subi];
              if (!subtab.inited) {
                subtab.oninit();
                subtab.inited = true;
              } else {
                subtab.html.setValue(subtab.value);
              }
              if (i !== this.currentIndex) {
                this.tabList[i].config[subi].active = false;
              } else {
                this.tabList[i].config[subi].active = true;
              }
            }
          }
        } else {
          let tab = this.tabList[u],
            subtab = tab.config[i];
          if (this.project && this.project.model) {
            tab.useGlobals = this.project.model.useGlobals ? this.project.model.useGlobals[this.menuList[u].subtitle] : [false, false, false, false];
          }
          subtab.value = res._body;
          setTimeout(() => {
            if (!subtab.inited) {
              subtab.oninit();
              subtab.inited = true;
            } else {
              subtab.html.setValue(subtab.value);
            }
            setTimeout(() => {
              if (i !== this.currentTabIndex) {
                subtab.active = false;
              } else {
                subtab.active = true;
              }
            }, 111);
          });
          tab.config.active = false;
          if (u === this.currentIndex && i == this.currentTabIndex) {
            this.curItem = tab;
            this.curItem.config.active = true;
            this.useGlobals = this.curItem.useGlobals[i];
          }

          // if(i==2){
          //     let  subtab = tab.config[i+1];
          //     subtab.value = [];
          //     if(!subtab.inited)
          //     {
          //         subtab.oninit();
          //         subtab.inited = true;
          //     }else {
          //         subtab.html.setValue(subtab.value);
          //     }
          //
          //     subtab.active = false;
          // }
        }

      },
      onFinish: (settings) => {
        if (settings && settings.settings && settings.settings.status) {
          this.inputed.linkAction = settings.settings.data.linkAction;
        }
        if (this.project && this.project.model && this.project.model.linkAction) {
          this.inputed.linkAction = this.project.model.linkAction;
        }
        //console.log(settings);
      }
    };

    if (this.project) {

      tempObj.model = this.modelData = this.project.model.data[0];
      tempObj.project = this.project;
    } else {
      tempObj.user = this.userS.getUser();
    }

    if (selected !== null) {
      tempObj.templateType = ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.TYPES[selected]
      tempObj._i = selected;
    }

    if (selectedTab !== null) {
      tempObj.configID = selectedTab;
      tempObj._i = selected;
    }

    TemplatesLoader.loadTemplated(tempObj);
  }

  codeChange(type: any = null) {

    if (this.curItem) {
      for (let i = 0; i < 4; i++) {
        if (!this.curItem.config[i]) {
          continue;
        }
        let curVal = this.curItem.config[i].html.getValue instanceof Function ? this.curItem.config[i].html.getValue() : this.curItem.config[i].html.value;
        this.curItem.config[i].value = curVal;
      }
      if (this.curItem.config[4]) {
        this.curItem.config[4].value = JSON.stringify([{test: 'sdf'}]);
      }

      if (this[this.curTemplate]) {
        switch (type) {
          case  ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.CSS: {
            if (this.curItem.config[0] && this[this.curTemplate].tempLoad) {
              this[this.curTemplate].tempLoad.updateCss(this.curItem.config[0].value);
            }
            break;
          }
          case  ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.HTML: {
            if (this.curItem.config[1]) {
              this[this.curTemplate].updateHTMLInput(this.curItem.config[1].value);
            }
            break;
          }
          case  ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS:
          case  ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE: {
            if (this.parserTimeOut) {
              clearTimeout(this.parserTimeOut);
            }
            this.parserTimeOut = setTimeout(() => {
              if (this.curItem.config[2] || this.curItem.config[3]) {
                this[this.curTemplate].updateJSInput(this.curItem.config[2].value, this.curItem.config[3].value);
                if (this.curItem.config[4]) {
                  this.curItem.config[4].value = this[this.curTemplate].dataElem;
                }
              }
            }, 2000);

            break;
          }
          default : {
            if (this.curItem.config[0] && this[this.curTemplate].tempLoad) {
              this[this.curTemplate].tempLoad.updateCss(this.curItem.config[0].value);
            }
            if (this.curItem.config[1]) {
              this[this.curTemplate].updateHTMLInput(this.curItem.config[1].value);
            }
            if (this.curItem.config[2]) {
              this[this.curTemplate].updateJSInput(this.curItem.config[2].value);
              if (this.curItem.config[4]) {
                this.curItem.config[4].value = this[this.curTemplate].dataElem;
              }
            }
            //console.log("-----",this);
          }

        }
      }
    }

  }

  onLoadTemplate(template) {
    this.curTemplate = template;
    this.codeChange();
  }

  private saveChanges(options: any) {

    let self = this,
      //model = this.project.model,
      //data = model.data[0],
      _FILE = ENTITY.Config.FILE,
      _DIR = _FILE.DIR,
      user: any = this.userS.getUser(),
      _form = new FormData(),
      onSave = () => {

        for (let u = 0, types = [_FILE.STORAGE.CONTROLS, _FILE.STORAGE.TOOLTIP, _FILE.STORAGE.PRELOADER],
               templatesName = [_DIR.PROJECT_TEMPLATE.CSS, _DIR.PROJECT_TEMPLATE.HTML, _DIR.PROJECT_TEMPLATE.JS, _DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE];
             u < types.length; u++) {

          for (let i = 0, arr = this.tabList[u].config; i < arr.length; i++) {
            if (i < 4 /*&& !this.tabList[u].useGlobals[i]*/) {
              _form.append(types[u], new File([new Blob([this.tabList[u].config[i].value], {type: 'text/*'})], templatesName[i]));
            }
          }


        }
        _form.append('linkAction', this.inputed.linkAction + '')
        _form.append('useGlobals.controls[0]', this.tabList[0].useGlobals[0] || false);
        _form.append('useGlobals.controls[1]', this.tabList[0].useGlobals[1] || false);
        _form.append('useGlobals.tooltip[0]', this.tabList[1].useGlobals[0] || false);
        _form.append('useGlobals.tooltip[1]', this.tabList[1].useGlobals[1] || false);
        _form.append('useGlobals.tooltip[2]', this.tabList[1].useGlobals[2] || false);
        _form.append('useGlobals.tooltip[3]', this.tabList[1].useGlobals[3] || false);
        _form.append('useGlobals.preloader[0]', this.tabList[2].useGlobals[0] || false);
        _form.append('useGlobals.preloader[1]', this.tabList[2].useGlobals[1] || false);
        self.authService.post('/api/projects/project/template/update', _form).subscribe((res: any) => {
          res = res.json();
          if (res.status) {
            if (res.templatesUrl) {
              if (!user.settings) {
                user.settings = {};
              }
              user.settings.link = res.templatesUrl;
            }
            if (this.project) {
              if (this.project.model) {
                this.project.model.linkAction = this.inputed.linkAction;
              }
              if (this.project.model && this.project.model.data && !this.project.model.data[0].templates.length) {
                this.project.model.data[0].templates = [0, 1, 2];
              }
            }
            alertify.success(res.message);
          } else {
            alertify.error(res.message);
          }
        });
      };
    if (this.project) {
      _form.append('dir', ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.NAME);
      _form.append('_id', self.project._id);
      if (user.role === ENTITY.Config.USER_ROLE.USER) {
        alertify.confirm('Warning, the new views for project(' + this.project.title + ') will be set, are you agreed?',
          () => {
            onSave();
          });
      } else {
        onSave();
      }

    } else {
      onSave();
    }
  }

  private saveGlobals(options: any) {

    let self = this,
      user: any = this.userS.getUser(),
      _form = new FormData(),
      onSave = () => {
        _form.append('_id', this.project._id);
        _form.append('useGlobals.controls[0]', this.tabList[0].useGlobals[0] || false);
        _form.append('useGlobals.controls[1]', this.tabList[0].useGlobals[1] || false);
        _form.append('useGlobals.tooltip[0]', this.tabList[1].useGlobals[0] || false);
        _form.append('useGlobals.tooltip[1]', this.tabList[1].useGlobals[1] || false);
        _form.append('useGlobals.tooltip[2]', this.tabList[1].useGlobals[2] || false);
        _form.append('useGlobals.tooltip[3]', this.tabList[1].useGlobals[3] || false);
        _form.append('useGlobals.preloader[0]', this.tabList[2].useGlobals[0] || false);
        _form.append('useGlobals.preloader[1]', this.tabList[2].useGlobals[1] || false);
        self.authService.post('/api/projects/project/template/globals/update', _form).subscribe((res: any) => {
          res = res.json();
          if (res.status) {
            alertify.success(res.message);
          } else {
            alertify.error(res.message);
          }
          self.loadTemplates(this.currentIndex, this.currentTabIndex);

        });
      };
    if (this.project) {
      onSave();
    } else {
      onSave();
    }

  }


  private selectCurItem(item, list, index) {
    let scope = this;
    for (let i = 0; i < list.length; i++) {
      list[i].active = false;
    }
    item.active = !item.active;
    if (!isNaN(index)) {
      this.curItem = this.tabList[index];
      this.currentIndex = index;

      var flag = false;
      this.curItem.config.forEach((obj, i) => {
        if (obj.active) {
          this.currentTabIndex = i;
          flag = true;
        }
      });

      setTimeout(() => {
        scope.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.HTML);
        scope.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.CSS);
        scope.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE);
      }, 1000);
      if (!flag) {
        this.selectCurItem(this.curItem.config[0], this.curItem.config, NaN)
      }
    } else {
      this.currentTabIndex = list.indexOf(item);
    }
    this.useGlobals = this.curItem.useGlobals[this.currentTabIndex];
  }

  private updateElementIndex() {
    let _list = this[this.curTemplate].dataElem;
    if (this.indexDatasource > _list.length || this.indexDatasource < 0) {
      return;
    }
    this[this.curTemplate].indexEl = this.indexDatasource;

  }

  /**
   * On checkbox of use globals is checked
   */
  private setGlobalSettings() {
    this.useGlobals = !this.useGlobals;
    this.curItem.useGlobals[this.currentTabIndex] = this.useGlobals;

    var _projectGlobals = this.project.model.useGlobals || {
      controls: [true, true],
      tooltip: [false, false, false, false],
      preloader: [false, false]
    }
    _projectGlobals[this.menuList[this.currentIndex].subtitle][this.currentTabIndex] = this.useGlobals;

    //save changes to the server
    this.saveGlobals({});


  }
}

class CodeConfig {
  config: Array<any>;
  useGlobals: Array<boolean>;

  constructor(options: any = {}) {
    this.useGlobals = [false, false, false, false];
    this.config = [
      {
        title: 'Css Code',
        active: true,
        oninit: function (elem) {
          const editor = this.html = CodeMirror.fromTextArea(this.html, {
            lineNumbers: true,
            matchBrackets: true,
            mode: 'css',
            indentUnit: 4,
            theme: 'ambiance'
          });
          this.html.on('change', () => {
            options.cstm.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.CSS);
          });
          const range = {from: editor.getCursor(true), to: editor.getCursor(false)}
        },
        inited: false,
        config: {
          autoFocus: true,
          addModeClass: true,
          language: 'css',
          rtl: true,
          lineNumbers: true,
          matchBrackets: true,
          mode: 'css',
          // indentUnit: 4,
          theme: 'ambiance',
          //mode: {name: 'javascript', json: true},
          value: ''
        }
      },
      {
        title: ('HTML') + ' Code',
        active: true,
        oninit: function (e) {
          this.html = CodeMirror.fromTextArea(this.html, {

            lineNumbers: true,
            matchBrackets: true,
            continueComments: 'Enter',
            extraKeys: {'Ctrl-Q': 'toggleComment'},
            mode: 'javascript',
            indentUnit: 4,
            theme: 'ambiance'
          });
          this.html.on('change', () => {
            options.cstm.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.HTML);
          });
        },
        inited: false,
        config: {lineNumbers: true, theme: 'ambiance', mode: 'text/html', value: ''}
      }
    ];
    if (options._jsMode) {
      this.config.push(
        {
          title: 'JS Code',
          isJS: true,
          active: true,
          oninit: function (e) {
            this.html = CodeMirror.fromTextArea(this.html, {
              lineNumbers: true,
              matchBrackets: true,
              continueComments: 'Enter',
              extraKeys: {'Ctrl-Q': 'toggleComment'},
              mode: 'javascript',
              indentUnit: 4,
              theme: 'ambiance'
            });
            this.html.on('change', () => {
              options.cstm.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS);
            });
          },
          inited: false,
          config: {lineNumbers: true, theme: 'ambiance', mode: 'text/javascript', value: ''}
        },
        {
          title: 'Data structure',
          isJS: true,
          active: true,
          oninit: function (e) {
            this.html = CodeMirror.fromTextArea(this.html, {
              lineNumbers: true,
              matchBrackets: true,
              continueComments: 'Enter',
              extraKeys: {'Ctrl-Q': 'toggleComment'},
              mode: 'javascript',
              indentUnit: 4,
              theme: 'ambiance'
            });
            this.html.on('change', () => {
              options.cstm.codeChange(ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE);
            });
          },
          inited: false,
          config: {lineNumbers: true, theme: 'ambiance', mode: 'text/javascript', value: ''}
        },
        {
          title: 'JSON',
          isJS: true,
          active: false,
          jsonView: true,
          oninit: function (e) {
            /*this.html = CodeMirror.fromTextArea(this.html, {
                lineNumbers: true,
                matchBrackets: true,
                continueComments: "Enter",
                extraKeys: {"Ctrl-Q": "toggleComment"},
                mode: "javascript",
                indentUnit: 4,
                theme: 'ambiance'
            });
            this.html.on('change', ()=> {
                console.error('You should not be able to edit');
            });*/
            setTimeout(() => {
              let _d: any = document.querySelector('.ngx-json-viewer');
              _d.style.height = '250px';
              _d.style.position = 'absolute';
              _d.style.overflow = 'auto';
              _d.style.width = '95%';
            }, 1500);
          },
          inited: false,
          config: {lineNumbers: true, theme: 'ambiance', mode: 'text/javascript', value: ''}
        }
      );
    }

  }

  private getAttr() {
    let html = document.createElement('textarea');
    html.className = 'cos-code';
    return html;
  }
}
