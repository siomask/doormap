import {ViewChild,Component,OnInit, AfterViewInit,Input } from '@angular/core';

declare var CodeMirror:any;


@Component({
    selector: 'app-project-text-code-mirror',
    templateUrl: './index.html',
    styleUrls: ['./index.sass']
})
export class TextEditor implements OnInit,AfterViewInit {
    @Input() config:any;
    @ViewChild("txtarea")
        txtarea:HTMLElement;

    constructor() {

    }

    change() {
        console.log("change");
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.config.html = (this.txtarea['nativeElement']);
    }
}