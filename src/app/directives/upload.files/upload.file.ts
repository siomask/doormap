import {Component,Input,ViewChild,OnInit} from '@angular/core';
import * as ENTITY from '../../entities/entities';

@Component({
    selector: 'app-file-upload',
    template: '<input type="file" accept="{{accept}}"   [(ngModel)]="filesModel" #filesModels="ngModel" class="hidden" #fileUpload>' +
    ' <label [class.full-op]="filesModels.invalid"  [hidden] = "filesModels.valid">{{title}} is required</label> ' +
    '<div #btnFile class="btn-def">{{title}}<span  *ngIf="hasFiles()">({{files.length}})</span></div> <div class="list-files" *ngIf="files" [innerText]="files.toString()"></div>',
    styleUrls: ['./upload.file.sass']
})
export class UploadFile implements OnInit {
    @Input() accept:string = '';
    @Input() category:any;
    @Input() multiple:string;
    @Input() required:string;
    @Input() title:string;
    @Input() inject:any;
    @Input() files:Array<any>;
    @ViewChild("fileUpload")
        fileUpload:HTMLElement;
    @ViewChild("btnFile")
        btnFile:HTMLElement;
    abstract:any;
    filesModel:any;

    constructor() {
        let strng = 'string';
        Array.prototype.toString = function () {
            if (!this.length || !this[0])return "";
            if (this.length == 1) {
                let _f = this[0];
                if (_f) {
                    if (_f.name || typeof _f == strng) {
                        return (_f.name||this[0]|| '');
                    } if(Array.isArray(_f)){
                        return _f.toString();
                    }else {
                        var res = '';
                        for (let i in _f) {
                           if(_f[i])res += (_f[i].name || _f[i] || '') + ", ";
                        }
                        return res.substr(0, res.length - 2);
                    }
                } else {
                    return '';
                }
            }

            return this.reduce(function (previousValue, currentValue, index, array) {
                return (previousValue && previousValue.name ? previousValue.name : previousValue) + "," + (currentValue && currentValue.name ? currentValue.name : currentValue);
            });
        }
    }

    private hasFiles() {
        let re0 = 0;
        if (!this.files)return false;
        for (let i = 0; i < this.files.length; i++) {
            if (this.files[i])re0++;
        }
        return re0 > 0;
    }

    ngOnInit() {
        let fileTag = this.fileUpload['nativeElement'];

        fileTag.addEventListener('change', (e)=> {
            let files = e.target.files;
            if (!files.length)return;
            let uploadFiles = [];

            for (let i = 0; i < files.length; i++) {
                if (this.category) {
                    if (this.category instanceof Array) {
                        let _fType = files[i].name.split(".");
                        _fType = _fType[_fType.length - 1];
                        files[i].category = this.category = ENTITY.Config.FILE.STORAGE.SVG_FILE.match(_fType) ? ENTITY.Config.FILE.TYPE.MODEL_SVG : ENTITY.Config.FILE.TYPE.MODEL_OBJ;

                    } else {
                        files[i].category = this.category;
                    }

                }

                let _multiaC = this.accept.split(",");
                if (_multiaC.length > 1) {
                    _multiaC.forEach((e)=> {
                        if (files[i].name.match(e) || files[i].type.match(e))uploadFiles.push(files[i]);
                    });
                } else {
                    if (files[i].name.match(this.accept) || files[i].type.match(this.accept))uploadFiles.push(files[i]);
                }
            }

            if (!uploadFiles.length || !this.inject || !this.inject.onFilesSelected)return;

            if (!this.inject.main.selected.camera.isSVG)this.files = uploadFiles;
            this.inject.onFilesSelected(uploadFiles);

        });

        this.btnFile['nativeElement'].addEventListener('click', (e)=> {
            fileTag.click();
        });

        if (this.multiple) fileTag.setAttribute('multiple', '');
        if (this.required) {
            fileTag.setAttribute('name', 'fileTag' + Math.random());
            fileTag.setAttribute('required', true);
        }

    }
}