import {Component,OnInit} from '@angular/core';
import {User} from "../../../interfaces/user.interface";
import {UserService,ShareService,AuthService} from "../../../services/services";

declare var alertify: any;

@Component({
    selector: 'app-main-component'
})
export class AbstractMainComponent implements OnInit {

    protected header: any = {
        title: 'Main',
        arrLength: 0,
        searchName: '',
        sortType: 'A-Z'
    };
    protected createNewProject: boolean = false;
    protected User: User;

    constructor(
        protected userService: UserService,
        protected shareService: ShareService,
        protected authService:AuthService=null
    ){
        this.User = this.userService.getUser();
    }

    ngOnInit() {
        this.shareService.changeHeaderSubject(this.header);
    }

}
