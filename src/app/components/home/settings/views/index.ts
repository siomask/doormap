import { Component, trigger, state, style, transition, animate } from '@angular/core';
import {AbstractMainComponent} from "../../main/main";
import {UserService,ShareService} from "../../../../services/services";

@Component({
  selector: 'app-view-settings',
  templateUrl: './index.html',
  styleUrls: ['./index.sass']
})
export class SettingViewComponent {

  constructor( protected userService: UserService,
               protected shareService: ShareService) {
  }

}
