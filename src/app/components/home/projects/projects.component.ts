import {Component} from '@angular/core';
import {User} from "../../../interfaces/user.interface";
import {UserService,ShareService} from "../../../services/services";
import {AbstractMainComponent} from "../main/main";

declare var alertify: any;

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.sass']
})
export class ProjectsComponent extends AbstractMainComponent{
  constructor(
        userService: UserService,
        shareService: ShareService
  ){
    super(userService,shareService);
    this.header.title = 'Projects';
  }
  ngOnInit() {
    super.ngOnInit();
  }
}
