import {Component} from '@angular/core';
import {Subscription} from "rxjs/Rx";
import * as USER from  "../../../interfaces/user.interface";
import {ShareService,UserService,AuthService} from "../../../services/services";
import {AbstractMainComponent} from "../main/main";
declare var alertify:any;

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.sass']
})
export class UsersComponent extends AbstractMainComponent{

//data work with user-card
    private selectedUser:USER.IUser;
    private canEdit:boolean = false;

//show user settings popup in list
    private settingsUser:USER.IUser;

//create new user
    private createNewUser:boolean = false;
    private subNewUser:Subscription;
    private message:any = {
        email: '',
        password: ''
    };

    constructor(  shareService:ShareService,
                  userService:UserService,
                  authService:AuthService) {
        super(userService,shareService,authService);
        this.header.title = 'Users';
    }

    ngOnInit() {
        super.ngOnInit();
        this.subNewUser = this.shareService.shareListener.subscribe((user:any) => {
            if (user != undefined) {
                if (user.newUser) {
                    user.newUser = false;
                    this.authService.post('/api/users/user', user).subscribe((res:any) => {
                        res = res.json();
                        if (res.status) {
                            this.User.users.push(res.res);
                            this.createNewUser = false;
                            alertify.success(res.message);
                        } else {
                            if (res.email)
                                this.message.email = res.message;
                            alertify.error(res.err ? res.err.message : res.message);
                        }

                    }, (error) => {
                    });
                } else {
                    this.createNewUser = false;
                }
            }
        })
    }

    ngOnDestroy() {
        this.subNewUser.unsubscribe();
    }

//pop-up functions

    private checkIfIsNew(created){
        return created > Date.now()+10*60*1000;
    }
    deactivateUser(user:any) {
        let temp = Object.assign({}, user);
        temp.active = !temp.active;
        this.authService.put('/api/users/user', temp).subscribe((res:any) => {
            res = res.json();
            if (res.status) {
                user.active = !user.active;
                alertify.success(res.message);
            } else {
                alertify.error(res.message);
            }
        }, (error) => {
        });
    }

    deleteUser(user:any) {
        this.userService.delete(user);

    }

// change user card
    selectUser(user:USER.IUser, edit:boolean) {
        if (this.selectedUser === user) {
            if (edit) {
                this.canEdit = edit;
            }
            return;
        }
        this.canEdit = edit;
        this.selectedUser = user;
    }


}
