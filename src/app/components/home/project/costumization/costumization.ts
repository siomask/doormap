import {Component,OnInit} from '@angular/core';
import {ProjectService} from "../../../../services/services";
import {Config} from "../../../../entities/constant.data";

declare var alertify:any;
declare var CodeMirror:any;

@Component({
    selector: 'app-project-costumization',
    templateUrl: './costumization.html',
    styleUrls: ['./costumization.sass']
})
export class Costumization implements OnInit {

    private project:any;
    private json:any=[];


    constructor(private projectService:ProjectService) {
    }

    ngOnInit() {

        this.json=[];
        this.project = this.projectService.getProject();
        if (this.project.model && this.project.model.data && this.project.model.data.length) {
        } else {
            this.project.select = (p)=> {
                delete this.project['select'];
            }
        }

    }

}
