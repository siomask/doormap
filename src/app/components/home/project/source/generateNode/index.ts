import {ViewChild,Component,Input,EventEmitter} from '@angular/core';
import {FormGroup,FormBuilder,FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Config} from '../../../../../entities/constant.data';


declare var alertify:any;

@Component({
    selector: 'app-projects-source-generate-node',
    templateUrl: './index.html',
    styleUrls: ['./index.sass']
})
export class GenerateNodeComponent{
    show:boolean =false;
    @Input() selectedChild:any;
    private loading: boolean=false;
    private generateNodeForm: FormGroup;
    private resol:any = {
        name: true,
        link: true,
        types:[
            {name:'Model/SVG',category:Config.PROJ_DESTINATION.ModelStructure},
            {name:'JS action',category:Config.PROJ_DESTINATION.GeneralStructure},
            {name:'Url link',category:Config.PROJ_DESTINATION.LinkGeneralStructure},
        ]
    };
    private node:any = {
        replace: false,
        removeExtra: true,
        name: '@name',
        link: 'http://www.oxivisuals.no/?flat=@id'
    };
    private _CONFIG:any = Config;

    constructor( private formBuilder: FormBuilder){
        this.node.type = this.resol.types[0].name;
        this.updateForm();
    }


    private accept(form:NgForm){
        this.generateNodeForm['submitted'] = true;
        if(form.invalid)return alertify.error('Please input all fields correctly');
        this.selectedChild.glApp.generateNode(this.node);
        this.cancel();
    }
    private cancel(){
        this.show = false;
    }
    private updateForm(){
        let group:any = {
            replace:new FormControl(this.node.replace, []),
            removeExtra:new FormControl(this.node.removeExtra, []),
            name:new FormControl(this.node.name, [Validators.required]),
            type:new FormControl(this.node.type, [Validators.required])
        },lastF =  this.generateNodeForm;


        if(this.node.type == this.resol.types[1].name){
            group.destination=new FormControl('alert(@name)', [Validators.required,Validators.minLength(6)]);
        }else if(this.node.type == this.resol.types[2].name){
            group.destination=new FormControl('http://www.oxivisuals.no/?flat=@id', [Validators.required,Validators.pattern(Config.PATTERNS.URL),Validators.minLength(6)]);
        }
        this.generateNodeForm = this.formBuilder.group(group);
        if(lastF)this.generateNodeForm['submitted'] = lastF['submitted'];
    }
}