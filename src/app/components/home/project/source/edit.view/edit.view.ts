import {ViewChild,Component,OnChanges,Input,SimpleChange,HostListener} from '@angular/core';
import { NgForm} from '@angular/forms';
import {UserService,AuthService,ProjectService} from "../../../../../services/services";
import * as ENTITY from "../../../../../entities/entities";
import {WebglView} from "../../../../../directives/directives";

declare var alertify:any;

@Component({
    selector: 'app-project-edit-view',
    templateUrl: './edit.view.html',
    styleUrls: ['./edit.view.sass']
})

export class EditView {

    @Input() modelStructure:any;

    private list:any = [{f: "TEstset", name: 1122}, {f: "dsf", name: 1122}];
    private _CONFIG:any;
    private selectedIframe:any;

    constructor() {
        this._CONFIG = ENTITY.Config;
        //console.log(this);
    }

    private onchange() {
        this.modelStructure.hasChanges = this.modelStructure._project.hasChanges = true;
        if (this.modelStructure.glApp)this.modelStructure.glApp.updateData('test');
    }

    private onInputNodeName() {
        this.modelStructure.name = this.modelStructure.name.trim();
        let self = this,
            _id = this.modelStructure._id,
            _name = this.modelStructure.name;

        console.log(_name);
        if (!_name)return;

        function isSame(nodes) {
            let node;
            console.log(_name);
            for (let i = 0; i < nodes.length; i++) {
                if (nodes[i]._id == _id) {
                    node = nodes[i];
                } else if (nodes[i].name == _name) {
                    _name = _name.split("");
                    _name.pop();
                    self.modelStructure.name = _name.join("");
                    return
                }

            }
            applyDataSource(node);
        }

        function applyDataSource(node) {
            if (!node)return;
            if (node.glApp) {
                node.dataSourceGeneratedId = null;
                if (node._modelFromeScene) {
                    node._modelFromeScene._dataSource = null;
                }
                node.glApp.dataSourceMatch(null, [node.name], (source)=> {
                    if (node._modelFromeScene) {
                        node._modelFromeScene._dataSource = source;
                    }
                    node.dataSourceGeneratedId = source._id;
                });
            }
        }

        (function traverseEl(list) {
            for (let i = 0; list&& i < list.length; i++) {
                if (list[i]._id == _id) {
                    return isSame(list);
                } else if (list[i].areas) {
                    traverseEl(list[i].areas);
                }
            }
        })([this.modelStructure.parent])
    }



}