import {ViewChild,Component,OnChanges,AfterViewInit,Output,EventEmitter,HostListener} from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { NgForm} from '@angular/forms';
//import {AbstractTemplateProject} from "../template/temp.view.project";
import { ActivatedRoute } from '@angular/router';
import {UserService,AuthService,ProjectService} from "../../../../services/services";
import * as ENTITY from "../../../../entities/entities";
import {MTree,UploadFile,WebglView,BlockScreen} from "../../../../directives/directives";
import {GenerateNodeComponent} from "./generateNode/index";

declare var alertify:any;

@Component({
    selector: 'app-projects-source',
    templateUrl: './source.project.html',
    styleUrls: ['./source.project.sass']
})
export class SourceProject {
    public events:any;
    public instance:SourceProject;
    private project:any;
    private isRequsting:boolean = false;
    private maxUploadNodes:number = 0;
    private curUploadNodes:number = 0;
    selectedChild:any;
    isSaving:boolean = false;
    _CONFIG:any;
    tempNewChild:ENTITY.ModelStructure;
    uploadChild:any;
    editview:boolean = false;

    @ViewChild("webglEl")
        webglEl:WebglView;
    @ViewChild("blockScreen")
        blockScreen:BlockScreen;
    @ViewChild("generatePopuP")
        generatePopuP:GenerateNodeComponent;
    @ViewChild("modelObj")
        modelObj:UploadFile;
    @ViewChild("framesObj")
        framesObj:UploadFile;

    constructor(private projectService:ProjectService, private authService:AuthService, private router:Router) {
        this.instance = this;
        this._CONFIG = ENTITY.Config;

        /*;
         window.addEventListener("hashchange", function(){
         console.log("chamge locsdf");
         return 'Are you sure you wantdsd to leave?';
         }, false);*/
    }


    recalcElViews() {
        //console.log(this.webglEl);
        if (this.webglEl) {
            this.webglEl.app.getViews();
        } else {
            alertify.error("Only for webgl view");
        }

    }

    checkOnChangeLocation() {
        if (this.project.hasChanges) {
            return window.confirm("There are some unsaved data, if leave it will loose, are you sure?");
        } else {
            return true;
        }
    }

    ngOnInit() {

        this.events = [
            {
                target: window, name: "beforeunload", callback: function (e) {
                let confirmationMessage:string = "There may be some unsaved data, if leave it will loose, are you sure?";
                e.returnValue = confirmationMessage;
                return confirmationMessage;
            }
            }
        ].map((el)=> {
                let target:any = el.target,
                    handler = (target.addEventListener || target.attachEvent).bind(target);
                handler(el.name, el.callback);
                return el;
            });

        this.project = this.projectService.getProject();
        //this.tempNewChild = new ENTITY.ModelStructure();
        this.project.select = (p)=> {
            this.select(p.data[0]);
            delete this.project['select'];
        }

        this.authService.progress$.subscribe(
                data => {
                this.blockScreen.statusUpload = data;
            });

        if (this.project.model && this.project.model.data && this.project.model.data.length) {
            this.project.select(this.project.model);
        }
    }

    ngOnDestroy() {
        this.events.forEach((el)=> {
            let target:any = el.target,
                handler = (target.removeEventListener || target.detachEvent).bind(target);
            handler(el.name, el.callback);
        });
        this.selectedChild._selected = false;
    }

    create(form:NgForm) {

        if (form.invalid)return alertify.error('Please fill all inputs correctly');

        this.isRequsting = true;
        let myForm = new FormData(),
            fileReader = new FileReader(),
            filesUpload = [
                {
                    a: this.modelObj,
                    n: (this.modelObj.category == ENTITY.Config.FILE.TYPE.MODEL_OBJ ? ENTITY.Config.FILE.STORAGE.MODEL_OBJ : ENTITY.Config.FILE.STORAGE.SVG_FILE)
                },
                {a: this.framesObj, n: ENTITY.Config.FILE.STORAGE.PREVIEW_IMG}
            ];
        myForm.append('name', this.project.model.name);
        myForm.append('_id', this.project._id);
        myForm.append('preview', this.project.image);

        for (let f = 0; f < filesUpload.length; f++) {
            let types = filesUpload[f];
            if (!types.a['files'] || !types.a['files'].length) return alertify.error('Please upload all files');
            for (var i = 0; i < types.a['files'].length; i++) {
                var file = types.a['files'][i];
                myForm.append(types.n, file, file.name);
            }
        }

        this.authService.post("/api/projects/project/model/create", myForm).subscribe((res:any) => {
            res = res.json();
            if (res.status) {
                this.project.model.link = res.model.link;
                this.project.model.data = [new ENTITY.ModelStructure(res.model.data)];
                this.select(this.project.model.data[0]);
                alertify.success(res.message);
            } else {
                alertify.error(res.message);
            }
            this.isRequsting = false;
        }, (error) => {
            this.isRequsting = false;
        });
    }


    private fillFormWithFiles(_form, filesUpload) {
        for (let f = 0; f < filesUpload.length; f++) {
            let types = filesUpload[f];
            if ((types.a instanceof Array)) {
                for (var i = 0; i < types.a.length; i++) {
                    var file = types.a[i].file;
                    if (file instanceof File)_form.append(types.n, file, file.name);
                }
            } else {
                for (let i in  types.a) {
                    let item = types.a[i];
                    if (item && item.file instanceof File)_form.append(types.n, item.file, item.file.name);
                }
            }


        }
    }

    update(form:NgForm) {
        if (form.invalid)return alertify.error('Please fill all inputs correctly');
        let hasSomeSvg = false;
        for (let f in this.selectedChild.svgDestination)if (this.selectedChild.svgDestination[f])hasSomeSvg = true;
        if (!this.selectedChild.destination && !this.selectedChild.camera.isSVG)return alertify.error('Model  are required');
        if (this.selectedChild.camera.isSVG && !hasSomeSvg)return alertify.error('SVG are required');


        let onFinish = ()=> {
                this.isSaving = true;
                let data = this.project.model.data[0],
                    self = this,
                    onFinishSave = ()=> {
                        this.blockScreen.toggle();
                        self.isSaving = false;
                    }, total = 1;

                //alertify.warning('please wait, don\'t close the browser');

                this.blockScreen.toggle(true);
                this.blockScreen.title = 'please wait, don\'t close the browser untill savings node(s)';

                (function getTotal(el) {
                    total++;
                    if (el.areas && el.areas.length) {
                        for (var i = 0; i < el.areas.length; i++) {
                            getTotal(el.areas[i]);
                        }
                    }
                })(data)

                this.curUploadNodes = 0;
                this.maxUploadNodes = total;
                this.uploadStructure(data, function (error) {
                    if (error)return onFinishSave();

                    let _form = new FormData();
                    _form.append('dir', ENTITY.Config.FILE.DIR.DELIMETER);
                    _form.append('_id', self.project._id);
                    _form.append(ENTITY.Config.FILE.STORAGE.SITE_STRUCTURE, new Blob([JSON.stringify([data.clone()])], {type: 'text/json'}));

                    self.authService.post("/api/projects/project/model/update", _form).subscribe((res:any) => {
                        res = res.json();
                        if (res.status) {
                            alertify.success(res.message);
                        } else {
                            alertify.error(res.message);
                        }
                        onFinishSave();
                    });
                }, data.projFilesDirname);
            },
            next = ()=> {
                if (this.selectedChild.camera.isSVG && this.selectedChild.destination) {
                    alertify.confirm("You are about to save using SVG, this will delete the MODEL polygons",
                        ()=> {
                            onFinish();
                        }, ()=> {
                        });
                } else if (!this.selectedChild.camera.isSVG && hasSomeSvg) {
                    alertify.confirm("You are about to save using MODEL, this will delete the SVG polygons you have made",
                        ()=> {
                            onFinish();
                        }, ()=> {
                        });
                } else {
                    onFinish();
                }
            }

        if (this.selectedChild.camera.isSVG) {
            next();
        } else if(this.webglEl) {
            this.webglEl.app._slider.saveView(-1, ()=> {
                next();
            })
        }else{
            next();
        }


    }


    private uploadStructure(area:any, callback, dirStartFrom) {
        let _self = this,
            siteStructure = [],
            isModel = area._category == ENTITY.Config.PROJ_DESTINATION.ModelStructure;

        if (area) {
            let _form = new FormData(),
                filesUpload = [
                    {a: area.svgDestination, n: ENTITY.Config.FILE.STORAGE.SVG_FILE},
                    {a: area.destination, n: ENTITY.Config.FILE.STORAGE.MODEL_OBJ},
                    {a: area.alignImages, n: ENTITY.Config.FILE.STORAGE.ALIGN_IMG},
                    {a: area.images, n: ENTITY.Config.FILE.STORAGE.PREVIEW_IMG}
                ];
            if (isModel) {
                if (area.camera.isSVG) {
                    filesUpload.splice(1, 1);
                } else {
                    filesUpload.splice(0, 1);
                }
            }

            _form.append('dir', dirStartFrom);
            //_form.append('destination', area.destination);
            _form.append('_id', this.project._id);
            this.fillFormWithFiles(_form, filesUpload);


            _self.authService.post("/api/projects/project/model/update", _form).subscribe((res:any) => {
                res = res.json();
                if (res.status) {
                    if (!dirStartFrom) {
                        area.dataSource = res.model.data.dataSource,
                            dirStartFrom = res.model.link;
                    }//only for first node from tree
                    area.projFilesDirname = dirStartFrom;
                    area.hasChanges = false;
                    if (area._project) area._project.hasChanges = false;
                    if (isModel) {
                        if (area.destination instanceof Array && area.destination[0]) {
                            area.destination = area.destination[0].name;
                        }
                        if (area.svgDestination) {
                            if (area.svgDestination instanceof Array) {
                                for (let i = 0; i < area.svgDestination.length; i++) if (area.svgDestination[i] && area.svgDestination[i].name)area.svgDestination[i] = area.svgDestination[i].name;
                            } else {
                                for (let i in area.svgDestination) if (area.svgDestination[i] && area.svgDestination[i].name)area.svgDestination[i] = area.svgDestination[i].name;
                            }
                        }
                        if (area.camera.isSVG) {
                            delete area.destination;
                        } else {
                            area.svgDestination = null;
                            delete area.svgDestination;
                        }
                        ['alignImages', 'images'].forEach((field)=> {
                            for (let f = 0; area[field] && f < area[field].length; f++) {
                                if (area[field][f] instanceof ENTITY.ProjFile || area[field][f].file)area[field][f] = area[field][f].name;
                            }
                        });
                    }

                    if (area.areas) {

                        var startAt = 0,
                            uploadChild = function (_ar) {
                                _self.blockScreen.blockStatus = 'levevl(s) saved: ' + (_self.curUploadNodes++) + '/' + _self.maxUploadNodes;

                                if (!_ar)return callback();
                                _self.uploadStructure(_ar, function (res) {
                                    uploadChild(area.areas[startAt++]);
                                }, _ar.projFilesDirname || (dirStartFrom + ENTITY.Config.FILE.DIR.DELIMETER + _ar._id))
                            };

                        uploadChild(area.areas[startAt++]);
                    } else {
                        callback();
                    }
                } else {
                    alertify.error(res.message);
                    callback(true);
                }


            });
        } else {
            callback();
        }
    }

    select(child:any) {
        if (this.selectedChild && this.selectedChild._id == child._id)return;
        if (this.selectedChild) {
            this.selectedChild._selected = false;
            if (this.selectedChild.glApp)this.selectedChild.glApp = this.selectedChild.parent = null;
            if (!this.selectedChild.preview)this.selectedChild.preview = this.project.image;
        }
        this.selectedChild = null;
        setTimeout(()=> {
            this.selectedChild = child;
            child.sourcesApp = this;
            child._project = this.project;
            child.parent = this.project.model.data[0];
            child.canEdit = true;
            child._selected = true;
        });

    }

    generateNode() {
        this.generatePopuP.show = true;
    }


}
export class ProjTabs {

    private source:SourceProject;
    private classes:Array<string>;

    constructor(source:SourceProject) {
        this.source = source;
        this.classes = ['hide'];
    }

    toggle(elem:any) {
        elem.className = elem.className.match(this.classes[0]) ? elem.className.replace(this.classes[0], '') : elem.className + " " + this.classes[0];
    }
}
