import {Routes, RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";

import {CanDeactivateOnChangeHash,AuthGuardService,LoggedGuardService,SettingsAvService} from "./services/services";
import {LoginComponent,SettingsComponent,HomeComponent,ProjectsComponent,
    UsersComponent,ProjectComponent,BasicProject,SourceProject,Costumization,PreviewProject,SettingViewComponent} from "./components";
//import {PreviewSceneComponent,PreviewSceneService} from "./components/preview/preview.project";
export const routes:Routes = [
    {
        path: '',
        redirectTo: 'projects',
        pathMatch: 'full'
    },
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuardService],
        resolve: {
            user: AuthGuardService
        },
        children: [
            {
                path: 'projects',
                component: ProjectsComponent
            },
            {
                path: 'settings',
                component: SettingsComponent,

                children: [
                    {
                        path: '',
                        redirectTo: 'views',
                        pathMatch: 'full'
                    }, {
                        path: 'views',
                        component: SettingViewComponent,
                        canActivate: [SettingsAvService],
                    }
                ]
            },

            {
                path: 'users',
                component: UsersComponent
            },
            {
                path: 'project/:id',
                component: ProjectComponent,
                children: [
                    {
                        path: '',
                        redirectTo: 'basic',
                        pathMatch: 'full'
                    },
                    {
                        path: 'basic',
                        component: BasicProject
                    },
                    {
                        path: 'custumization',
                        component: Costumization
                    },
                    {
                        path: 'source',
                        component: SourceProject,
                        canDeactivate: [CanDeactivateOnChangeHash]
                    },
                    {
                        path: 'preview',
                        component: PreviewProject
                    }
                ]
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoggedGuardService]
    },
    //{
    //    path: 'preview',
    //    component:  PreviewSceneComponent,
    //    canActivate: [PreviewSceneService]
    //},
    {
        path: "**",
        redirectTo: '/projects'
    }

];

export const routing:ModuleWithProviders = RouterModule.forRoot(routes);