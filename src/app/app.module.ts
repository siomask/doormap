import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule ,JsonpModule, RequestOptions, XHRBackend} from '@angular/http';
import { AlertModule } from 'ng2-bootstrap';
import { CodemirrorModule } from 'ng2-codemirror';
import {DynamicComponentModule} from 'angular2-dynamic-component/index';
import { ActivatedRouteSnapshot,RouterStateSnapshot } from '@angular/router';

import {routing} from "./router";
import {NamePipe} from "./pipes/name.pipe";
import { CanDeactivateOnChangeHash,ShareService,LoggedGuardService,AuthGuardService,UserService,StorageService,AuthService,MyHTTP,SettingsAvService,ProjectService} from "./services/services";
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { AppComponent } from './app.component';
import {
    LoginComponent,
    SettingsComponent,
    SettingViewComponent,
    ProjectsComponent,
    HomeComponent,
    AsideComponent,
    HeaderComponent,
    UsersComponent,
    UserCardComponent,
    NewUserComponent,
    EditProject,
    EditView,
    TextEditor,
    PreviewProject,
    Costumization,
    NewProjectComponent,
    ViewProject,
    SourceProject,
    BasicProject,
    ProjectComponent,
    Custumization,
    GenerateNodeComponent,

} from "./components";
import {
    PopUpAction,
    TooltipHelper,
    BlockScreen,
    WebglView,SVGView,SVGDragPoint,WTooltip,WControls,WebGLService,UploadFile,MNode,MTree,SlimScroll,Preloader,TemplatesLoader,
    HtmlDirective
} from "./directives/directives";

//import {PreviewSceneComponent,PreviewSceneService} from "./components/preview/preview.project";
declare var webpack:any;

export function httpFactory(backend:XHRBackend, options:RequestOptions) {
    return new MyHTTP(backend, options);
}
@NgModule({
    declarations: [
        PopUpAction,
        TooltipHelper,

        AppComponent,
        LoginComponent,
        SettingsComponent,
        SettingViewComponent,
        ProjectsComponent,
        HomeComponent,
        AsideComponent,
        HeaderComponent,
        UsersComponent,
        UserCardComponent,
        NewUserComponent,
        EditProject,
        EditView,
        TextEditor,
        PreviewProject,
        Costumization,
        NewProjectComponent,
        ViewProject,
        SourceProject,
        BasicProject,
        ProjectComponent,
        GenerateNodeComponent,
        BlockScreen,

        NamePipe,
        SlimScroll,
        UploadFile,
        WebglView,
        WControls,
        WTooltip,
        SVGView,
        SVGDragPoint,
        Custumization,
        Preloader,
        HtmlDirective,
        TemplatesLoader,
        MTree,
        MNode,
        //PreviewSceneComponent,
        //AbstractTemplateProject,
    ],
    imports: [
        NgxJsonViewerModule,
        CodemirrorModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        JsonpModule,
        AlertModule.forRoot(),
        DynamicComponentModule,
        routing
    ],
    providers: [
        {
            provide: MyHTTP,
            useFactory: httpFactory,
            deps: [XHRBackend, RequestOptions]
        },
        CanDeactivateOnChangeHash,
        AuthService,
        StorageService,
        AuthGuardService,
        LoggedGuardService,
        SettingsAvService,
        UserService,
        ShareService,
        WebGLService,
        //PreviewSceneService,
        ProjectService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
