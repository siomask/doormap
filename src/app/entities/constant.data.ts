export class Config {
    static SVG_MODE: any = {EDIT: 1, ADD: 2, NORMAL: 3, NO: 4, DRAG: 5, GROUP: 6, CONTINUE_CREATE: 7};
    static SITE_STRUCTURE: string = '/site_structure.json';
    static HOST_NAME: string = '';
    static PROJ_LOC: string = Config.HOST_NAME + 'uploads/projects/';
    static SETTING_LOC: string = Config.HOST_NAME + 'uploads/settings/';
    static SETTING_TEMPLET_LOC: string = Config.SETTING_LOC + 'templates/';
    static PROJ_DMNS: any = ['&', '=', '?'];
    static PROJ_DESTINATION: any = {
        GeneralStructure: 0,
        LinkGeneralStructure: 1,
        ModelStructure: 2,
        OxiCamera: 3,
        Vector3: 4,
        ProjFile: 5,
        OxiControls: 6,
    };
    static TEMP_SETTINGS: any = {};
    static TYPES: any = {
        MESH: 'Mesh',
    }
    static DOM_ATTR: any = {
        CNTX_CNTRLS: 'data-cntx-controls'
    }
    static LOCAL_PREFIX: string = 'oxivisuals:';
    static LOCAL_KEYS: Array<string> = ['tempUser'];

    static CURSOR: any = {
        DEFAULT: '-webkit-grab',
        POINTER: 'pointer',
        HOLDING: '-webkit-grabbing',
        UN_AVAILABLE: 'not-allowed',
    };
    static EVENTS_NAME = {
        KEY: {
            DOWN: 'keydown',
            UP: 'keyup',
        },
        RESIZE: 'resize',
        CNTXMENU: 'contextmenu',
        DB_CLICK: 'dblclick',
        SELECT_START: 'selectstart',
        CLICK: 'click',
        TOUCH_START: 'touchstart',
        TOUCH_MOVE: 'touchmove',
        TOUCH_END: 'touchend',
        MOUSE_OUT: 'mouseout',
        MOUSE_DOWN: 'mousedown',
        MOUSE_MOVE: 'mousemove',
        MOUSE_UP: 'mouseup'
    };
    static FILE = {
        TYPE: {
            MODEL_OBJ: 1,
            PREVIEW_IMG: 2,
            ALIGN_IMG: 3,
            MODEL_SVG: 4
        },
        DEFULT: {
            NO_IMAGE: 'assets/img/no-image.jpg',
        },
        STORAGE: {
            SITE_STRUCTURE: 'structure',
            MODEL_OBJ: 'model[]',
            PREVIEW_IMG: 'frames[]',
            ALIGN_IMG: 'alignFrames[]',
            PRELOADER: 'preloader[]',
            CONTROLS: 'controls[]',
            TOOLTIP: 'tooltip[]',
            SVG_FILE: 'svgs[]'
        },
        DIR: {
            DELIMETER: '/',
            PROJECT_PREVIEW: 'images/',
            PROJECT_ALIGN_IMG: 'align_images/',
            PROJECT_TEMPLATE: {
                NAME: 'assets/templates/',
                CSS: 'style.css',
                HTML: 'index.html',
                JS: 'index.js',
                JS_DATA_STRUCTURE: 'index.ds',
                TYPES: ['controls/', 'tooltip/', 'preloader/'],
                _TYPE: {
                    PRELOADER: 2,
                    TOOLTIP: 1,
                    CONTROLS: 0,
                }

            },
            PREVIEW: {
                LOW: 'low/',
                WEBP: 'webp/',
            }
        }
    };
    static PATTERNS: any = {
        URL: /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi,
        EMAIL: /\S+@\S+\.\S+/
    };
    static IGNORE: string = 'ignore';
    static DEF_IGNORE: string = 'ignore';
    static ANGLE_STEP: number = 10;
    static DYNAMIC_IJNECT: any = {
        PRELOADER: {
            HTML: './assets/defaults/preloader/index.html',
            CSS: './assets/defaults/preloader/style.css'
        }
    }
    static USER_ROLE: any = {
        SUPER: 1,
        ADMIN: 2,
        USER: 3
    }

    static randomArbitary(min, max) {

        return Math.random() * (max - min) + min;

    }

    static randomInteger(min = 0, max = Date.now()) {
        return Math.round(min + Math.random() * (max - min))
    }

    static randomstr() {
        return Math.random().toString(36).replace(/[^a-z]+/g, '');
    }

    static onEventPrevent(e) {
        e.preventDefault();
        return false;
    }

    static safeJS(jsCode): Function {
        let res = () => {
        };
        try {
            res = Function('return ' + jsCode)();
        } catch (e) {
            console.error(e);
        } finally {
            return res;
        }
    }

    static _safeJS(jsCode): Function {
        let res = () => {
        };
        try {
            res = jsCode();
        } catch (e) {
            console.error(e);
        } finally {
            return res;
        }
    }

}

export class ProjClasses {
    static IMG_SLIDER: string = 'img-slider-container';
    static CENTER_CONTAINER: string = 'center-container';
    static PROJ_TOOLTIP_CONTAINER: string = 'tooltip-container';
    static PROJ_BACK_AREA: string = 'back-area';
    static PROJ_CONTROLS: string = 'oxi-controls';
    static PROJ_CONTROLS_MOVE: string = 'oxi-controls-move';
    static PROJ_CONTROLS_CONTAINER: string = 'oxi-controls-container';
    static PROJ_COMPASS: string = 'kompass';
    static PROJ_TOOLTIPS: any = {
        CONTAINER: 'tooltip-container',
        TOOLTIP: 'tooltip',
        HEADER: 'header',
        BODY: 'body',
    };
    static PROJ_TOOLTIP: any = {
        CONTAINER: 'cos-info',
        TOOL_CONTAINER: 'cos-tooltip',
        HEADER: 'cos-tooltip-header',
        BODY: 'cos-tooltip-body',
        LABEL: 'cos-label'
    };
    static ACTIVE: string = 'active';
    static EMPTY: string = '';

}
