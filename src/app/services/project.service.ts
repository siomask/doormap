import { Injectable } from '@angular/core';
import * as ENTITY from "../entities/entities";
import {AuthService} from "./auth.service";
import {UserService} from "./user.service";

declare var alertify;

@Injectable()
export class ProjectService {

    private Project:any;

    constructor(private authService:AuthService,
                private userService:UserService) {
        this.Project = new ENTITY.Project();
    }

    setProject(project:any):void {

        if (!(project instanceof  ENTITY.Project)) {
            this.Project = new ENTITY.Project(project);
            let user = this.userService.getUser();
            for (let i = 0; i < user.projects.length; i++) {
                if (user.projects[i]._id == this.Project._id) {
                    user.projects.splice(i, 1, this.Project);
                    break;
                }
            }
        } else {
            this.Project = project;
        }
        if (!this.Project.owner) {
            this.Project.owner = {
                //_id:this.Project.owner._id,
                //avatar:this.Project.owner.avatar,
                //firstName:this.Project.owner.firstName,
                //lastName:this.Project.owner.lastName
            }
            alertify.warning('it`s seems like owner of this project has been removed');
        }
        if (!(this.Project.model instanceof  ENTITY.ProjectModel)) {
            this.Project.model = new ENTITY.ProjectModel(this.Project.model);
        }
        let onFinish = ()=> {
            if (this.Project.select)this.Project.select(this.Project.model);
        }
        if (this.Project.model.link) {
            if (!(this.Project.model.data instanceof  Array)) {
                this.authService.get(ENTITY.Config.PROJ_LOC + this.Project.model.link + ENTITY.Config.SITE_STRUCTURE + "?time=" + Date.now()).subscribe((res:any) => {
                    this.Project.model.data = [];
                    let _data;
                    try {
                        _data = res.json();
                        for (let i = 0; i < _data.length; i++) {
                            this.Project.model.data.push(ENTITY.ProjMain.inject(_data[i]));
                        }
                    } catch (e) {
                        alertify.error('Incorrect structure');
                        this.Project.model = null;
                    }
                    onFinish();
                });

            }
        } else {
            if (this.Project.model.data && this.Project.model.data[0] instanceof ENTITY.ModelStructure) {

            } else {
                this.Project.model.data = [new ENTITY.ModelStructure({_id: this.Project._id})];//default
            }
            onFinish();
        }
    }

    getProject() {
        return this.Project;
    }

    createProject(project:ENTITY.IProject) {
        let link = '/api/projects/project';

        this.authService.post(link, project).subscribe((res:any) => {
            res = res.json();
            if (res.status) {
                let user = this.userService.getUser();
                res.res.owner = user;
                user.projects.push(res.res);
            }
            alertify.success(res.message);
        }, (error) => {
        });
    }

    changeProject(project:any, next:Function = null) {
        let link = '/api/projects/project';

        this.authService.put(link, (project instanceof ENTITY.Project ? project.toSave() : project)).subscribe((res:any) => {
            res = res.json();
            if (res.status) {
                alertify.success(res.message);
                //this.setProject(res.res);
                for (let key in project) {
                    this.Project[key] = project[key];
                }

            } else {
                alertify.error(res.message);
            }
            if (next)next(res.status);
        }, (error) => {
        });
    }

    deleteProject(project:any, callback = null) {
        let link = '/api/projects/project';

        this.authService.delete(link, {_id: project._id}).subscribe((res:any) => {
            res = res.json();
            if (res.status) {
                let user = this.userService.getUser();
                for (let i = 0; i < user.projects.length; i++) {
                    if (user.projects[i]._id == project._id) {
                        user.projects.splice(i, 1);
                        break;
                    }
                }
                if (callback)callback();
                alertify.success(res.message);
            } else {
                alertify.error(res.message);
            }

        }, (error) => {
            alertify.error(error && error.message ? error.message : error);
        });
    }
}
