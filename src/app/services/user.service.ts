import { Injectable } from '@angular/core';
import {StorageService} from "./storage.service";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";
import {Resol} from "../interfaces/resol.interface";
import * as USER from "../interfaces/user.interface";
import * as ENTITY from "../entities/entities";

declare var alertify:any;

@Injectable()
export class UserService {

    private User = new USER.User();
    config:any;

    constructor(private storageService:StorageService,
                private authService:AuthService,
                private router:Router) {
        this.config = ENTITY.Config;
    }

    logIn(remember:boolean, user:any, done?:any) {
        this.authService.post('/auth/login', user).subscribe((response:any) => {
            let res = JSON.parse(response._body);
            if (res.status) {
                alertify.success(res.message);
                remember ? this.storageService.set('token', res.token) : this.storageService.setSession('token', res.token);
                this.router.navigate(['/']);
            } else {
                alertify.error(res.message);
                if (done)
                    done(res.message);
            }

        }, (error) => {
        });
    }

    logOut():void {
        this.storageService.remove('token');
        this.storageService.removeSession('token');
        this.authService.post('/auth/logout', {}).subscribe((response:any) => {
            let res = JSON.parse(response._body);
            if (res.status) {
                this.User = null;
            }
            alertify.success(res.message);
            this.router.navigate(['/login']);
        }, (error) => {
        });
    }

    setUser(user:any):void {
        this.User = user;
        if (user.projects) {
            for (let i = 0; i < user.projects.length; i++) {
                user.projects[i] = new ENTITY.Project(user.projects[i]);
            }
        }
    }

    getUser() {
        if(!this.User.settings)this.User.settings={};
        return this.User;
    }

    resolUser(resol:Resol, obj:any) {
        let resolFlag:boolean = true;
        for (let i in resol) {
            resol[i] = obj[i] ? true : false;
            if (!resol[i])
                resolFlag = false;
        }
        return resolFlag;
    }


    lettersNoImg(user:any):string {
        let l1 = '';
        let l2 = '';
        if (user.firstName) {
            l1 = user.firstName.charAt(0).toUpperCase();
        }
        if (user.secondName) {
            l2 = user.secondName.charAt(0).toUpperCase();
        }

        return l1 + l2;
    }
    delete(user:any, next:Function = null) {
        alertify.confirm("Warning, will destroy all user child, their projects and settings. Are you sure to drop the user " + user.email, ()=> {
            this.authService.delete('/api/users/user', user).subscribe((res:any) => {
                res = res.json();
                if (res.status) {
                    let idx = this.User.users.indexOf(user);
                    if (idx > -1) {
                        let user = this.User.users.splice(idx, 1)[0],
                            _projects = [];
                        if (user.projects)_projects = _projects.concat(user.projects);

                        if (user.users) {
                            for (let _u = 0; _u < user.users.length; _u++) {
                                for (let i = 0; i < this.User.users.length; i++) {
                                    if (this.User.users[i]._id == user.users[_u]) {
                                        if (this.User.users[i].projects)_projects = _projects.concat(this.User.users[i].projects);
                                        this.User.users.splice(i, 1);
                                        break;
                                    }
                                }
                            }
                        }

                        for (let _u = 0; _u < _projects.length; _u++) {
                            for (let i = 0; i < this.User.projects.length; i++) {
                                if (this.User.projects[i]._id == _projects[_u]) {
                                    this.User.projects.splice(i, 1);
                                    break;
                                }
                            }
                        }
                    }


                    alertify.success(res.message);
                } else {
                    alertify.error(res.message);
                }

                if (next)next();
            }, (error) => {
                alertify.error(error);
            });
        }, ()=> {

        })
    }
}
