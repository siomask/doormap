import { Injectable }     from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot }  from '@angular/router';
import {UserService} from "./user.service";
import {Config} from "../entities/constant.data";

@Injectable()
export class SettingsAvService implements CanActivate {

    constructor(
        private router: Router,
        private userService: UserService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean {
        if (this.userService.getUser() && [Config.USER_ROLE.ADMIN,Config.USER_ROLE.SUPER].indexOf(this.userService.getUser().role)>-1) {
            return true;
        }else{
            this.router.navigate(['/']);
            return false;
        }

    }

}