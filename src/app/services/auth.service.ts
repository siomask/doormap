import {Injectable} from '@angular/core';
import {
  Http,
  Headers,
  Jsonp,
  XHRBackend,
  ConnectionBackend,
  BrowserXhr,
  ResponseOptions,
  XSRFStrategy,
  BaseResponseOptions,
  CookieXSRFStrategy,
  RequestOptions,
  BaseRequestOptions
} from '@angular/http';
//import {HttpRequest,HttpClient} from "@angular/common/http";
import {StorageService} from './storage.service';
import 'rxjs/add/operator/map';
//import { FormControl } from '@angular/forms';
import {ReflectiveInjector} from '@angular/core';

declare var alertify: any, URLSearchParams: any, XDomainRequest: any, jQuery: any;
import {Observable} from 'rxjs/Rx';

@Injectable()
export class AuthService {

  private req;
  progress$: any;
  progress: any;
  progressObserver: any;

  constructor(private http: Http,
              private storageService: StorageService,
              private _jsonP: Jsonp) {
    this.progress$ = Observable.create(observer => {
      this.progressObserver = observer
    }).share();
  }

  createAuthorizationHeader(headers: Headers) {
    let token = this.authToken();
    headers.append('Authorization', token);
  }

  private authToken() {
    return this.storageService.get('token') || this.storageService.getSession('token');
  }

  get(url, options: any = {hasAuthHeader: true}) {
    let headers = new Headers({'Content-Type': 'application/json'});

    if (options.hasAuthHeader) {
      this.createAuthorizationHeader(headers);
    }
    if (options.isCross) {
      headers.append('Access-Control-Allow-Origin', '*');
    }
    return this.http.get(url, new RequestOptions({headers: headers}));
  }


  _post(url, data = {}, options: any = {hasAuthHeader: true}) {
    /* let headers = new Headers();
     if (options.hasAuthHeader)this.createAuthorizationHeader(headers);
     let req = new HttpRequest('POST', url, data, {
         reportProgress: true,
     });*/

    //return this.httpClient.request(req);

    //return this.http.post(url, data, new RequestOptions({headers: headers}));
  }

  post(url, data = {}, options: any = {hasAuthHeader: true}) {
    if (data instanceof FormData) {
      return this.postFormData(url, data)
    } else {
      let headers = new Headers();
      if (options.hasAuthHeader) {
        this.createAuthorizationHeader(headers);
      }
      return this.http.post(url, data, new RequestOptions({headers: headers}));
    }

  }


  private postFormData(url, formData) {
    return Observable.create(observer => {
      let xhr: XMLHttpRequest = new XMLHttpRequest();


      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            observer.next({
              json: () => {
                return JSON.parse(xhr.response)
              }
            });
            observer.complete();
          } else {
            observer.error({
              json: () => {
                return JSON.parse(xhr.response)
              }
            });
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round(event.loaded / event.total * 100);
        if (this.progressObserver) {
          this.progressObserver.next(this.progress);
        }
      };

      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', this.authToken());
      xhr.send(formData);
    });
  }

  put(url, data = {}) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(url, data, new RequestOptions({headers: headers}));
  }

  delete(url, data = {}) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.delete(url, new RequestOptions({
      headers: headers,
      body: data
    }));
  }

  safeJS(jsCode): Function {
    let res = () => {
    };
    try {
      res = Function('return ' + jsCode)();
    } catch (e) {
      const linkViewCode = URL.createObjectURL(new Blob([jsCode], {
        type: 'text/plain'
      }));
      alertify.error(e.message + `, some bad  <a href="${linkViewCode}" target="_blank"  >coding</a>`);
    } finally {
      return res;
    }
  }
}



@Injectable()
export class MyHTTP extends Http {
  private static _instance: MyHTTP;

  constructor(backend: XHRBackend, options: RequestOptions) {
    super(backend, options);
  }

  static getS(url, options: any = {hasAuthHeader: true}) {
    return MyHTTP.getInstance().get(url, new RequestOptions({headers: new Headers({'Content-Type': 'application/json'})}));
  }

  static posts(url, data = {}) {
    return MyHTTP.getInstance().post(url, data, new RequestOptions({headers: new Headers()}));
  }

  public static getInstance() {
    if (!(this._instance instanceof MyHTTP)) {
      this._instance = ReflectiveInjector.resolveAndCreate([
        MyHTTP, BrowserXhr, XHRBackend,
        {provide: ConnectionBackend, useClass: XHRBackend},
        {provide: ResponseOptions, useClass: BaseResponseOptions},
        {
          provide: XSRFStrategy, useFactory: () => {
            return new CookieXSRFStrategy();
          }
        },
        {provide: RequestOptions, useClass: BaseRequestOptions}
      ]).get(MyHTTP);
    }
    return this._instance;
  }
}
