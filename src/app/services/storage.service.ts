import { Injectable } from '@angular/core';
import { Config } from '../entities/constant.data';

declare var localStorage;
declare var sessionStorage;
@Injectable()
export class StorageService {

  public get(key: string) {
    return JSON.parse(localStorage.getItem(Config.LOCAL_PREFIX+key));
  }

  public set(key: string, value: any) {
    localStorage.setItem(Config.LOCAL_PREFIX+key, JSON.stringify(value));
  }

  public remove(key: string) {
    localStorage.removeItem(Config.LOCAL_PREFIX+key);
  }

  public getSession(key: string) {
    return JSON.parse(sessionStorage.getItem(Config.LOCAL_PREFIX+key));
  }

  public setSession(key: string, value: any) {
    sessionStorage.setItem(Config.LOCAL_PREFIX+key, JSON.stringify(value));
  }

  public removeSession(key: string) {
    sessionStorage.removeItem(Config.LOCAL_PREFIX+key);
  }
}