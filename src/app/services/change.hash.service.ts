import {Injectable} from '@angular/core';
import {CanDeactivate, RouterStateSnapshot,ActivatedRouteSnapshot} from '@angular/router';
import {SourceProject} from '../components/index';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CanDeactivateOnChangeHash /*implements CanDeactivate<any>*/ {


    canDeactivate(component:any,
                  currentRoute:ActivatedRouteSnapshot,
                  currentState:RouterStateSnapshot,
                  nextState:RouterStateSnapshot):any {
        return component.checkOnChangeLocation();
    }

}