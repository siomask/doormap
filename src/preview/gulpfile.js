'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
//rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    babelify = require("babelify"),
    watchify = require("watchify"),
    browserify = require("browserify"),
    rename = require("gulp-rename"),
    inject = require('gulp-inject-string'),
    notify = require('gulp-notify'),
    autoprefixer = require('gulp-autoprefixer'),
    livereload = require('gulp-livereload'),
    del = require('del'),
    cssnano = require('gulp-cssnano'),
    merge = require('event-stream').merge,
    typescript = require('gulp-typescript'),
    reload = browserSync.reload,
    ts = require("gulp-typescript"),
    tsProject = ts.createProject("tsConfig.json"),
    merge2 = require('merge2'),
    tsify = require("tsify"),
    gutil = require("gulp-util");

var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var concat = require('gulp-concat');



function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
var tempPath = "../../resources/";
var path = {
    build: {
        js: tempPath + 'build/',
        img: 'build/assets/images/'
    },
    src: {
        js: 'src/app/main.js',
        ts: 'src/app/main.ts',
        style: ['src/app/**/*.scss', 'src/libs/**/*.scss', 'src/app/**/*.sass'],
        img: 'src/images/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    out: {
        js: {origin: 'main.js', hash: guidGenerator() + '.js'},
        style: {origin: 'style.css', hash: guidGenerator() + '.css'},
    },
    watch: {
        js: ['src/app/**/*.ts', 'src/app/libs/**/*.js'],
        style: ['src/app/**/*.scss', 'src/libs/**/*.scss', 'src/app/**/*.sass'],
        img: 'src/images/**/*.*',
    },
    clean: './build'
};


var config = {
    server: {
        baseDir: "./"
    },
    //tunnel: true,
    host: 'localhost',
    port: 3007,
    logPrefix: "Frontend_server"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('style:build', function () {
    var csslibs = gulp.src([
        'src/libs/styles.css'
    ])
        .pipe(concat('vendors.css'));

    var sassS = gulp.src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['src/'],
            outputStyle: 'compressed',
            sourceMap: true,
            errLogToConsole: true
        }).on('error', sass.logError))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(cssnano())
        .pipe(autoprefixer({
            browsers: ['last 16 versions'],
            cascade: false
        }));


    return merge(sassS, csslibs)
        .pipe(concat("styles.css"))
        //.pipe(concat(path.out.style.hash))
        //.pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(notify({message: 'Styles task complete'}))
        .pipe(reload({stream: true}));
});
var watchedBrowserify = watchify(browserify({
    noImplicitAny: false,
    basedir: '.',
    debug: true,
    entries: ['src/app/main.ts'],
    cache: {},
    packageCache: {}
}).plugin(tsify, { noImplicitAny: false }));
function bundle() {
    var paths = "../../src/libs/",
        jslibs = gulp.src([
            paths + "alertify.js",
            paths + "fabric.js",
            paths + "pace.min.js",
            paths + "Tween.js",
            paths + "three/three.js",
            paths + "three/OBJLoader.js",
            paths + "three/OrbitControls.js",
            paths + "three/shaders/CopyShader.js",
            paths + "three/shaders/FXAAShader.js",
            paths + "three/postprocessing/three.composer.js",
            paths + "three/postprocessing/three.render.js",
            paths + "three/postprocessing/three.shader.js",
            paths + "three/postprocessing/OutlinePass.js",
            paths + "polylabel.js",
            '../../node_modules/mustache/mustache.js'

        ]).pipe(concat('vendors.js'));
    console.log('test123');
    var tss = watchedBrowserify
        .bundle().on('error',function (error) { console.error(error.toString())})
        .pipe(source('bundle.js'))
        .pipe(buffer());

    console.log('test');
    return merge(jslibs, tss)
        .pipe(concat('main.js'))
        //.pipe(concat(path.out.js.hash))
        //.pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(notify({message: 'JS task complete'}))
        .pipe(reload({stream: true}));
}
gulp.task('clean', function () {
    return del.sync([path.build.js])
});

gulp.task('build', [
    // 'clean',
    'style:build',
], bundle);
watchedBrowserify.on("update", bundle);
watchedBrowserify.on("log", gutil.log);
watchedBrowserify.on("error", gutil.log);

gulp.task('watch', function () {
    watch(path.watch.style, function (event, cb) {
        gulp.start('style:build');
    });
});


gulp.task('default',['build','webserver' , 'watch']);