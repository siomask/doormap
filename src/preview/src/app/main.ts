import {Preview} from './Preview';
import * as THREE from 'three';

declare const Image: any, window: any, HTMLElement: any, JSON: any, String: any, alertify;


document.addEventListener('DOMContentLoaded', () => {
    Image.prototype.load = function (url: any) {
        const thisImg: any = this;
        const xmlHTTP: any = new XMLHttpRequest();
        xmlHTTP.open('GET', url, true);
        xmlHTTP.responseType = 'arraybuffer';
        xmlHTTP.onload = function (e: any) {
            const blob = new Blob([this.response]);
            thisImg.src = window.URL.createObjectURL(blob);
        };
        xmlHTTP.onprogress = function (e: any) {
            thisImg.completedPercentage = parseInt(((e.loaded / e.total) * 100).toString(), 10);
            if (thisImg.onprogress) {
                thisImg.onprogress(thisImg.completedPercentage);
            }
        };
        xmlHTTP.onloadstart = function () {
            thisImg.completedPercentage = 0;
            if (thisImg.onStartLoad) {
                thisImg.onStartLoad();
            }
        };
        xmlHTTP.send();
    };
    String.prototype.json = function () {
        return JSON.parse(this);
    };
    HTMLElement.prototype.toggle = function (show: boolean = false) {
        if (show) {
            if (!this.className.match('active')) {
                this.className += ' active';
            }
        } else {
            this.className = this.className.replace(' active', '');
        }
    };
    HTMLElement.prototype.fade = function (show: boolean = false) {
        this.style.display = show ? '' : 'none';
    };
    HTMLElement.prototype.setVisibilty = function (show: boolean = false) {
        this.style.visibility = show ? '' : 'hidden';
    };
    window.APP = new Preview();
    window.appartment = Appart;
    document.body.onselectstart = (e) => {
        e.preventDefault();
        return false;
    };
    document.querySelector('#preloader-text').parentNode.removeChild(document.querySelector('#preloader-text'));

});

function Appart(meshName: any) {
    const app = window.APP,
        _selected = app.selected;
    let item = _selected.camera && _selected.camera.views ? _selected.camera.views[meshName] : {};
    if (!meshName || !_selected.camera.views || !_selected.camera.views[meshName] || !_selected.camera.views[meshName].iframeItem) {
        return alertify.error('no active view');
    } else {
        item = _selected.camera.views[meshName];
    }
    item = !item ? {} : item;

    item.show = () => {

        const moVeTo = +_selected.currentItem,
            activeIframe = +item.iframeItem;

        let model: THREE.Object3D;
        if (app.vieWer.glViwer) {
            model = app.vieWer.glViwer.app.model;
        }
        if (activeIframe === moVeTo) {
            return alertify.log('has same view');
        }
        if (activeIframe >= _selected.images.length || activeIframe < 0) {
            return alertify.error('no such view');
        }

        const half = _selected.images.length / 2,
            moveRight = ((moVeTo < activeIframe && Math.abs(moVeTo - activeIframe) < half)
                || ((moVeTo - activeIframe) > 0 && moVeTo - activeIframe > half)),
            interval = setInterval(() => {
                window.APP.vieWer._slider.move(moveRight ? 1 : -1);
                if (_selected.currentItem === activeIframe) {
                    clearInterval(interval);
                    if (model) {
                        model.traverse((mesh: THREE.Object3D) => {
                            if (mesh.name === meshName) {
                                if (mesh['_toolTip']) {
                                    mesh['_toolTip'].show(true);
                                    app.vieWer.glViwer.app._events.lastInter = {object: mesh};
                                }
                            }
                        });
                    }
                }
            }, 100);
    };
    return item;
}

Appart.prototype.getAll = function () {
    const meshes = [];
    if (window.APP.vieWer && window.APP.vieWer.glViwer) {
        window.APP.vieWer.glViwer.app.model.traverse((child) => {
            if (child.type === 'Mesh') {
                meshes.push(child.name);
            }
        });
    }
    return meshes;
};
