import * as ENTITY from './helper/Utils';
import {Viewer} from './viewer/Viewer';

declare const location: any, alertify: any;

export class Preview {
    http: ENTITY.Http;
    selected: any;
    private model: any;
    private areaData: any;
    private vieWer: Viewer;

    constructor() {
        const areas: any = Preview.checkUrl(),
            main: any = areas[0].split(ENTITY.Config.PROJ_DMNS[1])[1];

        this.http = new ENTITY.Http();
        this.loadSceneData(main, (_data: any) => {
            this.areaData = _data;
            this.model = new ENTITY.Project();
            this.model.data = [];
            this.updateLevel(areas);
        });
    }

    private static checkUrl() {
        const dmns: Array<string> = ENTITY.Config.PROJ_DMNS,
            scenePreview: any = location.href.split(dmns[2])[1];
        if (!scenePreview) {
            return alertify.error('please select some scene');
        }
        return scenePreview.split(dmns[0]);
    }

    updateLevel(_areas: any = null) {

        const _data = this.areaData,
            dmens: any = ENTITY.Config.PROJ_DMNS,
            isFirst = !!_areas,
            areas = _areas || Preview.checkUrl();
        if (!areas || !(areas instanceof Array && areas.length > 0)) {
            return;
        }

        // debugger;
        for (let i = 0; i < _data.length; i++) {
            if (isFirst) {
                this.model.data.push(ENTITY.ProjMain.inject(_data[i]));
            }
            if (areas.length > 1) {
                const curIArea = areas[areas.length - (_data.length - i)].split(dmens[1])[1];
                if (!curIArea) {
                    return alertify.error('Some bad level');
                }
                if (this.checkChild(this.model.data[i], curIArea, (c: any) => this.select(c))) {
                    return true;
                }
            } else {
                return this.select(this.model.data[0]);
            }
        }
        location.href = location.href.substr(0, location.href.indexOf('&'));
    }

    select(child: any) {
        this.selected = child;
        child.parent = this.model.data[0];

        if (this.vieWer) {
            this.vieWer.selectViewer(child);
        } else {
            this.vieWer = new Viewer(this);
        }

    }

    private loadSceneData(main: any, onSuccess: any) {
        if (!main) {
            return alertify.error('please select some project scene');
        }
        const reqData = {id: main},
            _userid = localStorage.getItem(ENTITY.Config.LOCAL_PREFIX + ENTITY.Config.LOCAL_KEYS[0]);
        if (_userid) {
            reqData['userId'] = JSON.parse(_userid);
            localStorage.removeItem(ENTITY.Config.LOCAL_PREFIX + ENTITY.Config.LOCAL_KEYS[0]);
        }


        this.http.post('public/project/isactive', reqData, (resp: any) => {
            resp = resp.json();
            if (!resp.status) {
                alertify.error(resp.message.message || 'No project found');
            } else if (resp.project.published && resp.project.model) {
                ENTITY.Config.TEMP_SETTINGS = {settings: resp.settings};
                this.http.get(ENTITY.Config.PROJ_LOC + resp.project.model.link +
                    ENTITY.Config.FILE.DIR.DELIMETER + ENTITY.Config.SITE_STRUCTURE, (res: any) => {
                    if (res._body && res._body.match('!doctype html')) {
                        alertify.error('No project found');
                    } else {
                        onSuccess(res.json());
                    }
                }, (e: any) => {
                    alertify.error(e);
                });
            } else {
                alertify.error('project not available' + (resp.message ? ', ' + resp.message : ''));
            }
        }, (err) => {
            alertify.error('there are some error,' + err);
        });
    }

    private checkChild(child: any, curIArea: any, callBack: any): boolean {
        let res = false;
        if (child.projFilesDirname.indexOf(curIArea) > -1) {
            callBack(child);
            res = true;
        } else if (child.areas) {
            let d = 0;
            while (d < child.areas.length && !res) {
                res = this.checkChild(child.areas[d++], curIArea, callBack);
            }
            return res;
        }
        return res;
    }
}
