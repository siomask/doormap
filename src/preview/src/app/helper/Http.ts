import * as ENTITY from './Utils';

declare var window: any;

export class Http {
    private static _instance: Http;
    HOST: string = ENTITY.Config.HOST_NAME;
    private _XHR: any;
    private xhr: any;

    constructor() {
        this._XHR = window.XMLHttpRequest || window.XDomainRequest || window.ActiveXObject;
        this.onConnect();
    }

    public static getInstance() {
        if (!(this._instance instanceof Http)) {
            this._instance = new Http();
        }
        return this._instance;
    }

    static posts(url: any, data: any, onSuccess: any, onError: any) {
        const xhr = this.getInstance().onConnect();
        xhr.open('POST', this.getInstance().HOST + url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }

            if (xhr.status !== 200) {
                onError(xhr.status + ': ' + xhr.statusText);
            } else {
                onSuccess(xhr.responseText);
            }
        };
        xhr.send(JSON.stringify(data));
    }

    static gets(url: any, onSuccess: any, onError: any) {
        const xhr = this.getInstance().onConnect();
        xhr.open('GET', url + '?r=' + Math.random(), true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            if (xhr.status !== 200) {
                onError(xhr.status + ': ' + xhr.statusText);
            } else {
                onSuccess(xhr.responseText);
            }
        };
        xhr.send();
    }

    onConnect() {
        const xhr = new this._XHR('Microsoft.XMLHTTP');
        xhr.timeout = 50000;
        xhr.ontimeout = function () {
            console.log(' time is out');
        };
        return xhr;
    }

    post(url: any, data: any, onSuccess: any, onError: any) {
        const xhr = this.onConnect();
        xhr.open('POST', this.HOST + url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            if (xhr.status !== 200) {
                onError(xhr.status + ': ' + xhr.statusText);
            } else {
                onSuccess(xhr.responseText);
            }
        };
        xhr.send(JSON.stringify(data));
    }

    postForm(url: any, data: any, onSuccess: any, onError: any) {
        const xhr = this.onConnect();
        xhr.open('POST', this.HOST + url, true);
        this.send(data, onSuccess, onError);
    }

    get(url: any, onSuccess: any, onError: any) {
        const xhr = this.onConnect();
        xhr.open('GET', url + '?r=' + Math.random(), true);
        xhr.onreadystatechange = () => {
            if (xhr.readyState !== 4) {
                return;
            }
            if (xhr.status !== 200) {
                onError(xhr.status + ': ' + xhr.statusText);
            } else {
                onSuccess(xhr.responseText);
            }
        };
        xhr.send();
    }

    send(data: any, onSuccess: any, onError: any) {
        this.xhr.onreadystatechange = () => {
            if (this.xhr.readyState !== 4) {
                return;
            }
            if (this.xhr.status !== 200) {
                onError(this.xhr.status + ': ' + this.xhr.statusText);
            } else {
                onSuccess(this.xhr.responseText);
            }

        };
        this.xhr.send(data);
    }
}
