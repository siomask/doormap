import {Viewer} from './Viewer';
import * as ENTITY from '../helper/Utils';

export class OxiViewer {
    parent: Viewer;
    selected: any;
    _self: OxiViewer;
    inited: boolean;
    _id: number = Date.now();

    constructor(parent: Viewer) {
        this.inited = false;
        this.parent = parent;
        this._self = this;
        this.selected = parent.main.selected;
    }

    isAvailableForAction(target: any) {
        return (!target._dataSource && target._data && ((target._data.areas && target._data.areas.length)
            || target._data._category === ENTITY.Config.PROJ_DESTINATION.ModelStructure
            || target._data._category === ENTITY.Config.PROJ_DESTINATION.GeneralStructure
            || target._data._category === ENTITY.Config.PROJ_DESTINATION.LinkGeneralStructure))
            || (target._dataSource && !target._dataSource.dataset.Sold);
    }
}