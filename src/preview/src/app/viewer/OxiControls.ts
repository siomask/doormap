import * as ENTITY from '../helper/Utils';
import {Viewer} from './Viewer';
import {WebglView} from './webgl/webgl.view';

declare const location: any;

export class OxiControls {
    private app: Viewer;
    private controls: any;
    private _backArea: any;
    private renderOrder: number;
    kompas: any;

    constructor(app: Viewer) {
        this.renderOrder = 1;
        this.app = app;
        const
            _self = this,
            CONTROLS = '.oxi-controls-container' + ' .',
            _div: any = document.querySelector(CONTROLS + ENTITY.ProjClasses.PROJ_CONTROLS_MOVE),
            _backArea: any = this._backArea = document.querySelector(CONTROLS + ENTITY.ProjClasses.PROJ_BACK_AREA),
            arrows = [{_c: 'left', _i: -1}, {_c: 'right', _i: 1}];
        let div: any = this.controls = document.createElement('div'),
            kompass: any = document.querySelector(CONTROLS + ENTITY.ProjClasses.PROJ_COMPASS);
        if (_div) {
            _div.fade();
        }
        if (_div) {
            div = this.controls = _div;
            for (let i = 0; i < _div.childNodes.length; i++) {
                const childDiv = _div.childNodes[i];
                for (let u = 0; u < arrows.length; u++) {
                    if (childDiv.localName === 'div') {
                        const dir = arrows.splice(u, 1)[0]._i;
                        childDiv.addEventListener((this.app.isMobile ?
                                ENTITY.Config.EVENTS_NAME.TOUCH_END : ENTITY.Config.EVENTS_NAME.CLICK),
                            (e: Event) => {
                                this.app._slider.move(dir);
                            });
                        childDiv.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, ENTITY.Config.onEventPrevent);
                        break;
                    }
                }

            }
        } else {
            div.className = ENTITY.ProjClasses.PROJ_CONTROLS_MOVE;
            app.container.appendChild(div);
            arrows.forEach((child) => {
                const childDiv = document.createElement('div');
                childDiv.className = child._c;
                childDiv.style.backgroundImage = 'url("assets/img/left_arrow.png")';
                div.appendChild(childDiv);
                childDiv.addEventListener((this.app.isMobile ?
                        ENTITY.Config.EVENTS_NAME.TOUCH_END : ENTITY.Config.EVENTS_NAME.CLICK),
                    (e: Event) => {
                        this.app._slider.move(child._i);
                    });
                childDiv.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, ENTITY.Config.onEventPrevent);
            });
        }
        div.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, ENTITY.Config.onEventPrevent);
        div.addEventListener(ENTITY.Config.EVENTS_NAME.CNTXMENU, ENTITY.Config.onEventPrevent);


        let back: any = this._backArea;
        if (!back) {
            back = this._backArea = document.createElement('div');
            back.className = ENTITY.ProjClasses.PROJ_BACK_AREA;
            back.style.backgroundImage = 'url(' + ENTITY.Config.HOST_NAME + '\'assets/img/android-system-back.png\')';
        }
        _backArea.fade();
        this.app.container.appendChild(back);
        back.addEventListener(ENTITY.Config.EVENTS_NAME.CLICK, (e: any) => {
            ENTITY.Config.onEventPrevent(e);
            const path = location.href,
                areas = path.split(ENTITY.Config.PROJ_DMNS[0]);
            areas.pop();
            if (window.history.replaceState) {
                window.history.replaceState({}, areas[areas.length - 1], areas.join(ENTITY.Config.PROJ_DMNS[0]));
                app.main.updateLevel();
                if (this.app._slider.updateView) {
                    this.app._slider.updateView(this.app.main.selected.currentItem);
                }
            }
        });
        back.addEventListener(ENTITY.Config.EVENTS_NAME.CNTXMENU, ENTITY.Config.onEventPrevent, false);

        if (!kompass) {
            kompass = document.createElement('div');
            kompass.className = 'kompass';
            kompass.style.backgroundImage = 'url(' + ENTITY.Config.HOST_NAME + '"assets/img/kompas.png")';
            app.container.appendChild(kompass);
        }
        this.kompas = kompass;
        kompass.onUpdate = () => {
            kompass.style.display = !_self.app.main.selected.camera.kompass.enabled ? 'none' : '';
            kompass.style.transform = 'rotate(' + (_self.app.main.selected.currentItem * app._angle
                + app.main.selected.camera.kompass.angle) + 'deg)';
        };
        kompass.onUpdate();
    }

    update() {
        this._backArea.fade(location.href.split(ENTITY.Config.PROJ_DMNS[0]).length > 1);
        this.controls.fade(this.app.main.selected.images.length > 1);
        this.kompas.onUpdate();
    }

    show(pos: any, flag: boolean = true, fl: boolean = true) {
        console.log('show');
        if (this.kompas) {
            this.kompas.onUpdate();
        }
        let _inter;
        if (this.app.currentViwer) {
            if (this.app.currentViwer instanceof WebglView && this.app.currentViwer.app._events) {
                _inter = this.app.currentViwer.app._events.lastInter;
            } else if (this.app.currentViwer.lastSelected) {
                this.app.currentViwer.lastSelected._toolTip.show(flag);
            }
        }

        if (_inter) {
            _inter = _inter.object;
            if (_inter._toolTip) {
                _inter._toolTip.show(flag);
            }
            if (!_inter.material.defColor) {
                _inter.material.defColor = _inter.material.color.clone();
            }
            if (!_inter.material.onSelectColor) {
                _inter.material.onSelectColor = this.app.HOVER_COLOR.ACTIVE;
            }

            if (flag) {
                _inter.renderOrder = this.renderOrder += 10;
            } else {
                _inter._onFinish = () => {
                    _inter.renderOrder = 0;
                };
            }

            _inter.material.transparent = fl;
        }
        this.app.refresh();
    }

}
