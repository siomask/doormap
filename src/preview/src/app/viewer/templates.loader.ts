import * as ENTITY from '../helper/Utils';

declare const alertify: any;

export class MTemplatesLoader {
    http: ENTITY.Http;
    cssUrl: any;
    htmlTemplate: any;
    jsTemplate: any;
    callbacks: Array<Function> = [];
    private cssElement: any;

    constructor(private options: any) {

        const cssId = ('cssInject' + ENTITY.Config.randomInteger());
        let cssEl = document.getElementById(cssId);
        if (!cssEl) {
            cssEl = document.createElement('style');
            cssEl.id = cssId;
            cssEl.setAttribute('type', 'text/css');
            document.head.appendChild(cssEl);
        }
        this.http = new ENTITY.Http();
        this.cssElement = cssEl;
        this.onInit();

    }

    onInit() {

        const model = this.options.model,
            _DIR = ENTITY.Config.FILE.DIR,
            templateType = this.options.templateType;
        if (!model || isNaN(templateType)) {
            return alertify.error('missing template type (' + templateType + ')');
        }
        const isTooltip = (+templateType) === 1;
        MTemplatesLoader.loadTemplated({
            templateType: _DIR.PROJECT_TEMPLATE.TYPES[templateType],
            model: model,
            callback: (res: any, tab: any) => {
                this[tab._f] = res;
                if (tab._f === 'cssUrl') {
                    this.updateCss(res);
                }
                if (this['cssUrl'] && this['htmlTemplate']) {
                    if (isTooltip && !this['jsTemplate']) {
                        return;
                    }
                    for (let i = 0; i < this.callbacks.length; i++) {
                        this.callbacks.shift()();
                    }
                }
            }
        });
    }

    static loadTemplated(opt: any) {
        let settings: any = ENTITY.Config.TEMP_SETTINGS,
            templatesUrls: any = [
                {link: null, _f: 'cssUrl', fileName: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.CSS},
                {link: null, _f: 'htmlTemplate', fileName: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.HTML},
                {link: null, _f: 'jsTemplate', fileName: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS},
                {link: null, _f: 'dsTemplate', fileName: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE.JS_DATA_STRUCTURE}
            ];
        const _DIR: any = ENTITY.Config.FILE.DIR,
            model: any = opt.model,
            project: any = opt.project,
            updateLinks = (list, _template, isTooltip, arrayOfIndexes: Array<Number> = null) => {
                for (let i = 0; i < list.length; i++) {
                    if (arrayOfIndexes && !arrayOfIndexes[i]) {
                        continue;
                    }
                    list[i].link = _template + list[i].fileName;
                    if (i >= 2 && !isTooltip) {
                        list[i].link = null;
                    }
                }
                return list;
            },
            onLoadTemp = function (templateType: any, template: any, next: any) {
                let list = templatesUrls.concat([]);
                const isTooltip = templateType === _DIR.PROJECT_TEMPLATE.TYPES[1],
                    checkSett = () => {
                        const onFinish = () => {
                            if (settings && settings.settings && settings.settings.status) {
                                list = updateLinks(list,
                                    ENTITY.Config.SETTING_TEMPLET_LOC
                                    + settings.settings.data.link
                                    + templateType,
                                    isTooltip);
                            }
                            next(list);
                        };
                        if (settings) {
                            onFinish();
                        } else {
                            if (project || opt.user || model._id) {
                                const req: any = {};
                                if (opt.user) {
                                    req.userId = opt.user._id;
                                } else if (project) {
                                    req.userId = project.owner && project.owner._id ? project.owner._id : project.owner;
                                    req.projId = project._id;
                                } else {
                                    req.projId = model._id;
                                }
                                ENTITY.Http.posts('public/project/settings', req, (res: any) => {
                                    settings = res.json();
                                    onFinish();
                                }, (e: any) => {
                                    alertify.error(e);
                                });
                            } else {

                                console.log('can`t get any parentara');
                                onFinish();

                            }

                        }
                    };
                if (model
                    && model.useGlobals
                    && model.useGlobals[templateType.split('/')[0]]
                    && model.useGlobals[templateType.split('/')[0]].filter((a) => {
                        return JSON.parse(a);
                    }).length > 0) {
                    const parent_link = model.linkGlobal;
                    const arr = model.useGlobals[templateType.split('/')[0]].slice();
                    const arr1 = [];
                    const arr2 = [];
                    arr.forEach((a) => {
                        arr1.push(JSON.parse(a));
                        arr2.push(!JSON.parse(a));
                    });
                    list = updateLinks(list,
                        ENTITY.Config.SETTING_TEMPLET_LOC
                        + parent_link.split('/')[0]
                        + _DIR.DELIMETER
                        + template.split('/')[2]
                        + _DIR.DELIMETER,
                        isTooltip, arr1);
                    next(updateLinks(list,
                        ENTITY.Config.PROJ_LOC
                        + model.projFilesDirname.split('/')[0]
                        + _DIR.DELIMETER
                        + template.replace('assets/', ''),
                        isTooltip, arr2));
                } else if (model && model.templates && model.templates.length) {
                    next(updateLinks(list,
                        ENTITY.Config.PROJ_LOC
                        + model.projFilesDirname.split('/')[0]
                        + _DIR.DELIMETER
                        + template.replace('assets/', ''),
                        isTooltip));
                } else if (opt.user) {
                    if (!opt.user.settings || !opt.user.settings.link) {
                        return checkSett();
                    } else {
                        next(updateLinks(list,
                            ENTITY.Config.SETTING_TEMPLET_LOC
                            + opt.user.settings.link
                            + templateType,
                            isTooltip));
                    }
                } else {
                    checkSett();
                }
            };
        if (opt.templateType && opt.configID) {
            const _template = _DIR.PROJECT_TEMPLATE.NAME + opt.templateType;
            templatesUrls = updateLinks(templatesUrls, _template, opt.templateType === _DIR.PROJECT_TEMPLATE.TYPES[1]);
            onLoadTemp(opt.templateType, _template, (list: any) => {
                let i = 0;
                (function loadTemp() {
                    if (i >= list.length) {
                        return;
                    }
                    if (!list[i].link) {
                        i += 1;
                        return loadTemp();
                    }
                    if (i !== opt.configID) {
                        i += 1;
                        return loadTemp();
                    }
                    i += 1;

                    ENTITY.Http.gets(list[i - 1].link, (res: any) => {
                        if (opt._i !== undefined) {
                            opt.callback(res, list[i - 1], i - 1, opt._i);
                        }else {
                            opt.callback(res, list[i - 1]);
                        }
                        if (i < list.length) {
                            loadTemp();
                        }
                    }, (e: any) => {
                        alertify.error(e);
                    });
                })();
            });
        } else if (opt.templateType) {
            const _template = _DIR.PROJECT_TEMPLATE.NAME + opt.templateType;
            templatesUrls = updateLinks(templatesUrls, _template, opt.templateType === _DIR.PROJECT_TEMPLATE.TYPES[1]);
            onLoadTemp(opt.templateType, _template, (list: any) => {
                let i = 0;
                (function loadTemp() {
                    if (i >= list.length) {
                        return;
                    }
                    if (!list[i++].link) {
                        return loadTemp();
                    }
                    ENTITY.Http.gets(list[i - 1].link, (res: any) => {
                        if (opt._i !== undefined) {
                            opt.callback(res, list[i - 1], i - 1, opt._i);
                        }else {
                            opt.callback(res, list[i - 1]);
                        }
                        if (i < list.length) {
                            loadTemp();
                        }
                    }, (e: any) => {
                        alertify.error(e);
                    });
                })();
            });
        }else {
            let u = 0;
            (function checkUrls() {
                if (u >= _DIR.PROJECT_TEMPLATE.TYPES.length) {
                    return;
                }
                const _template = _DIR.PROJECT_TEMPLATE.NAME + _DIR.PROJECT_TEMPLATE.TYPES[u++];
                templatesUrls = updateLinks(templatesUrls, _template, (u - 1) === 1);
                onLoadTemp(_DIR.PROJECT_TEMPLATE.TYPES[u - 1], _template, (list) => {
                    let i = 0;
                    (function loadTemp() {
                        if (i >= list.length) {
                            return checkUrls();
                        }
                        if (!list[i++].link) {
                            return loadTemp();
                        }
                        ENTITY.Http.gets(list[i - 1].link, (res) => {
                            opt.callback(res, list[i - 1], i - 1, u - 1);
                            if (i < list.length) {
                                loadTemp();
                            } else {
                                checkUrls();
                            }
                        }, (e) => {
                            alertify.error(e);
                        });
                    })();
                });
            })();
        }

    }

    updateCss(value: any) {
        if (value) {
            this.cssElement.innerText = value;
        }
    }

    onDestroy() {
        this.cssElement.parentNode.removeChild(this.cssElement);
    }
}
