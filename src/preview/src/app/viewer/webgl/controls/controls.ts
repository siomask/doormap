import {AbstractChangesView} from '../abstract.changes.view';


export class WControls extends AbstractChangesView {
    constructor(options: any) {
        super(options);
        this.domTemplate.className = 'oxi-controls-container';
        options.onFinish = (t: any) => {
            this.domTemplate.innerHTML = t;
        };

    }

}

