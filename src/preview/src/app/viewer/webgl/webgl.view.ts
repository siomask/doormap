import * as ENTITY from '../../helper/Utils';
import {OxiToolTip} from '../OxiToolTip';
import {OxiSlider} from '../OxiSlider';
import {OxiControls} from '../OxiControls';
import {Viewer} from '../Viewer';
import {OxiViewer} from '../OxiViewer';
import {Preloader} from './preloader/preloader';
import {WTooltip} from './tooltip/tooltip';

declare const alertify: any, Pace: any, TWEEN: any, THREE: any;


export class WebglView extends OxiViewer {
  preview: string;
  projCnt: any;
  app: OxiAPP;
  _container: any;

  constructor(p: Viewer) {
    super(p);
    this.initWebgl();
  }

  initWebgl() {
    if (this.inited) {
      return;
    }
    this.inited = true;

    this._container = document.createElement('div');
    this.parent.container.appendChild(this._container);
    this.app = new OxiAPP(this);
  }

  onDestroy() {
    this.app._animation.stop();
  }

}

class OxiAPP {
  isMobile: boolean;
  imgType: string;
  scene: any;
  model: any;
  camera: any;
  gl: any;
  _angle: number;
  screen: any = {};
  main: WebglView;
  loader: THREE.OBJLoader;
  _projControls: OxiControls;
  _slider: OxiSlider;
  _events: OxiEvents;
  _animation: OxiAnimation;
  controls: any;
  _container: any;
  _preloaderStatus: any;
  private shader: any;
  private isLoading: boolean;
  private category: any;
  infoHTML: Array<OxiToolTip> = [];
  preloader: Preloader;
  _tooltip: WTooltip;
  TEMPLATES: any = {
    TOOLTIP: 'app-project-webgl-tooltip',
    CONTROLS: 'app-project-webgl-controls',
    PRELOADER: 'app-project-preloader'
  };
  HOVER_COLOR: any = {
    ACTIVE: new THREE.Color(0.1, 1.0, 0.1),
    SOLGHT: new THREE.Color(1.0, 0.0, 0.1),
  };
  composer: any;
  selected: any;
  CATEGORIES: any;

  constructor(main: WebglView) {
    this.main = main;
    this.isMobile = false;
    this.imgType = '';
    this._angle = 10;
    this.preloader = this.main.parent.templates[2].template;
    this._tooltip = this.main.parent.templates[1].template;
    this._slider = main.parent._slider;
    this._projControls = main.parent._projControls;

    this._container = main._container;
    this.shader = {
      'outline': {
        vertex_shader: [
          'uniform float opacity;',
          'varying float _opacity;',
          'void main() {',
          'vec4 pos = modelViewMatrix * vec4( position + normal * 25.0, 1.0 );',
          '_opacity = opacity;',
          'gl_Position = projectionMatrix * pos;',
          '}'
        ].join('\n'),

        fragment_shader: [
          'varying float _opacity;',
          'void main(){',
          'gl_FragColor = vec4( 1.0, 1.0, 0.0, _opacity );',
          '}'
        ].join('\n')
      }
    };
    this.scene = new THREE.Scene();
    this.model = new THREE.Object3D();
    this.model.inters = [];
    this.scene.add(this.model);
    const renderer = this.gl = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true
      }),
      SCREEN_WIDTH = this.screen.width = 720,
      SCREEN_HEIGHT = this.screen.height = 405,
      _self = this;
    renderer.domElement.className = 'gl-view';

    if (main.selected.canEdit) {
      main.projCnt.nativeElement.style.height =
        main.projCnt.nativeElement.clientWidth * (SCREEN_HEIGHT / SCREEN_WIDTH) + 'px';
    }
    this._preloaderStatus = document.querySelector('.preloader-data.preloader-status') || {style: {}};
    renderer.setClearColor(0xffffff, 0);
    renderer.setPixelRatio(Math.min(devicePixelRatio, 1));
    renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    this.camera = new THREE.PerspectiveCamera(30, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 200000);
    this.controls = {
      update: () => {
      }, target: this.scene.position.clone()
    };


    /*-----------set config data----------*/
    this.camera.positionDef = new THREE.Vector3(34800, 18600, -600);

    this.camera.updateView = (angle: any) => {
      const _cm = this.selected.camera,
        _p = _cm.frameState[this.selected.currentItem];
      if (!angle) {
        angle = this.selected.currentItem;
      }
      if (_p) {
        this.camera.position.set(_p.x, _p.y, _p.z);
        if (_p.target) {
          this.controls.target.set(_p.target.x, _p.target.y, _p.target.z);
        }
      } else {
        const quaternion = new THREE.Quaternion();
        quaternion.setFromAxisAngle(new THREE.Vector3(0, 1, 0), (angle * this._angle) * Math.PI / 180);
        this.camera.position.copy(this.camera.positionDef.clone().applyQuaternion(quaternion));
        if (_cm.target) {
          this.controls.target.set(_cm.target.x, _cm.target.y, _cm.target.z);
        }
      }
      this.camera.lookAt(this.controls.target);
      this.camera.updateProjectionMatrix();
      this.camera.updateMatrixWorld();
      this.camera.updateMatrix();
      this.main.parent.refresh();

    };
    this.CATEGORIES = {
      FLAT: 0,
      BUILD: 1,
    };
    THREE.Mesh.prototype.getScreenPst = function () {
      const mesh: any = this,
        m = _self.gl.domElement,
        width = m.clientWidth,
        height = m.clientHeight,
        offsetTop = 0,
        offsetlefT = 0,
        widthHalf = width / 2 - offsetlefT,
        heightHalf = height / 2 - offsetTop,
        position: any = mesh.centerPst.clone(),
        _ds = main.selected.camera.views;
      if (_ds &&
        _ds[mesh.name].tooltips &&
        _ds[mesh.name].tooltips &&
        _ds[mesh.name].tooltips[main.selected.currentItem]) {
        const _c = _ds.tooltips[main.selected.currentItem];
        mesh.onscreenParams = new THREE.Vector2(_c.x / 100 * width, _c.y / 100 * height);
        mesh.onscreenParams.isPercent = true;
        return;
      }
      position.project(_self.camera);
      position.x = (position.x * widthHalf) + widthHalf;
      position.y = -(position.y * heightHalf) + heightHalf;
      if (position.x < 0 || position.x > width || position.y > height || position.y < 0) {
        return;
      }
      mesh.onscreenParams = position;
    };
    this._events = new OxiEvents(this);
    this.loadModel(() => {
      this._container.appendChild(renderer.domElement);
      this.camera.updateView();
      setTimeout(() => {
        main.parent.onWindowResize();
      }, 1500);
    });
  }

  private updateCamera() {
    const main = this;
    if (this.selected.camera) {
      //ENTITY.Config.IGNORE = main.selected.camera.forceIgnore ? '-----$$$$$------$$$' : ENTITY.Config.DEF_IGNORE;
      if (main.selected.camera.rotation) {
        this.camera.rotation.x = main.selected.camera.rotation._x;
        this.camera.rotation.y = main.selected.camera.rotation._y;
        this.camera.rotation.z = main.selected.camera.rotation._z;
      }
      if (main.selected.camera.position) {
        this.camera.positionDef.copy(main.selected.camera.position);
      }
      if (main.selected.camera.fov) {
        this.camera.fov = main.selected.camera.fov;
      }
      if (main.selected.camera.scale) {
        this.model.scale.multiplyScalar(main.selected.camera.scale);
      }
      if (main.selected.camera.zoom) {
        this.camera.zoom = main.selected.camera.zoom;
      }
      if (main.selected.camera.target) {
        this.controls.target.copy(main.selected.camera.target);
      }
    }
    this.camera.position.copy(this.camera.positionDef);
    this.camera.updateProjectionMatrix();
    this.camera.updateMatrixWorld();
    this.camera.updateMatrix();
  }

  loadModel(callback: Function = () => {
    console.log('Model loading was finished succesed');
  }) {

    if (this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.gl.clear();
    this.model.traverse((child) => {
      if (child._toolTip) {
        child._toolTip.fade();
      }
    });
    for (let i = 0; i < this.model.children.length; i++) {
      this.model.remove(this.model.children[i]);
    }
    const _self: any = this;
    this.selected = this.main.parent.main.selected;
    if (this.selected.cash.model) {
      this._onLoadModel(this.selected.cash.model, true);
      callback();
      this.isLoading = false;
    } else if (this.selected.projFilesDirname
      && this.selected.destination
      && this.selected.destination.match('.obj')) {
      const loader: THREE.OBJLoader = this.loader = this.loader || new THREE.OBJLoader();
      this.preloader.domTemplate.fade(true);
      loader.load(ENTITY.Config.PROJ_LOC + this.selected.projFilesDirname + '/' + this.selected.destination,
        (object: any) => {
          this._onLoadModel(object);
          if (this.main.parent.isLoaded) {
            this.preloader.domTemplate.fade();
          }
          callback();
          this.isLoading = false;
        }, (e: any) => this.onProgress(e), (e: any) => {
          this.onError(e);
          this._onLoadModel(new THREE.Object3D());
          if (this.main.parent.isLoaded) {
            this.preloader.domTemplate.fade();
          }
          callback();
          this.isLoading = false;
        });
    } else {
      this._onLoadModel(new THREE.Object3D());
      this.isLoading = false;
      alertify.error('some bad model data');
      callback();
    }
  }

  private onError(xhr: any) {
    alertify.error(xhr);
  }

  private onProgress(xhr: any) {
    if (xhr.lengthComputable) {
      this.preloader.onUpdatePreloaderStatus(xhr.loaded / xhr.total);
    }
  }

  private _onLoadModel(object: any, isCashed: boolean = false) {
    this.model.add(object);
    this.model.inters = object.children;
    this.updateCamera();
    object.traverse((child: any) => {
      if (child._toolTip) {
        child._toolTip.fade(true);
      }
    });
    if (isCashed) {
      return;
    }
    const _mat: any = new THREE.MeshBasicMaterial({
        transparent: true,
        opacity: this.main.selected.camera.opacity
      }),
      wTooltip = this.main.parent.templates[0].template,
      meshType: any = 'Mesh';
    this.model.inters = [];
    this.category = this.CATEGORIES.BUILD;
    object.traverse((child: any) => {
      if (child.type === meshType) {
        child.visible = this.main.selected.camera.forceIgnores.indexOf(child.name) < 0;
        if(!child.visible)return;
        child.material = _mat.clone();
        child.material.opacity0 = child.material.opacity;
        child.material.opacity = 0;
        child.material.color = new THREE.Color(Math.random(), Math.random(), Math.random());
        child.renderOrder = 0;
        child.category = -1111;
        child.updateMatrixWorld();
        child.updateMatrix();
        child.geometry.computeBoundingBox();
        child.geometry.computeBoundingSphere();
        const boundingBox = child.geometry.boundingBox;
        child.centerPst = new THREE.Vector3();
        child.centerPst.subVectors(boundingBox.max, boundingBox.min);
        child.centerPst.multiplyScalar(0.5);
        child.centerPst.add(boundingBox.min);
        this.model.inters.push(child);
        const areas: any = this.selected.areas;
        for (let i = 0; areas && i < areas.length; i++) {
          if (areas[i]._id.match(child.name)) {
            child._data = areas[i];
            break;
          }
        }
        const matches = [child._data && child._data.dataSourceId ?
          child._data.dataSourceId || child._data.dataSourceGeneratedId : child.name];
        this.dataSourceMatch(wTooltip.dataElem, matches, (source: any) => {
          if (source.active && !child._data.dataSourceId) {
          } else {
            child._dataSource = source;
          }
        });
        if (child._dataSource) {
          if (child._dataSource.model_type === 'unit') {
            child.category = this.CATEGORIES.FLAT;
          }
        } else if (child._data) {
          if (child._data._category !== ENTITY.Config.PROJ_DESTINATION.ModelStructure) {
            child.category = this.CATEGORIES.FLAT;
          }
        } else if (!child._data) {
          child.category = this.CATEGORIES.FLAT;
        }
        //if (child.name.toLowerCase().match(ENTITY.Config.IGNORE)) {
        //  child.material.color = new THREE.Color(0, 0, 0);
       // } else {
          child._toolTip = new OxiToolTip(child, this.main.parent);
        //}
      }
    });
    this.selected.cash.model = object;
  }

  dataSourceMatch(_data: any, matches: Array<string> = null, onSucces: Function = null) {
    const sources = _data;
    for (let i = 0; sources && i < sources.length; i++) {
      if (!matches) {
        onSucces(sources[i]);
        if (sources[i].children) {
          this.dataSourceMatch(sources[i].children, matches, onSucces);
        }
      } else if (matches.indexOf(sources[i]._id) > -1) {
        return onSucces(sources[i]);
      } else if (sources[i].children) {
        this.dataSourceMatch(sources[i].children, matches, onSucces);
      }
    }
  }


  _parent(): HTMLElement {
    return this.main.parent.container;
  }

  _offset() {
    return this.gl.domElement.getBoundingClientRect();
  }

  onEventPrevent(event: any) {
    event.preventDefault();
    return false;
  }


  render() {
    this.gl.render(this.scene, this.camera);
  }

}

class OxiEvents {
  private EVENTS_NAME: any;
  private mouse: OxiMouse;
  private raycaster: any;
  private main: OxiAPP;
  private canEdit: boolean;
  private pathOnMove: number;
  private lastEv: any;
  private firstSelected: any;
  lastInter: any;
  moveWithDown: any;


  constructor(app: OxiAPP) {
    this.canEdit = false;
    this.pathOnMove = 50;
    const
      elem = app.gl.domElement,
      handler = elem.addEventListener.bind(elem),
      elemDocument = document,
      handlerDocument = (elemDocument.addEventListener).bind(elemDocument);

    this.canEdit = app.main.selected.canEdit;
    this.main = app;
    this.EVENTS_NAME = ENTITY.Config.EVENTS_NAME;
    this.mouse = new OxiMouse(app);
    this.raycaster = new THREE.Raycaster();


    handler(this.EVENTS_NAME.MOUSE_DOWN, (e: any) => this.onMouseDown(e), {passive: false});
    handler(this.EVENTS_NAME.MOUSE_UP, (e: any) => this.onMouseUp(e), {passive: false});
    handler(this.EVENTS_NAME.MOUSE_MOVE, (e: any) => this.onMouseMove(e), {passive: false});

    handler(this.EVENTS_NAME.TOUCH_MOVE, (e: any) => this.onMouseMove(e), {passive: false});
    handler(this.EVENTS_NAME.TOUCH_START, (e: any) => this.onMouseDown(e), {passive: false});
    handler(this.EVENTS_NAME.TOUCH_END, (e: any) => this.onMouseUp(e), {passive: false});


    handlerDocument(this.EVENTS_NAME.TOUCH_START, (e: any) => this.onTouchStart(e), {passive: false});
    handlerDocument(this.EVENTS_NAME.TOUCH_END, (e: any) => this.onTouchEnd(e), {passive: false});
    handlerDocument(this.EVENTS_NAME.MOUSE_UP, (e: any) => this.onTouchEnd(e), {passive: false});


    handler(this.EVENTS_NAME.DB_CLICK, (e: any) => this.onDbClick(e), {passive: false});
    handler(this.EVENTS_NAME.SELECT_START, app.onEventPrevent, {passive: false});
  }

  onWindowResize(w: number = 0, h: number = 0) {
    const app = this.main,
      _w = w,
      _h = h,
      svgEl = this.main.main.parent.fabricViwer
    ;
    app.camera.aspect = _w / _h;
    app.camera.updateProjectionMatrix();
    app.gl.setPixelRatio(Math.min(devicePixelRatio, 1));
    app.gl.setSize(_w, _h);
    console.log(`Setted size width: ${_w}px and height: ${_h}px`);
    if (svgEl) {
      svgEl.resize(_w, _h);
    }
    if (app._animation) {
      app._animation.play();
    }
  }

  private onDocumentMouseUp(ev: any) {
    this.mouse.down = this.lastEv = false;
  }

  private onTouchEnd(ev: any) {
    this.mouse.down = this.lastEv = false;
  }

  private onMouseUp(ev: any) {
    ev.preventDefault();
    if (ev.changedTouches && ev.changedTouches.length) {
      ev.clientX = ev.changedTouches[0].clientX;
      ev.clientY = ev.changedTouches[0].clientY;
    }
    this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.DEFAULT;
    const btn: number = ev.button;
    this.mouse.down = this.lastEv = false;
    this.main._projControls.show(ev, false);
    if (ev.changedTouches) {
      this.onSelected(ev, (inter: any) => {
        this.main._projControls.show(ev);
        if (inter && !this.firstSelected) {
          if (inter.object._data || inter.object._dataSource) {
            this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.POINTER;
          } else {
            this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.UN_AVAILABLE;
          }
          this.firstSelected = inter;
          return;
        } else if (inter) {
          if (this.lastInter && this.lastInter.object.click && this.firstSelected.object === inter.object) {
            this.lastInter.object.click();
          }
          this.main._projControls.show(ev, false);
          this.firstSelected = false;
        } else {
          this.main._projControls.show(ev, false);
          this.firstSelected = false;
        }
      });
    } else {
      switch (btn) {
        case 0:
        case 1: {
          if(this.mouse.moveWithDown)return;
          this.onSelected(ev, (inter: any) => {
            if (inter) {
              if (inter.object.click ) {
                inter.object.click();
              }
            }
          });
          break;
        }
        case 2: {
          break;
        }

      }
    }


  }

  private onMouseMove(ev: any) {
    ev.preventDefault();
    this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.DEFAULT;
    const delta = 2;
    this.mouse.moveWithDown = this.mouse.down && ev && Math.abs(this.mouse.down.clientX-ev.clientX) >delta &&  Math.abs(this.mouse.down.clientY-ev.clientY) >delta?this.mouse.down:false;;

    if (this.mouse.down && ev.buttons === 0) {
      this.mouse.down = null;
    }

    if (this.mouse.down) {
      this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.HOLDING;
      if (this.lastInter) {
        this.main._projControls.show(ev, false);
        this.lastInter = this.firstSelected = null;
      }
      if (ev.changedTouches && ev.changedTouches.length) {
        ev.clientX = ev.changedTouches[0].clientX - ev.target.clientWidth;
        ev.clientY = ev.changedTouches[0].clientY - ev.target.clientHeight;
      }
      if (!this.lastEv) {
        return this.lastEv = ev;
      }
      if (
        Math.abs(ev.clientX - this.lastEv.clientX) > this.pathOnMove
      ) {
        this.main._slider.move((ev.clientX > this.lastEv.clientX ? -1 : 1));
        this.lastEv = ev;
      } else if (Math.abs(ev.clientY - this.lastEv.clientY) > this.pathOnMove) {
        // this.main._slider.move((ev.clientY > this.lastEv.clientY ? -1 : 1));
        this.lastEv = ev;
      }
    } else if (!ev.changedTouches) {
      this.onSelected(ev, (inter: any) => {
        // console.log(ev);
        this.main._projControls.show(ev);
        if (inter) {
          if (inter.object._data || inter.object._dataSource) {
            this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.POINTER;
          } else {
            this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.UN_AVAILABLE;
          }
        }
      });
    }

  }

  private onSelected(ev: any, callback: any, onNoteSame = (last: any) => {
    if (this.lastInter) {
      if (last) {
        if (last.object.uuid === this.lastInter.object.uuid) {
        } else {
          last.object._tooltipPs = last.point.clone();
          this.main._projControls.show(ev, false);
        }
      } else {
        this.lastInter.object._tooltipPs = null;
        this.main._projControls.show(ev, false);
      }

    } else {
      if (last) {
        last.object._tooltipPs = last.point.clone();
      }
    }

    this.lastInter = last;
  }) {
    const intersectList = this.inter(ev);
    if (intersectList && intersectList[0]) {
      //if (intersectList[0].object.name.toLowerCase().match(ENTITY.Config.IGNORE)) {
      //  return onNoteSame(null);
     // }
      onNoteSame(intersectList[0]);
      callback(intersectList[0]);
    } else {
      onNoteSame(null);
      callback(intersectList[0]);
    }
  }

  private onMouseDown(ev: Event) {
    ev.preventDefault();
    this.mouse.down = ev;
    this.lastEv = false;
    this.main.gl.domElement.style.cursor = ENTITY.Config.CURSOR.HOLDING;
  }

  private onTouchStart(ev: Event) {
    this.mouse.down = ev;
  }

  private onMouseOut(ev: any) {
    if (this.mouse.down) {
      this.onMouseUp(ev);
    }
  }

  private onDbClick(e: any) {
    if (this.canEdit) {
      this.main.controls.target.copy(this.main.scene.position);
      this.main.controls.update();
    }
    this.main.onEventPrevent(e);
  }

  onCntxMenu(event: any) {
    event.preventDefault();
    event.stopPropagation();
    return false;
  }

  inter(ev: any, arg: any = null) {
    const _self = this,
      elements = arg && arg.childs ? arg.childs : _self.main.model.inters;

    if (this.mouse.down || !elements) {
      return false;
    }
    if (arg && arg.position) {
      const direction = new THREE.Vector3().subVectors(arg.target, arg.position);
      _self.raycaster.set(arg.position, direction.clone().normalize());
    } else {
      const mouseVector = _self.mouse.interPoint(ev);
      _self.raycaster.setFromCamera(mouseVector, _self.main.camera);
    }

    return _self.raycaster.intersectObjects(elements, true);
  }
}

class OxiMouse {
  main: OxiAPP;
  isDown: boolean;
  down: any;


  constructor(main: any) {
    this.isDown = false;
    this.main = main;
  }

  interPoint(ev: any) {
    const _slider = this.main.gl.domElement,
      rect = _slider.getBoundingClientRect(),
      canvasW = _slider.clientWidth,
      canvasH = _slider.clientHeight;
    let _x = (ev ? ev.clientX : canvasW / 2) - rect.left,
      _y = (ev ? ev.clientY : canvasH / 2) - rect.top
    ;

    if (ev && (ev.changedTouches || ev.touches)) {
      const firstFing = ev.touches.length ? ev.touches[0] : ev.changedTouches[0];
      _x = firstFing.clientX - rect.left;
      _y = firstFing.clientY - rect.top;
    }
    return new THREE.Vector2(((_x) / canvasW) * 2 - 1, -((_y) / canvasH) * 2 + 1);
  }

  cumulativeOffset(element: any) {
    let top = 0, left = 0;
    do {
      top += element.offsetTop || 0;
      left += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);

    return {
      offsetLeft: left,
      offsetTop: top
    };
  }
}

export class OxiAnimation {
  private canAnimate: boolean;
  private isStop: boolean;
  private lastUpdate: number = Date.now();
  private maxTimeUpdate: number;
  private animations: Array<any> = [];
  private lastIter: number;
  private app: Viewer;

  constructor(main: Viewer) {
    this.canAnimate = false;
    this.isStop = false;
    this.maxTimeUpdate = 1500;
    this.lastIter = 0;
    this.app = main;
    this.play();
    setTimeout(() => {
      this.animate();
    }, 100);
  }

  add(callback: Function, id: number = Date.now()) {
    this.animations.push({cb: callback, id: id});
  }

  remove(id: any) {
    for (let i = 0; i < this.animations.length; i++) {
      if (this.animations[i].id === id) {
        return this.animations.splice(i, 1);
      }
    }
  }

  animate() {

    for (let i = 0; i < this.animations.length; i++) {
      this.animations[i].cb();
    }
    TWEEN.update();
    if (this.app.glViwer) {
      this.canAnimate = this.lastUpdate > Date.now();
      if (!this.canAnimate || this.lastIter > 2) {
        this.lastIter = 0;
      }
      this.app.glViwer.app.render();
    }
    requestAnimationFrame(() => {
      this.animate();
    });
  }

  play(flag: boolean = true) {
    this.lastUpdate = Date.now() + (this.maxTimeUpdate);
    if (this.canAnimate) {
      return;
    }
    this.canAnimate = flag || !Pace.running;
  }

  stop() {
    this.isStop = true;
    this.canAnimate = false;
    this.lastIter = 0;
  }
}

