import {AbstractChangesView} from '../abstract.changes.view';


export class Preloader extends AbstractChangesView {

    private prevImg: any;
    preloader: any;
    private progressB: any;

    constructor(opt: any) {
        super(opt);

        this.domTemplate.className = 'preloader-back';
        this.prevImg = document.createElement('img');
        this.prevImg.className = 'preview';
        const self = this;
        this.prevImg.onload = function () {
            self.prevImg.toggle(true);
            self.preloader.toggle(true);
        };
        this.preloader = document.createElement('div');
        this.preloader.className = 'preloader-data';
        opt.onFinish = (template: any) => {
            this.preloader.innerHTML = template;
        };

        this.domTemplate.appendChild(this.prevImg);
        this.domTemplate.appendChild(this.preloader);
        this.onUpdatePreloaderStatus(0);
        const curImg = opt.model.images[0] || opt.model.preview;
        if (!curImg) {
            return;
        }
        let imgSrc = this.Config.PROJ_LOC
            + opt.model.projFilesDirname
            + this.Config.FILE.DIR.DELIMETER
            + this.Config.FILE.DIR.PROJECT_PREVIEW
            + opt.parent.imgType;
        if (opt.parent.imgType === this.Config.FILE.DIR.PREVIEW.WEBP) {
            const imgD = curImg.split('.');
            imgD.pop();
            imgSrc += imgD.join('.') + '.webp';
        } else {
            imgSrc += curImg;
        }
        this.prevImg.src = imgSrc;
        this.prevImg.onload = () => {
            this.domTemplate.toggle(true);
        };

    }

    onUpdatePreloaderStatus(value: any) {
        value = Math.pow(value, 0.25);
        if (value > 1) {
            value = 1;
        }
        if (!this.progressB) {
            this.progressB = document.querySelector('.pre-progress-bar>.pre-progress-status');
        }else {
            this.progressB.style.width = (this.progressB.parentNode.clientWidth * value) + 'px';
        }
    }

}
