import {MTemplatesLoader} from '../templates.loader';
import * as ENTITY from '../../helper/Utils';

declare var Mustache: any;

export class AbstractChangesView {

    tempLoad: any;
    htmlOutPut: any;
    jsTemplate: any;
    dsTemplate: any;
    htmlTemplate: any;
    domTemplate: any;
    htmlUrl: any;
    cssUrl: any;
    parent: any;
    options: any;
    protected DIR: any;
    protected Config: any;
    protected ProjClasses: any;
    protected callbacks: Array<Function>;

    constructor(options: any = {}) {
        this.domTemplate = document.createElement('div');
        this.options = options;
        this.Config = ENTITY.Config;
        this.DIR = ENTITY.Config.FILE.DIR;
        this.ProjClasses = ENTITY.ProjClasses;
        this.callbacks = [];
        this.tempLoad = new MTemplatesLoader(options);
        this.onInit();

    }

    protected onInit() {
        this.tempLoad.callbacks.push(() => {
            this.updateHTMLInput();
            this.htmlTemplate = this.tempLoad.htmlTemplate;
            this.jsTemplate = this.tempLoad.jsTemplate;
            this.dsTemplate = this.tempLoad.dsTemplate;
            if (this.options.onFinish) {
                this.options.onFinish(this.tempLoad.htmlTemplate);
            }
            this.options.container.appendChild(this.domTemplate);
            for (let i = 0; i < this.callbacks.length; i++) {
                this.callbacks[i]();
            }
        });
    }

    updateAfterInput() {
    }

    updateHTMLInput(value: any = null, obj: any = {}) {
        if (!value) {
            value = this.tempLoad.htmlTemplate;
        }

        if (value && typeof value === 'string') {
            let html = '';
            try {
              html = Mustache.render(value, obj);
            } catch (e) {
                console.warn('Some bad input:', e);
            } finally {
                return html;
            }
        }
    }
}

