import {AbstractChangesView} from '../abstract.changes.view';

export class WTooltip extends AbstractChangesView {
    dataSource: any;
    tooltip: any;
    private dataSourceLoaded: boolean;
    private parser: any;
    private dsParser: any;
    dataElem: any;

    /**
     * Constructor of WTooltip
     * @param opt Object
     */
    constructor(opt: any) {
        super(opt);
        this.dataSourceLoaded = false;
        this.dataSource = {properties: [{info_box_header: '', info_box_title: ''}]};
        opt.onFinish = () => {
            const onFinish = () => {
                if (this.tempLoad.htmlTemplate) {
                    this.initParser();
                } else {
                    this.callbacks.push(() => {
                        this.initParser();
                    });
                }
            };
            if (!this.options || !this.options.dataSource) {
                onFinish();
                return console.error('Data source is not defined');
            }
            const res = this.options.dataSource;
            if (res.status) {
                try {
                    this.dataSource = JSON.parse(res.data);
                    this.dataSourceLoaded = true;
                } catch (e) {
                    console.error(e);
                } finally {
                    onFinish();
                }
            } else {
                onFinish();
            }

        };
        this.dataElem = [];
        this.domTemplate.className = this.ProjClasses.PROJ_TOOLTIP_CONTAINER;
        this.tooltip = document.createElement('div');
        this.tooltip.className = 'cos-info';
        this.onInit();
    }

    /**
     * Show template function
     * @param item Object has params inside such as active, alignLeft, alignRight, alignBottom
     */
    showTemplate(item: any) {
        this.tooltip.className = '';
        if (!item) {
            return;
        }
        this.tooltip.innerHTML = item.html;
        if (item.active) {
            this.tooltip.className += 'active act ';
        }
        if (item.alignLeft) {
            this.tooltip.className += 'tooltip-align-left ';
        } else if (item.alignRight) {
            this.tooltip.className += 'tooltip-align-right ';
        } else if (item.alignBottom) {
            this.tooltip.className += 'tooltip-align-bottom ';
        }
    }

    /**
     * Init parser function
     * @param JSvalue string Parser JS code
     * @param JSDSvalue string Data structure js code
     */
    initParser(JSvalue: any = null, JSDSvalue: any = null) {
        let val = JSvalue;
        let valDS = JSDSvalue;
        try {
            if (!JSvalue) {
                val = this.jsTemplate;
            }
            if (!JSDSvalue) {
                valDS = this.dsTemplate;
            }

            if (!val && this.tempLoad) {
                val = this.tempLoad.jsTemplate;
            }
            if (!valDS && this.tempLoad) {
                val = this.tempLoad.dsTemplate;
            }

            this.parser = this.Config.safeJS(val);
            this.dsParser = this.Config.safeJS(valDS);


            const _dataStructure = this.dsParser instanceof Function ? this.dsParser() : [];
            const _data = this.parser instanceof Function ? this.parser({dataSource: this.dataSource, dataStructure: _dataStructure}) : [];
            _data.forEach((el: any) => {
                el.onclick = () => {
                };
            });

            this.dataElem = _data;


        } catch (e) {
            console.error('Incorrect dataSource: ' + e);
        } finally {
            this.updateHTMLInput();
        }
    }

    /**
     * Function that updates HTML input
     * @param value string
     */
    updateHTMLInput(value: any = null) {

        this.dataElem.forEach((el: any) => {
            el.html = super.updateHTMLInput(value, el);
        });
        return '';
    }

}
