import {OxiAnimation, WebglView} from './webgl/webgl.view';
import {SVGView} from './svg.draw/svg.draw';
import {WTooltip} from './webgl/tooltip/tooltip';
import {WControls} from './webgl/controls/controls';
import {Preloader} from './webgl/preloader/preloader';
import {OxiSlider} from './OxiSlider';
import {OxiToolTip} from './OxiToolTip';
import {OxiControls} from './OxiControls';
import {Preview} from '../Preview';
import * as ENTITY from '../helper/Utils';

declare const document: any, Pace: any, THREE: any;

export class Viewer {
    private currentLevel: any;
    private curLoadedTemplates: number;
    private _dataSourceRemote: any;
    private _lastSize: any = {
        width: 0,
        height: 0,
    };

    fabricViwer: SVGView;
    glViwer: WebglView;
    currentViwer: any;
    _angle: number;
    isLoaded: boolean;
    isMobile: boolean;
    HOVER_COLOR: any = {
        ACTIVE: new THREE.Color(0.1, 1.0, 0.1),
        SOLGHT: new THREE.Color(1.0, 0.0, 0.1),
    };
    imgType: string;
    templates: Array<any> = [];
    _animation: OxiAnimation;
    _slider: OxiSlider;
    _projControls: OxiControls;
    infoHTML: Array<OxiToolTip> = [];
    container: any;
    tooltipParent: any;
    main: Preview;
    settingsHelper: any = {
        outLine: {
            opacity: 1,
            lineWidth: 6,
            color: 'yellow',
        },
        hover: {
            active: {
                color: '#00ff00',
                opacity: 0.7
            },
            unactive: {
                color: '#ff0000',
                opacity: 0.7
            }
        }

    };
    STYLES: Array<any> = [
        {elem: 'view-outline', props: ['color', 'line-height', 'opacity'], out: {}},
        {elem: 'view-fill-active', props: ['color', 'opacity'], out: {}},
        {elem: 'view-fill-in-active', props: ['color', 'opacity'], out: {}}
    ];

    constructor(main: Preview) {
        this.curLoadedTemplates = 0;
        this._angle = 10;
        this.isLoaded = false;
        this.imgType = '';
        this.main = main;
        this.container = document.createElement('div');
        this.container.className = 'viewer webgl-view';
        document.body.appendChild(this.container);
        this.isMobile = this.deviceCheck();

        this.checkLoadedImg(() => {
            this.loadDataSource(
                () => {
                    this.templates = [
                        {
                            opt: {
                                templateType: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE._TYPE.TOOLTIP,
                                dataSource: this._dataSourceRemote
                            }, ins: WTooltip
                        },
                        {opt: {templateType: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE._TYPE.CONTROLS}, ins: WControls},
                        {opt: {templateType: ENTITY.Config.FILE.DIR.PROJECT_TEMPLATE._TYPE.PRELOADER}, ins: Preloader},
                    ];
                    this.templates.forEach((el: any) => {
                        el.opt.container = this.container;
                        el.opt.model = this.main.selected.parent;
                        el.opt.parent = this;
                        el.template = new el.ins(el.opt);
                        el.template.callbacks.push(() => {
                            this.onFinishLoadTemplates();
                        });
                    });

                }
            );
        });

    }

    updateAngle() {
        this._angle = 360 / this.main.selected.images.length;
    }

    updateInfoHTML() {
        for (let i = 0; i < this.infoHTML.length; i++) {
            this.infoHTML[i].update();
        }
    }

    refresh() {
        this._animation.play();
    }

    onWindowResize() {
        const _w = window.innerWidth,
            _h = window.innerHeight,
            _px = 'px';
        let _fW = this._slider.currentFrame.naturalWidth,
            _fH = this._slider.currentFrame.naturalHeight,
            sizeX = _w,
            sizeY = _w * _fH / _fW;


        if (_fW === 0 || _fH === 0) {
            _fW = this._lastSize.width;
            _fH = this._lastSize.height;
        } else {
            this._lastSize.width = _fW;
            this._lastSize.height = _fH;
        }

        if (_w / _h > _fW / _fH) {
            sizeY = _h;
            sizeX = _h * _fW / _fH;
        } else {
            sizeX = _w;
            sizeY = _w * _fH / _fW;
        }
        this.container.style.width = sizeX + _px;
        this.container.style.height = sizeY + _px;

        if (this.currentViwer) {
            if (this.currentViwer === this.fabricViwer) {
                this.fabricViwer.resize(sizeX, sizeY);
            } else if (this.currentViwer === this.glViwer) {
                this.glViwer.app._events.onWindowResize(sizeX, sizeY);
                this.refresh();
            }
        }


        this.updateInfoHTML();
    }

    selectViewer(child: any, _onFinish: Function = () => {
    }) {
        let progressInterval,
            progressIterator: any = 0;
        const preloaderTemp: Preloader = this.templates[2].template,
            preloader = preloaderTemp.domTemplate,
            _cV = this.currentViwer,
            onFinish = () => {
                if (this._slider.isLoaded) {
                    preloader.fade();
                    clearInterval(progressInterval);
                } else {
                    this._slider.onFinish = () => {
                        preloader.fade();
                        this._slider.onFinish = null;
                        clearInterval(progressInterval);
                    };
                }

            },
            onProgress = () => {
                progressIterator++;
                preloaderTemp.onUpdatePreloaderStatus(progressIterator / 100);
            },
            resize = () => {
                setTimeout(() => {
                    if (this.currentViwer === this.glViwer) {
                        this.currentViwer.app.model.traverse((mesh) => {
                            if (mesh._toolTip && mesh._toolTip._parentDom) {
                                mesh._toolTip._parentDom.appendChild(mesh._toolTip.html);
                            }
                        });
                    } else if (this.currentViwer === this.fabricViwer) {
                        this.currentViwer.fabricJS._objects.forEach((el) => {
                            if (el._toolTip && el._toolTip._parentDom) {
                                el._toolTip._parentDom.appendChild(el._toolTip.html);
                            }
                        });
                    }
                    this.onWindowResize();
                    if (this.currentViwer === this.glViwer) {
                    } else {
                        this.currentViwer.isFinish = true;
                    }
                    _onFinish();
                    onFinish();
                }, 100);
            };


        this.currentViwer = false;
        this._slider.update();
        if (!this._slider.isLoaded) {
            onProgress();
            progressInterval = setInterval(() => {
                onProgress();
            }, 20);
            preloader.fade(true);
        }
        this._projControls.update();
        this.refresh();

        if (_cV) {
            _cV._container.fade();
            if (_cV === this.glViwer) {
                _cV.app.model.traverse((mesh) => {
                    if (mesh._toolTip) {
                        if (!mesh._toolTip._parentDom) {
                            mesh._toolTip._parentDom = mesh._toolTip.html.parentNode;
                        }
                        try {
                            mesh._toolTip._parentDom.removeChild(mesh._toolTip.html);
                        } catch (e) {

                        }
                    }

                });
            } else if (_cV === this.fabricViwer) {

                _cV.removeItems();
            }
        }

        if (child.camera && child.camera.isSVG) {
            if (!this.fabricViwer) {
                this.fabricViwer = new SVGView(this);
                this.onWindowResize();
                setTimeout(() => {
                    this.fabricViwer.loadModel(() => {
                        resize();
                    });
                }, 500);
            } else {
                this.fabricViwer.loadModel(() => {
                    resize();
                });
            }
            this.currentViwer = this.fabricViwer;
        } else {
            if (!this.glViwer) {
                this.glViwer = new WebglView(this);
                resize();
            } else {
                this.glViwer.app.loadModel(() => {
                    this._slider.updateView();
                    resize();

                });
            }
            this.currentViwer = this.glViwer;
        }
        if (this.currentViwer) {
            this.currentViwer._container.fade(true);
            this._projControls.update();

        }

    }

    private clearTooltips() {
        this.tooltipParent.innerHTML = '';
    }

    private onFinishLoadTemplates() {
        if (++this.curLoadedTemplates === this.templates.length) {
            let progressIterator: any = 0;
            const preloaderTemp: Preloader = this.templates[2].template,
                preloader = preloaderTemp.domTemplate,
                onFinish = (progressInterval) => {
                    const onEnd = () => {
                        clearInterval(progressInterval);
                        window.addEventListener('resize', () => this.onWindowResize());
                        this.parseStyles();
                        this.selectViewer(this.main.selected, () => {
                            this.isLoaded = true;
                            preloader.fade();
                        });
                    };
                    if (this._slider.isLoaded) {
                        onEnd();
                    } else {
                        this._slider.onFinish = () => {
                            onEnd();
                            this._slider.onFinish = null;
                        };
                    }
                },
                onProgress = () => {
                    progressIterator++;
                    preloaderTemp.onUpdatePreloaderStatus(progressIterator / 100);
                };
            onProgress();
            preloader.toggle(true);

            this._slider = new OxiSlider(this);
            this._animation = new OxiAnimation(this);
            this._projControls = new OxiControls(this);
            const _inter = setTimeout(() => {
                Pace.stop();
                const progressInterval = setInterval(() => {
                    onProgress();
                }, 20);
                onFinish(progressInterval);

            }, 2000);
        }
    }

    private parseStyles() {
        const _settingsHelper = this.settingsHelper;
        this.STYLES.forEach((style, key) => {
            const domElem = document.querySelector('.' + style.elem);
            if (domElem) {
                style.props.forEach((prop) => {
                    style.out[prop] = this.dumpComputedStyles(domElem, prop);
                });
            }
            const lineWidth = parseInt(style.out['line-height'], 10),
                color = style.out['color'],
                opacity = style.out['opacity'];

            switch (key) {
                case 0: {
                    // outline
                    if (!isNaN(lineWidth)) {
                        _settingsHelper.outLine.lineWidth = lineWidth;
                    }
                    if (!isNaN(opacity)) {
                        _settingsHelper.outLine.opacity = opacity;
                    }
                    if (color) {
                        _settingsHelper.outLine.color = color;
                    }
                    break;
                }
                case 1: {
                    // fll active
                    if (!isNaN(opacity)) {
                        _settingsHelper.hover.active.opacity = opacity;
                    }
                    if (color) {
                        _settingsHelper.hover.active.color = color;
                        this.HOVER_COLOR.ACTIVE = new THREE.Color(color);
                    }
                    break;
                }
                case 2: {
                    // fll for unactive
                    if (!isNaN(opacity)) {
                        _settingsHelper.hover.unactive.opacity = opacity;
                    }
                    if (color) {
                        _settingsHelper.hover.unactive.color = color;
                        this.HOVER_COLOR.SOLGHT = new THREE.Color(color);
                    }
                    break;
                }
            }
        });

    }

    private dumpComputedStyles(elem, prop) {
        const cs = window.getComputedStyle(elem, null);
        if (prop) {
            return cs.getPropertyValue(prop);
        }
        const len = cs.length;
        for (let i = 0; i < len; i++) {
            const style = cs[i];
        }

    }

    private loadDataSource(callback) {
        this.main.http.post('public/model/remote', {id: this.main.selected.parent._id}, (res: any) => {
            this._dataSourceRemote = res.json();
            callback();
        }, (res) => {
            callback();
            console.error('Incorrect dataSource: ' + res && res.message ? res.message : res);
        });
    }

    private checkLoadedImg(callback: any) {
        const _self = this,
            allows = ['1', '2'],
            checkLow = () => {
                if (this.isMobile) {
                    this.imgType = ENTITY.Config.FILE.DIR.PREVIEW.LOW;
                }
                callback();
            };

        if (this.main.selected.images.length) {
            const isAllowed: any = window.localStorage.getItem(ENTITY.Config.FILE.DIR.PREVIEW.WEBP);
            if (isAllowed) {
                if (isAllowed === allows[0]) {
                    _self.imgType = ENTITY.Config.FILE.DIR.PREVIEW.WEBP;
                    callback();
                } else {
                    checkLow();
                }
            } else {
                const img = new Image();
                img.onload = function () {
                    const isAllow = img.height > 0 && img.width > 0;
                    window.localStorage.setItem(ENTITY.Config.FILE.DIR.PREVIEW.WEBP, isAllow ? allows[0] : allows[1]);
                    if (isAllow) {
                        _self.imgType = ENTITY.Config.FILE.DIR.PREVIEW.WEBP;
                        callback();
                    } else {
                        checkLow();
                    }

                };
                img.onerror = function () {
                    window.localStorage.setItem(ENTITY.Config.FILE.DIR.PREVIEW.WEBP, allows[1]);
                    checkLow();
                };
                img.src = ENTITY.Config.HOST_NAME + 'assets/img/img_large_0_4.webp';
            }
        } else {
            checkLow();
        }
    }

    private deviceCheck() {
        let check: any = false;
        (function (a: any) {
            if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
                check = true;
            }
        })(navigator.userAgent || navigator.vendor || window['opera']);
        return check;
    }
}
