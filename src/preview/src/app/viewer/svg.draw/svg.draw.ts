import * as ENTITY from '../../helper/Utils';
import {OxiToolTip} from '../OxiToolTip';
import {Viewer} from '../Viewer';
import {OxiViewer} from '../OxiViewer';
import {WTooltip} from '../webgl/tooltip/tooltip';

declare var fabric: any;
declare var alertify: any;
declare var TWEEN: any;
declare var polylabel: any;

export class SVGView extends OxiViewer {

    currentShape: any;
    lastSelectedShape: any;
    shapes: any;
    settings: any;
    fabricJS: any;
    _container: any;
    dataEl: any;
    selected: any;
    glapp: any;
    sources: WTooltip;
    private isFinish: boolean;
    private options: any = {};
    private curFill: any;
    private curImgZoom: any;
    private curImgBufferZoom: any;
    private mode: any;
    private MODES: any = {};
    private MOUSE: any;
    private defOpacity: number;
    private lastSelected: any;
    private settingsHelper: any;
    private firstTouched: any;

    constructor(p: Viewer) {
        super(p);
        this.isFinish = false;
        this.defOpacity = 0.01;
        this.settingsHelper = p.settingsHelper;
        this.MODES = {EDIT: 1, ADD: 2, NORMAL: 3, NO: 4};
        this.MOUSE = {DOWN: 1, UP: 2, CUR: 0, GROUP: 3};
        this.mode = this.MODES.ADD;
        this.curImgZoom = new Image();
        this.curImgBufferZoom = new Image();

        this._container = document.createElement('div');
        this._container.className = 'svg-view';
        this.dataEl = document.createElement('canvas');
        this._container.appendChild(this.dataEl);
        p.container.appendChild(this._container);

        this.afterViewInit();
    }

    loadModel(callback) {
        this.selected = this.parent.main.selected;
        return this.updateView(-1, callback);
    }

    updateView(_prevItem: number = -1, next) {
        const _self = this,
            _selected = this.selected,
            prevItem = (typeof _prevItem === 'number' && _prevItem > -1 ? _prevItem : _selected.currentItem),
            _next = () => {
                this.updateTooltipsVisible();
                if (next) {
                    next();
                }
            };

        if (_prevItem === _selected.currentItem) {
            return;
        }
        this.removeItems();
        this.isFinish = false;
        this.fabricJS._set = {
            _w: 0,
            _h: 0,
            _viewBox: [0, 0, 0, 0]
        };

        if (_selected.svgDestination && _selected.svgDestination[prevItem]) {
            const svgUrl = ENTITY.Config.PROJ_LOC + this.selected.projFilesDirname + ENTITY.Config.FILE.DIR.DELIMETER +
                (typeof _selected.svgDestination === 'string' ? _selected.svgDestination : _selected.svgDestination[prevItem]);

            console.log(444, svgUrl);
            this.parent.main.http.get(svgUrl, (res: any) => {
                if (!res) {
                    return alertify.error('no svg found');
                }
                fabric.loadSVGFromString(res, (o, options) => {
                    this.options = options;
                    this.parseSVG(res, ((_objects, optionss) => {
                        this.fabricJS._set = {
                            _w: (options.width),
                            _h: (options.height),
                            _viewBox: optionss.viewBox
                        };
                        this.removeItems();

                        if (options) {
                            options.objects = o;
                        }
                        _self.onFinishParse(o, options, _objects);
                        _next();
                    }));
                });
            }, () => {
                console.error('Error getting the SVG');
            });

        } else {
            alertify.warning('There is nothing here');
            _next();
        }
    }

    removeItems() {
        const _self = this;
        this.fabricJS._lastObjects = this.fabricJS._objects.concat([]);
        for (let i = 0; i < this.fabricJS._objects.length; i++) {
            const el = this.fabricJS._objects[i];
            if (el._toolTip) {
                if (!el._toolTip._parentDom) {
                    el._toolTip._parentDom = el._toolTip.html.parentNode;
                }
                try {
                    el._toolTip._parentDom.removeChild(el._toolTip.html);
                } catch (e) {

                }
            }

            if (el._isHovered) {
                this.fadeSelected(this.fabricJS._objects[i], false, false, true);
            }
        }
        _self.fabricJS.clear();
    }

    getRandomColor() {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    public fadeSelected(target: any, show: boolean = false, fff: boolean = false, quick: boolean = false) {
        if (target.type === this.shapes.POLYLINE) {
            return;
        }
        target.isStartHovered = show;

        const levelOpacity = this.defOpacity,
            isGreen = this.isAvailableForAction(target),
            endO = (show) ?
                (isGreen ? this.settingsHelper.hover.active.opacity : this.settingsHelper.hover.unactive.opacity) : 0.99 || this.defOpacity,
            lineDeltaOpacity = this.settingsHelper.outLine.opacity,
            onFinish = () => {
                if (!show) {
                    if (target.type === this.shapes.GROUP) {
                        for (let i = 0; i < target._objects.length; i++) {
                            target._objects[i].set('fill', target._objects[i].defFill);
                            if (quick) {
                                target._objects[i].set('opacity', levelOpacity);
                                if (target._objects[i]._border) {
                                    target._objects[i]._border.set('opacity', 0);
                                }
                            }
                        }
                    } else {
                        target.set('fill', target.defFill);

                        if (quick) {
                            target.set('opacity', levelOpacity);
                            if (quick) {
                                target._border.set('opacity', 0);
                            }
                        }
                    }
                }
                target._isHovered = show;
                target._hasHavered = target.isStartHovered = false;
                this.fabricJS.renderAll();
                this.currentShape = show ? null : target;
            };

        target._toolTip.display(show);

        if (quick) {

            onFinish();
        } else {
            const
                delta = 1,
                _opct = show ? delta * endO : (1 - endO * delta),
                lineOpacity = show ? lineDeltaOpacity * delta : (1 - delta);
            if (target.type === this.shapes.GROUP) {
                for (let i = 0; i < target._objects.length; i++) {
                    const el = target._objects[i];
                    if (!el.defFill) {
                        el.defFill = el.fill;
                    }
                    if (!el.hoverFill) {
                        el.hoverFill = isGreen ? this.settingsHelper.hover.active.color : this.settingsHelper.hover.unactive.color;
                    }
                    el.set('fill', el.hoverFill);
                    el.set('opacity', _opct);
                    if (el._border) {
                        el._border.set('opacity', lineOpacity);
                    }
                }
            } else {
                if (!target.hoverFill) {
                    target.hoverFill = isGreen ? this.settingsHelper.hover.active.color : this.settingsHelper.hover.unactive.color;
                }
                target.set('fill', target.hoverFill).set('opacity', _opct);
                if (target._border) {
                    target._border.set('opacity', lineOpacity);
                }
            }
            onFinish();
            this.fabricJS.renderAll();
        }

        return target;
    }

    resize(_w = null, _h = null, options: any = null) {
        if (!this.fabricJS) {
            return;
        }
        _w = _w || this._container.clientWidth;
        _h = _h || this._container.clientHeight;
        this.fabricJS._set._w = this.fabricJS._set._w || _w;
        this.fabricJS._set._h = this.fabricJS._set._h || _h;
        const prevW = this.fabricJS._set._w,
            prevH = this.fabricJS._set._h,
            scaleMultiplierX = _w / prevW,
            scaleMultiplierY = _h / prevH,
            scaleX0 = scaleMultiplierX,
            scaleY0 = scaleMultiplierY,
            objects = this.fabricJS._objects;
        let zoom = 1;
        console.log(`Setted size width: ${_w}px and height: ${_h}px`);
        if (_w) {
            this.fabricJS.setWidth(_w);
        }
        if (_h) {
            this.fabricJS.setHeight(_h);
        }
        if (prevW < _w) {
            zoom = scaleMultiplierY;
            if ((zoom * prevW) > _w) {
                zoom = _w / prevW;
            }
        } else {
            zoom = scaleMultiplierX;
            if ((zoom * prevH) > _h) {
                zoom = _h / prevH;
            }
        }
        if (!isNaN(zoom)) {
            this.fabricJS.setZoom(zoom);
        }
        this.fabricJS.calcOffset().renderAll();
    }

    exportJSON(e) {
        e.preventDefault();
        const
            _self = this,
            fileName = 'export',
            pathesStore = [],
            scale = 100,
            xR = scale / _self.fabricJS.width,
            yR = scale / _self.fabricJS.height,
            round = 2;
        let itr = 0;

        (function parse(elements) {
            elements.forEach((el) => {
                if (el.type === _self.shapes.POLYGON) {
                    pathesStore.push({id: 'a' + itr++, path: el.points});
                } else if (el._objects) {
                    parse(el._objects);
                }
            });
        })(this.fabricJS._objects);


        pathesStore.forEach(function (o: any, i) {
            o.path = o.path.reduce((p, c, n) => {
                let s = p;
                if (p instanceof fabric.Point) {
                    s = 'M ' + (c.x * xR).toFixed(round) + ',' + (c.y * yR).toFixed(round);
                }
                return s + ' L ' + (c.x * xR).toFixed(round) + ',' + (c.y * yR).toFixed(round);
            }) + ' Z';
        });
        const file = new File([JSON.stringify(pathesStore)], fileName, {type: 'text/json'}),
            a = document.createElement('a');
        a.setAttribute('href', URL.createObjectURL(file));
        a.download = fileName + '.json';
        a.innerHTML = fileName;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }

    private afterViewInit() {
        let svg = this.selected.svgDestination;
        if (svg instanceof Array) {
            svg = svg[0].name;
        }

        const _self: any = this;
        fabric.Object.prototype.set({
            selectable: false,
            transparentCorners: false,
            cornerColor: 'rgba(102,153,255,0.5)',
            cornerSize: 12,
            padding: 7,
            _add: function (e) {
                this.remove(e);

                e._parent = this;
                return this.addWithUpdate(e);
            },
            dropSelf: function () {
                if (this._objects) {
                    while (this._objects.length) {
                        this._objects[0].dropSelf();
                    }

                } else if (this.type === _self.shapes.CIRCLE) {
                    if (this.parent) {
                        return this.parent.dropSelf();
                    }
                    if (this._parent) {
                        this._parent.remove(this);
                    } else {
                        _self.fabricJS.remove(this);
                    }
                } else {
                    const _p = this.get('_points');
                    if (_p) {
                        _p.forEach((e) => {
                            if (e._parent) {
                                e._parent.remove(e);
                            } else {
                                _self.fabricJS.remove(e);
                            }
                        });
                    }
                }
                if (this._dataSource) {
                    this._dataSource.active = false;
                }
                if (this._parent) {
                    this._parent.remove(this);
                } else {
                    _self.fabricJS.remove(this);
                }
                _self.fabricJS.renderAll();
                _self.toSVG();

            },
            _clone: function (isHard, its, withTransform, byPnts, withoutParent) {
                const selfT: any = this;
                this.setCoords();
                const clone = [
                        '_pointsList',
                        '_center',
                        '_centerProcessed',
                        'lastLeft',
                        'lastTop',
                        'fill',
                        'onOut',
                        'click',
                        'hoverFill',
                        'defFill',
                        'opacity',
                        '_hasUpdate',
                        'scaleX0',
                        'scaleY0',
                        'points0',
                        'id',
                        '_toolTip',
                        '_data',
                        '_dataSource',
                        'material',
                        'click',
                        'selectable',
                        '_objects',
                        '_border'
                    ],
                    _pn = this.get('_points');
                let _points = isHard || this._hasUpdate ? this.get('points') : this.get('points').map((el, d) => {
                        return new fabric.Point((el.x) * this.scaleX0, (el.y) * this.scaleY0);
                    }),
                    newObj = new this.constructor(_points);


                if (byPnts) {
                    if (selfT.get('_pointsList')) {
                        _points = selfT.get('_pointsList').map((point) => {
                            return new fabric.Point((point.x), (point.y));
                        });
                        newObj = new selfT.constructor(_points);
                    }
                }

                for (let i = 0; i < clone.length; i++) {
                    newObj[clone[i]] = this[clone[i]];
                }
                newObj.perPixelTargetFind = !_self.selected.canEdit;
                if (!newObj.id) {
                    newObj.set('id', ENTITY.Config.randomstr());
                }


                const lineWidth = _self.settingsHelper.outLine.lineWidth,
                    delimeter = lineWidth / 2,
                    lineBorder = new fabric.Polyline(_points.concat(_points[0]).map((el) => {
                        return new fabric.Point((el.x), (el.y));
                    }), {
                        left: newObj.left - delimeter,
                        top: newObj.top - delimeter,
                        stroke: _self.settingsHelper.outLine.color,
                        opacity: 0,
                        strokeWidth: lineWidth,
                        strokeLineCap: 'round',
                        strokeLineJoin: 'round',
                        selectable: false,
                        perPixelTargetFind: true,
                        fill: null
                    });
                if (this._border) {
                    _self.fabricJS.remove(this._border);
                }

                newObj._border = lineBorder;

                if (!withoutParent) {
                    if (this._parent) {
                        this._parent._add(newObj);
                        newObj._border._parentS = this._parent;
                    } else {
                        _self.fabricJS._add(newObj);
                        newObj._border._parentS = newObj;
                    }
                }

                if (_pn) {
                    newObj.set('_points', _pn);

                }
                if (this._parent) {
                    this._parent.remove(this);
                } else {
                    _self.fabricJS.remove(this);
                }
                newObj.setCoords();
                _self.fabricJS._add(lineBorder);
                return newObj;
            },
            getCenterInPath: function () {
                const points = this.get('points'),
                    center = this.getCenterPoint();
                const p = polylabel([points.slice().map(_p => [_p.x + center.x, _p.y + center.y])], 1);
                console.log('p', p);
                return center;

            },
            getScreenPst: function () {

                if (this.type === _self.shapes.POLYGON) {
                    this.onscreenParams = this._centerProcessed;
                } else {
                    this.onscreenParams = this.getCenterPoint();
                }
                this.onscreenParams = {
                    x: this.onscreenParams.x * _self.fabricJS.getZoom(),
                    y: this.onscreenParams.y * _self.fabricJS.getZoom()
                };
            },
            getBoundingBox: function () {
                if (this.type === _self.shapes.POLYGON) {
                    const points = this.get('points');
                    let xMin, xMax, yMin, yMax;

                    xMin = xMax = yMin = yMax = 0;
                    for (let i = 0; i < points.length; i++) {
                        const x = points[i].x,
                            y = points[i].y;

                        if (x < xMin) {
                            xMin = x;
                        }
                        if (x > xMax) {
                            xMax = x;
                        }

                        if (y < yMin) {
                            yMin = y;
                        }
                        if (y > yMax) {
                            yMax = y;
                        }
                    }

                    this.width = xMax - xMin;
                    this.height = yMax - yMin;
                    if (this._parent) {

                        this.minX = this._parent.left - xMin;
                        this.minY = this._parent.top - yMin;
                    } else {
                        this.minX = xMin;
                        this.minY = yMin;

                    }
                    this.left += this.minX;
                    this.top += this.minY;

                }
            }
        });
        fabric.Canvas.prototype.set({
            _add: function (e) {
                this.remove(e);
                e._parent = this;
                return this.add(e);
            }
        });
        this.fabricJS = new fabric.Canvas(this.dataEl, {
            selection: false,
            borderColor: 'red',
            cornerColor: 'green',
            cornerSize: 6,
            width: this._container.clientWidth,
            height: this._container.clientHeight,
            enableRetinaScaling: false
        });
        this.curFill = this.getRandomColor();
        this.settings = {
            CIRCLE: {
                radius: 1,
                opacity: 0.75,
                strokeWidth: 5,
                stroke: 'red',
                selectable: true,
                excludeFromExport: true,
                hasControls: false,
                originX: 'center',
                originY: 'center'
            },
            POLYGON: {
                fill: this.curFill,
                opacity: this.defOpacity,
                selectable: false,
                perPixelTargetFind: !this.selected.canEdit

            }, GROUP: {
                selectable: false,
                perPixelTargetFind: !this.selected.canEdit
            }
        };
        this.shapes = {
            POLYLINE: 'polyline',
            GROUP: 'group',
            POLYGON: 'polygon',
            CIRCLE: 'circle'
        };

        const fabricJS = this.fabricJS;
        fabricJS.mouse = {pathOnMove: 50};
        fabricJS.on('mouse:out', (e) => {
            if (e.e.changedTouches) {
                return;
            }
            this.lastSelected = null;
            if (!e.target) {
                return;
            }
            if (e.target._toolTip && e.target._toolTip.compare(e.e.relatedTarget)) {
            } else {
                this.intersectingCheck(e.target, () => {
                    this.fadeSelected(e.target);
                });
            }
        });

        fabricJS.on('mouse:over', (e) => {
            if (e.e.changedTouches) {
                return;
            }
            const lastSelected = this.lastSelected;
            this.lastSelected = null;
            if (lastSelected && e.target && lastSelected._isHovered && lastSelected.id !== e.target.id) {
                this.fadeSelected(lastSelected, false, false, true);
            }
            if (!e.target || fabricJS.mouse.down || e.target.isStartHovered || e.target._hasHavered) {
                return;
            }

            this.intersectingCheck(e.target, () => {

                this.lastSelected = this.fadeSelected(e.target, true);

                if (this.lastSelected) {
                    this.lastSelected.onOut = function () {
                        if (this._isHovered) {
                            _self.fadeSelected(this);
                        }
                    };
                }
            });

        });
        fabricJS.on('mouse:up', (e) => {
            const ev = e.e;
            ev.preventDefault();
            fabricJS.mouse.lastEv = null;
            const hasMove = fabricJS.mouse.move;
            fabricJS.mouse.down = fabricJS.mouse.move = false;
            if (((e.target && !this.firstTouched)) && ev.changedTouches && e.target) {
                this.firstTouched = e.target;
                this.fadeSelected(e.target, true);
                return;
            } else if (e.target !== this.firstTouched && ev.changedTouches && e.target) {
                this.fadeSelected(this.firstTouched);
                this.firstTouched = e.target;
                this.fadeSelected(this.firstTouched, true);
                return;
            }
            if (e.target && e.target.click && (e.target === this.firstTouched || !ev.changedTouches)) {
                if (this.firstTouched) {
                    this.fadeSelected(this.firstTouched);
                    this.firstTouched = null;
                }

                e.target.click();
            } else if (this.firstTouched) {
                this.fadeSelected(this.firstTouched);
                this.firstTouched = null;
            }
            if (!e.target || hasMove) {
                return;
            }
        });
        fabricJS.on('mouse:down', (e) => {
            e.e.preventDefault();
            fabricJS.mouse.down = e;
        });
        fabricJS.on('mouse:move', (e) => {
            const ev = e.e;
            ev.preventDefault();
            if (fabricJS.mouse.down) {
                fabricJS.mouse.move = e;
                if (ev.touches && ev.touches.length) {
                    ev.clientX = ev.touches[0].clientX;
                    ev.clientY = ev.touches[0].clientY;
                }
                if (!fabricJS.mouse.lastEv) {
                    return fabricJS.mouse.lastEv = ev;
                }
                if (
                    Math.abs(ev.clientX - fabricJS.mouse.lastEv.clientX) > fabricJS.mouse.pathOnMove
                ) {
                    this.parent._slider.move((ev.clientX < fabricJS.mouse.lastEv.clientX ? 1 : -1));
                    fabricJS.mouse.lastEv = ev;
                } else if (Math.abs(ev.clientY - fabricJS.mouse.lastEv.clientY) > fabricJS.mouse.pathOnMove) {
                    fabricJS.mouse.lastEv = ev;
                }
            } else {
                if (ev.changedTouches) {
                    return;
                }
                e.e.target.style.cursor = 'default';
                if (!this.lastSelected) {
                    if (e.target) {
                        e.e.target.style.cursor = ENTITY.Config.CURSOR.POINTER;

                        this.intersectingCheck(e.target, () => {

                            if (!this.firstTouched) {
                                this.lastSelected = this.fadeSelected(e.target, true);
                                this.firstTouched = e.target;
                            } else {
                                this.firstTouched = e.target;
                            }
                        });
                    } else {
                        for (let i = 0; i < this.fabricJS._objects.length; i++) {
                            if (this.fabricJS._objects[i]._isHovered) {
                                this.fadeSelected(this.fabricJS._objects[i]);
                            }
                        }
                        this.firstTouched = null;
                    }
                } else if (this.lastSelected && !e.target) {
                    // e.e.target.style.cursor = '';
                    this.intersectingCheck(e.target, () => {
                        this.fadeSelected(e.target);
                        this.lastSelected = null;
                        this.firstTouched = null;
                    });
                } else {
                    e.e.target.style.cursor = ENTITY.Config.CURSOR.POINTER;
                }
            }
        });
        fabricJS.on('after:render', function () {
            fabricJS.calcOffset();
        });
        this.sources = this.parent.templates[0].template;
    }

    private updateTooltipsVisible() {
        const wholeElem = this.selected.cash.svg;
        for (let i = 0; i < wholeElem.length; i++) {
            if (wholeElem[i]) {
                while (wholeElem[i].length) {
                    const _el = wholeElem[i].shift();
                    if (_el._tooltip) {
                        _el._tooltip.destroy();
                    }
                }
            }
        }
    }

    private onFinishParse(o, options, _objects) {
        const _self = this;
        if (o && options) {
            options.objects = o;
            _self.resize(false, false, options);
            const deltaXViewport = options.width / this.fabricJS._set._viewBox[2],
                deltaYViewport = options.height / this.fabricJS._set._viewBox[3],
                outs = [], groups = [];

            this.fabricJS._set.scale = deltaXViewport;

            const maxL = o.length;
            for (let itm = 0; itm < maxL; itm++) {
                const cur = o[itm];
                if (cur.type === _self.shapes.POLYGON) {
                    const center = cur.getCenterPoint(),
                        centerProcessed = cur.getCenterInPath(),
                        _points = [],
                        points0 = cur.get('points'),
                        _p = points0.concat([]).map((el, d) => {
                            cur.left = cur.top = 0;
                            cur.scaleX = cur.scaleY = 1;
                            return new fabric.Point(center.x + el.x * deltaXViewport, center.y + el.y * deltaYViewport);
                        });
                    if (_points.length) {
                        cur.set('_points', _points);
                    }
                    cur.set('_pointsList', _p.map((el) => {
                        return new fabric.Point(el.x, el.y);
                    }));
                    cur.set('points', _p);
                    cur.set('points0', points0);
                    cur.set('_center', center);
                    cur.set('_centerProcessed', centerProcessed);
                    outs.push(cur);
                } else if (cur.type === _self.shapes.GROUP) {
                }
                let orCloner, cloneCur = cur._clone(true);
                orCloner = cloneCur;
                for (let u = 0; u < _objects.length; u++) {
                    if (cloneCur.id === _objects[u].id) {
                        const _c = _objects.splice(u, 1)[0];
                        if (_c.group) {
                            let _group;
                            for (let g = 0; g < groups.length; g++) {
                                if (_c.group.id === groups[g].id) {
                                    _group = groups[g];
                                    break;
                                }
                            }
                            if (!_group) {
                                _group = new SVGGroup(_self, _c.group.id);
                                groups.push(_group);
                            }
                            _group.add(cloneCur, cur);
                            cloneCur = _group;
                        }
                        break;
                    }
                }
                // debugger;
                const areas = _self.selected.areas;
                for (let i = 0; areas && i < areas.length; i++) {
                    if (areas[i]._id.match(cloneCur.id)) {
                        cloneCur._data = areas[i];
                        break;
                    }
                }
                const sources = this.sources.dataElem;
                for (let i = 0; sources && i < sources.length; i++) {
                    if (sources[i]._id === cloneCur.id
                        || (cloneCur._data && (sources[i]._id === cloneCur._data.dataSourceId))
                        || (cloneCur._data && sources[i]._id === cloneCur._data.dataSourceGeneratedId && !cloneCur._data.dataSourceId)) {
                        cloneCur._dataSource = sources[i];
                        break;
                    }
                }
                orCloner.opacity = _self.defOpacity;
                if (!orCloner.defFill) {
                    orCloner.defFill = orCloner.fill;
                }
                if (!cloneCur._toolTip && cloneCur.type === _self.shapes.POLYGON) {

                    cloneCur._toolTip = new OxiToolTip(cloneCur, _self.parent);
                    cloneCur.onOut = function () {
                        if (!_self.lastSelected) {
                            _self.fadeSelected(this);
                        }
                    };
                }
            }

            for (let g = 0; g < groups.length; g++) {
                const _n = groups[g].make(),
                    args = ['_data', '_dataSource', 'id'];
                for (let f = 0; f < args.length; f++) {
                    _n[args[f]] = groups[g][args[f]];
                }

                _n._toolTip = new OxiToolTip(_n, _self.parent);
                _n.onOut = function () {
                    if (!_self.lastSelected) {
                        _self.fadeSelected(this);
                    }
                };
            }
 
            for(let i=0;i<this.fabricJS._objects.length;i++){
                const child:any = this.fabricJS._objects[i];
                if(this.parent.main.selected.camera.forceIgnores.indexOf(child.id) >-1){ 
                    child._parent.remove(child);
                    i--;
                }
            } 
            this.fabricJS.calcOffset().renderAll();
            this.options = options;
        }
    }

    private resizeElements(objects, scaleX0, scaleY0, scaleMultiplierX, scaleMultiplierY, withoutParent: boolean = false) {
        const list = objects.concat([]);
        for (let i = 0; i < list.length; i++) {
            const cur = list[i];
            cur.scaleX0 = scaleX0;
            cur.scaleY0 = scaleY0;
            cur._hasUpdate = 0;
            cur.left = ((cur.left * scaleMultiplierX));
            cur.top = (cur.top * scaleMultiplierY);
            if (cur.type !== this.shapes.CIRCLE) {
                cur.scaleX *= scaleMultiplierX;
                cur.scaleY *= scaleMultiplierY;
            }
            if (this.isFinish) {
                if (cur.type === this.shapes.POLYGON) {
                    if (cur.get('_pointsList')) {
                        cur.get('_pointsList').forEach((el: any, key) => {
                            el.x *= scaleMultiplierX;
                            el.y *= scaleMultiplierY;
                        });
                    }
                    const newObj = cur._clone(
                        true,
                        false,
                        false,
                        true,
                        withoutParent && cur._parent.type !== this.shapes.GROUP
                    );
                    if (newObj._center) {
                        newObj._center.x *= scaleMultiplierX;
                        newObj._center.y *= scaleMultiplierY;
                    }

                    if (withoutParent) {
                        objects[i] = cur;
                    }
                } else if (cur.type === this.shapes.GROUP) {
                    this.resizeElements(cur._objects, scaleX0, scaleY0, scaleMultiplierX, scaleMultiplierY, withoutParent);
                }
            }


        }
    }

    private componentToHex(c) {
        const hex = c.toString(16);
        return hex.length === 1 ? '0' + hex : hex;
    }

    private parseSVG(svg, callback) {

        const
            _self = this,
            parser = new DOMParser(),
            doc = parser.parseFromString(svg, 'image/svg+xml'),
            allElem = [],
            options = {},
            _svg: any = doc.childNodes[1],
            parseDom = (function parseDom(_el, parent = null) {
                for (let i = 0; i < _el.childNodes.length; i++) {
                    let _elem;
                    const e: any = _el.childNodes[i],
                        setAttr = function setAttr(elem) {
                            if (elem) {
                                if (elem === 2) {
                                    elem = {};
                                } else {
                                    if (parent) {
                                        parent._add(elem);
                                    }
                                    allElem.push(elem);

                                }

                                for (const attr of Object.keys(e.attributes)) {
                                    const cntn = e.attributes[attr].textContent,
                                        _field = e.attributes[attr].localName;
                                    switch (_field) {
                                        case 'id': {
                                            elem[_field] = cntn;
                                            break;
                                        }
                                        case 'style': {
                                            cntn.split(';').forEach((_e) => {
                                                if (!e || !_e.trim().length) {
                                                    return;
                                                }
                                                const __el = _e.split(':'),
                                                    _f = __el[0].trim();
                                                switch (_f) {
                                                    case 'opacity': {
                                                        elem[_f] = parseFloat(__el[1]);
                                                        break;
                                                    }
                                                    case 'fill': {
                                                        elem[_f] = __el[1].trim();
                                                        break;
                                                    }
                                                }
                                            });
                                            break;
                                        }
                                        case 'transform': {
                                            const _f = ['left', 'top', 'scaleX', 'scaleY'];
                                            cntn.split(' ').forEach((_e: any, key) => {
                                                if (!_f[key] || !_e) {
                                                    return;
                                                }
                                                elem[_f[key]] = parseFloat(_e.replace(/[^-0-9.]/g, ''));
                                            });
                                            break;
                                        }
                                    }
                                }


                                if (!elem.id) {
                                    elem.id = ENTITY.Config.randomstr();
                                }
                                elem.getBoundingBox();
                                return elem;


                            }
                        };

                    if (e.nodeName === _self.shapes.POLYGON) {
                        _elem = new fabric.Polygon(e.attributes.points.textContent.split(' ').map((el) => {
                            const _d = el.split(',');
                            if (_d.length < 2) {
                                return null;
                            }
                            return new fabric.Point(parseFloat(_d[0]), parseFloat(_d[1]));
                        }).filter((_e) => {
                            if (_e) {
                                return _e;
                            }
                        }), _self.settings.POLYGON);

                        setAttr(_elem);

                    } else if (e.nodeName === 'g') {
                        _elem = new fabric.Group();
                        if (e.childNodes.length) {

                            setAttr(_elem);
                            parseDom(e, _elem);
                        }
                    }

                }
            })(_svg);


        for (const attr of Object.keys(_svg.attributes)) {
            const _fN = _svg.attributes[attr].localName;
            switch (_fN) {
                case 'height':
                case 'width': {
                    options[_fN] = parseFloat(_svg.attributes[attr].textContent);
                    break;
                }
                case 'viewBox': {
                    options[_fN] = (_svg.attributes[attr].textContent.split(' ').map((el) => {
                        return parseFloat(el);
                    }));
                    break;
                }
            }
        }
        callback(allElem, options);

    }

    private intersectingCheck(activeObject, onSuccess) {
        if (!activeObject || !(activeObject.type === this.shapes.POLYGON || activeObject.type === this.shapes.GROUP)) {
            return;
        }
        activeObject.setCoords();
        if (typeof activeObject.refreshLast !== 'boolean') {
            activeObject.refreshLast = true;
        }

        // loop canvas objects
        activeObject.canvas.forEachObject(function (targ) {
            if (targ === activeObject) {
                // bypass self
                return;
            }

            // check intersections with every object in canvas
            if (activeObject.intersectsWithObject(targ)
                || activeObject.isContainedWithinObject(targ)
                || targ.isContainedWithinObject(activeObject)) {
                // objects are intersecting - deny saving last non-intersection position and break loop

                if (typeof activeObject.lastLeft === 'number') {
                    activeObject.left = activeObject.lastLeft;
                    activeObject.top = activeObject.lastTop;
                    activeObject.refreshLast = false;
                    return;
                }
            } else {
                activeObject.refreshLast = true;
            }
        });

        if (activeObject.refreshLast) {
            // save last non-intersecting position if possible
            // activeObject.lastLeft = activeObject.left;
            // activeObject.lastTop = activeObject.top;
        }
        if (onSuccess) {
            onSuccess();
        }

    }

}

class SVGGroup {
    group: Array<any>;
    editPoints: Array<any>;
    private id: any;
    private canvas: SVGView;
    private color: any;

    constructor(canvas: SVGView, id = ENTITY.Config.randomstr()) {
        this.group = [];
        this.editPoints = [];
        this.canvas = canvas;
        this.id = id;
    }

    getScreenPst() {
        return {x: 0, y: 0};
    }

    add(element) {
        if (element && element.type === this.canvas.shapes.POLYGON) {
            if (!this.color) {
                this.color = element.fill;
            }
            element.fill = this.color;
            [element].concat(element.get('_points')).forEach((elem: any) => {
                if (!elem) {
                    return;
                }
                elem._parent.remove(elem);
                if (elem._dataSource) {
                    elem._dataSource.active = false;
                }
                elem._data = elem._dataSource = elem._toolTip = null;
                if (elem.type === element.type) {
                    this.group.push(elem);

                } else {
                    this.editPoints.push(elem);
                }

            });
            this.canvas.fabricJS.renderAll();
        }
    }

    childs() {
        return this.group;
    }

    clone(from) {
        if (from._parent) {
            from._parent.remove(from);
        } else {
            this.canvas.fabricJS.remove(from);
        }
        from._objects.forEach((el) => {
            this.add(el);
        });
        return this.make();
    }

    make() {
        const group = new fabric.Group(this.childs(), this.canvas.settings.GROUP);
        group.set('id', this.id);
        this.group.forEach((e: any) => {
            e._parent = group;
        });
        this.canvas.fabricJS._add(group);
        this.editPoints.forEach((el) => {
            el._parent._add(el);
        });
        this.canvas.fabricJS.renderAll();

        return group;
    }
}
