import * as ENTITY from '../helper/Utils';
import { Viewer } from './Viewer';
import { WebglView } from './webgl/webgl.view';
import { SVGView } from './svg.draw/svg.draw';

export class OxiSlider {
  currentFrame: any = {};
  currentAlignFrame: any = {};
  private currentPagination: any = {};
  container: HTMLElement;
  alignImgContainer: HTMLElement;
  imgPagination: HTMLElement;
  app: Viewer;
  isDebug: boolean;
  isLoaded: boolean;
  onFinish: any;
  private previewTemp: any = {};
  private curTemp: any;

  constructor(app: Viewer) {
    this.app = app;
    this.isDebug = false;
    this.isLoaded = true;

    this.container = document.createElement('div');
    this.container.className = [ENTITY.ProjClasses.CENTER_CONTAINER, ENTITY.ProjClasses.IMG_SLIDER].join(' ');
    this.app.container.appendChild(this.container);

    this.addFrames();
    this.app.updateAngle();
  }

  update() {
    this.addFrames();
    this.updateView();
    this.app.updateAngle();
  }

  private addFrames() {
    const app: Viewer = this.app,
      _selected = app.main.selected;

    if (this.previewTemp[_selected._id]) {
      this.previewTemp[_selected._id].style.display = '';
      this.previewTemp[_selected._id].classList.remove('hidded');
    }
    if (this.curTemp) {
      this.curTemp.classList.add('hidded');
    }
    if (this.previewTemp[_selected._id]) {
      this.curTemp = this.previewTemp[_selected._id];
      this.previewTemp[_selected._id].classList.remove('hidded');
      return;
    }

    this.loadImages(app.main.selected, true);
    // Load images from children scenes, if is not touch device
    if (app.main.selected.areas && !('ontouchstart' in document.documentElement)) {
      for (let i = 0; i < app.main.selected.areas.length; i++) {
        this.loadImages(app.main.selected.areas[i], false);
      }
    }

  }

  loadImages(element, isSelected) {
    const _resol = element.camera.resolution,
      curTemp = document.createElement('div');
    if (isSelected) {
      this.curTemp = curTemp;
    }
    curTemp.className = 'img-item-slider-container' + (!isSelected ? ' hidded' : '');
    if (!element.images || !element.images.length) {
      return;
    }
    let images = [];
    images = images.concat(element.images);
    for (let i = 0; i < images.length; i++) {
      const img = document.createElement('img'),
        curImg = images[i];
      let countOfErrors = 0;
      console.log(curImg);
      img.onerror = () => {
        if (countOfErrors++ < 2) {
          img.src = ENTITY.Config.PROJ_LOC
            + element.projFilesDirname
            + ENTITY.Config.FILE.DIR.DELIMETER
            + ENTITY.Config.FILE.DIR.PROJECT_PREVIEW
            + curImg;
        } else {
          img.src = ENTITY.Config.FILE.DEFULT.NO_IMAGE;
        }
      };
      if (typeof curImg === 'string') {
        img.src = ENTITY.Config.PROJ_LOC
          + element.projFilesDirname
          + ENTITY.Config.FILE.DIR.DELIMETER
          + ENTITY.Config.FILE.DIR.PROJECT_PREVIEW
          + this.app.imgType;
        if (this.app.imgType === ENTITY.Config.FILE.DIR.PREVIEW.WEBP) {
          const imgD = curImg.split('.');
          imgD.pop();
          img.src += imgD.join('.') + '.webp';
        } else {
          img.src += curImg;
        }
      } else {
        img.src = curImg.data;
      }
      if (i === element.currentItem && isSelected) {
        img.className = ENTITY.ProjClasses.ACTIVE;
        this.currentFrame = img;
        this.isLoaded = false;
        img.onload = () => {
          if (!_resol.x) {
            _resol.x = this._W();
            _resol.y = this._H();
          }
          this.isLoaded = true;
          if (this.onFinish) {
            this.onFinish();
          }
        };
      }
      curTemp.appendChild(img);
    }

    this.previewTemp[element._id] = curTemp;
    this.container.appendChild(curTemp);
  }

  onResize() {
    const val: any = this.app.main.selected.camera.resolution,
      _px: any = 'px',
      elem: any = [this.container.childNodes];

    if (!elem[0] || !elem[0].length) {
      return;
    }
    if (this.alignImgContainer instanceof Node) {
      const el = this.alignImgContainer.childNodes;
      if (el && el.length) {
        elem.push(el);
      }
    }
    elem.forEach(function(lstChilds: any) {
      [].forEach.call(lstChilds, function(el: any) {
        el.style.height = val.y + _px;
        el.style.width = val.x + _px;
      });
    });
  }

  toggleDebug() {
    this.isDebug = !this.isDebug;
  }

  updateView(selectedItem: any = this.app.main.selected.currentItem || 0) {
    this.currentFrame['className'] = this.currentAlignFrame['className'] = this.currentPagination['className'] = '';

    // debugger;

    if (this.app.currentViwer instanceof SVGView) {
      this.app.currentViwer.updateView(selectedItem, () => {
      });
    }
    this.app.main.selected.currentItem = selectedItem;
    if (this.app.currentViwer instanceof WebglView) {
      this.app.currentViwer.app.camera.updateView(selectedItem - this.app.main.selected.currentItem0);
    }

    this.currentFrame = this.curTemp.childNodes[selectedItem];
    if (!this.currentFrame) {
      const _location = window.location.href.split('&');
      _location.pop();
      return window.location.href = _location.join('&');
    }

    this.currentPagination = this.imgPagination && this.imgPagination.childNodes[selectedItem] ?
      this.imgPagination.childNodes[selectedItem] : {};
    this.currentAlignFrame = this.alignImgContainer && this.alignImgContainer.childNodes[selectedItem] ?
      this.alignImgContainer.childNodes[selectedItem] : {};
    this.currentFrame['className']
      = this.currentAlignFrame['className']
      = this.currentPagination['className']
      = ENTITY.ProjClasses.ACTIVE;
    this.app.updateAngle();
    this.app._projControls.show({}, false);
    this.app.updateInfoHTML();
  }

  move(next: number) {
    if (next < 0) {
      if (this.app.main.selected.currentItem < 1) {
        return this.updateView(this.app.main.selected.images.length - 1);
      }
    } else {
      if (this.app.main.selected.currentItem >= this.app.main.selected.images.length - 1) {
        return this.updateView(0);
      }
    }
    this.updateView(this.app.main.selected.currentItem + next);
  }

  _W() {
    return this.currentFrame.clientWidth
      || this.currentFrame.width
      || this.container.clientWidth
      || this.app.main.selected.camera.resolution.x
      || window.innerWidth;
  }

  _H() {
    return this.currentFrame.clientHeight
      || this.currentFrame.height
      || this.container.clientHeight
      || this.app.main.selected.camera.resolution.y
      || window.innerHeight;
  }

  _offsetLeft() {
    return this.container.offsetLeft
      || this.app.container.offsetLeft;
  }

  _offsetTop() {
    return this.container.offsetTop
      || this.app.container.offsetTop;
  }
}
