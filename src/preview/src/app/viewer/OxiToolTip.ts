import * as ENTITY from '../helper/Utils';
import {Viewer} from './Viewer';
import {WTooltip} from './webgl/tooltip/tooltip';

declare const alertify, THREE;
declare var Mustache: any;


export class OxiToolTip {
    tooltip: any;
    visible: boolean;
    private mesh: any;
    private main: Viewer;
    private canEdit: boolean;
    private _id: number;
    private html: any;
    private tooltipCnt: any;
    private tooltipCntl: WTooltip;
    private title: any;
    private desc: any;
    private label: any;

    constructor(mesh: any, main: Viewer) {
        this.main = main;
        this.visible = true;
        this.canEdit = false;
        this._id = Date.now() * Math.random();
        if (mesh._dataSource) {
            this.html = mesh._dataSource.html;
            mesh._dataSource.active = true;
            if (mesh._data) {
                mesh._data.dataSourceId = mesh._dataSource._id || mesh.name;
            }
        }
        this.mesh = mesh;
        if (!mesh.material) {
            mesh.material = {};
        }
        mesh.material.onSelectColor = main.HOVER_COLOR.SOLGHT;

        if (mesh._data || mesh._dataSource) {
            if (mesh._data && (
                    mesh._data._category === ENTITY.Config.PROJ_DESTINATION.ModelStructure ||
                    mesh._data._category === ENTITY.Config.PROJ_DESTINATION.GeneralStructure ||
                    mesh._data._category === ENTITY.Config.PROJ_DESTINATION.LinkGeneralStructure
                ) ||
                (mesh._dataSource && !mesh._dataSource.sold)) {
                mesh.material.onSelectColor = main.HOVER_COLOR.ACTIVE;
            }
            mesh.click = () => {
                let _dest = ENTITY.Config.TEMP_SETTINGS;
                if (_dest) {
                    _dest = _dest.settings;
                }
                if (mesh._dataSource && mesh._dataSource.tooltip && mesh._dataSource.tooltip.URL) {
                    mesh._data._category = 1;
                }
                if (mesh._data) {
                    switch (mesh._data._category) {
                        case ENTITY.Config.PROJ_DESTINATION.ModelStructure: {
                            const _url = mesh._data.projFilesDirname.split('/');
                            if (window.history.replaceState) {
                                window.history.replaceState({}, _url[_url.length - 1], location.href + '&area=' + _url[_url.length - 1]);
                            }
                            main.main.updateLevel();
                            break;
                        }
                        case ENTITY.Config.PROJ_DESTINATION.LinkGeneralStructure: {
                            if (mesh._dataSource && mesh._dataSource.tooltip && mesh._dataSource.tooltip.URL) {
                                window.open(mesh._dataSource.tooltip.URL);
                            } else if (mesh._dataSource && _dest && _dest.data && _dest.data.linkAction) {
                                let _newUrl = _dest.data.linkAction;
                                const _replacer = '@',
                                    _urls = _newUrl.split(_replacer);
                                if (_newUrl.length > 2) {
                                    for (let i = 0; i < _urls.length; i++) {
                                        if (i > 0 && i % 2 !== 0) {
                                            _newUrl = _newUrl.replace(_replacer + _urls[i] + _replacer, mesh._dataSource[_urls[i]]);
                                        }
                                    }
                                }
                                window.open(_newUrl);
                            } else {
                                window.open(mesh._data.destination);
                            }
                            break;
                        }
                        case ENTITY.Config.PROJ_DESTINATION.GeneralStructure: {
                            if (mesh._dataSource && mesh._dataSource.tooltip && mesh._dataSource.tooltip.action) {
                                const _func = ENTITY.Config.safeJS(mesh._dataSource.tooltip.action);
                                if (_func instanceof Function) {
                                    try {
                                        _func();
                                    } catch (e) {
                                        console.error(e);
                                    }
                                }
                            } else {
                                ENTITY.Config.safeJS(mesh._data.destination)();
                            }
                            break;
                        }
                        default: {
                            console.log('any actions from admin');
                        }
                    }
                } else if (mesh._dataSource) {
                    if (mesh._dataSource.sold) {
                        return alertify.warning('Prevent all actioins, beacause this datasource is SOLD');
                    }
                    if (mesh._dataSource.tooltip.URL) {
                        window.open(mesh._dataSource.tooltip.URL);
                    } else if (mesh._dataSource.tooltip.action) {
                        const _func = ENTITY.Config.safeJS(mesh._dataSource.tooltip.action);
                        if (_func instanceof Function) {
                            try {
                                _func();
                            } catch (e) {
                                console.error(e);
                            }
                        }
                    } else if (_dest && _dest.data && _dest.data.linkAction) {
                        let _newUrl = _dest.data.linkAction;
                        const _replacer = '@',
                            _urls = _newUrl.split(_replacer);
                        if (_newUrl.length > 2) {
                            for (let i = 0; i < _urls.length; i++) {
                                if (i > 0 && i % 2 !== 0) {
                                    _newUrl = _newUrl.replace(_replacer + _urls[i] + _replacer, mesh._dataSource[_urls[i]]);
                                }
                            }
                        }
                        window.open(_newUrl);
                    } else {
                        console.log('any actions from datasource');
                    }
                    mesh._dataSource.onclick = mesh.click;
                } else {
                    if (_dest && _dest.data && _dest.data.linkAction) {
                        let _newUrl = _dest.data.linkAction;
                        const _replacer = '@',
                            _urls = _newUrl.split(_replacer);
                        if (_newUrl.length > 2) {
                            for (let i = 0; i < _urls.length; i++) {
                                if (i > 0 && i % 2 !== 0) {
                                    _newUrl = _newUrl.replace(_replacer + _urls[i] + _replacer, mesh[_urls[i]]);
                                }
                            }
                        }
                        window.open(_newUrl);
                    }
                    console.log('has to attach data');
                }

            };

        } else {
            mesh.click = () => {
                let _dest = ENTITY.Config.TEMP_SETTINGS;
                if (_dest) {
                    _dest = _dest.settings;
                }
                if (_dest && _dest.data && _dest.data.linkAction) {
                    let _newUrl = _dest.data.linkAction;
                    const _replacer = '@',
                        _urls = _newUrl.split(_replacer);
                    if (_newUrl.length > 2) {
                        for (let i = 0; i < _urls.length; i++) {
                            if (i > 0 && i % 2 !== 0) {
                                _newUrl = _newUrl.replace(_replacer + _urls[i] + _replacer, mesh[_urls[i]]);
                            }
                        }
                    }
                    window.open(_newUrl);
                }
                console.warn('no any attachments');
            };

        }

        main.infoHTML.push(this);
        this.addTooltip();
    }

    compare(html) {
        return html && ((this.html.innerHTML === html.innerHTML) || this.html.innerHTML.match(html.innerHTML));
    }

    destroy() {
        this.html.parentNode.removeChild(this.html);
    }

    show(show: boolean = true) {
        this.main.container.style.cursor = show ? this.mesh.click ?
            ENTITY.Config.CURSOR.POINTER : ENTITY.Config.CURSOR.UN_AVAILABLE : ENTITY.Config.CURSOR.DEFAULT;
        if (this.mesh.tween) {
            if (this.mesh.tween.show === show) {
                return;
            } else {
                this.mesh.tween.stop();
            }
        }
        const maxOp = this.mesh.material.opacity,
            _maxOpc = this.mesh.material.onSelectColor === this.main.HOVER_COLOR.ACTIVE ?
                this.main.settingsHelper.hover.active.opacity : this.main.settingsHelper.hover.unactive.opacity,
            endO = (show || this.canEdit) ? _maxOpc : 0;
        this.main.refresh();
        if (show) {
            this.mesh.material.color = this.mesh.material.onSelectColor;
        }
        this.mesh.material.opacity = endO;
        if (!show) {
            this.mesh.material.color = this.mesh.material.defColor;
        }
        if (this.mesh._onFinish) {
            this.mesh._onFinish();
            this.mesh._onFinish = false;
        }


        this.display(show, this.mesh);
    }

    display(show: any, mesh: any) {
        this.html.style.zIndex = show ? 10 : 3;
        this.html.className = ENTITY.ProjClasses.PROJ_TOOLTIP.CONTAINER + ' ' + ENTITY.ProjClasses.ACTIVE + (show ? ' act' : '');
        this.tooltipCnt.className = ENTITY.ProjClasses.PROJ_TOOLTIP.TOOL_CONTAINER + (show ? ' ' + ENTITY.ProjClasses.ACTIVE : '');
        this.update();
    }

    update() {
        const mesh = this.mesh,
            _las = this.html.lastClassName;
        this.html.fade(this.visible);
        if (!this.visible) {
            return;
        }
        mesh.getScreenPst();

        const _t = 'px';
        this.html.style.left = mesh.onscreenParams.x + _t;
        this.html.style.top = mesh.onscreenParams.y + _t;
        this.html.style.display = mesh.onscreenParams.y === 0 && mesh.onscreenParams.x === 0 ? 'none' : '';

        this.tooltipCnt.style.marginTop = '';
        this.tooltipCnt.style.marginRight = '';
        this.tooltipCnt.style.marginBottom = '';
        this.tooltipCnt.style.marginLeft = '';
        if (mesh) {
            const generatedCenter = {x: 1, y: 1};
            const points = (((mesh.points0 && mesh.points0.length) || (mesh._objects))) ? (_mesh => {
                return [
                    {x: -_mesh.width * _mesh.zoomX / 2, y: -_mesh.height * _mesh.zoomY / 2},
                    {x: -_mesh.width * _mesh.zoomX / 2, y: _mesh.height * _mesh.zoomY / 2},
                    {x: _mesh.width * _mesh.zoomX / 2, y: _mesh.height * _mesh.zoomY / 2},
                    {x: _mesh.width * _mesh.zoomX / 2, y: -_mesh.height * _mesh.zoomY / 2},
                ];
            })(mesh) : mesh.geometry ? (_mesh => {
                const width = this.main.container.clientWidth,
                    height = this.main.container.clientHeight;
                const widthHalf = width / 2,
                    heightHalf = height / 2;
                const camera = this.main.glViwer.app.camera;
                let positions = [];
                if (!_mesh.geometry.positions) {
                    const vertices = _mesh.geometry.attributes.position.array;
                    for (let i = 0; i < vertices.length; i += 3) {
                        positions.push(new THREE.Vector3(vertices[i], vertices[i + 1], vertices[i + 2]));
                    }
                    _mesh.geometry.positions = positions;
                }
                positions = _mesh.geometry.positions;

                const screenPoints = positions.map(vec => {
                    const pos = vec.clone().project(camera);
                    return new THREE.Vector2((pos.x * widthHalf) + widthHalf, -(pos.y * heightHalf) + heightHalf);
                });
                const box2 = new THREE.Box2().setFromPoints(screenPoints);
                _mesh.onscreenParams = box2.getCenter();
                const size = box2.getSize();
                return [
                    {x: -size.x / 2, y: -size.y / 2},
                    {x: -size.x / 2, y: size.y / 2},
                    {x: size.x / 2, y: size.y / 2},
                    {x: size.x / 2, y: -size.y / 2},
                ];

            })(mesh) : [];
            const center = mesh.onscreenParams,
                pixelRatio = 1;
            this.html.style.left = center.x + _t;
            this.html.style.top = center.y + _t;
            const objPos = this.checkPstPath(0, 0, center, points, this.tooltipCnt, pixelRatio);
            this.html.lastClassName = ' ' + objPos['_class'] + ' ';
            if (objPos['offset'].x > 0) {
                this.tooltipCnt.style.marginLeft = `${objPos['offset'].x / pixelRatio}px`;
            } else if (objPos['offset'].x < 0) {
                this.tooltipCnt.style.marginRight = `${-objPos['offset'].x / pixelRatio}px`;
            }
            if (objPos['offset'].y > 0) {
                this.tooltipCnt.style.marginTop = `${objPos['offset'].y / pixelRatio }px`;
            } else if (objPos['offset'].y < 0) {
                this.tooltipCnt.style.marginBottom = `${-objPos['offset'].y / pixelRatio}px`;
            }
        } else {
            this.html.lastClassName = ' ' + this.checkPst(mesh.onscreenParams.x, mesh.onscreenParams.y) + ' ';
        }

        if (_las && this.html.className.match(_las)) {
            this.html.className = this.html.className.replace(_las, this.html.lastClassName);
        } else {
            this.html.className += this.html.lastClassName;
        }

    }

    fade(show: boolean = false) {
        this.visible = show;
        this.update();
    }

    private addTooltip() {
        let tooltipParent = document.querySelector('.' + ENTITY.ProjClasses.PROJ_TOOLTIPS.CONTAINER);
        this.tooltipCntl = this.main.templates[0].template;
        if (this.html) {
            const d = this.html;
            this.html = document.createElement('div');
            this.html.className = ENTITY.ProjClasses.PROJ_TOOLTIP.CONTAINER + ' ' + ENTITY.ProjClasses.ACTIVE;
            this.html.innerHTML += d;
        }
        if (!this.html) {
            this.html = document.createElement('div');
            // this.initHTMLTemplate();
            this.html.className = ENTITY.ProjClasses.PROJ_TOOLTIP.CONTAINER + ' ' + ENTITY.ProjClasses.ACTIVE;
            try {
                this.html.innerHTML += Mustache.render(this.main.templates[0].template.htmlTemplate, {});
            } catch (e) {
                console.warn('Some bad input:', e);
            }
        }
        if (this.html) {
            let noNeedRefresh;
            const arrList = [
                {n: 'tooltipCnt', className: ENTITY.ProjClasses.PROJ_TOOLTIP.TOOL_CONTAINER},
                {n: 'title', className: ENTITY.ProjClasses.PROJ_TOOLTIP.HEADER},
                {n: 'desc', className: ENTITY.ProjClasses.PROJ_TOOLTIP.BODY},
                {n: 'label', className: ENTITY.ProjClasses.PROJ_TOOLTIP.LABEL}
            ];
            for (let i = 0; i < arrList.length; i++) {
                this[arrList[i].n] = this.html.querySelector('.' + arrList[i].className);
                if (i < arrList.length - 1) {
                    if (!this[arrList[i].n]) {
                        this.initHTMLTemplate();
                        noNeedRefresh = true;
                        break;
                    }
                    if (i === 0) {
                        this[arrList[i].n].className = ENTITY.ProjClasses.PROJ_TOOLTIP.TOOL_CONTAINER;
                    } else {
                        if (this[arrList[i].n].children.length) {
                            this[arrList[i].n] = this[arrList[i].n].children[0];
                        }
                    }
                } else {
                    if (this[arrList[i].n]) {
                        if (this[arrList[i].n].children.length) {
                            this[arrList[i].n] = this[arrList[i].n].children[0];
                        } else {
                            const label = document.createElement('span');
                            this[arrList[i].n].appendChild(label);
                            this[arrList[i].n] = label;
                        }
                    }
                }
            }
            if (!noNeedRefresh) {
                this.fillTooltipData();
            }
        }

        if (!tooltipParent) {
            tooltipParent = document.createElement('div');
            tooltipParent.className = '.' + ENTITY.ProjClasses.PROJ_TOOLTIPS.CONTAINER;
            this.main.container.appendChild(tooltipParent);
        }
        this.main.tooltipParent = tooltipParent;
        this.html.style.left = window.innerWidth / 2 + 'px';
        this.html.style.top = window.innerHeight / 2 + 'px';
        this.html.id = this.mesh.id;
        tooltipParent.appendChild(this.html);
        this.html.addEventListener(ENTITY.Config.EVENTS_NAME.MOUSE_UP, (e) => {
            e.preventDefault();
            e.stopPropagation();
            if (this.mesh.click) {
                this.mesh.click();
            }
        });
        this.html.addEventListener(ENTITY.Config.EVENTS_NAME.MOUSE_OUT, (e: any) => {

            this.mesh._hasHavered = false;
            if (!this.isDescendant(this.html, e.relatedTarget) && this.mesh.onOut) {
                this.mesh.onOut();
                this.main.currentViwer.fadeSelected(this.mesh);
            }
        }, false);
        this.html.addEventListener(ENTITY.Config.EVENTS_NAME.MOUSE_MOVE, (e: any) => {
            this.mesh._hasHavered = true;
            if (this.main.currentViwer && this.main.currentViwer.fadeSelected) {
                this.main.currentViwer.fadeSelected(this.mesh, true);
            }
        }, false);


        setTimeout(() => {
            this.update();
        }, 500);
    }

    private isDescendant(parent, child) {
        if (!child) {
            return false;
        }
        let node = child.parentNode;
        while (node !== null) {
            if (node === parent) {
                return true;
            }
            node = node.parentNode;
        }
        return false;
    }

    private getPointOfIntersection(p1: any, p2: any, p3: any, p4: any) {
        const line1StartX = p1.x,
            line1StartY = p1.y,
            line1EndX = p2.x,
            line1EndY = p2.y,
            line2StartX = p3.x,
            line2StartY = p3.y,
            line2EndX = p4.x,
            line2EndY = p4.y;
        // if the lines intersect, the result contains the x and y of the intersection (treating the lines as infinite) and booleans
        // for whether line segment 1 or line segment 2 contain the point
        let denominator, a, b, numerator1, numerator2;
        const result = {
            x: null,
            y: null,
            onLine1: false,
            onLine2: false
        };
        denominator = ((line2EndY - line2StartY) * (line1EndX - line1StartX)) - ((line2EndX - line2StartX) * (line1EndY - line1StartY));
        if (denominator === 0) {
            return null;
        }
        a = line1StartY - line2StartY;
        b = line1StartX - line2StartX;
        numerator1 = ((line2EndX - line2StartX) * a) - ((line2EndY - line2StartY) * b);
        numerator2 = ((line1EndX - line1StartX) * a) - ((line1EndY - line1StartY) * b);
        a = numerator1 / denominator;
        b = numerator2 / denominator;

        // if we cast these lines infinitely in both directions, they intersect here:
        result.x = line1StartX + (a * (line1EndX - line1StartX));
        result.y = line1StartY + (a * (line1EndY - line1StartY));
        /*
                // it is worth noting that this should be the same as:
                x = line2StartX + (b * (line2EndX - line2StartX));
                y = line2StartX + (b * (line2EndY - line2StartY));
                */
        // if line1 is a segment and line2 is infinite, they intersect if:
        // if line2 is a segment and line1 is infinite, they intersect if:
        if (a > 0 && a < 1 && b > 0 && b < 1) {
            return result;
        }

        // if line1 and line2 are segments, they intersect if both of the above are true
        return null;
    }

    private checkPstPath(x: any, y: any, center: any, path: any, element: any, pixelRatio: any, dataSource: any = null) {
        if (!this.main._slider) {
            return '';
        }
        const
            domeEl = this.main.container,
            minX = 0,
            minY = 0,
            maxX = domeEl.clientWidth,
            maxY = domeEl.clientHeight,
            tooltipWidth = element.clientWidth,
            tooltipHeight = element.clientHeight,
            classes = [' tooltip-align-left', ' tooltip-align-right', ' tooltip-align-bottom'];
        if (dataSource) {
            dataSource.alignLeft = dataSource.alignRight = dataSource.alignBottom = false;
        }
        const endPoints = [{x: x, y: -9999}, {x: 9999, y: y}, {x: x, y: 9999}, {x: -9999, y: y}];
        const checkBest = [(p1, p2) => p1.y < p2.y, (p1, p2) => p1.x > p2.x, (p1, p2) => p1.y > p2.y, (p1, p2) => p1.x < p2.x];
        const offsets = [
            [{x: -tooltipWidth / 2, y: 0}, {x: 0, y: 0}, {x: tooltipWidth / 2, y: 0}],
            [{x: 0, y: -tooltipHeight / 2}, {x: 0, y: 0}, {x: 0, y: tooltipHeight / 2}],
            [{x: -tooltipWidth / 2, y: 0}, {x: 0, y: 0}, {x: tooltipWidth / 2, y: 0}],
            [{x: 0, y: -tooltipHeight / 2}, {x: 0, y: 0}, {x: 0, y: tooltipHeight / 2}]
        ];
        const intersectionPoints = [];
        endPoints.forEach((endPoint, sideItterator) => {
            let bestPoint = {x: x, y: y};
            for (let k = 0; k < offsets[sideItterator].length; k++) {
                for (let i = 0; i < path.length; i++) {
                    const p1 = path[i];
                    const p2 = i + 1 >= path.length ? path[0] : path[i + 1];
                    const intersected = this.getPointOfIntersection({
                        x: offsets[sideItterator][k].x,
                        y: offsets[sideItterator][k].y
                    }, endPoint, p1, p2);
                    if (intersected) {
                        bestPoint = checkBest[sideItterator](bestPoint, intersected) ? bestPoint : intersected;
                    }
                }
            }
            intersectionPoints.push(bestPoint);
        });
        if (center.y - (Math.abs(intersectionPoints[0].y)) / pixelRatio - tooltipHeight - 25 > minY) {
            if (dataSource) {
                dataSource.alignTop = true;
            }
            return {_class: '', offset: {x: 0, y: intersectionPoints[0].y}};
        } else if (center.x + (Math.abs(intersectionPoints[1].x)) / pixelRatio + tooltipWidth + 25 < maxX) {
            if (dataSource) {
                dataSource.alignRight = true;
            }
            return {_class: classes[1], offset: {x: intersectionPoints[1].x, y: 0}};

        } else if (center.y + (Math.abs(intersectionPoints[2].y)) / pixelRatio + tooltipHeight + 25 < maxY) {
            if (dataSource) {
                dataSource.alignBottom = true;
            }
            return {_class: classes[2], offset: {x: 0, y: intersectionPoints[2].y}};
        } else if (center.x - (Math.abs(intersectionPoints[3].x)) / pixelRatio - tooltipWidth - 25 > minX) {
            if (dataSource) {
                dataSource.alignLeft = true;
            }
            return {_class: classes[0], offset: {x: intersectionPoints[3].x, y: 0}};
        }

        return {_class: '', offset: {x: 0, y: 0}};
    }

    private checkPst(x: any, y: any, dataSource: any = null) {
        if (!this.main._slider) {
            return '';
        }
        const
            domeEl = this.main.container,
            minX = 0,
            minY = 0,
            maxX = domeEl.clientWidth,
            maxY = domeEl.clientHeight,
            tootlipWidth = 280,
            tooltipHeight = 160,
            classes = [' tooltip-align-left', ' tooltip-align-right', ' tooltip-align-bottom'];
        if (dataSource) {
            dataSource.alignLeft = dataSource.alignRight = dataSource.alignBottom = false;
        }
        if (y - tooltipHeight > minY) {
            if (dataSource) {
                dataSource.alignTop = true;
            }
            return '';
        } else if (x - tootlipWidth < minX) {
            if (dataSource) {
                dataSource.alignRight = true;
            }
            return classes[1];
        } else if (x + tootlipWidth > maxX) {
            if (dataSource) {
                dataSource.alignLeft = true;
            }
            return classes[0];
        } else if (y + tooltipHeight < maxY) {
            if (dataSource) {
                dataSource.alignBottom = true;
            }
            return classes[2];
        }
        return '';
    }

    private htmlToElement(html: any) {
        const template: any = document.createElement('template');
        template.innerHTML = html;
        return template.content.firstChild;
    }

    private initHTMLTemplate() {
        this.html.innerHTML = '';
        const
            tooltip = this.html,
            tooltCnt: any = this.tooltipCnt = document.createElement('div'),
            head: any = document.createElement('div'),
            spanHead: any = this.title = document.createElement('span'),
            spanBody: any = this.desc = document.createElement('div'),
            sp = document.createElement('div'),
            ps = this.label = document.createElement('span'),
            body: any = document.createElement('div');
        body.appendChild(spanBody);
        head.appendChild(spanHead);
        tooltCnt.appendChild(head);
        tooltCnt.appendChild(document.createElement('hr'));
        tooltCnt.appendChild(body);
        tooltip.appendChild(tooltCnt);
        tooltip.className = ENTITY.ProjClasses.PROJ_TOOLTIP.CONTAINER + ' ' + ENTITY.ProjClasses.ACTIVE;
        tooltCnt.className = ENTITY.ProjClasses.PROJ_TOOLTIP.TOOL_CONTAINER;
        body.className = ENTITY.ProjClasses.PROJ_TOOLTIP.BODY;
        spanBody.className = 'cos-tooltip-body-title';
        head.className = ENTITY.ProjClasses.PROJ_TOOLTIP.HEADER;
        sp.className = ENTITY.ProjClasses.PROJ_TOOLTIP.LABEL;
        sp.appendChild(ps);
        tooltip.appendChild(sp);

        [].forEach.call([].concat.call([tooltip], tooltip.children), (e) => {
            if (e && e.addEventListener) {
                e['on' + ENTITY.Config.EVENTS_NAME.SELECT_START] = ENTITY.Config.onEventPrevent;
                e.addEventListener(ENTITY.Config.EVENTS_NAME.SELECT_START, ENTITY.Config.onEventPrevent);
            }
        });

        this.fillTooltipData();

    }

    private fillTooltipData() {
        const
            mesh = this.mesh,
            selected = this.main.main.selected;
        if (selected.camera && selected.camera.isSVG) {
            this.label.parentNode.fade(false);
        }
        if (mesh._dataSource && mesh._dataSource.tooltip && mesh._dataSource.tooltip.active) {
        } else if (mesh._dataSource && mesh._dataSource.tooltip && !mesh._dataSource.tooltip.active) {
        } else if (mesh._data) {
            this.title.innerText = mesh._data.name;
            // this.desc.innerText = mesh._data._id;
        } else {
            this.title.innerText = mesh.name ;//|| mesh.id;
            // this.desc.innerText = mesh.uuid || '';
        }
        if (this.label && mesh._dataSource && mesh._dataSource.label && mesh._dataSource.label.active && selected.areas) {
            this.label.parentNode.fade(true);
        }
    }
}
